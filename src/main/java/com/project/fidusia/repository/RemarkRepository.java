package com.project.fidusia.repository;

import com.project.fidusia.domain.Remark;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Remark entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RemarkRepository extends JpaRepository<Remark, Long> {}
