package com.project.fidusia.repository;

import com.project.fidusia.domain.Sertifikat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Sertifikat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SertifikatRepository extends JpaRepository<Sertifikat, Long> {}
