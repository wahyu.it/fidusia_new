package com.project.fidusia.repository;

import com.project.fidusia.domain.Cabang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Cabang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CabangRepository extends JpaRepository<Cabang, Long> {}
