package com.project.fidusia.repository;

import com.project.fidusia.domain.Akta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Akta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AktaRepository extends JpaRepository<Akta, Long> {}
