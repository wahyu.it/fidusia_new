package com.project.fidusia.repository;

import com.project.fidusia.domain.Kendaraan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Kendaraan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KendaraanRepository extends JpaRepository<Kendaraan, Long> {}
