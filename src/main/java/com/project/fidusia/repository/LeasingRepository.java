package com.project.fidusia.repository;

import com.project.fidusia.domain.Leasing;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Leasing entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LeasingRepository extends JpaRepository<Leasing, Long> {}
