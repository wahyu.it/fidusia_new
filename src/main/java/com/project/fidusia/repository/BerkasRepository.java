package com.project.fidusia.repository;

import com.project.fidusia.domain.Berkas;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Berkas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BerkasRepository extends JpaRepository<Berkas, Long> {}
