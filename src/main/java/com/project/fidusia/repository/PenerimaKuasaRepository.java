package com.project.fidusia.repository;

import com.project.fidusia.domain.PenerimaKuasa;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PenerimaKuasa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PenerimaKuasaRepository extends JpaRepository<PenerimaKuasa, Long> {}
