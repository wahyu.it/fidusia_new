package com.project.fidusia.repository;

import com.project.fidusia.domain.Ppk;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Ppk entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PpkRepository extends JpaRepository<Ppk, Long> {}
