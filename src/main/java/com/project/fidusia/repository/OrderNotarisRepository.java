package com.project.fidusia.repository;

import com.project.fidusia.domain.OrderNotaris;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the OrderNotaris entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderNotarisRepository extends JpaRepository<OrderNotaris, Long> {}
