package com.project.fidusia.repository;

import com.project.fidusia.domain.RawData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RawData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RawDataRepository extends JpaRepository<RawData, Long> {}
