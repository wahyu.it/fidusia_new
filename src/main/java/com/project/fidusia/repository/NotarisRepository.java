package com.project.fidusia.repository;

import com.project.fidusia.domain.Notaris;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Notaris entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotarisRepository extends JpaRepository<Notaris, Long> {}
