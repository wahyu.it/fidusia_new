package com.project.fidusia.repository;

import com.project.fidusia.domain.SkSubtitusi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SkSubtitusi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SkSubtitusiRepository extends JpaRepository<SkSubtitusi, Long> {}
