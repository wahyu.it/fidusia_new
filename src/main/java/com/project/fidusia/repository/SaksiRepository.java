package com.project.fidusia.repository;

import com.project.fidusia.domain.Saksi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Saksi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SaksiRepository extends JpaRepository<Saksi, Long> {}
