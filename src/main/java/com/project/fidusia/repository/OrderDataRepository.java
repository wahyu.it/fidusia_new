package com.project.fidusia.repository;

import com.project.fidusia.domain.OrderData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the OrderData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderDataRepository extends JpaRepository<OrderData, Long> {}
