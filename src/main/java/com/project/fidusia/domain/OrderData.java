package com.project.fidusia.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A OrderData.
 */
@Entity
@Table(name = "order_data")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class OrderData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "ppk_nomor")
    private String ppkNomor;

    @Column(name = "ppk_tanggal")
    private LocalDate ppkTanggal;

    @Column(name = "nasabah_nama")
    private String nasabahNama;

    @Column(name = "nasabah_jenis")
    private Integer nasabahJenis;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "status")
    private Integer status;

    @Column(name = "type")
    private Integer type;

    @ManyToOne(fetch = FetchType.LAZY)
    private RawData rawData;

    @ManyToOne(fetch = FetchType.LAZY)
    private SkSubtitusi skSubtitusi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "remark", "cabang" }, allowSetters = true)
    private Berkas berkas;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "leasing" }, allowSetters = true)
    private Cabang cabang;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OrderData id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPpkNomor() {
        return this.ppkNomor;
    }

    public OrderData ppkNomor(String ppkNomor) {
        this.setPpkNomor(ppkNomor);
        return this;
    }

    public void setPpkNomor(String ppkNomor) {
        this.ppkNomor = ppkNomor;
    }

    public LocalDate getPpkTanggal() {
        return this.ppkTanggal;
    }

    public OrderData ppkTanggal(LocalDate ppkTanggal) {
        this.setPpkTanggal(ppkTanggal);
        return this;
    }

    public void setPpkTanggal(LocalDate ppkTanggal) {
        this.ppkTanggal = ppkTanggal;
    }

    public String getNasabahNama() {
        return this.nasabahNama;
    }

    public OrderData nasabahNama(String nasabahNama) {
        this.setNasabahNama(nasabahNama);
        return this;
    }

    public void setNasabahNama(String nasabahNama) {
        this.nasabahNama = nasabahNama;
    }

    public Integer getNasabahJenis() {
        return this.nasabahJenis;
    }

    public OrderData nasabahJenis(Integer nasabahJenis) {
        this.setNasabahJenis(nasabahJenis);
        return this;
    }

    public void setNasabahJenis(Integer nasabahJenis) {
        this.nasabahJenis = nasabahJenis;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public OrderData tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getStatus() {
        return this.status;
    }

    public OrderData status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return this.type;
    }

    public OrderData type(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public RawData getRawData() {
        return this.rawData;
    }

    public void setRawData(RawData rawData) {
        this.rawData = rawData;
    }

    public OrderData rawData(RawData rawData) {
        this.setRawData(rawData);
        return this;
    }

    public SkSubtitusi getSkSubtitusi() {
        return this.skSubtitusi;
    }

    public void setSkSubtitusi(SkSubtitusi skSubtitusi) {
        this.skSubtitusi = skSubtitusi;
    }

    public OrderData skSubtitusi(SkSubtitusi skSubtitusi) {
        this.setSkSubtitusi(skSubtitusi);
        return this;
    }

    public Berkas getBerkas() {
        return this.berkas;
    }

    public void setBerkas(Berkas berkas) {
        this.berkas = berkas;
    }

    public OrderData berkas(Berkas berkas) {
        this.setBerkas(berkas);
        return this;
    }

    public Cabang getCabang() {
        return this.cabang;
    }

    public void setCabang(Cabang cabang) {
        this.cabang = cabang;
    }

    public OrderData cabang(Cabang cabang) {
        this.setCabang(cabang);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderData)) {
            return false;
        }
        return getId() != null && getId().equals(((OrderData) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderData{" +
            "id=" + getId() +
            ", ppkNomor='" + getPpkNomor() + "'" +
            ", ppkTanggal='" + getPpkTanggal() + "'" +
            ", nasabahNama='" + getNasabahNama() + "'" +
            ", nasabahJenis=" + getNasabahJenis() +
            ", tanggal='" + getTanggal() + "'" +
            ", status=" + getStatus() +
            ", type=" + getType() +
            "}";
    }
}
