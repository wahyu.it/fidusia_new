package com.project.fidusia.domain;

import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Invoice.
 */
@Entity
@Table(name = "invoice")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "type")
    private Integer type;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "tgl_order")
    private LocalDate tglOrder;

    @Column(name = "jml_order")
    private Long jmlOrder;

    @Column(name = "jml_order_cancel")
    private Long jmlOrderCancel;

    @Column(name = "no_proforma_pnbp")
    private String noProformaPnbp;

    @Column(name = "no_proforma_jasa")
    private String noProformaJasa;

    @Column(name = "no_billpym_pnbp")
    private String noBillpymPnbp;

    @Column(name = "no_billpym_jasa")
    private String noBillpymJasa;

    @Column(name = "amount_pnbp")
    private Long amountPnbp;

    @Column(name = "tax_pnbp")
    private Long taxPnbp;

    @Column(name = "total_amount_pnbp")
    private Long totalAmountPnbp;

    @Column(name = "amount_jasa")
    private Long amountJasa;

    @Column(name = "vat_jasa")
    private Long vatJasa;

    @Column(name = "tax_jasa")
    private Long taxJasa;

    @Column(name = "total_amount_jasa")
    private Long totalAmountJasa;

    @Column(name = "file_proforma_pnbp")
    private String fileProformaPnbp;

    @Column(name = "file_proforma_jasa")
    private String fileProformaJasa;

    @Column(name = "file_billpym_pnbp")
    private String fileBillpymPnbp;

    @Column(name = "file_billpym_jasa")
    private String fileBillpymJasa;

    @Column(name = "file_bulk")
    private String fileBulk;

    @Column(name = "file_tax")
    private String fileTax;

    @Column(name = "file_txt")
    private String fileTxt;

    @Column(name = "status")
    private Integer status;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Invoice id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return this.type;
    }

    public Invoice type(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public Invoice tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public LocalDate getTglOrder() {
        return this.tglOrder;
    }

    public Invoice tglOrder(LocalDate tglOrder) {
        this.setTglOrder(tglOrder);
        return this;
    }

    public void setTglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
    }

    public Long getJmlOrder() {
        return this.jmlOrder;
    }

    public Invoice jmlOrder(Long jmlOrder) {
        this.setJmlOrder(jmlOrder);
        return this;
    }

    public void setJmlOrder(Long jmlOrder) {
        this.jmlOrder = jmlOrder;
    }

    public Long getJmlOrderCancel() {
        return this.jmlOrderCancel;
    }

    public Invoice jmlOrderCancel(Long jmlOrderCancel) {
        this.setJmlOrderCancel(jmlOrderCancel);
        return this;
    }

    public void setJmlOrderCancel(Long jmlOrderCancel) {
        this.jmlOrderCancel = jmlOrderCancel;
    }

    public String getNoProformaPnbp() {
        return this.noProformaPnbp;
    }

    public Invoice noProformaPnbp(String noProformaPnbp) {
        this.setNoProformaPnbp(noProformaPnbp);
        return this;
    }

    public void setNoProformaPnbp(String noProformaPnbp) {
        this.noProformaPnbp = noProformaPnbp;
    }

    public String getNoProformaJasa() {
        return this.noProformaJasa;
    }

    public Invoice noProformaJasa(String noProformaJasa) {
        this.setNoProformaJasa(noProformaJasa);
        return this;
    }

    public void setNoProformaJasa(String noProformaJasa) {
        this.noProformaJasa = noProformaJasa;
    }

    public String getNoBillpymPnbp() {
        return this.noBillpymPnbp;
    }

    public Invoice noBillpymPnbp(String noBillpymPnbp) {
        this.setNoBillpymPnbp(noBillpymPnbp);
        return this;
    }

    public void setNoBillpymPnbp(String noBillpymPnbp) {
        this.noBillpymPnbp = noBillpymPnbp;
    }

    public String getNoBillpymJasa() {
        return this.noBillpymJasa;
    }

    public Invoice noBillpymJasa(String noBillpymJasa) {
        this.setNoBillpymJasa(noBillpymJasa);
        return this;
    }

    public void setNoBillpymJasa(String noBillpymJasa) {
        this.noBillpymJasa = noBillpymJasa;
    }

    public Long getAmountPnbp() {
        return this.amountPnbp;
    }

    public Invoice amountPnbp(Long amountPnbp) {
        this.setAmountPnbp(amountPnbp);
        return this;
    }

    public void setAmountPnbp(Long amountPnbp) {
        this.amountPnbp = amountPnbp;
    }

    public Long getTaxPnbp() {
        return this.taxPnbp;
    }

    public Invoice taxPnbp(Long taxPnbp) {
        this.setTaxPnbp(taxPnbp);
        return this;
    }

    public void setTaxPnbp(Long taxPnbp) {
        this.taxPnbp = taxPnbp;
    }

    public Long getTotalAmountPnbp() {
        return this.totalAmountPnbp;
    }

    public Invoice totalAmountPnbp(Long totalAmountPnbp) {
        this.setTotalAmountPnbp(totalAmountPnbp);
        return this;
    }

    public void setTotalAmountPnbp(Long totalAmountPnbp) {
        this.totalAmountPnbp = totalAmountPnbp;
    }

    public Long getAmountJasa() {
        return this.amountJasa;
    }

    public Invoice amountJasa(Long amountJasa) {
        this.setAmountJasa(amountJasa);
        return this;
    }

    public void setAmountJasa(Long amountJasa) {
        this.amountJasa = amountJasa;
    }

    public Long getVatJasa() {
        return this.vatJasa;
    }

    public Invoice vatJasa(Long vatJasa) {
        this.setVatJasa(vatJasa);
        return this;
    }

    public void setVatJasa(Long vatJasa) {
        this.vatJasa = vatJasa;
    }

    public Long getTaxJasa() {
        return this.taxJasa;
    }

    public Invoice taxJasa(Long taxJasa) {
        this.setTaxJasa(taxJasa);
        return this;
    }

    public void setTaxJasa(Long taxJasa) {
        this.taxJasa = taxJasa;
    }

    public Long getTotalAmountJasa() {
        return this.totalAmountJasa;
    }

    public Invoice totalAmountJasa(Long totalAmountJasa) {
        this.setTotalAmountJasa(totalAmountJasa);
        return this;
    }

    public void setTotalAmountJasa(Long totalAmountJasa) {
        this.totalAmountJasa = totalAmountJasa;
    }

    public String getFileProformaPnbp() {
        return this.fileProformaPnbp;
    }

    public Invoice fileProformaPnbp(String fileProformaPnbp) {
        this.setFileProformaPnbp(fileProformaPnbp);
        return this;
    }

    public void setFileProformaPnbp(String fileProformaPnbp) {
        this.fileProformaPnbp = fileProformaPnbp;
    }

    public String getFileProformaJasa() {
        return this.fileProformaJasa;
    }

    public Invoice fileProformaJasa(String fileProformaJasa) {
        this.setFileProformaJasa(fileProformaJasa);
        return this;
    }

    public void setFileProformaJasa(String fileProformaJasa) {
        this.fileProformaJasa = fileProformaJasa;
    }

    public String getFileBillpymPnbp() {
        return this.fileBillpymPnbp;
    }

    public Invoice fileBillpymPnbp(String fileBillpymPnbp) {
        this.setFileBillpymPnbp(fileBillpymPnbp);
        return this;
    }

    public void setFileBillpymPnbp(String fileBillpymPnbp) {
        this.fileBillpymPnbp = fileBillpymPnbp;
    }

    public String getFileBillpymJasa() {
        return this.fileBillpymJasa;
    }

    public Invoice fileBillpymJasa(String fileBillpymJasa) {
        this.setFileBillpymJasa(fileBillpymJasa);
        return this;
    }

    public void setFileBillpymJasa(String fileBillpymJasa) {
        this.fileBillpymJasa = fileBillpymJasa;
    }

    public String getFileBulk() {
        return this.fileBulk;
    }

    public Invoice fileBulk(String fileBulk) {
        this.setFileBulk(fileBulk);
        return this;
    }

    public void setFileBulk(String fileBulk) {
        this.fileBulk = fileBulk;
    }

    public String getFileTax() {
        return this.fileTax;
    }

    public Invoice fileTax(String fileTax) {
        this.setFileTax(fileTax);
        return this;
    }

    public void setFileTax(String fileTax) {
        this.fileTax = fileTax;
    }

    public String getFileTxt() {
        return this.fileTxt;
    }

    public Invoice fileTxt(String fileTxt) {
        this.setFileTxt(fileTxt);
        return this;
    }

    public void setFileTxt(String fileTxt) {
        this.fileTxt = fileTxt;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Invoice status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Invoice updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public Invoice updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Invoice)) {
            return false;
        }
        return getId() != null && getId().equals(((Invoice) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Invoice{" +
            "id=" + getId() +
            ", type=" + getType() +
            ", tanggal='" + getTanggal() + "'" +
            ", tglOrder='" + getTglOrder() + "'" +
            ", jmlOrder=" + getJmlOrder() +
            ", jmlOrderCancel=" + getJmlOrderCancel() +
            ", noProformaPnbp='" + getNoProformaPnbp() + "'" +
            ", noProformaJasa='" + getNoProformaJasa() + "'" +
            ", noBillpymPnbp='" + getNoBillpymPnbp() + "'" +
            ", noBillpymJasa='" + getNoBillpymJasa() + "'" +
            ", amountPnbp=" + getAmountPnbp() +
            ", taxPnbp=" + getTaxPnbp() +
            ", totalAmountPnbp=" + getTotalAmountPnbp() +
            ", amountJasa=" + getAmountJasa() +
            ", vatJasa=" + getVatJasa() +
            ", taxJasa=" + getTaxJasa() +
            ", totalAmountJasa=" + getTotalAmountJasa() +
            ", fileProformaPnbp='" + getFileProformaPnbp() + "'" +
            ", fileProformaJasa='" + getFileProformaJasa() + "'" +
            ", fileBillpymPnbp='" + getFileBillpymPnbp() + "'" +
            ", fileBillpymJasa='" + getFileBillpymJasa() + "'" +
            ", fileBulk='" + getFileBulk() + "'" +
            ", fileTax='" + getFileTax() + "'" +
            ", fileTxt='" + getFileTxt() + "'" +
            ", status=" + getStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            "}";
    }
}
