package com.project.fidusia.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Berkas.
 */
@Entity
@Table(name = "berkas")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Berkas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "identitas")
    private String identitas;

    @Column(name = "kk")
    private String kk;

    @Column(name = "ppk")
    private String ppk;

    @Column(name = "sk_nasabah")
    private String skNasabah;

    @Column(name = "akta_pendirian")
    private String aktaPendirian;

    @Column(name = "akta_perubahan")
    private String aktaPerubahan;

    @Column(name = "sk_pendirian")
    private String skPendirian;

    @Column(name = "sk_perubahan")
    private String skPerubahan;

    @Column(name = "npwp")
    private String npwp;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "upload_by")
    private String uploadBy;

    @Column(name = "upload_on")
    private ZonedDateTime uploadOn;

    @Column(name = "ppk_nomor")
    private String ppkNomor;

    @Column(name = "nsb_jenis")
    private Integer nsbJenis;

    @Column(name = "berkas_status")
    private Integer berkasStatus;

    @Column(name = "nama_nasabah")
    private String namaNasabah;

    @Column(name = "tanggal_ppk")
    private LocalDate tanggalPpk;

    @Column(name = "metode_kirim")
    private Integer metodeKirim;

    @Column(name = "partition_id")
    private Integer partitionId;

    @Column(name = "verify_by")
    private String verifyBy;

    @Column(name = "verify_on")
    private ZonedDateTime verifyOn;

    @Column(name = "order_type")
    private Integer orderType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Remark remark;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "leasing" }, allowSetters = true)
    private Cabang cabang;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Berkas id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentitas() {
        return this.identitas;
    }

    public Berkas identitas(String identitas) {
        this.setIdentitas(identitas);
        return this;
    }

    public void setIdentitas(String identitas) {
        this.identitas = identitas;
    }

    public String getKk() {
        return this.kk;
    }

    public Berkas kk(String kk) {
        this.setKk(kk);
        return this;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public String getPpk() {
        return this.ppk;
    }

    public Berkas ppk(String ppk) {
        this.setPpk(ppk);
        return this;
    }

    public void setPpk(String ppk) {
        this.ppk = ppk;
    }

    public String getSkNasabah() {
        return this.skNasabah;
    }

    public Berkas skNasabah(String skNasabah) {
        this.setSkNasabah(skNasabah);
        return this;
    }

    public void setSkNasabah(String skNasabah) {
        this.skNasabah = skNasabah;
    }

    public String getAktaPendirian() {
        return this.aktaPendirian;
    }

    public Berkas aktaPendirian(String aktaPendirian) {
        this.setAktaPendirian(aktaPendirian);
        return this;
    }

    public void setAktaPendirian(String aktaPendirian) {
        this.aktaPendirian = aktaPendirian;
    }

    public String getAktaPerubahan() {
        return this.aktaPerubahan;
    }

    public Berkas aktaPerubahan(String aktaPerubahan) {
        this.setAktaPerubahan(aktaPerubahan);
        return this;
    }

    public void setAktaPerubahan(String aktaPerubahan) {
        this.aktaPerubahan = aktaPerubahan;
    }

    public String getSkPendirian() {
        return this.skPendirian;
    }

    public Berkas skPendirian(String skPendirian) {
        this.setSkPendirian(skPendirian);
        return this;
    }

    public void setSkPendirian(String skPendirian) {
        this.skPendirian = skPendirian;
    }

    public String getSkPerubahan() {
        return this.skPerubahan;
    }

    public Berkas skPerubahan(String skPerubahan) {
        this.setSkPerubahan(skPerubahan);
        return this;
    }

    public void setSkPerubahan(String skPerubahan) {
        this.skPerubahan = skPerubahan;
    }

    public String getNpwp() {
        return this.npwp;
    }

    public Berkas npwp(String npwp) {
        this.setNpwp(npwp);
        return this;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Berkas updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public Berkas updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public String getUploadBy() {
        return this.uploadBy;
    }

    public Berkas uploadBy(String uploadBy) {
        this.setUploadBy(uploadBy);
        return this;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public ZonedDateTime getUploadOn() {
        return this.uploadOn;
    }

    public Berkas uploadOn(ZonedDateTime uploadOn) {
        this.setUploadOn(uploadOn);
        return this;
    }

    public void setUploadOn(ZonedDateTime uploadOn) {
        this.uploadOn = uploadOn;
    }

    public String getPpkNomor() {
        return this.ppkNomor;
    }

    public Berkas ppkNomor(String ppkNomor) {
        this.setPpkNomor(ppkNomor);
        return this;
    }

    public void setPpkNomor(String ppkNomor) {
        this.ppkNomor = ppkNomor;
    }

    public Integer getNsbJenis() {
        return this.nsbJenis;
    }

    public Berkas nsbJenis(Integer nsbJenis) {
        this.setNsbJenis(nsbJenis);
        return this;
    }

    public void setNsbJenis(Integer nsbJenis) {
        this.nsbJenis = nsbJenis;
    }

    public Integer getBerkasStatus() {
        return this.berkasStatus;
    }

    public Berkas berkasStatus(Integer berkasStatus) {
        this.setBerkasStatus(berkasStatus);
        return this;
    }

    public void setBerkasStatus(Integer berkasStatus) {
        this.berkasStatus = berkasStatus;
    }

    public String getNamaNasabah() {
        return this.namaNasabah;
    }

    public Berkas namaNasabah(String namaNasabah) {
        this.setNamaNasabah(namaNasabah);
        return this;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public LocalDate getTanggalPpk() {
        return this.tanggalPpk;
    }

    public Berkas tanggalPpk(LocalDate tanggalPpk) {
        this.setTanggalPpk(tanggalPpk);
        return this;
    }

    public void setTanggalPpk(LocalDate tanggalPpk) {
        this.tanggalPpk = tanggalPpk;
    }

    public Integer getMetodeKirim() {
        return this.metodeKirim;
    }

    public Berkas metodeKirim(Integer metodeKirim) {
        this.setMetodeKirim(metodeKirim);
        return this;
    }

    public void setMetodeKirim(Integer metodeKirim) {
        this.metodeKirim = metodeKirim;
    }

    public Integer getPartitionId() {
        return this.partitionId;
    }

    public Berkas partitionId(Integer partitionId) {
        this.setPartitionId(partitionId);
        return this;
    }

    public void setPartitionId(Integer partitionId) {
        this.partitionId = partitionId;
    }

    public String getVerifyBy() {
        return this.verifyBy;
    }

    public Berkas verifyBy(String verifyBy) {
        this.setVerifyBy(verifyBy);
        return this;
    }

    public void setVerifyBy(String verifyBy) {
        this.verifyBy = verifyBy;
    }

    public ZonedDateTime getVerifyOn() {
        return this.verifyOn;
    }

    public Berkas verifyOn(ZonedDateTime verifyOn) {
        this.setVerifyOn(verifyOn);
        return this;
    }

    public void setVerifyOn(ZonedDateTime verifyOn) {
        this.verifyOn = verifyOn;
    }

    public Integer getOrderType() {
        return this.orderType;
    }

    public Berkas orderType(Integer orderType) {
        this.setOrderType(orderType);
        return this;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Remark getRemark() {
        return this.remark;
    }

    public void setRemark(Remark remark) {
        this.remark = remark;
    }

    public Berkas remark(Remark remark) {
        this.setRemark(remark);
        return this;
    }

    public Cabang getCabang() {
        return this.cabang;
    }

    public void setCabang(Cabang cabang) {
        this.cabang = cabang;
    }

    public Berkas cabang(Cabang cabang) {
        this.setCabang(cabang);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Berkas)) {
            return false;
        }
        return getId() != null && getId().equals(((Berkas) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Berkas{" +
            "id=" + getId() +
            ", identitas='" + getIdentitas() + "'" +
            ", kk='" + getKk() + "'" +
            ", ppk='" + getPpk() + "'" +
            ", skNasabah='" + getSkNasabah() + "'" +
            ", aktaPendirian='" + getAktaPendirian() + "'" +
            ", aktaPerubahan='" + getAktaPerubahan() + "'" +
            ", skPendirian='" + getSkPendirian() + "'" +
            ", skPerubahan='" + getSkPerubahan() + "'" +
            ", npwp='" + getNpwp() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            ", uploadBy='" + getUploadBy() + "'" +
            ", uploadOn='" + getUploadOn() + "'" +
            ", ppkNomor='" + getPpkNomor() + "'" +
            ", nsbJenis=" + getNsbJenis() +
            ", berkasStatus=" + getBerkasStatus() +
            ", namaNasabah='" + getNamaNasabah() + "'" +
            ", tanggalPpk='" + getTanggalPpk() + "'" +
            ", metodeKirim=" + getMetodeKirim() +
            ", partitionId=" + getPartitionId() +
            ", verifyBy='" + getVerifyBy() + "'" +
            ", verifyOn='" + getVerifyOn() + "'" +
            ", orderType=" + getOrderType() +
            "}";
    }
}
