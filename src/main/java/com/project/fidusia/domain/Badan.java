package com.project.fidusia.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Badan.
 */
@Entity
@Table(name = "badan")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Badan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "golongan_usaha")
    private String golonganUsaha;

    @Column(name = "jenis")
    private String jenis;

    @Column(name = "nama")
    private String nama;

    @Column(name = "kedudukan")
    private String kedudukan;

    @Column(name = "no_akta")
    private String noAkta;

    @Column(name = "tgl_akta")
    private LocalDate tglAkta;

    @Column(name = "nama_notaris")
    private String namaNotaris;

    @Column(name = "wil_kerja_notaris")
    private String wilKerjaNotaris;

    @Column(name = "sk_notaris")
    private String skNotaris;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "rt")
    private String rt;

    @Column(name = "rw")
    private String rw;

    @Column(name = "kelurahan")
    private String kelurahan;

    @Column(name = "kecamatan")
    private String kecamatan;

    @Column(name = "kota")
    private String kota;

    @Column(name = "provinsi")
    private String provinsi;

    @Column(name = "kode_pos")
    private String kodePos;

    @Column(name = "no_kontak")
    private String noKontak;

    @Column(name = "nama_sk")
    private String namaSk;

    @Column(name = "nomor_sk")
    private String nomorSk;

    @Column(name = "tgl_sk")
    private String tglSk;

    @Column(name = "npwp")
    private String npwp;

    @Column(name = "pj_nama")
    private String pjNama;

    @Column(name = "pj_jenis_kelamin")
    private String pjJenisKelamin;

    @Column(name = "pj_status_kawin")
    private String pjStatusKawin;

    @Column(name = "pj_kelahiran")
    private String pjKelahiran;

    @Column(name = "pj_tgl_lahir")
    private LocalDate pjTglLahir;

    @Column(name = "pj_pekerjaan")
    private String pjPekerjaan;

    @Column(name = "pj_warga_negara")
    private String pjWargaNegara;

    @Column(name = "pj_jenis_id")
    private String pjJenisId;

    @Column(name = "pj_no_id")
    private String pjNoId;

    @Column(name = "pj_alamat")
    private String pjAlamat;

    @Column(name = "pj_rt")
    private String pjRt;

    @Column(name = "pj_rw")
    private String pjRw;

    @Column(name = "pj_kelurahan")
    private String pjKelurahan;

    @Column(name = "pj_kecamatan")
    private String pjKecamatan;

    @Column(name = "pj_kota")
    private String pjKota;

    @Column(name = "pj_provinsi")
    private String pjProvinsi;

    @Column(name = "pj_kode_pos")
    private String pjKodePos;

    @Column(name = "template")
    private String template;

    @Column(name = "komparisi")
    private String komparisi;

    @JsonIgnoreProperties(value = { "orang", "badan", "akta" }, allowSetters = true)
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "badan")
    private Ppk ppk;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Badan id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGolonganUsaha() {
        return this.golonganUsaha;
    }

    public Badan golonganUsaha(String golonganUsaha) {
        this.setGolonganUsaha(golonganUsaha);
        return this;
    }

    public void setGolonganUsaha(String golonganUsaha) {
        this.golonganUsaha = golonganUsaha;
    }

    public String getJenis() {
        return this.jenis;
    }

    public Badan jenis(String jenis) {
        this.setJenis(jenis);
        return this;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getNama() {
        return this.nama;
    }

    public Badan nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKedudukan() {
        return this.kedudukan;
    }

    public Badan kedudukan(String kedudukan) {
        this.setKedudukan(kedudukan);
        return this;
    }

    public void setKedudukan(String kedudukan) {
        this.kedudukan = kedudukan;
    }

    public String getNoAkta() {
        return this.noAkta;
    }

    public Badan noAkta(String noAkta) {
        this.setNoAkta(noAkta);
        return this;
    }

    public void setNoAkta(String noAkta) {
        this.noAkta = noAkta;
    }

    public LocalDate getTglAkta() {
        return this.tglAkta;
    }

    public Badan tglAkta(LocalDate tglAkta) {
        this.setTglAkta(tglAkta);
        return this;
    }

    public void setTglAkta(LocalDate tglAkta) {
        this.tglAkta = tglAkta;
    }

    public String getNamaNotaris() {
        return this.namaNotaris;
    }

    public Badan namaNotaris(String namaNotaris) {
        this.setNamaNotaris(namaNotaris);
        return this;
    }

    public void setNamaNotaris(String namaNotaris) {
        this.namaNotaris = namaNotaris;
    }

    public String getWilKerjaNotaris() {
        return this.wilKerjaNotaris;
    }

    public Badan wilKerjaNotaris(String wilKerjaNotaris) {
        this.setWilKerjaNotaris(wilKerjaNotaris);
        return this;
    }

    public void setWilKerjaNotaris(String wilKerjaNotaris) {
        this.wilKerjaNotaris = wilKerjaNotaris;
    }

    public String getSkNotaris() {
        return this.skNotaris;
    }

    public Badan skNotaris(String skNotaris) {
        this.setSkNotaris(skNotaris);
        return this;
    }

    public void setSkNotaris(String skNotaris) {
        this.skNotaris = skNotaris;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public Badan alamat(String alamat) {
        this.setAlamat(alamat);
        return this;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getRt() {
        return this.rt;
    }

    public Badan rt(String rt) {
        this.setRt(rt);
        return this;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return this.rw;
    }

    public Badan rw(String rw) {
        this.setRw(rw);
        return this;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getKelurahan() {
        return this.kelurahan;
    }

    public Badan kelurahan(String kelurahan) {
        this.setKelurahan(kelurahan);
        return this;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return this.kecamatan;
    }

    public Badan kecamatan(String kecamatan) {
        this.setKecamatan(kecamatan);
        return this;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKota() {
        return this.kota;
    }

    public Badan kota(String kota) {
        this.setKota(kota);
        return this;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getProvinsi() {
        return this.provinsi;
    }

    public Badan provinsi(String provinsi) {
        this.setProvinsi(provinsi);
        return this;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKodePos() {
        return this.kodePos;
    }

    public Badan kodePos(String kodePos) {
        this.setKodePos(kodePos);
        return this;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public String getNoKontak() {
        return this.noKontak;
    }

    public Badan noKontak(String noKontak) {
        this.setNoKontak(noKontak);
        return this;
    }

    public void setNoKontak(String noKontak) {
        this.noKontak = noKontak;
    }

    public String getNamaSk() {
        return this.namaSk;
    }

    public Badan namaSk(String namaSk) {
        this.setNamaSk(namaSk);
        return this;
    }

    public void setNamaSk(String namaSk) {
        this.namaSk = namaSk;
    }

    public String getNomorSk() {
        return this.nomorSk;
    }

    public Badan nomorSk(String nomorSk) {
        this.setNomorSk(nomorSk);
        return this;
    }

    public void setNomorSk(String nomorSk) {
        this.nomorSk = nomorSk;
    }

    public String getTglSk() {
        return this.tglSk;
    }

    public Badan tglSk(String tglSk) {
        this.setTglSk(tglSk);
        return this;
    }

    public void setTglSk(String tglSk) {
        this.tglSk = tglSk;
    }

    public String getNpwp() {
        return this.npwp;
    }

    public Badan npwp(String npwp) {
        this.setNpwp(npwp);
        return this;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getPjNama() {
        return this.pjNama;
    }

    public Badan pjNama(String pjNama) {
        this.setPjNama(pjNama);
        return this;
    }

    public void setPjNama(String pjNama) {
        this.pjNama = pjNama;
    }

    public String getPjJenisKelamin() {
        return this.pjJenisKelamin;
    }

    public Badan pjJenisKelamin(String pjJenisKelamin) {
        this.setPjJenisKelamin(pjJenisKelamin);
        return this;
    }

    public void setPjJenisKelamin(String pjJenisKelamin) {
        this.pjJenisKelamin = pjJenisKelamin;
    }

    public String getPjStatusKawin() {
        return this.pjStatusKawin;
    }

    public Badan pjStatusKawin(String pjStatusKawin) {
        this.setPjStatusKawin(pjStatusKawin);
        return this;
    }

    public void setPjStatusKawin(String pjStatusKawin) {
        this.pjStatusKawin = pjStatusKawin;
    }

    public String getPjKelahiran() {
        return this.pjKelahiran;
    }

    public Badan pjKelahiran(String pjKelahiran) {
        this.setPjKelahiran(pjKelahiran);
        return this;
    }

    public void setPjKelahiran(String pjKelahiran) {
        this.pjKelahiran = pjKelahiran;
    }

    public LocalDate getPjTglLahir() {
        return this.pjTglLahir;
    }

    public Badan pjTglLahir(LocalDate pjTglLahir) {
        this.setPjTglLahir(pjTglLahir);
        return this;
    }

    public void setPjTglLahir(LocalDate pjTglLahir) {
        this.pjTglLahir = pjTglLahir;
    }

    public String getPjPekerjaan() {
        return this.pjPekerjaan;
    }

    public Badan pjPekerjaan(String pjPekerjaan) {
        this.setPjPekerjaan(pjPekerjaan);
        return this;
    }

    public void setPjPekerjaan(String pjPekerjaan) {
        this.pjPekerjaan = pjPekerjaan;
    }

    public String getPjWargaNegara() {
        return this.pjWargaNegara;
    }

    public Badan pjWargaNegara(String pjWargaNegara) {
        this.setPjWargaNegara(pjWargaNegara);
        return this;
    }

    public void setPjWargaNegara(String pjWargaNegara) {
        this.pjWargaNegara = pjWargaNegara;
    }

    public String getPjJenisId() {
        return this.pjJenisId;
    }

    public Badan pjJenisId(String pjJenisId) {
        this.setPjJenisId(pjJenisId);
        return this;
    }

    public void setPjJenisId(String pjJenisId) {
        this.pjJenisId = pjJenisId;
    }

    public String getPjNoId() {
        return this.pjNoId;
    }

    public Badan pjNoId(String pjNoId) {
        this.setPjNoId(pjNoId);
        return this;
    }

    public void setPjNoId(String pjNoId) {
        this.pjNoId = pjNoId;
    }

    public String getPjAlamat() {
        return this.pjAlamat;
    }

    public Badan pjAlamat(String pjAlamat) {
        this.setPjAlamat(pjAlamat);
        return this;
    }

    public void setPjAlamat(String pjAlamat) {
        this.pjAlamat = pjAlamat;
    }

    public String getPjRt() {
        return this.pjRt;
    }

    public Badan pjRt(String pjRt) {
        this.setPjRt(pjRt);
        return this;
    }

    public void setPjRt(String pjRt) {
        this.pjRt = pjRt;
    }

    public String getPjRw() {
        return this.pjRw;
    }

    public Badan pjRw(String pjRw) {
        this.setPjRw(pjRw);
        return this;
    }

    public void setPjRw(String pjRw) {
        this.pjRw = pjRw;
    }

    public String getPjKelurahan() {
        return this.pjKelurahan;
    }

    public Badan pjKelurahan(String pjKelurahan) {
        this.setPjKelurahan(pjKelurahan);
        return this;
    }

    public void setPjKelurahan(String pjKelurahan) {
        this.pjKelurahan = pjKelurahan;
    }

    public String getPjKecamatan() {
        return this.pjKecamatan;
    }

    public Badan pjKecamatan(String pjKecamatan) {
        this.setPjKecamatan(pjKecamatan);
        return this;
    }

    public void setPjKecamatan(String pjKecamatan) {
        this.pjKecamatan = pjKecamatan;
    }

    public String getPjKota() {
        return this.pjKota;
    }

    public Badan pjKota(String pjKota) {
        this.setPjKota(pjKota);
        return this;
    }

    public void setPjKota(String pjKota) {
        this.pjKota = pjKota;
    }

    public String getPjProvinsi() {
        return this.pjProvinsi;
    }

    public Badan pjProvinsi(String pjProvinsi) {
        this.setPjProvinsi(pjProvinsi);
        return this;
    }

    public void setPjProvinsi(String pjProvinsi) {
        this.pjProvinsi = pjProvinsi;
    }

    public String getPjKodePos() {
        return this.pjKodePos;
    }

    public Badan pjKodePos(String pjKodePos) {
        this.setPjKodePos(pjKodePos);
        return this;
    }

    public void setPjKodePos(String pjKodePos) {
        this.pjKodePos = pjKodePos;
    }

    public String getTemplate() {
        return this.template;
    }

    public Badan template(String template) {
        this.setTemplate(template);
        return this;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getKomparisi() {
        return this.komparisi;
    }

    public Badan komparisi(String komparisi) {
        this.setKomparisi(komparisi);
        return this;
    }

    public void setKomparisi(String komparisi) {
        this.komparisi = komparisi;
    }

    public Ppk getPpk() {
        return this.ppk;
    }

    public void setPpk(Ppk ppk) {
        if (this.ppk != null) {
            this.ppk.setBadan(null);
        }
        if (ppk != null) {
            ppk.setBadan(this);
        }
        this.ppk = ppk;
    }

    public Badan ppk(Ppk ppk) {
        this.setPpk(ppk);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Badan)) {
            return false;
        }
        return getId() != null && getId().equals(((Badan) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Badan{" +
            "id=" + getId() +
            ", golonganUsaha='" + getGolonganUsaha() + "'" +
            ", jenis='" + getJenis() + "'" +
            ", nama='" + getNama() + "'" +
            ", kedudukan='" + getKedudukan() + "'" +
            ", noAkta='" + getNoAkta() + "'" +
            ", tglAkta='" + getTglAkta() + "'" +
            ", namaNotaris='" + getNamaNotaris() + "'" +
            ", wilKerjaNotaris='" + getWilKerjaNotaris() + "'" +
            ", skNotaris='" + getSkNotaris() + "'" +
            ", alamat='" + getAlamat() + "'" +
            ", rt='" + getRt() + "'" +
            ", rw='" + getRw() + "'" +
            ", kelurahan='" + getKelurahan() + "'" +
            ", kecamatan='" + getKecamatan() + "'" +
            ", kota='" + getKota() + "'" +
            ", provinsi='" + getProvinsi() + "'" +
            ", kodePos='" + getKodePos() + "'" +
            ", noKontak='" + getNoKontak() + "'" +
            ", namaSk='" + getNamaSk() + "'" +
            ", nomorSk='" + getNomorSk() + "'" +
            ", tglSk='" + getTglSk() + "'" +
            ", npwp='" + getNpwp() + "'" +
            ", pjNama='" + getPjNama() + "'" +
            ", pjJenisKelamin='" + getPjJenisKelamin() + "'" +
            ", pjStatusKawin='" + getPjStatusKawin() + "'" +
            ", pjKelahiran='" + getPjKelahiran() + "'" +
            ", pjTglLahir='" + getPjTglLahir() + "'" +
            ", pjPekerjaan='" + getPjPekerjaan() + "'" +
            ", pjWargaNegara='" + getPjWargaNegara() + "'" +
            ", pjJenisId='" + getPjJenisId() + "'" +
            ", pjNoId='" + getPjNoId() + "'" +
            ", pjAlamat='" + getPjAlamat() + "'" +
            ", pjRt='" + getPjRt() + "'" +
            ", pjRw='" + getPjRw() + "'" +
            ", pjKelurahan='" + getPjKelurahan() + "'" +
            ", pjKecamatan='" + getPjKecamatan() + "'" +
            ", pjKota='" + getPjKota() + "'" +
            ", pjProvinsi='" + getPjProvinsi() + "'" +
            ", pjKodePos='" + getPjKodePos() + "'" +
            ", template='" + getTemplate() + "'" +
            ", komparisi='" + getKomparisi() + "'" +
            "}";
    }
}
