package com.project.fidusia.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Orang.
 */
@Entity
@Table(name = "orang")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Orang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "penggunaan")
    private String penggunaan;

    @Column(name = "nama")
    private String nama;

    @Column(name = "jenis_kelamin")
    private String jenisKelamin;

    @Column(name = "status_kawin")
    private String statusKawin;

    @Column(name = "kelahiran")
    private String kelahiran;

    @Column(name = "tgl_lahir")
    private LocalDate tglLahir;

    @Column(name = "pekerjaan")
    private String pekerjaan;

    @Column(name = "warga_negara")
    private String wargaNegara;

    @Column(name = "jenis_id")
    private String jenisId;

    @Column(name = "no_id")
    private String noId;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "rt")
    private String rt;

    @Column(name = "rw")
    private String rw;

    @Column(name = "kelurahan")
    private String kelurahan;

    @Column(name = "kecamatan")
    private String kecamatan;

    @Column(name = "kota")
    private String kota;

    @Column(name = "provinsi")
    private String provinsi;

    @Column(name = "kode_pos")
    private String kodePos;

    @Column(name = "no_kontak")
    private String noKontak;

    @JsonIgnoreProperties(value = { "orang", "badan", "akta" }, allowSetters = true)
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "orang")
    private Ppk ppk;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Orang id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPenggunaan() {
        return this.penggunaan;
    }

    public Orang penggunaan(String penggunaan) {
        this.setPenggunaan(penggunaan);
        return this;
    }

    public void setPenggunaan(String penggunaan) {
        this.penggunaan = penggunaan;
    }

    public String getNama() {
        return this.nama;
    }

    public Orang nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisKelamin() {
        return this.jenisKelamin;
    }

    public Orang jenisKelamin(String jenisKelamin) {
        this.setJenisKelamin(jenisKelamin);
        return this;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getStatusKawin() {
        return this.statusKawin;
    }

    public Orang statusKawin(String statusKawin) {
        this.setStatusKawin(statusKawin);
        return this;
    }

    public void setStatusKawin(String statusKawin) {
        this.statusKawin = statusKawin;
    }

    public String getKelahiran() {
        return this.kelahiran;
    }

    public Orang kelahiran(String kelahiran) {
        this.setKelahiran(kelahiran);
        return this;
    }

    public void setKelahiran(String kelahiran) {
        this.kelahiran = kelahiran;
    }

    public LocalDate getTglLahir() {
        return this.tglLahir;
    }

    public Orang tglLahir(LocalDate tglLahir) {
        this.setTglLahir(tglLahir);
        return this;
    }

    public void setTglLahir(LocalDate tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getPekerjaan() {
        return this.pekerjaan;
    }

    public Orang pekerjaan(String pekerjaan) {
        this.setPekerjaan(pekerjaan);
        return this;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getWargaNegara() {
        return this.wargaNegara;
    }

    public Orang wargaNegara(String wargaNegara) {
        this.setWargaNegara(wargaNegara);
        return this;
    }

    public void setWargaNegara(String wargaNegara) {
        this.wargaNegara = wargaNegara;
    }

    public String getJenisId() {
        return this.jenisId;
    }

    public Orang jenisId(String jenisId) {
        this.setJenisId(jenisId);
        return this;
    }

    public void setJenisId(String jenisId) {
        this.jenisId = jenisId;
    }

    public String getNoId() {
        return this.noId;
    }

    public Orang noId(String noId) {
        this.setNoId(noId);
        return this;
    }

    public void setNoId(String noId) {
        this.noId = noId;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public Orang alamat(String alamat) {
        this.setAlamat(alamat);
        return this;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getRt() {
        return this.rt;
    }

    public Orang rt(String rt) {
        this.setRt(rt);
        return this;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return this.rw;
    }

    public Orang rw(String rw) {
        this.setRw(rw);
        return this;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getKelurahan() {
        return this.kelurahan;
    }

    public Orang kelurahan(String kelurahan) {
        this.setKelurahan(kelurahan);
        return this;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return this.kecamatan;
    }

    public Orang kecamatan(String kecamatan) {
        this.setKecamatan(kecamatan);
        return this;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKota() {
        return this.kota;
    }

    public Orang kota(String kota) {
        this.setKota(kota);
        return this;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getProvinsi() {
        return this.provinsi;
    }

    public Orang provinsi(String provinsi) {
        this.setProvinsi(provinsi);
        return this;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKodePos() {
        return this.kodePos;
    }

    public Orang kodePos(String kodePos) {
        this.setKodePos(kodePos);
        return this;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public String getNoKontak() {
        return this.noKontak;
    }

    public Orang noKontak(String noKontak) {
        this.setNoKontak(noKontak);
        return this;
    }

    public void setNoKontak(String noKontak) {
        this.noKontak = noKontak;
    }

    public Ppk getPpk() {
        return this.ppk;
    }

    public void setPpk(Ppk ppk) {
        if (this.ppk != null) {
            this.ppk.setOrang(null);
        }
        if (ppk != null) {
            ppk.setOrang(this);
        }
        this.ppk = ppk;
    }

    public Orang ppk(Ppk ppk) {
        this.setPpk(ppk);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Orang)) {
            return false;
        }
        return getId() != null && getId().equals(((Orang) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Orang{" +
            "id=" + getId() +
            ", penggunaan='" + getPenggunaan() + "'" +
            ", nama='" + getNama() + "'" +
            ", jenisKelamin='" + getJenisKelamin() + "'" +
            ", statusKawin='" + getStatusKawin() + "'" +
            ", kelahiran='" + getKelahiran() + "'" +
            ", tglLahir='" + getTglLahir() + "'" +
            ", pekerjaan='" + getPekerjaan() + "'" +
            ", wargaNegara='" + getWargaNegara() + "'" +
            ", jenisId='" + getJenisId() + "'" +
            ", noId='" + getNoId() + "'" +
            ", alamat='" + getAlamat() + "'" +
            ", rt='" + getRt() + "'" +
            ", rw='" + getRw() + "'" +
            ", kelurahan='" + getKelurahan() + "'" +
            ", kecamatan='" + getKecamatan() + "'" +
            ", kota='" + getKota() + "'" +
            ", provinsi='" + getProvinsi() + "'" +
            ", kodePos='" + getKodePos() + "'" +
            ", noKontak='" + getNoKontak() + "'" +
            "}";
    }
}
