package com.project.fidusia.web.rest;

import com.project.fidusia.domain.Invoice;
import com.project.fidusia.repository.InvoiceRepository;
import com.project.fidusia.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.project.fidusia.domain.Invoice}.
 */
@RestController
@RequestMapping("/api/invoices")
@Transactional
public class InvoiceResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceResource.class);

    private static final String ENTITY_NAME = "invoice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InvoiceRepository invoiceRepository;

    public InvoiceResource(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    /**
     * {@code POST  /invoices} : Create a new invoice.
     *
     * @param invoice the invoice to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new invoice, or with status {@code 400 (Bad Request)} if the invoice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Invoice> createInvoice(@RequestBody Invoice invoice) throws URISyntaxException {
        log.debug("REST request to save Invoice : {}", invoice);
        if (invoice.getId() != null) {
            throw new BadRequestAlertException("A new invoice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Invoice result = invoiceRepository.save(invoice);
        return ResponseEntity
            .created(new URI("/api/invoices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /invoices/:id} : Updates an existing invoice.
     *
     * @param id the id of the invoice to save.
     * @param invoice the invoice to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated invoice,
     * or with status {@code 400 (Bad Request)} if the invoice is not valid,
     * or with status {@code 500 (Internal Server Error)} if the invoice couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Invoice> updateInvoice(@PathVariable(value = "id", required = false) final Long id, @RequestBody Invoice invoice)
        throws URISyntaxException {
        log.debug("REST request to update Invoice : {}, {}", id, invoice);
        if (invoice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, invoice.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!invoiceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Invoice result = invoiceRepository.save(invoice);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, invoice.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /invoices/:id} : Partial updates given fields of an existing invoice, field will ignore if it is null
     *
     * @param id the id of the invoice to save.
     * @param invoice the invoice to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated invoice,
     * or with status {@code 400 (Bad Request)} if the invoice is not valid,
     * or with status {@code 404 (Not Found)} if the invoice is not found,
     * or with status {@code 500 (Internal Server Error)} if the invoice couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Invoice> partialUpdateInvoice(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Invoice invoice
    ) throws URISyntaxException {
        log.debug("REST request to partial update Invoice partially : {}, {}", id, invoice);
        if (invoice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, invoice.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!invoiceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Invoice> result = invoiceRepository
            .findById(invoice.getId())
            .map(existingInvoice -> {
                if (invoice.getType() != null) {
                    existingInvoice.setType(invoice.getType());
                }
                if (invoice.getTanggal() != null) {
                    existingInvoice.setTanggal(invoice.getTanggal());
                }
                if (invoice.getTglOrder() != null) {
                    existingInvoice.setTglOrder(invoice.getTglOrder());
                }
                if (invoice.getJmlOrder() != null) {
                    existingInvoice.setJmlOrder(invoice.getJmlOrder());
                }
                if (invoice.getJmlOrderCancel() != null) {
                    existingInvoice.setJmlOrderCancel(invoice.getJmlOrderCancel());
                }
                if (invoice.getNoProformaPnbp() != null) {
                    existingInvoice.setNoProformaPnbp(invoice.getNoProformaPnbp());
                }
                if (invoice.getNoProformaJasa() != null) {
                    existingInvoice.setNoProformaJasa(invoice.getNoProformaJasa());
                }
                if (invoice.getNoBillpymPnbp() != null) {
                    existingInvoice.setNoBillpymPnbp(invoice.getNoBillpymPnbp());
                }
                if (invoice.getNoBillpymJasa() != null) {
                    existingInvoice.setNoBillpymJasa(invoice.getNoBillpymJasa());
                }
                if (invoice.getAmountPnbp() != null) {
                    existingInvoice.setAmountPnbp(invoice.getAmountPnbp());
                }
                if (invoice.getTaxPnbp() != null) {
                    existingInvoice.setTaxPnbp(invoice.getTaxPnbp());
                }
                if (invoice.getTotalAmountPnbp() != null) {
                    existingInvoice.setTotalAmountPnbp(invoice.getTotalAmountPnbp());
                }
                if (invoice.getAmountJasa() != null) {
                    existingInvoice.setAmountJasa(invoice.getAmountJasa());
                }
                if (invoice.getVatJasa() != null) {
                    existingInvoice.setVatJasa(invoice.getVatJasa());
                }
                if (invoice.getTaxJasa() != null) {
                    existingInvoice.setTaxJasa(invoice.getTaxJasa());
                }
                if (invoice.getTotalAmountJasa() != null) {
                    existingInvoice.setTotalAmountJasa(invoice.getTotalAmountJasa());
                }
                if (invoice.getFileProformaPnbp() != null) {
                    existingInvoice.setFileProformaPnbp(invoice.getFileProformaPnbp());
                }
                if (invoice.getFileProformaJasa() != null) {
                    existingInvoice.setFileProformaJasa(invoice.getFileProformaJasa());
                }
                if (invoice.getFileBillpymPnbp() != null) {
                    existingInvoice.setFileBillpymPnbp(invoice.getFileBillpymPnbp());
                }
                if (invoice.getFileBillpymJasa() != null) {
                    existingInvoice.setFileBillpymJasa(invoice.getFileBillpymJasa());
                }
                if (invoice.getFileBulk() != null) {
                    existingInvoice.setFileBulk(invoice.getFileBulk());
                }
                if (invoice.getFileTax() != null) {
                    existingInvoice.setFileTax(invoice.getFileTax());
                }
                if (invoice.getFileTxt() != null) {
                    existingInvoice.setFileTxt(invoice.getFileTxt());
                }
                if (invoice.getStatus() != null) {
                    existingInvoice.setStatus(invoice.getStatus());
                }
                if (invoice.getUpdateBy() != null) {
                    existingInvoice.setUpdateBy(invoice.getUpdateBy());
                }
                if (invoice.getUpdateOn() != null) {
                    existingInvoice.setUpdateOn(invoice.getUpdateOn());
                }

                return existingInvoice;
            })
            .map(invoiceRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, invoice.getId().toString())
        );
    }

    /**
     * {@code GET  /invoices} : get all the invoices.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of invoices in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Invoice>> getAllInvoices(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Invoices");
        Page<Invoice> page = invoiceRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /invoices/:id} : get the "id" invoice.
     *
     * @param id the id of the invoice to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the invoice, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Invoice> getInvoice(@PathVariable("id") Long id) {
        log.debug("REST request to get Invoice : {}", id);
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(invoice);
    }

    /**
     * {@code DELETE  /invoices/:id} : delete the "id" invoice.
     *
     * @param id the id of the invoice to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteInvoice(@PathVariable("id") Long id) {
        log.debug("REST request to delete Invoice : {}", id);
        invoiceRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
