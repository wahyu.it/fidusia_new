package com.project.fidusia.web.rest;

import com.project.fidusia.domain.Sertifikat;
import com.project.fidusia.repository.SertifikatRepository;
import com.project.fidusia.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.project.fidusia.domain.Sertifikat}.
 */
@RestController
@RequestMapping("/api/sertifikats")
@Transactional
public class SertifikatResource {

    private final Logger log = LoggerFactory.getLogger(SertifikatResource.class);

    private static final String ENTITY_NAME = "sertifikat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SertifikatRepository sertifikatRepository;

    public SertifikatResource(SertifikatRepository sertifikatRepository) {
        this.sertifikatRepository = sertifikatRepository;
    }

    /**
     * {@code POST  /sertifikats} : Create a new sertifikat.
     *
     * @param sertifikat the sertifikat to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sertifikat, or with status {@code 400 (Bad Request)} if the sertifikat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Sertifikat> createSertifikat(@RequestBody Sertifikat sertifikat) throws URISyntaxException {
        log.debug("REST request to save Sertifikat : {}", sertifikat);
        if (sertifikat.getId() != null) {
            throw new BadRequestAlertException("A new sertifikat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sertifikat result = sertifikatRepository.save(sertifikat);
        return ResponseEntity
            .created(new URI("/api/sertifikats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sertifikats/:id} : Updates an existing sertifikat.
     *
     * @param id the id of the sertifikat to save.
     * @param sertifikat the sertifikat to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sertifikat,
     * or with status {@code 400 (Bad Request)} if the sertifikat is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sertifikat couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Sertifikat> updateSertifikat(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sertifikat sertifikat
    ) throws URISyntaxException {
        log.debug("REST request to update Sertifikat : {}, {}", id, sertifikat);
        if (sertifikat.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sertifikat.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sertifikatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Sertifikat result = sertifikatRepository.save(sertifikat);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sertifikat.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sertifikats/:id} : Partial updates given fields of an existing sertifikat, field will ignore if it is null
     *
     * @param id the id of the sertifikat to save.
     * @param sertifikat the sertifikat to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sertifikat,
     * or with status {@code 400 (Bad Request)} if the sertifikat is not valid,
     * or with status {@code 404 (Not Found)} if the sertifikat is not found,
     * or with status {@code 500 (Internal Server Error)} if the sertifikat couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Sertifikat> partialUpdateSertifikat(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sertifikat sertifikat
    ) throws URISyntaxException {
        log.debug("REST request to partial update Sertifikat partially : {}, {}", id, sertifikat);
        if (sertifikat.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sertifikat.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sertifikatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Sertifikat> result = sertifikatRepository
            .findById(sertifikat.getId())
            .map(existingSertifikat -> {
                if (sertifikat.getKodeVoucher() != null) {
                    existingSertifikat.setKodeVoucher(sertifikat.getKodeVoucher());
                }
                if (sertifikat.getTglVoucher() != null) {
                    existingSertifikat.setTglVoucher(sertifikat.getTglVoucher());
                }
                if (sertifikat.getNoSertifikat() != null) {
                    existingSertifikat.setNoSertifikat(sertifikat.getNoSertifikat());
                }
                if (sertifikat.getTglSertifikat() != null) {
                    existingSertifikat.setTglSertifikat(sertifikat.getTglSertifikat());
                }
                if (sertifikat.getBiayaPnbp() != null) {
                    existingSertifikat.setBiayaPnbp(sertifikat.getBiayaPnbp());
                }
                if (sertifikat.getNoReferensiBni() != null) {
                    existingSertifikat.setNoReferensiBni(sertifikat.getNoReferensiBni());
                }
                if (sertifikat.getStatus() != null) {
                    existingSertifikat.setStatus(sertifikat.getStatus());
                }
                if (sertifikat.getUpdateBy() != null) {
                    existingSertifikat.setUpdateBy(sertifikat.getUpdateBy());
                }
                if (sertifikat.getUpdateOn() != null) {
                    existingSertifikat.setUpdateOn(sertifikat.getUpdateOn());
                }
                if (sertifikat.getRegistrationId() != null) {
                    existingSertifikat.setRegistrationId(sertifikat.getRegistrationId());
                }
                if (sertifikat.getTglOrder() != null) {
                    existingSertifikat.setTglOrder(sertifikat.getTglOrder());
                }
                if (sertifikat.getTglCancel() != null) {
                    existingSertifikat.setTglCancel(sertifikat.getTglCancel());
                }
                if (sertifikat.getBiayaJasa() != null) {
                    existingSertifikat.setBiayaJasa(sertifikat.getBiayaJasa());
                }
                if (sertifikat.getInvoiceSubmitted() != null) {
                    existingSertifikat.setInvoiceSubmitted(sertifikat.getInvoiceSubmitted());
                }
                if (sertifikat.getReportSubmitted() != null) {
                    existingSertifikat.setReportSubmitted(sertifikat.getReportSubmitted());
                }

                return existingSertifikat;
            })
            .map(sertifikatRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sertifikat.getId().toString())
        );
    }

    /**
     * {@code GET  /sertifikats} : get all the sertifikats.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sertifikats in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Sertifikat>> getAllSertifikats(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Sertifikats");
        Page<Sertifikat> page = sertifikatRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sertifikats/:id} : get the "id" sertifikat.
     *
     * @param id the id of the sertifikat to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sertifikat, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Sertifikat> getSertifikat(@PathVariable("id") Long id) {
        log.debug("REST request to get Sertifikat : {}", id);
        Optional<Sertifikat> sertifikat = sertifikatRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sertifikat);
    }

    /**
     * {@code DELETE  /sertifikats/:id} : delete the "id" sertifikat.
     *
     * @param id the id of the sertifikat to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSertifikat(@PathVariable("id") Long id) {
        log.debug("REST request to delete Sertifikat : {}", id);
        sertifikatRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
