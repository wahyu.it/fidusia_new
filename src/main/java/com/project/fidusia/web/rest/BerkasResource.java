package com.project.fidusia.web.rest;

import com.project.fidusia.domain.Berkas;
import com.project.fidusia.repository.BerkasRepository;
import com.project.fidusia.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.project.fidusia.domain.Berkas}.
 */
@RestController
@RequestMapping("/api/berkas")
@Transactional
public class BerkasResource {

    private final Logger log = LoggerFactory.getLogger(BerkasResource.class);

    private static final String ENTITY_NAME = "berkas";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BerkasRepository berkasRepository;

    public BerkasResource(BerkasRepository berkasRepository) {
        this.berkasRepository = berkasRepository;
    }

    /**
     * {@code POST  /berkas} : Create a new berkas.
     *
     * @param berkas the berkas to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new berkas, or with status {@code 400 (Bad Request)} if the berkas has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Berkas> createBerkas(@RequestBody Berkas berkas) throws URISyntaxException {
        log.debug("REST request to save Berkas : {}", berkas);
        if (berkas.getId() != null) {
            throw new BadRequestAlertException("A new berkas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Berkas result = berkasRepository.save(berkas);
        return ResponseEntity
            .created(new URI("/api/berkas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /berkas/:id} : Updates an existing berkas.
     *
     * @param id the id of the berkas to save.
     * @param berkas the berkas to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated berkas,
     * or with status {@code 400 (Bad Request)} if the berkas is not valid,
     * or with status {@code 500 (Internal Server Error)} if the berkas couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Berkas> updateBerkas(@PathVariable(value = "id", required = false) final Long id, @RequestBody Berkas berkas)
        throws URISyntaxException {
        log.debug("REST request to update Berkas : {}, {}", id, berkas);
        if (berkas.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, berkas.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!berkasRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Berkas result = berkasRepository.save(berkas);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, berkas.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /berkas/:id} : Partial updates given fields of an existing berkas, field will ignore if it is null
     *
     * @param id the id of the berkas to save.
     * @param berkas the berkas to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated berkas,
     * or with status {@code 400 (Bad Request)} if the berkas is not valid,
     * or with status {@code 404 (Not Found)} if the berkas is not found,
     * or with status {@code 500 (Internal Server Error)} if the berkas couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Berkas> partialUpdateBerkas(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Berkas berkas
    ) throws URISyntaxException {
        log.debug("REST request to partial update Berkas partially : {}, {}", id, berkas);
        if (berkas.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, berkas.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!berkasRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Berkas> result = berkasRepository
            .findById(berkas.getId())
            .map(existingBerkas -> {
                if (berkas.getIdentitas() != null) {
                    existingBerkas.setIdentitas(berkas.getIdentitas());
                }
                if (berkas.getKk() != null) {
                    existingBerkas.setKk(berkas.getKk());
                }
                if (berkas.getPpk() != null) {
                    existingBerkas.setPpk(berkas.getPpk());
                }
                if (berkas.getSkNasabah() != null) {
                    existingBerkas.setSkNasabah(berkas.getSkNasabah());
                }
                if (berkas.getAktaPendirian() != null) {
                    existingBerkas.setAktaPendirian(berkas.getAktaPendirian());
                }
                if (berkas.getAktaPerubahan() != null) {
                    existingBerkas.setAktaPerubahan(berkas.getAktaPerubahan());
                }
                if (berkas.getSkPendirian() != null) {
                    existingBerkas.setSkPendirian(berkas.getSkPendirian());
                }
                if (berkas.getSkPerubahan() != null) {
                    existingBerkas.setSkPerubahan(berkas.getSkPerubahan());
                }
                if (berkas.getNpwp() != null) {
                    existingBerkas.setNpwp(berkas.getNpwp());
                }
                if (berkas.getUpdateBy() != null) {
                    existingBerkas.setUpdateBy(berkas.getUpdateBy());
                }
                if (berkas.getUpdateOn() != null) {
                    existingBerkas.setUpdateOn(berkas.getUpdateOn());
                }
                if (berkas.getUploadBy() != null) {
                    existingBerkas.setUploadBy(berkas.getUploadBy());
                }
                if (berkas.getUploadOn() != null) {
                    existingBerkas.setUploadOn(berkas.getUploadOn());
                }
                if (berkas.getPpkNomor() != null) {
                    existingBerkas.setPpkNomor(berkas.getPpkNomor());
                }
                if (berkas.getNsbJenis() != null) {
                    existingBerkas.setNsbJenis(berkas.getNsbJenis());
                }
                if (berkas.getBerkasStatus() != null) {
                    existingBerkas.setBerkasStatus(berkas.getBerkasStatus());
                }
                if (berkas.getNamaNasabah() != null) {
                    existingBerkas.setNamaNasabah(berkas.getNamaNasabah());
                }
                if (berkas.getTanggalPpk() != null) {
                    existingBerkas.setTanggalPpk(berkas.getTanggalPpk());
                }
                if (berkas.getMetodeKirim() != null) {
                    existingBerkas.setMetodeKirim(berkas.getMetodeKirim());
                }
                if (berkas.getPartitionId() != null) {
                    existingBerkas.setPartitionId(berkas.getPartitionId());
                }
                if (berkas.getVerifyBy() != null) {
                    existingBerkas.setVerifyBy(berkas.getVerifyBy());
                }
                if (berkas.getVerifyOn() != null) {
                    existingBerkas.setVerifyOn(berkas.getVerifyOn());
                }
                if (berkas.getOrderType() != null) {
                    existingBerkas.setOrderType(berkas.getOrderType());
                }

                return existingBerkas;
            })
            .map(berkasRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, berkas.getId().toString())
        );
    }

    /**
     * {@code GET  /berkas} : get all the berkas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of berkas in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Berkas>> getAllBerkas(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Berkas");
        Page<Berkas> page = berkasRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /berkas/:id} : get the "id" berkas.
     *
     * @param id the id of the berkas to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the berkas, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Berkas> getBerkas(@PathVariable("id") Long id) {
        log.debug("REST request to get Berkas : {}", id);
        Optional<Berkas> berkas = berkasRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(berkas);
    }

    /**
     * {@code DELETE  /berkas/:id} : delete the "id" berkas.
     *
     * @param id the id of the berkas to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBerkas(@PathVariable("id") Long id) {
        log.debug("REST request to delete Berkas : {}", id);
        berkasRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
