import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IKendaraan, NewKendaraan } from '../kendaraan.model';

export type PartialUpdateKendaraan = Partial<IKendaraan> & Pick<IKendaraan, 'id'>;

export type EntityResponseType = HttpResponse<IKendaraan>;
export type EntityArrayResponseType = HttpResponse<IKendaraan[]>;

@Injectable({ providedIn: 'root' })
export class KendaraanService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/kendaraans');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(kendaraan: NewKendaraan): Observable<EntityResponseType> {
    return this.http.post<IKendaraan>(this.resourceUrl, kendaraan, { observe: 'response' });
  }

  update(kendaraan: IKendaraan): Observable<EntityResponseType> {
    return this.http.put<IKendaraan>(`${this.resourceUrl}/${this.getKendaraanIdentifier(kendaraan)}`, kendaraan, { observe: 'response' });
  }

  partialUpdate(kendaraan: PartialUpdateKendaraan): Observable<EntityResponseType> {
    return this.http.patch<IKendaraan>(`${this.resourceUrl}/${this.getKendaraanIdentifier(kendaraan)}`, kendaraan, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IKendaraan>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IKendaraan[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getKendaraanIdentifier(kendaraan: Pick<IKendaraan, 'id'>): number {
    return kendaraan.id;
  }

  compareKendaraan(o1: Pick<IKendaraan, 'id'> | null, o2: Pick<IKendaraan, 'id'> | null): boolean {
    return o1 && o2 ? this.getKendaraanIdentifier(o1) === this.getKendaraanIdentifier(o2) : o1 === o2;
  }

  addKendaraanToCollectionIfMissing<Type extends Pick<IKendaraan, 'id'>>(
    kendaraanCollection: Type[],
    ...kendaraansToCheck: (Type | null | undefined)[]
  ): Type[] {
    const kendaraans: Type[] = kendaraansToCheck.filter(isPresent);
    if (kendaraans.length > 0) {
      const kendaraanCollectionIdentifiers = kendaraanCollection.map(kendaraanItem => this.getKendaraanIdentifier(kendaraanItem)!);
      const kendaraansToAdd = kendaraans.filter(kendaraanItem => {
        const kendaraanIdentifier = this.getKendaraanIdentifier(kendaraanItem);
        if (kendaraanCollectionIdentifiers.includes(kendaraanIdentifier)) {
          return false;
        }
        kendaraanCollectionIdentifiers.push(kendaraanIdentifier);
        return true;
      });
      return [...kendaraansToAdd, ...kendaraanCollection];
    }
    return kendaraanCollection;
  }
}
