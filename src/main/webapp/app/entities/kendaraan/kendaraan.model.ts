import { IPpk } from 'app/entities/ppk/ppk.model';

export interface IKendaraan {
  id: number;
  kondisi?: string | null;
  roda?: number | null;
  merk?: string | null;
  tipe?: string | null;
  warna?: string | null;
  tahunPembuatan?: string | null;
  noRangka?: string | null;
  noMesin?: string | null;
  noBpkb?: string | null;
  nilaiObjekJaminan?: number | null;
  ppk?: IPpk | null;
}

export type NewKendaraan = Omit<IKendaraan, 'id'> & { id: null };
