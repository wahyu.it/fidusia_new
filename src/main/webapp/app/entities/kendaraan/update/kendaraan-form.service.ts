import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IKendaraan, NewKendaraan } from '../kendaraan.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IKendaraan for edit and NewKendaraanFormGroupInput for create.
 */
type KendaraanFormGroupInput = IKendaraan | PartialWithRequiredKeyOf<NewKendaraan>;

type KendaraanFormDefaults = Pick<NewKendaraan, 'id'>;

type KendaraanFormGroupContent = {
  id: FormControl<IKendaraan['id'] | NewKendaraan['id']>;
  kondisi: FormControl<IKendaraan['kondisi']>;
  roda: FormControl<IKendaraan['roda']>;
  merk: FormControl<IKendaraan['merk']>;
  tipe: FormControl<IKendaraan['tipe']>;
  warna: FormControl<IKendaraan['warna']>;
  tahunPembuatan: FormControl<IKendaraan['tahunPembuatan']>;
  noRangka: FormControl<IKendaraan['noRangka']>;
  noMesin: FormControl<IKendaraan['noMesin']>;
  noBpkb: FormControl<IKendaraan['noBpkb']>;
  nilaiObjekJaminan: FormControl<IKendaraan['nilaiObjekJaminan']>;
  ppk: FormControl<IKendaraan['ppk']>;
};

export type KendaraanFormGroup = FormGroup<KendaraanFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class KendaraanFormService {
  createKendaraanFormGroup(kendaraan: KendaraanFormGroupInput = { id: null }): KendaraanFormGroup {
    const kendaraanRawValue = {
      ...this.getFormDefaults(),
      ...kendaraan,
    };
    return new FormGroup<KendaraanFormGroupContent>({
      id: new FormControl(
        { value: kendaraanRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      kondisi: new FormControl(kendaraanRawValue.kondisi),
      roda: new FormControl(kendaraanRawValue.roda),
      merk: new FormControl(kendaraanRawValue.merk),
      tipe: new FormControl(kendaraanRawValue.tipe),
      warna: new FormControl(kendaraanRawValue.warna),
      tahunPembuatan: new FormControl(kendaraanRawValue.tahunPembuatan),
      noRangka: new FormControl(kendaraanRawValue.noRangka),
      noMesin: new FormControl(kendaraanRawValue.noMesin),
      noBpkb: new FormControl(kendaraanRawValue.noBpkb),
      nilaiObjekJaminan: new FormControl(kendaraanRawValue.nilaiObjekJaminan),
      ppk: new FormControl(kendaraanRawValue.ppk),
    });
  }

  getKendaraan(form: KendaraanFormGroup): IKendaraan | NewKendaraan {
    return form.getRawValue() as IKendaraan | NewKendaraan;
  }

  resetForm(form: KendaraanFormGroup, kendaraan: KendaraanFormGroupInput): void {
    const kendaraanRawValue = { ...this.getFormDefaults(), ...kendaraan };
    form.reset(
      {
        ...kendaraanRawValue,
        id: { value: kendaraanRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): KendaraanFormDefaults {
    return {
      id: null,
    };
  }
}
