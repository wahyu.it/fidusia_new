import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../kendaraan.test-samples';

import { KendaraanFormService } from './kendaraan-form.service';

describe('Kendaraan Form Service', () => {
  let service: KendaraanFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KendaraanFormService);
  });

  describe('Service methods', () => {
    describe('createKendaraanFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createKendaraanFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kondisi: expect.any(Object),
            roda: expect.any(Object),
            merk: expect.any(Object),
            tipe: expect.any(Object),
            warna: expect.any(Object),
            tahunPembuatan: expect.any(Object),
            noRangka: expect.any(Object),
            noMesin: expect.any(Object),
            noBpkb: expect.any(Object),
            nilaiObjekJaminan: expect.any(Object),
            ppk: expect.any(Object),
          }),
        );
      });

      it('passing IKendaraan should create a new form with FormGroup', () => {
        const formGroup = service.createKendaraanFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kondisi: expect.any(Object),
            roda: expect.any(Object),
            merk: expect.any(Object),
            tipe: expect.any(Object),
            warna: expect.any(Object),
            tahunPembuatan: expect.any(Object),
            noRangka: expect.any(Object),
            noMesin: expect.any(Object),
            noBpkb: expect.any(Object),
            nilaiObjekJaminan: expect.any(Object),
            ppk: expect.any(Object),
          }),
        );
      });
    });

    describe('getKendaraan', () => {
      it('should return NewKendaraan for default Kendaraan initial value', () => {
        const formGroup = service.createKendaraanFormGroup(sampleWithNewData);

        const kendaraan = service.getKendaraan(formGroup) as any;

        expect(kendaraan).toMatchObject(sampleWithNewData);
      });

      it('should return NewKendaraan for empty Kendaraan initial value', () => {
        const formGroup = service.createKendaraanFormGroup();

        const kendaraan = service.getKendaraan(formGroup) as any;

        expect(kendaraan).toMatchObject({});
      });

      it('should return IKendaraan', () => {
        const formGroup = service.createKendaraanFormGroup(sampleWithRequiredData);

        const kendaraan = service.getKendaraan(formGroup) as any;

        expect(kendaraan).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IKendaraan should not enable id FormControl', () => {
        const formGroup = service.createKendaraanFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewKendaraan should disable id FormControl', () => {
        const formGroup = service.createKendaraanFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
