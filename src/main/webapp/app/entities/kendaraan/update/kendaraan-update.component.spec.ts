import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IPpk } from 'app/entities/ppk/ppk.model';
import { PpkService } from 'app/entities/ppk/service/ppk.service';
import { KendaraanService } from '../service/kendaraan.service';
import { IKendaraan } from '../kendaraan.model';
import { KendaraanFormService } from './kendaraan-form.service';

import { KendaraanUpdateComponent } from './kendaraan-update.component';

describe('Kendaraan Management Update Component', () => {
  let comp: KendaraanUpdateComponent;
  let fixture: ComponentFixture<KendaraanUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let kendaraanFormService: KendaraanFormService;
  let kendaraanService: KendaraanService;
  let ppkService: PpkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), KendaraanUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(KendaraanUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(KendaraanUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    kendaraanFormService = TestBed.inject(KendaraanFormService);
    kendaraanService = TestBed.inject(KendaraanService);
    ppkService = TestBed.inject(PpkService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Ppk query and add missing value', () => {
      const kendaraan: IKendaraan = { id: 456 };
      const ppk: IPpk = { id: 1664 };
      kendaraan.ppk = ppk;

      const ppkCollection: IPpk[] = [{ id: 13755 }];
      jest.spyOn(ppkService, 'query').mockReturnValue(of(new HttpResponse({ body: ppkCollection })));
      const additionalPpks = [ppk];
      const expectedCollection: IPpk[] = [...additionalPpks, ...ppkCollection];
      jest.spyOn(ppkService, 'addPpkToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ kendaraan });
      comp.ngOnInit();

      expect(ppkService.query).toHaveBeenCalled();
      expect(ppkService.addPpkToCollectionIfMissing).toHaveBeenCalledWith(ppkCollection, ...additionalPpks.map(expect.objectContaining));
      expect(comp.ppksSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const kendaraan: IKendaraan = { id: 456 };
      const ppk: IPpk = { id: 19382 };
      kendaraan.ppk = ppk;

      activatedRoute.data = of({ kendaraan });
      comp.ngOnInit();

      expect(comp.ppksSharedCollection).toContain(ppk);
      expect(comp.kendaraan).toEqual(kendaraan);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IKendaraan>>();
      const kendaraan = { id: 123 };
      jest.spyOn(kendaraanFormService, 'getKendaraan').mockReturnValue(kendaraan);
      jest.spyOn(kendaraanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ kendaraan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: kendaraan }));
      saveSubject.complete();

      // THEN
      expect(kendaraanFormService.getKendaraan).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(kendaraanService.update).toHaveBeenCalledWith(expect.objectContaining(kendaraan));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IKendaraan>>();
      const kendaraan = { id: 123 };
      jest.spyOn(kendaraanFormService, 'getKendaraan').mockReturnValue({ id: null });
      jest.spyOn(kendaraanService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ kendaraan: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: kendaraan }));
      saveSubject.complete();

      // THEN
      expect(kendaraanFormService.getKendaraan).toHaveBeenCalled();
      expect(kendaraanService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IKendaraan>>();
      const kendaraan = { id: 123 };
      jest.spyOn(kendaraanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ kendaraan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(kendaraanService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePpk', () => {
      it('Should forward to ppkService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(ppkService, 'comparePpk');
        comp.comparePpk(entity, entity2);
        expect(ppkService.comparePpk).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
