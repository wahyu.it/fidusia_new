import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPpk } from 'app/entities/ppk/ppk.model';
import { PpkService } from 'app/entities/ppk/service/ppk.service';
import { IKendaraan } from '../kendaraan.model';
import { KendaraanService } from '../service/kendaraan.service';
import { KendaraanFormService, KendaraanFormGroup } from './kendaraan-form.service';

@Component({
  standalone: true,
  selector: 'jhi-kendaraan-update',
  templateUrl: './kendaraan-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class KendaraanUpdateComponent implements OnInit {
  isSaving = false;
  kendaraan: IKendaraan | null = null;

  ppksSharedCollection: IPpk[] = [];

  editForm: KendaraanFormGroup = this.kendaraanFormService.createKendaraanFormGroup();

  constructor(
    protected kendaraanService: KendaraanService,
    protected kendaraanFormService: KendaraanFormService,
    protected ppkService: PpkService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  comparePpk = (o1: IPpk | null, o2: IPpk | null): boolean => this.ppkService.comparePpk(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ kendaraan }) => {
      this.kendaraan = kendaraan;
      if (kendaraan) {
        this.updateForm(kendaraan);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const kendaraan = this.kendaraanFormService.getKendaraan(this.editForm);
    if (kendaraan.id !== null) {
      this.subscribeToSaveResponse(this.kendaraanService.update(kendaraan));
    } else {
      this.subscribeToSaveResponse(this.kendaraanService.create(kendaraan));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKendaraan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(kendaraan: IKendaraan): void {
    this.kendaraan = kendaraan;
    this.kendaraanFormService.resetForm(this.editForm, kendaraan);

    this.ppksSharedCollection = this.ppkService.addPpkToCollectionIfMissing<IPpk>(this.ppksSharedCollection, kendaraan.ppk);
  }

  protected loadRelationshipsOptions(): void {
    this.ppkService
      .query()
      .pipe(map((res: HttpResponse<IPpk[]>) => res.body ?? []))
      .pipe(map((ppks: IPpk[]) => this.ppkService.addPpkToCollectionIfMissing<IPpk>(ppks, this.kendaraan?.ppk)))
      .subscribe((ppks: IPpk[]) => (this.ppksSharedCollection = ppks));
  }
}
