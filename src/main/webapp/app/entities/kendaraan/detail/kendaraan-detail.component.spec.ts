import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { KendaraanDetailComponent } from './kendaraan-detail.component';

describe('Kendaraan Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [KendaraanDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: KendaraanDetailComponent,
              resolve: { kendaraan: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(KendaraanDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load kendaraan on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', KendaraanDetailComponent);

      // THEN
      expect(instance.kendaraan).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
