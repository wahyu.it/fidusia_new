import { IKendaraan, NewKendaraan } from './kendaraan.model';

export const sampleWithRequiredData: IKendaraan = {
  id: 22255,
};

export const sampleWithPartialData: IKendaraan = {
  id: 3637,
  kondisi: 'nippy hm',
  tipe: 'when acceptance through',
  warna: 'whether puff bidet',
  nilaiObjekJaminan: 16718,
};

export const sampleWithFullData: IKendaraan = {
  id: 27073,
  kondisi: 'agile near flawed',
  roda: 20822,
  merk: 'raise loan',
  tipe: 'wry ligate',
  warna: 'phooey scarily',
  tahunPembuatan: 'even submarine',
  noRangka: 'phew',
  noMesin: 'ferociously yuck',
  noBpkb: 'oof repeatedly spry',
  nilaiObjekJaminan: 8444,
};

export const sampleWithNewData: NewKendaraan = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
