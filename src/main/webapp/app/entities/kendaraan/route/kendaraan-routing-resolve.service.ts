import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IKendaraan } from '../kendaraan.model';
import { KendaraanService } from '../service/kendaraan.service';

export const kendaraanResolve = (route: ActivatedRouteSnapshot): Observable<null | IKendaraan> => {
  const id = route.params['id'];
  if (id) {
    return inject(KendaraanService)
      .find(id)
      .pipe(
        mergeMap((kendaraan: HttpResponse<IKendaraan>) => {
          if (kendaraan.body) {
            return of(kendaraan.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default kendaraanResolve;
