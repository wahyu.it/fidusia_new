import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { KendaraanComponent } from './list/kendaraan.component';
import { KendaraanDetailComponent } from './detail/kendaraan-detail.component';
import { KendaraanUpdateComponent } from './update/kendaraan-update.component';
import KendaraanResolve from './route/kendaraan-routing-resolve.service';

const kendaraanRoute: Routes = [
  {
    path: '',
    component: KendaraanComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: KendaraanDetailComponent,
    resolve: {
      kendaraan: KendaraanResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: KendaraanUpdateComponent,
    resolve: {
      kendaraan: KendaraanResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: KendaraanUpdateComponent,
    resolve: {
      kendaraan: KendaraanResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default kendaraanRoute;
