import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IKendaraan } from '../kendaraan.model';
import { KendaraanService } from '../service/kendaraan.service';

@Component({
  standalone: true,
  templateUrl: './kendaraan-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class KendaraanDeleteDialogComponent {
  kendaraan?: IKendaraan;

  constructor(
    protected kendaraanService: KendaraanService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.kendaraanService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
