import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { SaksiComponent } from './list/saksi.component';
import { SaksiDetailComponent } from './detail/saksi-detail.component';
import { SaksiUpdateComponent } from './update/saksi-update.component';
import SaksiResolve from './route/saksi-routing-resolve.service';

const saksiRoute: Routes = [
  {
    path: '',
    component: SaksiComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SaksiDetailComponent,
    resolve: {
      saksi: SaksiResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SaksiUpdateComponent,
    resolve: {
      saksi: SaksiResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SaksiUpdateComponent,
    resolve: {
      saksi: SaksiResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default saksiRoute;
