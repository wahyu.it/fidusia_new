import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ISaksi } from '../saksi.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../saksi.test-samples';

import { SaksiService, RestSaksi } from './saksi.service';

const requireRestSample: RestSaksi = {
  ...sampleWithRequiredData,
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
};

describe('Saksi Service', () => {
  let service: SaksiService;
  let httpMock: HttpTestingController;
  let expectedResult: ISaksi | ISaksi[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SaksiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Saksi', () => {
      const saksi = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(saksi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Saksi', () => {
      const saksi = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(saksi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Saksi', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Saksi', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Saksi', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addSaksiToCollectionIfMissing', () => {
      it('should add a Saksi to an empty array', () => {
        const saksi: ISaksi = sampleWithRequiredData;
        expectedResult = service.addSaksiToCollectionIfMissing([], saksi);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(saksi);
      });

      it('should not add a Saksi to an array that contains it', () => {
        const saksi: ISaksi = sampleWithRequiredData;
        const saksiCollection: ISaksi[] = [
          {
            ...saksi,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addSaksiToCollectionIfMissing(saksiCollection, saksi);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Saksi to an array that doesn't contain it", () => {
        const saksi: ISaksi = sampleWithRequiredData;
        const saksiCollection: ISaksi[] = [sampleWithPartialData];
        expectedResult = service.addSaksiToCollectionIfMissing(saksiCollection, saksi);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(saksi);
      });

      it('should add only unique Saksi to an array', () => {
        const saksiArray: ISaksi[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const saksiCollection: ISaksi[] = [sampleWithRequiredData];
        expectedResult = service.addSaksiToCollectionIfMissing(saksiCollection, ...saksiArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const saksi: ISaksi = sampleWithRequiredData;
        const saksi2: ISaksi = sampleWithPartialData;
        expectedResult = service.addSaksiToCollectionIfMissing([], saksi, saksi2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(saksi);
        expect(expectedResult).toContain(saksi2);
      });

      it('should accept null and undefined values', () => {
        const saksi: ISaksi = sampleWithRequiredData;
        expectedResult = service.addSaksiToCollectionIfMissing([], null, saksi, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(saksi);
      });

      it('should return initial array if no Saksi is added', () => {
        const saksiCollection: ISaksi[] = [sampleWithRequiredData];
        expectedResult = service.addSaksiToCollectionIfMissing(saksiCollection, undefined, null);
        expect(expectedResult).toEqual(saksiCollection);
      });
    });

    describe('compareSaksi', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareSaksi(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareSaksi(entity1, entity2);
        const compareResult2 = service.compareSaksi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareSaksi(entity1, entity2);
        const compareResult2 = service.compareSaksi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareSaksi(entity1, entity2);
        const compareResult2 = service.compareSaksi(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
