import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISaksi, NewSaksi } from '../saksi.model';

export type PartialUpdateSaksi = Partial<ISaksi> & Pick<ISaksi, 'id'>;

type RestOf<T extends ISaksi | NewSaksi> = Omit<T, 'updateOn'> & {
  updateOn?: string | null;
};

export type RestSaksi = RestOf<ISaksi>;

export type NewRestSaksi = RestOf<NewSaksi>;

export type PartialUpdateRestSaksi = RestOf<PartialUpdateSaksi>;

export type EntityResponseType = HttpResponse<ISaksi>;
export type EntityArrayResponseType = HttpResponse<ISaksi[]>;

@Injectable({ providedIn: 'root' })
export class SaksiService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/saksis');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(saksi: NewSaksi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(saksi);
    return this.http.post<RestSaksi>(this.resourceUrl, copy, { observe: 'response' }).pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(saksi: ISaksi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(saksi);
    return this.http
      .put<RestSaksi>(`${this.resourceUrl}/${this.getSaksiIdentifier(saksi)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(saksi: PartialUpdateSaksi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(saksi);
    return this.http
      .patch<RestSaksi>(`${this.resourceUrl}/${this.getSaksiIdentifier(saksi)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestSaksi>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestSaksi[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSaksiIdentifier(saksi: Pick<ISaksi, 'id'>): number {
    return saksi.id;
  }

  compareSaksi(o1: Pick<ISaksi, 'id'> | null, o2: Pick<ISaksi, 'id'> | null): boolean {
    return o1 && o2 ? this.getSaksiIdentifier(o1) === this.getSaksiIdentifier(o2) : o1 === o2;
  }

  addSaksiToCollectionIfMissing<Type extends Pick<ISaksi, 'id'>>(
    saksiCollection: Type[],
    ...saksisToCheck: (Type | null | undefined)[]
  ): Type[] {
    const saksis: Type[] = saksisToCheck.filter(isPresent);
    if (saksis.length > 0) {
      const saksiCollectionIdentifiers = saksiCollection.map(saksiItem => this.getSaksiIdentifier(saksiItem)!);
      const saksisToAdd = saksis.filter(saksiItem => {
        const saksiIdentifier = this.getSaksiIdentifier(saksiItem);
        if (saksiCollectionIdentifiers.includes(saksiIdentifier)) {
          return false;
        }
        saksiCollectionIdentifiers.push(saksiIdentifier);
        return true;
      });
      return [...saksisToAdd, ...saksiCollection];
    }
    return saksiCollection;
  }

  protected convertDateFromClient<T extends ISaksi | NewSaksi | PartialUpdateSaksi>(saksi: T): RestOf<T> {
    return {
      ...saksi,
      updateOn: saksi.updateOn?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restSaksi: RestSaksi): ISaksi {
    return {
      ...restSaksi,
      updateOn: restSaksi.updateOn ? dayjs(restSaksi.updateOn) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestSaksi>): HttpResponse<ISaksi> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestSaksi[]>): HttpResponse<ISaksi[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
