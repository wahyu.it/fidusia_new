import dayjs from 'dayjs/esm';
import { INotaris } from 'app/entities/notaris/notaris.model';

export interface ISaksi {
  id: number;
  titel?: string | null;
  nama?: string | null;
  komparisi?: string | null;
  tts?: string | null;
  recordStatus?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  notaris?: INotaris | null;
}

export type NewSaksi = Omit<ISaksi, 'id'> & { id: null };
