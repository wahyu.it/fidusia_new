import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISaksi } from '../saksi.model';
import { SaksiService } from '../service/saksi.service';

export const saksiResolve = (route: ActivatedRouteSnapshot): Observable<null | ISaksi> => {
  const id = route.params['id'];
  if (id) {
    return inject(SaksiService)
      .find(id)
      .pipe(
        mergeMap((saksi: HttpResponse<ISaksi>) => {
          if (saksi.body) {
            return of(saksi.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default saksiResolve;
