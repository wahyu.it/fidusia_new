import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ISaksi, NewSaksi } from '../saksi.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISaksi for edit and NewSaksiFormGroupInput for create.
 */
type SaksiFormGroupInput = ISaksi | PartialWithRequiredKeyOf<NewSaksi>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ISaksi | NewSaksi> = Omit<T, 'updateOn'> & {
  updateOn?: string | null;
};

type SaksiFormRawValue = FormValueOf<ISaksi>;

type NewSaksiFormRawValue = FormValueOf<NewSaksi>;

type SaksiFormDefaults = Pick<NewSaksi, 'id' | 'updateOn'>;

type SaksiFormGroupContent = {
  id: FormControl<SaksiFormRawValue['id'] | NewSaksi['id']>;
  titel: FormControl<SaksiFormRawValue['titel']>;
  nama: FormControl<SaksiFormRawValue['nama']>;
  komparisi: FormControl<SaksiFormRawValue['komparisi']>;
  tts: FormControl<SaksiFormRawValue['tts']>;
  recordStatus: FormControl<SaksiFormRawValue['recordStatus']>;
  updateBy: FormControl<SaksiFormRawValue['updateBy']>;
  updateOn: FormControl<SaksiFormRawValue['updateOn']>;
  notaris: FormControl<SaksiFormRawValue['notaris']>;
};

export type SaksiFormGroup = FormGroup<SaksiFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SaksiFormService {
  createSaksiFormGroup(saksi: SaksiFormGroupInput = { id: null }): SaksiFormGroup {
    const saksiRawValue = this.convertSaksiToSaksiRawValue({
      ...this.getFormDefaults(),
      ...saksi,
    });
    return new FormGroup<SaksiFormGroupContent>({
      id: new FormControl(
        { value: saksiRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      titel: new FormControl(saksiRawValue.titel),
      nama: new FormControl(saksiRawValue.nama),
      komparisi: new FormControl(saksiRawValue.komparisi),
      tts: new FormControl(saksiRawValue.tts),
      recordStatus: new FormControl(saksiRawValue.recordStatus),
      updateBy: new FormControl(saksiRawValue.updateBy),
      updateOn: new FormControl(saksiRawValue.updateOn),
      notaris: new FormControl(saksiRawValue.notaris),
    });
  }

  getSaksi(form: SaksiFormGroup): ISaksi | NewSaksi {
    return this.convertSaksiRawValueToSaksi(form.getRawValue() as SaksiFormRawValue | NewSaksiFormRawValue);
  }

  resetForm(form: SaksiFormGroup, saksi: SaksiFormGroupInput): void {
    const saksiRawValue = this.convertSaksiToSaksiRawValue({ ...this.getFormDefaults(), ...saksi });
    form.reset(
      {
        ...saksiRawValue,
        id: { value: saksiRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): SaksiFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      updateOn: currentTime,
    };
  }

  private convertSaksiRawValueToSaksi(rawSaksi: SaksiFormRawValue | NewSaksiFormRawValue): ISaksi | NewSaksi {
    return {
      ...rawSaksi,
      updateOn: dayjs(rawSaksi.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertSaksiToSaksiRawValue(
    saksi: ISaksi | (Partial<NewSaksi> & SaksiFormDefaults),
  ): SaksiFormRawValue | PartialWithRequiredKeyOf<NewSaksiFormRawValue> {
    return {
      ...saksi,
      updateOn: saksi.updateOn ? saksi.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
