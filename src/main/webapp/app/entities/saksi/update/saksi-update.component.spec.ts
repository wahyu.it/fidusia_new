import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { SaksiService } from '../service/saksi.service';
import { ISaksi } from '../saksi.model';
import { SaksiFormService } from './saksi-form.service';

import { SaksiUpdateComponent } from './saksi-update.component';

describe('Saksi Management Update Component', () => {
  let comp: SaksiUpdateComponent;
  let fixture: ComponentFixture<SaksiUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let saksiFormService: SaksiFormService;
  let saksiService: SaksiService;
  let notarisService: NotarisService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), SaksiUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SaksiUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SaksiUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    saksiFormService = TestBed.inject(SaksiFormService);
    saksiService = TestBed.inject(SaksiService);
    notarisService = TestBed.inject(NotarisService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Notaris query and add missing value', () => {
      const saksi: ISaksi = { id: 456 };
      const notaris: INotaris = { id: 18270 };
      saksi.notaris = notaris;

      const notarisCollection: INotaris[] = [{ id: 10931 }];
      jest.spyOn(notarisService, 'query').mockReturnValue(of(new HttpResponse({ body: notarisCollection })));
      const additionalNotarises = [notaris];
      const expectedCollection: INotaris[] = [...additionalNotarises, ...notarisCollection];
      jest.spyOn(notarisService, 'addNotarisToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ saksi });
      comp.ngOnInit();

      expect(notarisService.query).toHaveBeenCalled();
      expect(notarisService.addNotarisToCollectionIfMissing).toHaveBeenCalledWith(
        notarisCollection,
        ...additionalNotarises.map(expect.objectContaining),
      );
      expect(comp.notarisesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const saksi: ISaksi = { id: 456 };
      const notaris: INotaris = { id: 13922 };
      saksi.notaris = notaris;

      activatedRoute.data = of({ saksi });
      comp.ngOnInit();

      expect(comp.notarisesSharedCollection).toContain(notaris);
      expect(comp.saksi).toEqual(saksi);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISaksi>>();
      const saksi = { id: 123 };
      jest.spyOn(saksiFormService, 'getSaksi').mockReturnValue(saksi);
      jest.spyOn(saksiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ saksi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: saksi }));
      saveSubject.complete();

      // THEN
      expect(saksiFormService.getSaksi).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(saksiService.update).toHaveBeenCalledWith(expect.objectContaining(saksi));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISaksi>>();
      const saksi = { id: 123 };
      jest.spyOn(saksiFormService, 'getSaksi').mockReturnValue({ id: null });
      jest.spyOn(saksiService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ saksi: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: saksi }));
      saveSubject.complete();

      // THEN
      expect(saksiFormService.getSaksi).toHaveBeenCalled();
      expect(saksiService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISaksi>>();
      const saksi = { id: 123 };
      jest.spyOn(saksiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ saksi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(saksiService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareNotaris', () => {
      it('Should forward to notarisService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(notarisService, 'compareNotaris');
        comp.compareNotaris(entity, entity2);
        expect(notarisService.compareNotaris).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
