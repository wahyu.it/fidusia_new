import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../saksi.test-samples';

import { SaksiFormService } from './saksi-form.service';

describe('Saksi Form Service', () => {
  let service: SaksiFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaksiFormService);
  });

  describe('Service methods', () => {
    describe('createSaksiFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createSaksiFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            titel: expect.any(Object),
            nama: expect.any(Object),
            komparisi: expect.any(Object),
            tts: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            notaris: expect.any(Object),
          }),
        );
      });

      it('passing ISaksi should create a new form with FormGroup', () => {
        const formGroup = service.createSaksiFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            titel: expect.any(Object),
            nama: expect.any(Object),
            komparisi: expect.any(Object),
            tts: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            notaris: expect.any(Object),
          }),
        );
      });
    });

    describe('getSaksi', () => {
      it('should return NewSaksi for default Saksi initial value', () => {
        const formGroup = service.createSaksiFormGroup(sampleWithNewData);

        const saksi = service.getSaksi(formGroup) as any;

        expect(saksi).toMatchObject(sampleWithNewData);
      });

      it('should return NewSaksi for empty Saksi initial value', () => {
        const formGroup = service.createSaksiFormGroup();

        const saksi = service.getSaksi(formGroup) as any;

        expect(saksi).toMatchObject({});
      });

      it('should return ISaksi', () => {
        const formGroup = service.createSaksiFormGroup(sampleWithRequiredData);

        const saksi = service.getSaksi(formGroup) as any;

        expect(saksi).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ISaksi should not enable id FormControl', () => {
        const formGroup = service.createSaksiFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewSaksi should disable id FormControl', () => {
        const formGroup = service.createSaksiFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
