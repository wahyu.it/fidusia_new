import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { ISaksi } from '../saksi.model';
import { SaksiService } from '../service/saksi.service';
import { SaksiFormService, SaksiFormGroup } from './saksi-form.service';

@Component({
  standalone: true,
  selector: 'jhi-saksi-update',
  templateUrl: './saksi-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class SaksiUpdateComponent implements OnInit {
  isSaving = false;
  saksi: ISaksi | null = null;

  notarisesSharedCollection: INotaris[] = [];

  editForm: SaksiFormGroup = this.saksiFormService.createSaksiFormGroup();

  constructor(
    protected saksiService: SaksiService,
    protected saksiFormService: SaksiFormService,
    protected notarisService: NotarisService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareNotaris = (o1: INotaris | null, o2: INotaris | null): boolean => this.notarisService.compareNotaris(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ saksi }) => {
      this.saksi = saksi;
      if (saksi) {
        this.updateForm(saksi);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const saksi = this.saksiFormService.getSaksi(this.editForm);
    if (saksi.id !== null) {
      this.subscribeToSaveResponse(this.saksiService.update(saksi));
    } else {
      this.subscribeToSaveResponse(this.saksiService.create(saksi));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISaksi>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(saksi: ISaksi): void {
    this.saksi = saksi;
    this.saksiFormService.resetForm(this.editForm, saksi);

    this.notarisesSharedCollection = this.notarisService.addNotarisToCollectionIfMissing<INotaris>(
      this.notarisesSharedCollection,
      saksi.notaris,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.notarisService
      .query()
      .pipe(map((res: HttpResponse<INotaris[]>) => res.body ?? []))
      .pipe(map((notarises: INotaris[]) => this.notarisService.addNotarisToCollectionIfMissing<INotaris>(notarises, this.saksi?.notaris)))
      .subscribe((notarises: INotaris[]) => (this.notarisesSharedCollection = notarises));
  }
}
