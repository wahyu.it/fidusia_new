import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { SaksiDetailComponent } from './saksi-detail.component';

describe('Saksi Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SaksiDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: SaksiDetailComponent,
              resolve: { saksi: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(SaksiDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load saksi on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', SaksiDetailComponent);

      // THEN
      expect(instance.saksi).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
