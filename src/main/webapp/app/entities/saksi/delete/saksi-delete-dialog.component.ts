import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { ISaksi } from '../saksi.model';
import { SaksiService } from '../service/saksi.service';

@Component({
  standalone: true,
  templateUrl: './saksi-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class SaksiDeleteDialogComponent {
  saksi?: ISaksi;

  constructor(
    protected saksiService: SaksiService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.saksiService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
