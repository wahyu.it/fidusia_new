import dayjs from 'dayjs/esm';

import { ISaksi, NewSaksi } from './saksi.model';

export const sampleWithRequiredData: ISaksi = {
  id: 23362,
};

export const sampleWithPartialData: ISaksi = {
  id: 24474,
  titel: 'ew knowingly',
  nama: 'psst remould',
  komparisi: 'disloyal',
  tts: 'but shroom valiantly',
  recordStatus: 23364,
  updateBy: 'doughnut',
  updateOn: dayjs('2023-12-14T22:04'),
};

export const sampleWithFullData: ISaksi = {
  id: 31779,
  titel: 'zowie',
  nama: 'patiently vice excitable',
  komparisi: 'than hence positively',
  tts: 'certainly',
  recordStatus: 8412,
  updateBy: 'gadzooks',
  updateOn: dayjs('2023-12-14T11:31'),
};

export const sampleWithNewData: NewSaksi = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
