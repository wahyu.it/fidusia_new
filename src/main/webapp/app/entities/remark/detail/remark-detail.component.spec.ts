import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { RemarkDetailComponent } from './remark-detail.component';

describe('Remark Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RemarkDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: RemarkDetailComponent,
              resolve: { remark: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(RemarkDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load remark on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', RemarkDetailComponent);

      // THEN
      expect(instance.remark).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
