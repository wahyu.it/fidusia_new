import { IRemark, NewRemark } from './remark.model';

export const sampleWithRequiredData: IRemark = {
  id: 3065,
};

export const sampleWithPartialData: IRemark = {
  id: 14026,
  category: 'thankfully that ringed',
  name: 'gee the delightfully',
};

export const sampleWithFullData: IRemark = {
  id: 9392,
  category: 'civilian as',
  name: 'boohoo',
  description: 'whenever usefully grim',
  berkasRevisi: 'pollinate',
};

export const sampleWithNewData: NewRemark = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
