export interface IRemark {
  id: number;
  category?: string | null;
  name?: string | null;
  description?: string | null;
  berkasRevisi?: string | null;
}

export type NewRemark = Omit<IRemark, 'id'> & { id: null };
