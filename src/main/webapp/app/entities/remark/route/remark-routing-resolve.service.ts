import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRemark } from '../remark.model';
import { RemarkService } from '../service/remark.service';

export const remarkResolve = (route: ActivatedRouteSnapshot): Observable<null | IRemark> => {
  const id = route.params['id'];
  if (id) {
    return inject(RemarkService)
      .find(id)
      .pipe(
        mergeMap((remark: HttpResponse<IRemark>) => {
          if (remark.body) {
            return of(remark.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default remarkResolve;
