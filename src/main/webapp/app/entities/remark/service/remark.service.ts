import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IRemark, NewRemark } from '../remark.model';

export type PartialUpdateRemark = Partial<IRemark> & Pick<IRemark, 'id'>;

export type EntityResponseType = HttpResponse<IRemark>;
export type EntityArrayResponseType = HttpResponse<IRemark[]>;

@Injectable({ providedIn: 'root' })
export class RemarkService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/remarks');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(remark: NewRemark): Observable<EntityResponseType> {
    return this.http.post<IRemark>(this.resourceUrl, remark, { observe: 'response' });
  }

  update(remark: IRemark): Observable<EntityResponseType> {
    return this.http.put<IRemark>(`${this.resourceUrl}/${this.getRemarkIdentifier(remark)}`, remark, { observe: 'response' });
  }

  partialUpdate(remark: PartialUpdateRemark): Observable<EntityResponseType> {
    return this.http.patch<IRemark>(`${this.resourceUrl}/${this.getRemarkIdentifier(remark)}`, remark, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRemark>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRemark[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getRemarkIdentifier(remark: Pick<IRemark, 'id'>): number {
    return remark.id;
  }

  compareRemark(o1: Pick<IRemark, 'id'> | null, o2: Pick<IRemark, 'id'> | null): boolean {
    return o1 && o2 ? this.getRemarkIdentifier(o1) === this.getRemarkIdentifier(o2) : o1 === o2;
  }

  addRemarkToCollectionIfMissing<Type extends Pick<IRemark, 'id'>>(
    remarkCollection: Type[],
    ...remarksToCheck: (Type | null | undefined)[]
  ): Type[] {
    const remarks: Type[] = remarksToCheck.filter(isPresent);
    if (remarks.length > 0) {
      const remarkCollectionIdentifiers = remarkCollection.map(remarkItem => this.getRemarkIdentifier(remarkItem)!);
      const remarksToAdd = remarks.filter(remarkItem => {
        const remarkIdentifier = this.getRemarkIdentifier(remarkItem);
        if (remarkCollectionIdentifiers.includes(remarkIdentifier)) {
          return false;
        }
        remarkCollectionIdentifiers.push(remarkIdentifier);
        return true;
      });
      return [...remarksToAdd, ...remarkCollection];
    }
    return remarkCollection;
  }
}
