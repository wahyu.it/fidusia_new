import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { RemarkComponent } from './list/remark.component';
import { RemarkDetailComponent } from './detail/remark-detail.component';
import { RemarkUpdateComponent } from './update/remark-update.component';
import RemarkResolve from './route/remark-routing-resolve.service';

const remarkRoute: Routes = [
  {
    path: '',
    component: RemarkComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RemarkDetailComponent,
    resolve: {
      remark: RemarkResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RemarkUpdateComponent,
    resolve: {
      remark: RemarkResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RemarkUpdateComponent,
    resolve: {
      remark: RemarkResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default remarkRoute;
