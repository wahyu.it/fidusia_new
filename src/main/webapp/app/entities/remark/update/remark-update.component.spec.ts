import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { RemarkService } from '../service/remark.service';
import { IRemark } from '../remark.model';
import { RemarkFormService } from './remark-form.service';

import { RemarkUpdateComponent } from './remark-update.component';

describe('Remark Management Update Component', () => {
  let comp: RemarkUpdateComponent;
  let fixture: ComponentFixture<RemarkUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let remarkFormService: RemarkFormService;
  let remarkService: RemarkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), RemarkUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(RemarkUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RemarkUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    remarkFormService = TestBed.inject(RemarkFormService);
    remarkService = TestBed.inject(RemarkService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const remark: IRemark = { id: 456 };

      activatedRoute.data = of({ remark });
      comp.ngOnInit();

      expect(comp.remark).toEqual(remark);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IRemark>>();
      const remark = { id: 123 };
      jest.spyOn(remarkFormService, 'getRemark').mockReturnValue(remark);
      jest.spyOn(remarkService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ remark });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: remark }));
      saveSubject.complete();

      // THEN
      expect(remarkFormService.getRemark).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(remarkService.update).toHaveBeenCalledWith(expect.objectContaining(remark));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IRemark>>();
      const remark = { id: 123 };
      jest.spyOn(remarkFormService, 'getRemark').mockReturnValue({ id: null });
      jest.spyOn(remarkService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ remark: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: remark }));
      saveSubject.complete();

      // THEN
      expect(remarkFormService.getRemark).toHaveBeenCalled();
      expect(remarkService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IRemark>>();
      const remark = { id: 123 };
      jest.spyOn(remarkService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ remark });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(remarkService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
