import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IRemark } from '../remark.model';
import { RemarkService } from '../service/remark.service';
import { RemarkFormService, RemarkFormGroup } from './remark-form.service';

@Component({
  standalone: true,
  selector: 'jhi-remark-update',
  templateUrl: './remark-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class RemarkUpdateComponent implements OnInit {
  isSaving = false;
  remark: IRemark | null = null;

  editForm: RemarkFormGroup = this.remarkFormService.createRemarkFormGroup();

  constructor(
    protected remarkService: RemarkService,
    protected remarkFormService: RemarkFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ remark }) => {
      this.remark = remark;
      if (remark) {
        this.updateForm(remark);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const remark = this.remarkFormService.getRemark(this.editForm);
    if (remark.id !== null) {
      this.subscribeToSaveResponse(this.remarkService.update(remark));
    } else {
      this.subscribeToSaveResponse(this.remarkService.create(remark));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRemark>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(remark: IRemark): void {
    this.remark = remark;
    this.remarkFormService.resetForm(this.editForm, remark);
  }
}
