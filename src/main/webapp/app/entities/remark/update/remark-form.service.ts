import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IRemark, NewRemark } from '../remark.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IRemark for edit and NewRemarkFormGroupInput for create.
 */
type RemarkFormGroupInput = IRemark | PartialWithRequiredKeyOf<NewRemark>;

type RemarkFormDefaults = Pick<NewRemark, 'id'>;

type RemarkFormGroupContent = {
  id: FormControl<IRemark['id'] | NewRemark['id']>;
  category: FormControl<IRemark['category']>;
  name: FormControl<IRemark['name']>;
  description: FormControl<IRemark['description']>;
  berkasRevisi: FormControl<IRemark['berkasRevisi']>;
};

export type RemarkFormGroup = FormGroup<RemarkFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class RemarkFormService {
  createRemarkFormGroup(remark: RemarkFormGroupInput = { id: null }): RemarkFormGroup {
    const remarkRawValue = {
      ...this.getFormDefaults(),
      ...remark,
    };
    return new FormGroup<RemarkFormGroupContent>({
      id: new FormControl(
        { value: remarkRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      category: new FormControl(remarkRawValue.category),
      name: new FormControl(remarkRawValue.name),
      description: new FormControl(remarkRawValue.description),
      berkasRevisi: new FormControl(remarkRawValue.berkasRevisi),
    });
  }

  getRemark(form: RemarkFormGroup): IRemark | NewRemark {
    return form.getRawValue() as IRemark | NewRemark;
  }

  resetForm(form: RemarkFormGroup, remark: RemarkFormGroupInput): void {
    const remarkRawValue = { ...this.getFormDefaults(), ...remark };
    form.reset(
      {
        ...remarkRawValue,
        id: { value: remarkRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): RemarkFormDefaults {
    return {
      id: null,
    };
  }
}
