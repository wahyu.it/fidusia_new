import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IRemark } from '../remark.model';
import { RemarkService } from '../service/remark.service';

@Component({
  standalone: true,
  templateUrl: './remark-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class RemarkDeleteDialogComponent {
  remark?: IRemark;

  constructor(
    protected remarkService: RemarkService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.remarkService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
