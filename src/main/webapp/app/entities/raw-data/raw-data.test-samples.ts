import dayjs from 'dayjs/esm';

import { IRawData, NewRawData } from './raw-data.model';

export const sampleWithRequiredData: IRawData = {
  id: 19675,
};

export const sampleWithPartialData: IRawData = {
  id: 17434,
  uniqueKey: 'edible pish',
  fileName: 'instead hark',
  delimeter: 'blame lord yahoo',
  status: 2594,
};

export const sampleWithFullData: IRawData = {
  id: 26483,
  uniqueKey: 'pricey because',
  orderDate: dayjs('2023-12-14'),
  fileName: 'cassava athwart',
  line: 'regarding inexperienced piccolo',
  delimeter: 'whereas what hard-to-find',
  lineNumber: 4401,
  valid: false,
  invalidReason: 'than',
  status: 21337,
};

export const sampleWithNewData: NewRawData = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
