import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRawData } from '../raw-data.model';
import { RawDataService } from '../service/raw-data.service';

export const rawDataResolve = (route: ActivatedRouteSnapshot): Observable<null | IRawData> => {
  const id = route.params['id'];
  if (id) {
    return inject(RawDataService)
      .find(id)
      .pipe(
        mergeMap((rawData: HttpResponse<IRawData>) => {
          if (rawData.body) {
            return of(rawData.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default rawDataResolve;
