import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { RawDataComponent } from './list/raw-data.component';
import { RawDataDetailComponent } from './detail/raw-data-detail.component';
import { RawDataUpdateComponent } from './update/raw-data-update.component';
import RawDataResolve from './route/raw-data-routing-resolve.service';

const rawDataRoute: Routes = [
  {
    path: '',
    component: RawDataComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RawDataDetailComponent,
    resolve: {
      rawData: RawDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RawDataUpdateComponent,
    resolve: {
      rawData: RawDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RawDataUpdateComponent,
    resolve: {
      rawData: RawDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default rawDataRoute;
