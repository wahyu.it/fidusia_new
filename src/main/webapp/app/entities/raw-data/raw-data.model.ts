import dayjs from 'dayjs/esm';

export interface IRawData {
  id: number;
  uniqueKey?: string | null;
  orderDate?: dayjs.Dayjs | null;
  fileName?: string | null;
  line?: string | null;
  delimeter?: string | null;
  lineNumber?: number | null;
  valid?: boolean | null;
  invalidReason?: string | null;
  status?: number | null;
}

export type NewRawData = Omit<IRawData, 'id'> & { id: null };
