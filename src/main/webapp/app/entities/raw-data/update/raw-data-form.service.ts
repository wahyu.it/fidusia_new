import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IRawData, NewRawData } from '../raw-data.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IRawData for edit and NewRawDataFormGroupInput for create.
 */
type RawDataFormGroupInput = IRawData | PartialWithRequiredKeyOf<NewRawData>;

type RawDataFormDefaults = Pick<NewRawData, 'id' | 'valid'>;

type RawDataFormGroupContent = {
  id: FormControl<IRawData['id'] | NewRawData['id']>;
  uniqueKey: FormControl<IRawData['uniqueKey']>;
  orderDate: FormControl<IRawData['orderDate']>;
  fileName: FormControl<IRawData['fileName']>;
  line: FormControl<IRawData['line']>;
  delimeter: FormControl<IRawData['delimeter']>;
  lineNumber: FormControl<IRawData['lineNumber']>;
  valid: FormControl<IRawData['valid']>;
  invalidReason: FormControl<IRawData['invalidReason']>;
  status: FormControl<IRawData['status']>;
};

export type RawDataFormGroup = FormGroup<RawDataFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class RawDataFormService {
  createRawDataFormGroup(rawData: RawDataFormGroupInput = { id: null }): RawDataFormGroup {
    const rawDataRawValue = {
      ...this.getFormDefaults(),
      ...rawData,
    };
    return new FormGroup<RawDataFormGroupContent>({
      id: new FormControl(
        { value: rawDataRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      uniqueKey: new FormControl(rawDataRawValue.uniqueKey),
      orderDate: new FormControl(rawDataRawValue.orderDate),
      fileName: new FormControl(rawDataRawValue.fileName),
      line: new FormControl(rawDataRawValue.line),
      delimeter: new FormControl(rawDataRawValue.delimeter),
      lineNumber: new FormControl(rawDataRawValue.lineNumber),
      valid: new FormControl(rawDataRawValue.valid),
      invalidReason: new FormControl(rawDataRawValue.invalidReason),
      status: new FormControl(rawDataRawValue.status),
    });
  }

  getRawData(form: RawDataFormGroup): IRawData | NewRawData {
    return form.getRawValue() as IRawData | NewRawData;
  }

  resetForm(form: RawDataFormGroup, rawData: RawDataFormGroupInput): void {
    const rawDataRawValue = { ...this.getFormDefaults(), ...rawData };
    form.reset(
      {
        ...rawDataRawValue,
        id: { value: rawDataRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): RawDataFormDefaults {
    return {
      id: null,
      valid: false,
    };
  }
}
