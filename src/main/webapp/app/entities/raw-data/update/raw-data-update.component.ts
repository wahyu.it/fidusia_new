import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IRawData } from '../raw-data.model';
import { RawDataService } from '../service/raw-data.service';
import { RawDataFormService, RawDataFormGroup } from './raw-data-form.service';

@Component({
  standalone: true,
  selector: 'jhi-raw-data-update',
  templateUrl: './raw-data-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class RawDataUpdateComponent implements OnInit {
  isSaving = false;
  rawData: IRawData | null = null;

  editForm: RawDataFormGroup = this.rawDataFormService.createRawDataFormGroup();

  constructor(
    protected rawDataService: RawDataService,
    protected rawDataFormService: RawDataFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ rawData }) => {
      this.rawData = rawData;
      if (rawData) {
        this.updateForm(rawData);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const rawData = this.rawDataFormService.getRawData(this.editForm);
    if (rawData.id !== null) {
      this.subscribeToSaveResponse(this.rawDataService.update(rawData));
    } else {
      this.subscribeToSaveResponse(this.rawDataService.create(rawData));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRawData>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(rawData: IRawData): void {
    this.rawData = rawData;
    this.rawDataFormService.resetForm(this.editForm, rawData);
  }
}
