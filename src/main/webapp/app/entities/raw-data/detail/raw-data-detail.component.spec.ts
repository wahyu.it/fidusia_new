import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { RawDataDetailComponent } from './raw-data-detail.component';

describe('RawData Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RawDataDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: RawDataDetailComponent,
              resolve: { rawData: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(RawDataDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load rawData on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', RawDataDetailComponent);

      // THEN
      expect(instance.rawData).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
