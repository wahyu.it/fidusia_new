import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { ILeasing } from '../leasing.model';
import { LeasingService } from '../service/leasing.service';

@Component({
  standalone: true,
  templateUrl: './leasing-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class LeasingDeleteDialogComponent {
  leasing?: ILeasing;

  constructor(
    protected leasingService: LeasingService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.leasingService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
