import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ILeasing } from '../leasing.model';
import { LeasingService } from '../service/leasing.service';
import { LeasingFormService, LeasingFormGroup } from './leasing-form.service';

@Component({
  standalone: true,
  selector: 'jhi-leasing-update',
  templateUrl: './leasing-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class LeasingUpdateComponent implements OnInit {
  isSaving = false;
  leasing: ILeasing | null = null;

  editForm: LeasingFormGroup = this.leasingFormService.createLeasingFormGroup();

  constructor(
    protected leasingService: LeasingService,
    protected leasingFormService: LeasingFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ leasing }) => {
      this.leasing = leasing;
      if (leasing) {
        this.updateForm(leasing);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const leasing = this.leasingFormService.getLeasing(this.editForm);
    if (leasing.id !== null) {
      this.subscribeToSaveResponse(this.leasingService.update(leasing));
    } else {
      this.subscribeToSaveResponse(this.leasingService.create(leasing));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILeasing>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(leasing: ILeasing): void {
    this.leasing = leasing;
    this.leasingFormService.resetForm(this.editForm, leasing);
  }
}
