import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { LeasingService } from '../service/leasing.service';
import { ILeasing } from '../leasing.model';
import { LeasingFormService } from './leasing-form.service';

import { LeasingUpdateComponent } from './leasing-update.component';

describe('Leasing Management Update Component', () => {
  let comp: LeasingUpdateComponent;
  let fixture: ComponentFixture<LeasingUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let leasingFormService: LeasingFormService;
  let leasingService: LeasingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), LeasingUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(LeasingUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(LeasingUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    leasingFormService = TestBed.inject(LeasingFormService);
    leasingService = TestBed.inject(LeasingService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const leasing: ILeasing = { id: 456 };

      activatedRoute.data = of({ leasing });
      comp.ngOnInit();

      expect(comp.leasing).toEqual(leasing);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ILeasing>>();
      const leasing = { id: 123 };
      jest.spyOn(leasingFormService, 'getLeasing').mockReturnValue(leasing);
      jest.spyOn(leasingService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ leasing });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: leasing }));
      saveSubject.complete();

      // THEN
      expect(leasingFormService.getLeasing).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(leasingService.update).toHaveBeenCalledWith(expect.objectContaining(leasing));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ILeasing>>();
      const leasing = { id: 123 };
      jest.spyOn(leasingFormService, 'getLeasing').mockReturnValue({ id: null });
      jest.spyOn(leasingService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ leasing: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: leasing }));
      saveSubject.complete();

      // THEN
      expect(leasingFormService.getLeasing).toHaveBeenCalled();
      expect(leasingService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ILeasing>>();
      const leasing = { id: 123 };
      jest.spyOn(leasingService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ leasing });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(leasingService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
