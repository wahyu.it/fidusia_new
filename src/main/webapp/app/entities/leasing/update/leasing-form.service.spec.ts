import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../leasing.test-samples';

import { LeasingFormService } from './leasing-form.service';

describe('Leasing Form Service', () => {
  let service: LeasingFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeasingFormService);
  });

  describe('Service methods', () => {
    describe('createLeasingFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createLeasingFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kode: expect.any(Object),
            nama: expect.any(Object),
            alamat: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            kecamatan: expect.any(Object),
            kelurahan: expect.any(Object),
            rt: expect.any(Object),
            rw: expect.any(Object),
            npwp: expect.any(Object),
            kodePose: expect.any(Object),
            noKontak: expect.any(Object),
            techNoKontak: expect.any(Object),
            techEmail: expect.any(Object),
            acctNoKontak: expect.any(Object),
            acctEmail: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOne: expect.any(Object),
            email: expect.any(Object),
            sk: expect.any(Object),
          }),
        );
      });

      it('passing ILeasing should create a new form with FormGroup', () => {
        const formGroup = service.createLeasingFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kode: expect.any(Object),
            nama: expect.any(Object),
            alamat: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            kecamatan: expect.any(Object),
            kelurahan: expect.any(Object),
            rt: expect.any(Object),
            rw: expect.any(Object),
            npwp: expect.any(Object),
            kodePose: expect.any(Object),
            noKontak: expect.any(Object),
            techNoKontak: expect.any(Object),
            techEmail: expect.any(Object),
            acctNoKontak: expect.any(Object),
            acctEmail: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOne: expect.any(Object),
            email: expect.any(Object),
            sk: expect.any(Object),
          }),
        );
      });
    });

    describe('getLeasing', () => {
      it('should return NewLeasing for default Leasing initial value', () => {
        const formGroup = service.createLeasingFormGroup(sampleWithNewData);

        const leasing = service.getLeasing(formGroup) as any;

        expect(leasing).toMatchObject(sampleWithNewData);
      });

      it('should return NewLeasing for empty Leasing initial value', () => {
        const formGroup = service.createLeasingFormGroup();

        const leasing = service.getLeasing(formGroup) as any;

        expect(leasing).toMatchObject({});
      });

      it('should return ILeasing', () => {
        const formGroup = service.createLeasingFormGroup(sampleWithRequiredData);

        const leasing = service.getLeasing(formGroup) as any;

        expect(leasing).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ILeasing should not enable id FormControl', () => {
        const formGroup = service.createLeasingFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewLeasing should disable id FormControl', () => {
        const formGroup = service.createLeasingFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
