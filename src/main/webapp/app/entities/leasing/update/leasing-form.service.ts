import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ILeasing, NewLeasing } from '../leasing.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ILeasing for edit and NewLeasingFormGroupInput for create.
 */
type LeasingFormGroupInput = ILeasing | PartialWithRequiredKeyOf<NewLeasing>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ILeasing | NewLeasing> = Omit<T, 'updateOne'> & {
  updateOne?: string | null;
};

type LeasingFormRawValue = FormValueOf<ILeasing>;

type NewLeasingFormRawValue = FormValueOf<NewLeasing>;

type LeasingFormDefaults = Pick<NewLeasing, 'id' | 'updateOne'>;

type LeasingFormGroupContent = {
  id: FormControl<LeasingFormRawValue['id'] | NewLeasing['id']>;
  kode: FormControl<LeasingFormRawValue['kode']>;
  nama: FormControl<LeasingFormRawValue['nama']>;
  alamat: FormControl<LeasingFormRawValue['alamat']>;
  kota: FormControl<LeasingFormRawValue['kota']>;
  provinsi: FormControl<LeasingFormRawValue['provinsi']>;
  kecamatan: FormControl<LeasingFormRawValue['kecamatan']>;
  kelurahan: FormControl<LeasingFormRawValue['kelurahan']>;
  rt: FormControl<LeasingFormRawValue['rt']>;
  rw: FormControl<LeasingFormRawValue['rw']>;
  npwp: FormControl<LeasingFormRawValue['npwp']>;
  kodePose: FormControl<LeasingFormRawValue['kodePose']>;
  noKontak: FormControl<LeasingFormRawValue['noKontak']>;
  techNoKontak: FormControl<LeasingFormRawValue['techNoKontak']>;
  techEmail: FormControl<LeasingFormRawValue['techEmail']>;
  acctNoKontak: FormControl<LeasingFormRawValue['acctNoKontak']>;
  acctEmail: FormControl<LeasingFormRawValue['acctEmail']>;
  recordStatus: FormControl<LeasingFormRawValue['recordStatus']>;
  updateBy: FormControl<LeasingFormRawValue['updateBy']>;
  updateOne: FormControl<LeasingFormRawValue['updateOne']>;
  email: FormControl<LeasingFormRawValue['email']>;
  sk: FormControl<LeasingFormRawValue['sk']>;
};

export type LeasingFormGroup = FormGroup<LeasingFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class LeasingFormService {
  createLeasingFormGroup(leasing: LeasingFormGroupInput = { id: null }): LeasingFormGroup {
    const leasingRawValue = this.convertLeasingToLeasingRawValue({
      ...this.getFormDefaults(),
      ...leasing,
    });
    return new FormGroup<LeasingFormGroupContent>({
      id: new FormControl(
        { value: leasingRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      kode: new FormControl(leasingRawValue.kode),
      nama: new FormControl(leasingRawValue.nama),
      alamat: new FormControl(leasingRawValue.alamat),
      kota: new FormControl(leasingRawValue.kota),
      provinsi: new FormControl(leasingRawValue.provinsi),
      kecamatan: new FormControl(leasingRawValue.kecamatan),
      kelurahan: new FormControl(leasingRawValue.kelurahan),
      rt: new FormControl(leasingRawValue.rt),
      rw: new FormControl(leasingRawValue.rw),
      npwp: new FormControl(leasingRawValue.npwp),
      kodePose: new FormControl(leasingRawValue.kodePose),
      noKontak: new FormControl(leasingRawValue.noKontak),
      techNoKontak: new FormControl(leasingRawValue.techNoKontak),
      techEmail: new FormControl(leasingRawValue.techEmail),
      acctNoKontak: new FormControl(leasingRawValue.acctNoKontak),
      acctEmail: new FormControl(leasingRawValue.acctEmail),
      recordStatus: new FormControl(leasingRawValue.recordStatus),
      updateBy: new FormControl(leasingRawValue.updateBy),
      updateOne: new FormControl(leasingRawValue.updateOne),
      email: new FormControl(leasingRawValue.email),
      sk: new FormControl(leasingRawValue.sk),
    });
  }

  getLeasing(form: LeasingFormGroup): ILeasing | NewLeasing {
    return this.convertLeasingRawValueToLeasing(form.getRawValue() as LeasingFormRawValue | NewLeasingFormRawValue);
  }

  resetForm(form: LeasingFormGroup, leasing: LeasingFormGroupInput): void {
    const leasingRawValue = this.convertLeasingToLeasingRawValue({ ...this.getFormDefaults(), ...leasing });
    form.reset(
      {
        ...leasingRawValue,
        id: { value: leasingRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): LeasingFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      updateOne: currentTime,
    };
  }

  private convertLeasingRawValueToLeasing(rawLeasing: LeasingFormRawValue | NewLeasingFormRawValue): ILeasing | NewLeasing {
    return {
      ...rawLeasing,
      updateOne: dayjs(rawLeasing.updateOne, DATE_TIME_FORMAT),
    };
  }

  private convertLeasingToLeasingRawValue(
    leasing: ILeasing | (Partial<NewLeasing> & LeasingFormDefaults),
  ): LeasingFormRawValue | PartialWithRequiredKeyOf<NewLeasingFormRawValue> {
    return {
      ...leasing,
      updateOne: leasing.updateOne ? leasing.updateOne.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
