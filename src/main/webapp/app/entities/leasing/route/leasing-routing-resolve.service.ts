import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ILeasing } from '../leasing.model';
import { LeasingService } from '../service/leasing.service';

export const leasingResolve = (route: ActivatedRouteSnapshot): Observable<null | ILeasing> => {
  const id = route.params['id'];
  if (id) {
    return inject(LeasingService)
      .find(id)
      .pipe(
        mergeMap((leasing: HttpResponse<ILeasing>) => {
          if (leasing.body) {
            return of(leasing.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default leasingResolve;
