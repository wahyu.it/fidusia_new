import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { LeasingComponent } from './list/leasing.component';
import { LeasingDetailComponent } from './detail/leasing-detail.component';
import { LeasingUpdateComponent } from './update/leasing-update.component';
import LeasingResolve from './route/leasing-routing-resolve.service';

const leasingRoute: Routes = [
  {
    path: '',
    component: LeasingComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: LeasingDetailComponent,
    resolve: {
      leasing: LeasingResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: LeasingUpdateComponent,
    resolve: {
      leasing: LeasingResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: LeasingUpdateComponent,
    resolve: {
      leasing: LeasingResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default leasingRoute;
