import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ILeasing } from '../leasing.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../leasing.test-samples';

import { LeasingService, RestLeasing } from './leasing.service';

const requireRestSample: RestLeasing = {
  ...sampleWithRequiredData,
  updateOne: sampleWithRequiredData.updateOne?.toJSON(),
};

describe('Leasing Service', () => {
  let service: LeasingService;
  let httpMock: HttpTestingController;
  let expectedResult: ILeasing | ILeasing[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(LeasingService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Leasing', () => {
      const leasing = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(leasing).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Leasing', () => {
      const leasing = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(leasing).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Leasing', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Leasing', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Leasing', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addLeasingToCollectionIfMissing', () => {
      it('should add a Leasing to an empty array', () => {
        const leasing: ILeasing = sampleWithRequiredData;
        expectedResult = service.addLeasingToCollectionIfMissing([], leasing);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(leasing);
      });

      it('should not add a Leasing to an array that contains it', () => {
        const leasing: ILeasing = sampleWithRequiredData;
        const leasingCollection: ILeasing[] = [
          {
            ...leasing,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addLeasingToCollectionIfMissing(leasingCollection, leasing);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Leasing to an array that doesn't contain it", () => {
        const leasing: ILeasing = sampleWithRequiredData;
        const leasingCollection: ILeasing[] = [sampleWithPartialData];
        expectedResult = service.addLeasingToCollectionIfMissing(leasingCollection, leasing);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(leasing);
      });

      it('should add only unique Leasing to an array', () => {
        const leasingArray: ILeasing[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const leasingCollection: ILeasing[] = [sampleWithRequiredData];
        expectedResult = service.addLeasingToCollectionIfMissing(leasingCollection, ...leasingArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const leasing: ILeasing = sampleWithRequiredData;
        const leasing2: ILeasing = sampleWithPartialData;
        expectedResult = service.addLeasingToCollectionIfMissing([], leasing, leasing2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(leasing);
        expect(expectedResult).toContain(leasing2);
      });

      it('should accept null and undefined values', () => {
        const leasing: ILeasing = sampleWithRequiredData;
        expectedResult = service.addLeasingToCollectionIfMissing([], null, leasing, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(leasing);
      });

      it('should return initial array if no Leasing is added', () => {
        const leasingCollection: ILeasing[] = [sampleWithRequiredData];
        expectedResult = service.addLeasingToCollectionIfMissing(leasingCollection, undefined, null);
        expect(expectedResult).toEqual(leasingCollection);
      });
    });

    describe('compareLeasing', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareLeasing(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareLeasing(entity1, entity2);
        const compareResult2 = service.compareLeasing(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareLeasing(entity1, entity2);
        const compareResult2 = service.compareLeasing(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareLeasing(entity1, entity2);
        const compareResult2 = service.compareLeasing(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
