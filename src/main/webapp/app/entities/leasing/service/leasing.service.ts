import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ILeasing, NewLeasing } from '../leasing.model';

export type PartialUpdateLeasing = Partial<ILeasing> & Pick<ILeasing, 'id'>;

type RestOf<T extends ILeasing | NewLeasing> = Omit<T, 'updateOne'> & {
  updateOne?: string | null;
};

export type RestLeasing = RestOf<ILeasing>;

export type NewRestLeasing = RestOf<NewLeasing>;

export type PartialUpdateRestLeasing = RestOf<PartialUpdateLeasing>;

export type EntityResponseType = HttpResponse<ILeasing>;
export type EntityArrayResponseType = HttpResponse<ILeasing[]>;

@Injectable({ providedIn: 'root' })
export class LeasingService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/leasings');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(leasing: NewLeasing): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(leasing);
    return this.http
      .post<RestLeasing>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(leasing: ILeasing): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(leasing);
    return this.http
      .put<RestLeasing>(`${this.resourceUrl}/${this.getLeasingIdentifier(leasing)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(leasing: PartialUpdateLeasing): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(leasing);
    return this.http
      .patch<RestLeasing>(`${this.resourceUrl}/${this.getLeasingIdentifier(leasing)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestLeasing>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestLeasing[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getLeasingIdentifier(leasing: Pick<ILeasing, 'id'>): number {
    return leasing.id;
  }

  compareLeasing(o1: Pick<ILeasing, 'id'> | null, o2: Pick<ILeasing, 'id'> | null): boolean {
    return o1 && o2 ? this.getLeasingIdentifier(o1) === this.getLeasingIdentifier(o2) : o1 === o2;
  }

  addLeasingToCollectionIfMissing<Type extends Pick<ILeasing, 'id'>>(
    leasingCollection: Type[],
    ...leasingsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const leasings: Type[] = leasingsToCheck.filter(isPresent);
    if (leasings.length > 0) {
      const leasingCollectionIdentifiers = leasingCollection.map(leasingItem => this.getLeasingIdentifier(leasingItem)!);
      const leasingsToAdd = leasings.filter(leasingItem => {
        const leasingIdentifier = this.getLeasingIdentifier(leasingItem);
        if (leasingCollectionIdentifiers.includes(leasingIdentifier)) {
          return false;
        }
        leasingCollectionIdentifiers.push(leasingIdentifier);
        return true;
      });
      return [...leasingsToAdd, ...leasingCollection];
    }
    return leasingCollection;
  }

  protected convertDateFromClient<T extends ILeasing | NewLeasing | PartialUpdateLeasing>(leasing: T): RestOf<T> {
    return {
      ...leasing,
      updateOne: leasing.updateOne?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restLeasing: RestLeasing): ILeasing {
    return {
      ...restLeasing,
      updateOne: restLeasing.updateOne ? dayjs(restLeasing.updateOne) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestLeasing>): HttpResponse<ILeasing> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestLeasing[]>): HttpResponse<ILeasing[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
