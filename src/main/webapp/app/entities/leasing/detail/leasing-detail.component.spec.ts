import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { LeasingDetailComponent } from './leasing-detail.component';

describe('Leasing Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LeasingDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: LeasingDetailComponent,
              resolve: { leasing: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(LeasingDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load leasing on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', LeasingDetailComponent);

      // THEN
      expect(instance.leasing).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
