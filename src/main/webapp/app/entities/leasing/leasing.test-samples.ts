import dayjs from 'dayjs/esm';

import { ILeasing, NewLeasing } from './leasing.model';

export const sampleWithRequiredData: ILeasing = {
  id: 20760,
};

export const sampleWithPartialData: ILeasing = {
  id: 11378,
  nama: 'darling',
  alamat: 'taper creature',
  kecamatan: 'throughout palace',
  kelurahan: 'during among',
  rt: 'far cranberry and',
  rw: 'supposing wisely whoever',
  kodePose: 'mmm',
  noKontak: 'boohoo below',
  techNoKontak: 'by vaguely',
  techEmail: 'although unnecessarily descale',
  acctNoKontak: 'longitude finalise',
  acctEmail: 'longingly',
  recordStatus: 485,
  updateOne: dayjs('2023-12-14T07:05'),
};

export const sampleWithFullData: ILeasing = {
  id: 19584,
  kode: 'unloosen',
  nama: 'for boo',
  alamat: 'yuck failing',
  kota: 'rudely',
  provinsi: 'muddy',
  kecamatan: 'boastfully waterbed rigidly',
  kelurahan: 'dangerous',
  rt: 'along',
  rw: 'consequently huzzah',
  npwp: 'beyond rundown',
  kodePose: 'bunkhouse barber',
  noKontak: 'vice',
  techNoKontak: 'very',
  techEmail: 'loosen glistening wherever',
  acctNoKontak: 'ack area because',
  acctEmail: 'immediately shyly',
  recordStatus: 32407,
  updateBy: 'as until helplessly',
  updateOne: dayjs('2023-12-14T02:53'),
  email: 'Clint_Ortiz78@gmail.com',
  sk: 'towards',
};

export const sampleWithNewData: NewLeasing = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
