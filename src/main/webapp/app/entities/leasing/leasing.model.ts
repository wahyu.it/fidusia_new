import dayjs from 'dayjs/esm';

export interface ILeasing {
  id: number;
  kode?: string | null;
  nama?: string | null;
  alamat?: string | null;
  kota?: string | null;
  provinsi?: string | null;
  kecamatan?: string | null;
  kelurahan?: string | null;
  rt?: string | null;
  rw?: string | null;
  npwp?: string | null;
  kodePose?: string | null;
  noKontak?: string | null;
  techNoKontak?: string | null;
  techEmail?: string | null;
  acctNoKontak?: string | null;
  acctEmail?: string | null;
  recordStatus?: number | null;
  updateBy?: string | null;
  updateOne?: dayjs.Dayjs | null;
  email?: string | null;
  sk?: string | null;
}

export type NewLeasing = Omit<ILeasing, 'id'> & { id: null };
