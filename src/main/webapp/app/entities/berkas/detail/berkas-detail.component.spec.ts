import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { BerkasDetailComponent } from './berkas-detail.component';

describe('Berkas Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BerkasDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: BerkasDetailComponent,
              resolve: { berkas: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(BerkasDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load berkas on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', BerkasDetailComponent);

      // THEN
      expect(instance.berkas).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
