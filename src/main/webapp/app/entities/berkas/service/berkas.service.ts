import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBerkas, NewBerkas } from '../berkas.model';

export type PartialUpdateBerkas = Partial<IBerkas> & Pick<IBerkas, 'id'>;

type RestOf<T extends IBerkas | NewBerkas> = Omit<T, 'updateOn' | 'uploadOn' | 'tanggalPpk' | 'verifyOn'> & {
  updateOn?: string | null;
  uploadOn?: string | null;
  tanggalPpk?: string | null;
  verifyOn?: string | null;
};

export type RestBerkas = RestOf<IBerkas>;

export type NewRestBerkas = RestOf<NewBerkas>;

export type PartialUpdateRestBerkas = RestOf<PartialUpdateBerkas>;

export type EntityResponseType = HttpResponse<IBerkas>;
export type EntityArrayResponseType = HttpResponse<IBerkas[]>;

@Injectable({ providedIn: 'root' })
export class BerkasService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/berkas');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(berkas: NewBerkas): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(berkas);
    return this.http
      .post<RestBerkas>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(berkas: IBerkas): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(berkas);
    return this.http
      .put<RestBerkas>(`${this.resourceUrl}/${this.getBerkasIdentifier(berkas)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(berkas: PartialUpdateBerkas): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(berkas);
    return this.http
      .patch<RestBerkas>(`${this.resourceUrl}/${this.getBerkasIdentifier(berkas)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestBerkas>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestBerkas[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getBerkasIdentifier(berkas: Pick<IBerkas, 'id'>): number {
    return berkas.id;
  }

  compareBerkas(o1: Pick<IBerkas, 'id'> | null, o2: Pick<IBerkas, 'id'> | null): boolean {
    return o1 && o2 ? this.getBerkasIdentifier(o1) === this.getBerkasIdentifier(o2) : o1 === o2;
  }

  addBerkasToCollectionIfMissing<Type extends Pick<IBerkas, 'id'>>(
    berkasCollection: Type[],
    ...berkasToCheck: (Type | null | undefined)[]
  ): Type[] {
    const berkas: Type[] = berkasToCheck.filter(isPresent);
    if (berkas.length > 0) {
      const berkasCollectionIdentifiers = berkasCollection.map(berkasItem => this.getBerkasIdentifier(berkasItem)!);
      const berkasToAdd = berkas.filter(berkasItem => {
        const berkasIdentifier = this.getBerkasIdentifier(berkasItem);
        if (berkasCollectionIdentifiers.includes(berkasIdentifier)) {
          return false;
        }
        berkasCollectionIdentifiers.push(berkasIdentifier);
        return true;
      });
      return [...berkasToAdd, ...berkasCollection];
    }
    return berkasCollection;
  }

  protected convertDateFromClient<T extends IBerkas | NewBerkas | PartialUpdateBerkas>(berkas: T): RestOf<T> {
    return {
      ...berkas,
      updateOn: berkas.updateOn?.toJSON() ?? null,
      uploadOn: berkas.uploadOn?.toJSON() ?? null,
      tanggalPpk: berkas.tanggalPpk?.format(DATE_FORMAT) ?? null,
      verifyOn: berkas.verifyOn?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restBerkas: RestBerkas): IBerkas {
    return {
      ...restBerkas,
      updateOn: restBerkas.updateOn ? dayjs(restBerkas.updateOn) : undefined,
      uploadOn: restBerkas.uploadOn ? dayjs(restBerkas.uploadOn) : undefined,
      tanggalPpk: restBerkas.tanggalPpk ? dayjs(restBerkas.tanggalPpk) : undefined,
      verifyOn: restBerkas.verifyOn ? dayjs(restBerkas.verifyOn) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestBerkas>): HttpResponse<IBerkas> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestBerkas[]>): HttpResponse<IBerkas[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
