import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IBerkas } from '../berkas.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../berkas.test-samples';

import { BerkasService, RestBerkas } from './berkas.service';

const requireRestSample: RestBerkas = {
  ...sampleWithRequiredData,
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
  uploadOn: sampleWithRequiredData.uploadOn?.toJSON(),
  tanggalPpk: sampleWithRequiredData.tanggalPpk?.format(DATE_FORMAT),
  verifyOn: sampleWithRequiredData.verifyOn?.toJSON(),
};

describe('Berkas Service', () => {
  let service: BerkasService;
  let httpMock: HttpTestingController;
  let expectedResult: IBerkas | IBerkas[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(BerkasService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Berkas', () => {
      const berkas = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(berkas).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Berkas', () => {
      const berkas = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(berkas).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Berkas', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Berkas', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Berkas', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addBerkasToCollectionIfMissing', () => {
      it('should add a Berkas to an empty array', () => {
        const berkas: IBerkas = sampleWithRequiredData;
        expectedResult = service.addBerkasToCollectionIfMissing([], berkas);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(berkas);
      });

      it('should not add a Berkas to an array that contains it', () => {
        const berkas: IBerkas = sampleWithRequiredData;
        const berkasCollection: IBerkas[] = [
          {
            ...berkas,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addBerkasToCollectionIfMissing(berkasCollection, berkas);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Berkas to an array that doesn't contain it", () => {
        const berkas: IBerkas = sampleWithRequiredData;
        const berkasCollection: IBerkas[] = [sampleWithPartialData];
        expectedResult = service.addBerkasToCollectionIfMissing(berkasCollection, berkas);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(berkas);
      });

      it('should add only unique Berkas to an array', () => {
        const berkasArray: IBerkas[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const berkasCollection: IBerkas[] = [sampleWithRequiredData];
        expectedResult = service.addBerkasToCollectionIfMissing(berkasCollection, ...berkasArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const berkas: IBerkas = sampleWithRequiredData;
        const berkas2: IBerkas = sampleWithPartialData;
        expectedResult = service.addBerkasToCollectionIfMissing([], berkas, berkas2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(berkas);
        expect(expectedResult).toContain(berkas2);
      });

      it('should accept null and undefined values', () => {
        const berkas: IBerkas = sampleWithRequiredData;
        expectedResult = service.addBerkasToCollectionIfMissing([], null, berkas, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(berkas);
      });

      it('should return initial array if no Berkas is added', () => {
        const berkasCollection: IBerkas[] = [sampleWithRequiredData];
        expectedResult = service.addBerkasToCollectionIfMissing(berkasCollection, undefined, null);
        expect(expectedResult).toEqual(berkasCollection);
      });
    });

    describe('compareBerkas', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareBerkas(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareBerkas(entity1, entity2);
        const compareResult2 = service.compareBerkas(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareBerkas(entity1, entity2);
        const compareResult2 = service.compareBerkas(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareBerkas(entity1, entity2);
        const compareResult2 = service.compareBerkas(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
