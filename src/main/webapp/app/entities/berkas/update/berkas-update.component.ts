import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IRemark } from 'app/entities/remark/remark.model';
import { RemarkService } from 'app/entities/remark/service/remark.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { BerkasService } from '../service/berkas.service';
import { IBerkas } from '../berkas.model';
import { BerkasFormService, BerkasFormGroup } from './berkas-form.service';

@Component({
  standalone: true,
  selector: 'jhi-berkas-update',
  templateUrl: './berkas-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class BerkasUpdateComponent implements OnInit {
  isSaving = false;
  berkas: IBerkas | null = null;

  remarksSharedCollection: IRemark[] = [];
  cabangsSharedCollection: ICabang[] = [];

  editForm: BerkasFormGroup = this.berkasFormService.createBerkasFormGroup();

  constructor(
    protected berkasService: BerkasService,
    protected berkasFormService: BerkasFormService,
    protected remarkService: RemarkService,
    protected cabangService: CabangService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareRemark = (o1: IRemark | null, o2: IRemark | null): boolean => this.remarkService.compareRemark(o1, o2);

  compareCabang = (o1: ICabang | null, o2: ICabang | null): boolean => this.cabangService.compareCabang(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ berkas }) => {
      this.berkas = berkas;
      if (berkas) {
        this.updateForm(berkas);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const berkas = this.berkasFormService.getBerkas(this.editForm);
    if (berkas.id !== null) {
      this.subscribeToSaveResponse(this.berkasService.update(berkas));
    } else {
      this.subscribeToSaveResponse(this.berkasService.create(berkas));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBerkas>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(berkas: IBerkas): void {
    this.berkas = berkas;
    this.berkasFormService.resetForm(this.editForm, berkas);

    this.remarksSharedCollection = this.remarkService.addRemarkToCollectionIfMissing<IRemark>(this.remarksSharedCollection, berkas.remark);
    this.cabangsSharedCollection = this.cabangService.addCabangToCollectionIfMissing<ICabang>(this.cabangsSharedCollection, berkas.cabang);
  }

  protected loadRelationshipsOptions(): void {
    this.remarkService
      .query()
      .pipe(map((res: HttpResponse<IRemark[]>) => res.body ?? []))
      .pipe(map((remarks: IRemark[]) => this.remarkService.addRemarkToCollectionIfMissing<IRemark>(remarks, this.berkas?.remark)))
      .subscribe((remarks: IRemark[]) => (this.remarksSharedCollection = remarks));

    this.cabangService
      .query()
      .pipe(map((res: HttpResponse<ICabang[]>) => res.body ?? []))
      .pipe(map((cabangs: ICabang[]) => this.cabangService.addCabangToCollectionIfMissing<ICabang>(cabangs, this.berkas?.cabang)))
      .subscribe((cabangs: ICabang[]) => (this.cabangsSharedCollection = cabangs));
  }
}
