import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IBerkas, NewBerkas } from '../berkas.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IBerkas for edit and NewBerkasFormGroupInput for create.
 */
type BerkasFormGroupInput = IBerkas | PartialWithRequiredKeyOf<NewBerkas>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IBerkas | NewBerkas> = Omit<T, 'updateOn' | 'uploadOn' | 'verifyOn'> & {
  updateOn?: string | null;
  uploadOn?: string | null;
  verifyOn?: string | null;
};

type BerkasFormRawValue = FormValueOf<IBerkas>;

type NewBerkasFormRawValue = FormValueOf<NewBerkas>;

type BerkasFormDefaults = Pick<NewBerkas, 'id' | 'updateOn' | 'uploadOn' | 'verifyOn'>;

type BerkasFormGroupContent = {
  id: FormControl<BerkasFormRawValue['id'] | NewBerkas['id']>;
  identitas: FormControl<BerkasFormRawValue['identitas']>;
  kk: FormControl<BerkasFormRawValue['kk']>;
  ppk: FormControl<BerkasFormRawValue['ppk']>;
  skNasabah: FormControl<BerkasFormRawValue['skNasabah']>;
  aktaPendirian: FormControl<BerkasFormRawValue['aktaPendirian']>;
  aktaPerubahan: FormControl<BerkasFormRawValue['aktaPerubahan']>;
  skPendirian: FormControl<BerkasFormRawValue['skPendirian']>;
  skPerubahan: FormControl<BerkasFormRawValue['skPerubahan']>;
  npwp: FormControl<BerkasFormRawValue['npwp']>;
  updateBy: FormControl<BerkasFormRawValue['updateBy']>;
  updateOn: FormControl<BerkasFormRawValue['updateOn']>;
  uploadBy: FormControl<BerkasFormRawValue['uploadBy']>;
  uploadOn: FormControl<BerkasFormRawValue['uploadOn']>;
  ppkNomor: FormControl<BerkasFormRawValue['ppkNomor']>;
  nsbJenis: FormControl<BerkasFormRawValue['nsbJenis']>;
  berkasStatus: FormControl<BerkasFormRawValue['berkasStatus']>;
  namaNasabah: FormControl<BerkasFormRawValue['namaNasabah']>;
  tanggalPpk: FormControl<BerkasFormRawValue['tanggalPpk']>;
  metodeKirim: FormControl<BerkasFormRawValue['metodeKirim']>;
  partitionId: FormControl<BerkasFormRawValue['partitionId']>;
  verifyBy: FormControl<BerkasFormRawValue['verifyBy']>;
  verifyOn: FormControl<BerkasFormRawValue['verifyOn']>;
  orderType: FormControl<BerkasFormRawValue['orderType']>;
  remark: FormControl<BerkasFormRawValue['remark']>;
  cabang: FormControl<BerkasFormRawValue['cabang']>;
};

export type BerkasFormGroup = FormGroup<BerkasFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class BerkasFormService {
  createBerkasFormGroup(berkas: BerkasFormGroupInput = { id: null }): BerkasFormGroup {
    const berkasRawValue = this.convertBerkasToBerkasRawValue({
      ...this.getFormDefaults(),
      ...berkas,
    });
    return new FormGroup<BerkasFormGroupContent>({
      id: new FormControl(
        { value: berkasRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      identitas: new FormControl(berkasRawValue.identitas),
      kk: new FormControl(berkasRawValue.kk),
      ppk: new FormControl(berkasRawValue.ppk),
      skNasabah: new FormControl(berkasRawValue.skNasabah),
      aktaPendirian: new FormControl(berkasRawValue.aktaPendirian),
      aktaPerubahan: new FormControl(berkasRawValue.aktaPerubahan),
      skPendirian: new FormControl(berkasRawValue.skPendirian),
      skPerubahan: new FormControl(berkasRawValue.skPerubahan),
      npwp: new FormControl(berkasRawValue.npwp),
      updateBy: new FormControl(berkasRawValue.updateBy),
      updateOn: new FormControl(berkasRawValue.updateOn),
      uploadBy: new FormControl(berkasRawValue.uploadBy),
      uploadOn: new FormControl(berkasRawValue.uploadOn),
      ppkNomor: new FormControl(berkasRawValue.ppkNomor),
      nsbJenis: new FormControl(berkasRawValue.nsbJenis),
      berkasStatus: new FormControl(berkasRawValue.berkasStatus),
      namaNasabah: new FormControl(berkasRawValue.namaNasabah),
      tanggalPpk: new FormControl(berkasRawValue.tanggalPpk),
      metodeKirim: new FormControl(berkasRawValue.metodeKirim),
      partitionId: new FormControl(berkasRawValue.partitionId),
      verifyBy: new FormControl(berkasRawValue.verifyBy),
      verifyOn: new FormControl(berkasRawValue.verifyOn),
      orderType: new FormControl(berkasRawValue.orderType),
      remark: new FormControl(berkasRawValue.remark),
      cabang: new FormControl(berkasRawValue.cabang),
    });
  }

  getBerkas(form: BerkasFormGroup): IBerkas | NewBerkas {
    return this.convertBerkasRawValueToBerkas(form.getRawValue() as BerkasFormRawValue | NewBerkasFormRawValue);
  }

  resetForm(form: BerkasFormGroup, berkas: BerkasFormGroupInput): void {
    const berkasRawValue = this.convertBerkasToBerkasRawValue({ ...this.getFormDefaults(), ...berkas });
    form.reset(
      {
        ...berkasRawValue,
        id: { value: berkasRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): BerkasFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      updateOn: currentTime,
      uploadOn: currentTime,
      verifyOn: currentTime,
    };
  }

  private convertBerkasRawValueToBerkas(rawBerkas: BerkasFormRawValue | NewBerkasFormRawValue): IBerkas | NewBerkas {
    return {
      ...rawBerkas,
      updateOn: dayjs(rawBerkas.updateOn, DATE_TIME_FORMAT),
      uploadOn: dayjs(rawBerkas.uploadOn, DATE_TIME_FORMAT),
      verifyOn: dayjs(rawBerkas.verifyOn, DATE_TIME_FORMAT),
    };
  }

  private convertBerkasToBerkasRawValue(
    berkas: IBerkas | (Partial<NewBerkas> & BerkasFormDefaults),
  ): BerkasFormRawValue | PartialWithRequiredKeyOf<NewBerkasFormRawValue> {
    return {
      ...berkas,
      updateOn: berkas.updateOn ? berkas.updateOn.format(DATE_TIME_FORMAT) : undefined,
      uploadOn: berkas.uploadOn ? berkas.uploadOn.format(DATE_TIME_FORMAT) : undefined,
      verifyOn: berkas.verifyOn ? berkas.verifyOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
