import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../berkas.test-samples';

import { BerkasFormService } from './berkas-form.service';

describe('Berkas Form Service', () => {
  let service: BerkasFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BerkasFormService);
  });

  describe('Service methods', () => {
    describe('createBerkasFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createBerkasFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            identitas: expect.any(Object),
            kk: expect.any(Object),
            ppk: expect.any(Object),
            skNasabah: expect.any(Object),
            aktaPendirian: expect.any(Object),
            aktaPerubahan: expect.any(Object),
            skPendirian: expect.any(Object),
            skPerubahan: expect.any(Object),
            npwp: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            uploadBy: expect.any(Object),
            uploadOn: expect.any(Object),
            ppkNomor: expect.any(Object),
            nsbJenis: expect.any(Object),
            berkasStatus: expect.any(Object),
            namaNasabah: expect.any(Object),
            tanggalPpk: expect.any(Object),
            metodeKirim: expect.any(Object),
            partitionId: expect.any(Object),
            verifyBy: expect.any(Object),
            verifyOn: expect.any(Object),
            orderType: expect.any(Object),
            remark: expect.any(Object),
            cabang: expect.any(Object),
          }),
        );
      });

      it('passing IBerkas should create a new form with FormGroup', () => {
        const formGroup = service.createBerkasFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            identitas: expect.any(Object),
            kk: expect.any(Object),
            ppk: expect.any(Object),
            skNasabah: expect.any(Object),
            aktaPendirian: expect.any(Object),
            aktaPerubahan: expect.any(Object),
            skPendirian: expect.any(Object),
            skPerubahan: expect.any(Object),
            npwp: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            uploadBy: expect.any(Object),
            uploadOn: expect.any(Object),
            ppkNomor: expect.any(Object),
            nsbJenis: expect.any(Object),
            berkasStatus: expect.any(Object),
            namaNasabah: expect.any(Object),
            tanggalPpk: expect.any(Object),
            metodeKirim: expect.any(Object),
            partitionId: expect.any(Object),
            verifyBy: expect.any(Object),
            verifyOn: expect.any(Object),
            orderType: expect.any(Object),
            remark: expect.any(Object),
            cabang: expect.any(Object),
          }),
        );
      });
    });

    describe('getBerkas', () => {
      it('should return NewBerkas for default Berkas initial value', () => {
        const formGroup = service.createBerkasFormGroup(sampleWithNewData);

        const berkas = service.getBerkas(formGroup) as any;

        expect(berkas).toMatchObject(sampleWithNewData);
      });

      it('should return NewBerkas for empty Berkas initial value', () => {
        const formGroup = service.createBerkasFormGroup();

        const berkas = service.getBerkas(formGroup) as any;

        expect(berkas).toMatchObject({});
      });

      it('should return IBerkas', () => {
        const formGroup = service.createBerkasFormGroup(sampleWithRequiredData);

        const berkas = service.getBerkas(formGroup) as any;

        expect(berkas).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IBerkas should not enable id FormControl', () => {
        const formGroup = service.createBerkasFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewBerkas should disable id FormControl', () => {
        const formGroup = service.createBerkasFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
