import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IRemark } from 'app/entities/remark/remark.model';
import { RemarkService } from 'app/entities/remark/service/remark.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { IBerkas } from '../berkas.model';
import { BerkasService } from '../service/berkas.service';
import { BerkasFormService } from './berkas-form.service';

import { BerkasUpdateComponent } from './berkas-update.component';

describe('Berkas Management Update Component', () => {
  let comp: BerkasUpdateComponent;
  let fixture: ComponentFixture<BerkasUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let berkasFormService: BerkasFormService;
  let berkasService: BerkasService;
  let remarkService: RemarkService;
  let cabangService: CabangService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), BerkasUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BerkasUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BerkasUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    berkasFormService = TestBed.inject(BerkasFormService);
    berkasService = TestBed.inject(BerkasService);
    remarkService = TestBed.inject(RemarkService);
    cabangService = TestBed.inject(CabangService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Remark query and add missing value', () => {
      const berkas: IBerkas = { id: 456 };
      const remark: IRemark = { id: 16309 };
      berkas.remark = remark;

      const remarkCollection: IRemark[] = [{ id: 17570 }];
      jest.spyOn(remarkService, 'query').mockReturnValue(of(new HttpResponse({ body: remarkCollection })));
      const additionalRemarks = [remark];
      const expectedCollection: IRemark[] = [...additionalRemarks, ...remarkCollection];
      jest.spyOn(remarkService, 'addRemarkToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ berkas });
      comp.ngOnInit();

      expect(remarkService.query).toHaveBeenCalled();
      expect(remarkService.addRemarkToCollectionIfMissing).toHaveBeenCalledWith(
        remarkCollection,
        ...additionalRemarks.map(expect.objectContaining),
      );
      expect(comp.remarksSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Cabang query and add missing value', () => {
      const berkas: IBerkas = { id: 456 };
      const cabang: ICabang = { id: 32329 };
      berkas.cabang = cabang;

      const cabangCollection: ICabang[] = [{ id: 20196 }];
      jest.spyOn(cabangService, 'query').mockReturnValue(of(new HttpResponse({ body: cabangCollection })));
      const additionalCabangs = [cabang];
      const expectedCollection: ICabang[] = [...additionalCabangs, ...cabangCollection];
      jest.spyOn(cabangService, 'addCabangToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ berkas });
      comp.ngOnInit();

      expect(cabangService.query).toHaveBeenCalled();
      expect(cabangService.addCabangToCollectionIfMissing).toHaveBeenCalledWith(
        cabangCollection,
        ...additionalCabangs.map(expect.objectContaining),
      );
      expect(comp.cabangsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const berkas: IBerkas = { id: 456 };
      const remark: IRemark = { id: 11770 };
      berkas.remark = remark;
      const cabang: ICabang = { id: 16480 };
      berkas.cabang = cabang;

      activatedRoute.data = of({ berkas });
      comp.ngOnInit();

      expect(comp.remarksSharedCollection).toContain(remark);
      expect(comp.cabangsSharedCollection).toContain(cabang);
      expect(comp.berkas).toEqual(berkas);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBerkas>>();
      const berkas = { id: 123 };
      jest.spyOn(berkasFormService, 'getBerkas').mockReturnValue(berkas);
      jest.spyOn(berkasService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ berkas });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: berkas }));
      saveSubject.complete();

      // THEN
      expect(berkasFormService.getBerkas).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(berkasService.update).toHaveBeenCalledWith(expect.objectContaining(berkas));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBerkas>>();
      const berkas = { id: 123 };
      jest.spyOn(berkasFormService, 'getBerkas').mockReturnValue({ id: null });
      jest.spyOn(berkasService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ berkas: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: berkas }));
      saveSubject.complete();

      // THEN
      expect(berkasFormService.getBerkas).toHaveBeenCalled();
      expect(berkasService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBerkas>>();
      const berkas = { id: 123 };
      jest.spyOn(berkasService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ berkas });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(berkasService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareRemark', () => {
      it('Should forward to remarkService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(remarkService, 'compareRemark');
        comp.compareRemark(entity, entity2);
        expect(remarkService.compareRemark).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareCabang', () => {
      it('Should forward to cabangService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(cabangService, 'compareCabang');
        comp.compareCabang(entity, entity2);
        expect(cabangService.compareCabang).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
