import dayjs from 'dayjs/esm';

import { IBerkas, NewBerkas } from './berkas.model';

export const sampleWithRequiredData: IBerkas = {
  id: 32290,
};

export const sampleWithPartialData: IBerkas = {
  id: 8854,
  kk: 'fooey pro',
  skNasabah: 'parody',
  aktaPendirian: 'huzzah though during',
  aktaPerubahan: 'opposite wholly burn-out',
  skPendirian: 'excluding gladiolus',
  uploadBy: 'slow high speedily',
  uploadOn: dayjs('2023-12-14T18:56'),
  ppkNomor: 'a once',
  nsbJenis: 11145,
  berkasStatus: 32353,
  tanggalPpk: dayjs('2023-12-14'),
  verifyOn: dayjs('2023-12-15T01:02'),
  orderType: 18382,
};

export const sampleWithFullData: IBerkas = {
  id: 17170,
  identitas: 'eek um identical',
  kk: 'hourly oily',
  ppk: 'so semicircle hairy',
  skNasabah: 'shareholder willfully classroom',
  aktaPendirian: 'furthermore brushing woot',
  aktaPerubahan: 'cartoon aromatic',
  skPendirian: 'hm',
  skPerubahan: 'teepee',
  npwp: 'innocently supposing writer',
  updateBy: 'outside picket after',
  updateOn: dayjs('2023-12-14T17:40'),
  uploadBy: 'usable',
  uploadOn: dayjs('2023-12-14T21:50'),
  ppkNomor: 'directive ghost angry',
  nsbJenis: 5149,
  berkasStatus: 16125,
  namaNasabah: 'happy whimsical humiliating',
  tanggalPpk: dayjs('2023-12-14'),
  metodeKirim: 32048,
  partitionId: 14900,
  verifyBy: 'webbed',
  verifyOn: dayjs('2023-12-14T03:12'),
  orderType: 22722,
};

export const sampleWithNewData: NewBerkas = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
