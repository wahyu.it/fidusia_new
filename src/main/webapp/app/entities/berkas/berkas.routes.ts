import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { BerkasComponent } from './list/berkas.component';
import { BerkasDetailComponent } from './detail/berkas-detail.component';
import { BerkasUpdateComponent } from './update/berkas-update.component';
import BerkasResolve from './route/berkas-routing-resolve.service';

const berkasRoute: Routes = [
  {
    path: '',
    component: BerkasComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BerkasDetailComponent,
    resolve: {
      berkas: BerkasResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BerkasUpdateComponent,
    resolve: {
      berkas: BerkasResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BerkasUpdateComponent,
    resolve: {
      berkas: BerkasResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default berkasRoute;
