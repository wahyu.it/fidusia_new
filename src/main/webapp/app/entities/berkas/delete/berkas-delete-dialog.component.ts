import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IBerkas } from '../berkas.model';
import { BerkasService } from '../service/berkas.service';

@Component({
  standalone: true,
  templateUrl: './berkas-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class BerkasDeleteDialogComponent {
  berkas?: IBerkas;

  constructor(
    protected berkasService: BerkasService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.berkasService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
