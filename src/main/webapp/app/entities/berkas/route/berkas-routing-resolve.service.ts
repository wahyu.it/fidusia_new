import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBerkas } from '../berkas.model';
import { BerkasService } from '../service/berkas.service';

export const berkasResolve = (route: ActivatedRouteSnapshot): Observable<null | IBerkas> => {
  const id = route.params['id'];
  if (id) {
    return inject(BerkasService)
      .find(id)
      .pipe(
        mergeMap((berkas: HttpResponse<IBerkas>) => {
          if (berkas.body) {
            return of(berkas.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default berkasResolve;
