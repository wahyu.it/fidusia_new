import dayjs from 'dayjs/esm';
import { IRemark } from 'app/entities/remark/remark.model';
import { ICabang } from 'app/entities/cabang/cabang.model';

export interface IBerkas {
  id: number;
  identitas?: string | null;
  kk?: string | null;
  ppk?: string | null;
  skNasabah?: string | null;
  aktaPendirian?: string | null;
  aktaPerubahan?: string | null;
  skPendirian?: string | null;
  skPerubahan?: string | null;
  npwp?: string | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  uploadBy?: string | null;
  uploadOn?: dayjs.Dayjs | null;
  ppkNomor?: string | null;
  nsbJenis?: number | null;
  berkasStatus?: number | null;
  namaNasabah?: string | null;
  tanggalPpk?: dayjs.Dayjs | null;
  metodeKirim?: number | null;
  partitionId?: number | null;
  verifyBy?: string | null;
  verifyOn?: dayjs.Dayjs | null;
  orderType?: number | null;
  remark?: IRemark | null;
  cabang?: ICabang | null;
}

export type NewBerkas = Omit<IBerkas, 'id'> & { id: null };
