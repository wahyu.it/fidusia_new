import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { PenerimaKuasaComponent } from './list/penerima-kuasa.component';
import { PenerimaKuasaDetailComponent } from './detail/penerima-kuasa-detail.component';
import { PenerimaKuasaUpdateComponent } from './update/penerima-kuasa-update.component';
import PenerimaKuasaResolve from './route/penerima-kuasa-routing-resolve.service';

const penerimaKuasaRoute: Routes = [
  {
    path: '',
    component: PenerimaKuasaComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PenerimaKuasaDetailComponent,
    resolve: {
      penerimaKuasa: PenerimaKuasaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PenerimaKuasaUpdateComponent,
    resolve: {
      penerimaKuasa: PenerimaKuasaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PenerimaKuasaUpdateComponent,
    resolve: {
      penerimaKuasa: PenerimaKuasaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default penerimaKuasaRoute;
