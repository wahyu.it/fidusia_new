import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IPenerimaKuasa } from '../penerima-kuasa.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../penerima-kuasa.test-samples';

import { PenerimaKuasaService, RestPenerimaKuasa } from './penerima-kuasa.service';

const requireRestSample: RestPenerimaKuasa = {
  ...sampleWithRequiredData,
  tanggalSk: sampleWithRequiredData.tanggalSk?.format(DATE_FORMAT),
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
};

describe('PenerimaKuasa Service', () => {
  let service: PenerimaKuasaService;
  let httpMock: HttpTestingController;
  let expectedResult: IPenerimaKuasa | IPenerimaKuasa[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PenerimaKuasaService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a PenerimaKuasa', () => {
      const penerimaKuasa = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(penerimaKuasa).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PenerimaKuasa', () => {
      const penerimaKuasa = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(penerimaKuasa).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PenerimaKuasa', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PenerimaKuasa', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a PenerimaKuasa', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addPenerimaKuasaToCollectionIfMissing', () => {
      it('should add a PenerimaKuasa to an empty array', () => {
        const penerimaKuasa: IPenerimaKuasa = sampleWithRequiredData;
        expectedResult = service.addPenerimaKuasaToCollectionIfMissing([], penerimaKuasa);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(penerimaKuasa);
      });

      it('should not add a PenerimaKuasa to an array that contains it', () => {
        const penerimaKuasa: IPenerimaKuasa = sampleWithRequiredData;
        const penerimaKuasaCollection: IPenerimaKuasa[] = [
          {
            ...penerimaKuasa,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addPenerimaKuasaToCollectionIfMissing(penerimaKuasaCollection, penerimaKuasa);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PenerimaKuasa to an array that doesn't contain it", () => {
        const penerimaKuasa: IPenerimaKuasa = sampleWithRequiredData;
        const penerimaKuasaCollection: IPenerimaKuasa[] = [sampleWithPartialData];
        expectedResult = service.addPenerimaKuasaToCollectionIfMissing(penerimaKuasaCollection, penerimaKuasa);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(penerimaKuasa);
      });

      it('should add only unique PenerimaKuasa to an array', () => {
        const penerimaKuasaArray: IPenerimaKuasa[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const penerimaKuasaCollection: IPenerimaKuasa[] = [sampleWithRequiredData];
        expectedResult = service.addPenerimaKuasaToCollectionIfMissing(penerimaKuasaCollection, ...penerimaKuasaArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const penerimaKuasa: IPenerimaKuasa = sampleWithRequiredData;
        const penerimaKuasa2: IPenerimaKuasa = sampleWithPartialData;
        expectedResult = service.addPenerimaKuasaToCollectionIfMissing([], penerimaKuasa, penerimaKuasa2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(penerimaKuasa);
        expect(expectedResult).toContain(penerimaKuasa2);
      });

      it('should accept null and undefined values', () => {
        const penerimaKuasa: IPenerimaKuasa = sampleWithRequiredData;
        expectedResult = service.addPenerimaKuasaToCollectionIfMissing([], null, penerimaKuasa, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(penerimaKuasa);
      });

      it('should return initial array if no PenerimaKuasa is added', () => {
        const penerimaKuasaCollection: IPenerimaKuasa[] = [sampleWithRequiredData];
        expectedResult = service.addPenerimaKuasaToCollectionIfMissing(penerimaKuasaCollection, undefined, null);
        expect(expectedResult).toEqual(penerimaKuasaCollection);
      });
    });

    describe('comparePenerimaKuasa', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.comparePenerimaKuasa(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.comparePenerimaKuasa(entity1, entity2);
        const compareResult2 = service.comparePenerimaKuasa(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.comparePenerimaKuasa(entity1, entity2);
        const compareResult2 = service.comparePenerimaKuasa(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.comparePenerimaKuasa(entity1, entity2);
        const compareResult2 = service.comparePenerimaKuasa(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
