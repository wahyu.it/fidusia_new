import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPenerimaKuasa, NewPenerimaKuasa } from '../penerima-kuasa.model';

export type PartialUpdatePenerimaKuasa = Partial<IPenerimaKuasa> & Pick<IPenerimaKuasa, 'id'>;

type RestOf<T extends IPenerimaKuasa | NewPenerimaKuasa> = Omit<T, 'tanggalSk' | 'updateOn'> & {
  tanggalSk?: string | null;
  updateOn?: string | null;
};

export type RestPenerimaKuasa = RestOf<IPenerimaKuasa>;

export type NewRestPenerimaKuasa = RestOf<NewPenerimaKuasa>;

export type PartialUpdateRestPenerimaKuasa = RestOf<PartialUpdatePenerimaKuasa>;

export type EntityResponseType = HttpResponse<IPenerimaKuasa>;
export type EntityArrayResponseType = HttpResponse<IPenerimaKuasa[]>;

@Injectable({ providedIn: 'root' })
export class PenerimaKuasaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/penerima-kuasas');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(penerimaKuasa: NewPenerimaKuasa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(penerimaKuasa);
    return this.http
      .post<RestPenerimaKuasa>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(penerimaKuasa: IPenerimaKuasa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(penerimaKuasa);
    return this.http
      .put<RestPenerimaKuasa>(`${this.resourceUrl}/${this.getPenerimaKuasaIdentifier(penerimaKuasa)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(penerimaKuasa: PartialUpdatePenerimaKuasa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(penerimaKuasa);
    return this.http
      .patch<RestPenerimaKuasa>(`${this.resourceUrl}/${this.getPenerimaKuasaIdentifier(penerimaKuasa)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestPenerimaKuasa>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestPenerimaKuasa[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPenerimaKuasaIdentifier(penerimaKuasa: Pick<IPenerimaKuasa, 'id'>): number {
    return penerimaKuasa.id;
  }

  comparePenerimaKuasa(o1: Pick<IPenerimaKuasa, 'id'> | null, o2: Pick<IPenerimaKuasa, 'id'> | null): boolean {
    return o1 && o2 ? this.getPenerimaKuasaIdentifier(o1) === this.getPenerimaKuasaIdentifier(o2) : o1 === o2;
  }

  addPenerimaKuasaToCollectionIfMissing<Type extends Pick<IPenerimaKuasa, 'id'>>(
    penerimaKuasaCollection: Type[],
    ...penerimaKuasasToCheck: (Type | null | undefined)[]
  ): Type[] {
    const penerimaKuasas: Type[] = penerimaKuasasToCheck.filter(isPresent);
    if (penerimaKuasas.length > 0) {
      const penerimaKuasaCollectionIdentifiers = penerimaKuasaCollection.map(
        penerimaKuasaItem => this.getPenerimaKuasaIdentifier(penerimaKuasaItem)!,
      );
      const penerimaKuasasToAdd = penerimaKuasas.filter(penerimaKuasaItem => {
        const penerimaKuasaIdentifier = this.getPenerimaKuasaIdentifier(penerimaKuasaItem);
        if (penerimaKuasaCollectionIdentifiers.includes(penerimaKuasaIdentifier)) {
          return false;
        }
        penerimaKuasaCollectionIdentifiers.push(penerimaKuasaIdentifier);
        return true;
      });
      return [...penerimaKuasasToAdd, ...penerimaKuasaCollection];
    }
    return penerimaKuasaCollection;
  }

  protected convertDateFromClient<T extends IPenerimaKuasa | NewPenerimaKuasa | PartialUpdatePenerimaKuasa>(penerimaKuasa: T): RestOf<T> {
    return {
      ...penerimaKuasa,
      tanggalSk: penerimaKuasa.tanggalSk?.format(DATE_FORMAT) ?? null,
      updateOn: penerimaKuasa.updateOn?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restPenerimaKuasa: RestPenerimaKuasa): IPenerimaKuasa {
    return {
      ...restPenerimaKuasa,
      tanggalSk: restPenerimaKuasa.tanggalSk ? dayjs(restPenerimaKuasa.tanggalSk) : undefined,
      updateOn: restPenerimaKuasa.updateOn ? dayjs(restPenerimaKuasa.updateOn) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestPenerimaKuasa>): HttpResponse<IPenerimaKuasa> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestPenerimaKuasa[]>): HttpResponse<IPenerimaKuasa[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
