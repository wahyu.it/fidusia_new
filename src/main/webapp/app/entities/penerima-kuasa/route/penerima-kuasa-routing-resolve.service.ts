import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPenerimaKuasa } from '../penerima-kuasa.model';
import { PenerimaKuasaService } from '../service/penerima-kuasa.service';

export const penerimaKuasaResolve = (route: ActivatedRouteSnapshot): Observable<null | IPenerimaKuasa> => {
  const id = route.params['id'];
  if (id) {
    return inject(PenerimaKuasaService)
      .find(id)
      .pipe(
        mergeMap((penerimaKuasa: HttpResponse<IPenerimaKuasa>) => {
          if (penerimaKuasa.body) {
            return of(penerimaKuasa.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default penerimaKuasaResolve;
