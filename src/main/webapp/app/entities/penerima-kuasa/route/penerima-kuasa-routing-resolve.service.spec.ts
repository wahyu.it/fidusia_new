import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IPenerimaKuasa } from '../penerima-kuasa.model';
import { PenerimaKuasaService } from '../service/penerima-kuasa.service';

import penerimaKuasaResolve from './penerima-kuasa-routing-resolve.service';

describe('PenerimaKuasa routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let service: PenerimaKuasaService;
  let resultPenerimaKuasa: IPenerimaKuasa | null | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    service = TestBed.inject(PenerimaKuasaService);
    resultPenerimaKuasa = undefined;
  });

  describe('resolve', () => {
    it('should return IPenerimaKuasa returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      TestBed.runInInjectionContext(() => {
        penerimaKuasaResolve(mockActivatedRouteSnapshot).subscribe({
          next(result) {
            resultPenerimaKuasa = result;
          },
        });
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPenerimaKuasa).toEqual({ id: 123 });
    });

    it('should return null if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      TestBed.runInInjectionContext(() => {
        penerimaKuasaResolve(mockActivatedRouteSnapshot).subscribe({
          next(result) {
            resultPenerimaKuasa = result;
          },
        });
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPenerimaKuasa).toEqual(null);
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse<IPenerimaKuasa>({ body: null })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      TestBed.runInInjectionContext(() => {
        penerimaKuasaResolve(mockActivatedRouteSnapshot).subscribe({
          next(result) {
            resultPenerimaKuasa = result;
          },
        });
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPenerimaKuasa).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
