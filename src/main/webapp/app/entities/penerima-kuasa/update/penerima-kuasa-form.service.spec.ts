import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../penerima-kuasa.test-samples';

import { PenerimaKuasaFormService } from './penerima-kuasa-form.service';

describe('PenerimaKuasa Form Service', () => {
  let service: PenerimaKuasaFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PenerimaKuasaFormService);
  });

  describe('Service methods', () => {
    describe('createPenerimaKuasaFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createPenerimaKuasaFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            titel: expect.any(Object),
            nama: expect.any(Object),
            komparisi: expect.any(Object),
            tts: expect.any(Object),
            sk: expect.any(Object),
            tanggalSk: expect.any(Object),
            noKontak: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
          }),
        );
      });

      it('passing IPenerimaKuasa should create a new form with FormGroup', () => {
        const formGroup = service.createPenerimaKuasaFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            titel: expect.any(Object),
            nama: expect.any(Object),
            komparisi: expect.any(Object),
            tts: expect.any(Object),
            sk: expect.any(Object),
            tanggalSk: expect.any(Object),
            noKontak: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
          }),
        );
      });
    });

    describe('getPenerimaKuasa', () => {
      it('should return NewPenerimaKuasa for default PenerimaKuasa initial value', () => {
        const formGroup = service.createPenerimaKuasaFormGroup(sampleWithNewData);

        const penerimaKuasa = service.getPenerimaKuasa(formGroup) as any;

        expect(penerimaKuasa).toMatchObject(sampleWithNewData);
      });

      it('should return NewPenerimaKuasa for empty PenerimaKuasa initial value', () => {
        const formGroup = service.createPenerimaKuasaFormGroup();

        const penerimaKuasa = service.getPenerimaKuasa(formGroup) as any;

        expect(penerimaKuasa).toMatchObject({});
      });

      it('should return IPenerimaKuasa', () => {
        const formGroup = service.createPenerimaKuasaFormGroup(sampleWithRequiredData);

        const penerimaKuasa = service.getPenerimaKuasa(formGroup) as any;

        expect(penerimaKuasa).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IPenerimaKuasa should not enable id FormControl', () => {
        const formGroup = service.createPenerimaKuasaFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewPenerimaKuasa should disable id FormControl', () => {
        const formGroup = service.createPenerimaKuasaFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
