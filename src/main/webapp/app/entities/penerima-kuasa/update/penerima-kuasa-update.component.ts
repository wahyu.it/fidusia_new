import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPenerimaKuasa } from '../penerima-kuasa.model';
import { PenerimaKuasaService } from '../service/penerima-kuasa.service';
import { PenerimaKuasaFormService, PenerimaKuasaFormGroup } from './penerima-kuasa-form.service';

@Component({
  standalone: true,
  selector: 'jhi-penerima-kuasa-update',
  templateUrl: './penerima-kuasa-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class PenerimaKuasaUpdateComponent implements OnInit {
  isSaving = false;
  penerimaKuasa: IPenerimaKuasa | null = null;

  editForm: PenerimaKuasaFormGroup = this.penerimaKuasaFormService.createPenerimaKuasaFormGroup();

  constructor(
    protected penerimaKuasaService: PenerimaKuasaService,
    protected penerimaKuasaFormService: PenerimaKuasaFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ penerimaKuasa }) => {
      this.penerimaKuasa = penerimaKuasa;
      if (penerimaKuasa) {
        this.updateForm(penerimaKuasa);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const penerimaKuasa = this.penerimaKuasaFormService.getPenerimaKuasa(this.editForm);
    if (penerimaKuasa.id !== null) {
      this.subscribeToSaveResponse(this.penerimaKuasaService.update(penerimaKuasa));
    } else {
      this.subscribeToSaveResponse(this.penerimaKuasaService.create(penerimaKuasa));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPenerimaKuasa>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(penerimaKuasa: IPenerimaKuasa): void {
    this.penerimaKuasa = penerimaKuasa;
    this.penerimaKuasaFormService.resetForm(this.editForm, penerimaKuasa);
  }
}
