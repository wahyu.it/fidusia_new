import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PenerimaKuasaService } from '../service/penerima-kuasa.service';
import { IPenerimaKuasa } from '../penerima-kuasa.model';
import { PenerimaKuasaFormService } from './penerima-kuasa-form.service';

import { PenerimaKuasaUpdateComponent } from './penerima-kuasa-update.component';

describe('PenerimaKuasa Management Update Component', () => {
  let comp: PenerimaKuasaUpdateComponent;
  let fixture: ComponentFixture<PenerimaKuasaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let penerimaKuasaFormService: PenerimaKuasaFormService;
  let penerimaKuasaService: PenerimaKuasaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), PenerimaKuasaUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PenerimaKuasaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PenerimaKuasaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    penerimaKuasaFormService = TestBed.inject(PenerimaKuasaFormService);
    penerimaKuasaService = TestBed.inject(PenerimaKuasaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const penerimaKuasa: IPenerimaKuasa = { id: 456 };

      activatedRoute.data = of({ penerimaKuasa });
      comp.ngOnInit();

      expect(comp.penerimaKuasa).toEqual(penerimaKuasa);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPenerimaKuasa>>();
      const penerimaKuasa = { id: 123 };
      jest.spyOn(penerimaKuasaFormService, 'getPenerimaKuasa').mockReturnValue(penerimaKuasa);
      jest.spyOn(penerimaKuasaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ penerimaKuasa });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: penerimaKuasa }));
      saveSubject.complete();

      // THEN
      expect(penerimaKuasaFormService.getPenerimaKuasa).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(penerimaKuasaService.update).toHaveBeenCalledWith(expect.objectContaining(penerimaKuasa));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPenerimaKuasa>>();
      const penerimaKuasa = { id: 123 };
      jest.spyOn(penerimaKuasaFormService, 'getPenerimaKuasa').mockReturnValue({ id: null });
      jest.spyOn(penerimaKuasaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ penerimaKuasa: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: penerimaKuasa }));
      saveSubject.complete();

      // THEN
      expect(penerimaKuasaFormService.getPenerimaKuasa).toHaveBeenCalled();
      expect(penerimaKuasaService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPenerimaKuasa>>();
      const penerimaKuasa = { id: 123 };
      jest.spyOn(penerimaKuasaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ penerimaKuasa });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(penerimaKuasaService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
