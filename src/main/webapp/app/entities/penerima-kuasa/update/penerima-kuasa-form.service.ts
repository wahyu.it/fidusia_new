import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPenerimaKuasa, NewPenerimaKuasa } from '../penerima-kuasa.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPenerimaKuasa for edit and NewPenerimaKuasaFormGroupInput for create.
 */
type PenerimaKuasaFormGroupInput = IPenerimaKuasa | PartialWithRequiredKeyOf<NewPenerimaKuasa>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IPenerimaKuasa | NewPenerimaKuasa> = Omit<T, 'updateOn'> & {
  updateOn?: string | null;
};

type PenerimaKuasaFormRawValue = FormValueOf<IPenerimaKuasa>;

type NewPenerimaKuasaFormRawValue = FormValueOf<NewPenerimaKuasa>;

type PenerimaKuasaFormDefaults = Pick<NewPenerimaKuasa, 'id' | 'updateOn'>;

type PenerimaKuasaFormGroupContent = {
  id: FormControl<PenerimaKuasaFormRawValue['id'] | NewPenerimaKuasa['id']>;
  titel: FormControl<PenerimaKuasaFormRawValue['titel']>;
  nama: FormControl<PenerimaKuasaFormRawValue['nama']>;
  komparisi: FormControl<PenerimaKuasaFormRawValue['komparisi']>;
  tts: FormControl<PenerimaKuasaFormRawValue['tts']>;
  sk: FormControl<PenerimaKuasaFormRawValue['sk']>;
  tanggalSk: FormControl<PenerimaKuasaFormRawValue['tanggalSk']>;
  noKontak: FormControl<PenerimaKuasaFormRawValue['noKontak']>;
  recordStatus: FormControl<PenerimaKuasaFormRawValue['recordStatus']>;
  updateBy: FormControl<PenerimaKuasaFormRawValue['updateBy']>;
  updateOn: FormControl<PenerimaKuasaFormRawValue['updateOn']>;
};

export type PenerimaKuasaFormGroup = FormGroup<PenerimaKuasaFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PenerimaKuasaFormService {
  createPenerimaKuasaFormGroup(penerimaKuasa: PenerimaKuasaFormGroupInput = { id: null }): PenerimaKuasaFormGroup {
    const penerimaKuasaRawValue = this.convertPenerimaKuasaToPenerimaKuasaRawValue({
      ...this.getFormDefaults(),
      ...penerimaKuasa,
    });
    return new FormGroup<PenerimaKuasaFormGroupContent>({
      id: new FormControl(
        { value: penerimaKuasaRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      titel: new FormControl(penerimaKuasaRawValue.titel),
      nama: new FormControl(penerimaKuasaRawValue.nama),
      komparisi: new FormControl(penerimaKuasaRawValue.komparisi),
      tts: new FormControl(penerimaKuasaRawValue.tts),
      sk: new FormControl(penerimaKuasaRawValue.sk),
      tanggalSk: new FormControl(penerimaKuasaRawValue.tanggalSk),
      noKontak: new FormControl(penerimaKuasaRawValue.noKontak),
      recordStatus: new FormControl(penerimaKuasaRawValue.recordStatus),
      updateBy: new FormControl(penerimaKuasaRawValue.updateBy),
      updateOn: new FormControl(penerimaKuasaRawValue.updateOn),
    });
  }

  getPenerimaKuasa(form: PenerimaKuasaFormGroup): IPenerimaKuasa | NewPenerimaKuasa {
    return this.convertPenerimaKuasaRawValueToPenerimaKuasa(form.getRawValue() as PenerimaKuasaFormRawValue | NewPenerimaKuasaFormRawValue);
  }

  resetForm(form: PenerimaKuasaFormGroup, penerimaKuasa: PenerimaKuasaFormGroupInput): void {
    const penerimaKuasaRawValue = this.convertPenerimaKuasaToPenerimaKuasaRawValue({ ...this.getFormDefaults(), ...penerimaKuasa });
    form.reset(
      {
        ...penerimaKuasaRawValue,
        id: { value: penerimaKuasaRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): PenerimaKuasaFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      updateOn: currentTime,
    };
  }

  private convertPenerimaKuasaRawValueToPenerimaKuasa(
    rawPenerimaKuasa: PenerimaKuasaFormRawValue | NewPenerimaKuasaFormRawValue,
  ): IPenerimaKuasa | NewPenerimaKuasa {
    return {
      ...rawPenerimaKuasa,
      updateOn: dayjs(rawPenerimaKuasa.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertPenerimaKuasaToPenerimaKuasaRawValue(
    penerimaKuasa: IPenerimaKuasa | (Partial<NewPenerimaKuasa> & PenerimaKuasaFormDefaults),
  ): PenerimaKuasaFormRawValue | PartialWithRequiredKeyOf<NewPenerimaKuasaFormRawValue> {
    return {
      ...penerimaKuasa,
      updateOn: penerimaKuasa.updateOn ? penerimaKuasa.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
