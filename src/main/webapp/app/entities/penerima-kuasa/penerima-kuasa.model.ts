import dayjs from 'dayjs/esm';
import { INotaris } from 'app/entities/notaris/notaris.model';

export interface IPenerimaKuasa {
  id: number;
  titel?: string | null;
  nama?: string | null;
  komparisi?: string | null;
  tts?: string | null;
  sk?: string | null;
  tanggalSk?: dayjs.Dayjs | null;
  noKontak?: string | null;
  recordStatus?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  notaris?: INotaris | null;
}

export type NewPenerimaKuasa = Omit<IPenerimaKuasa, 'id'> & { id: null };
