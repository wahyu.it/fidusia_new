import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PenerimaKuasaDetailComponent } from './penerima-kuasa-detail.component';

describe('PenerimaKuasa Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PenerimaKuasaDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: PenerimaKuasaDetailComponent,
              resolve: { penerimaKuasa: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(PenerimaKuasaDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load penerimaKuasa on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', PenerimaKuasaDetailComponent);

      // THEN
      expect(instance.penerimaKuasa).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
