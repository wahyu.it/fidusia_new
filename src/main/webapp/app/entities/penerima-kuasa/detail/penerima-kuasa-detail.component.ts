import { Component, Input } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IPenerimaKuasa } from '../penerima-kuasa.model';

@Component({
  standalone: true,
  selector: 'jhi-penerima-kuasa-detail',
  templateUrl: './penerima-kuasa-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class PenerimaKuasaDetailComponent {
  @Input() penerimaKuasa: IPenerimaKuasa | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  previousState(): void {
    window.history.back();
  }
}
