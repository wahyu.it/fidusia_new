import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IPenerimaKuasa } from '../penerima-kuasa.model';
import { PenerimaKuasaService } from '../service/penerima-kuasa.service';

@Component({
  standalone: true,
  templateUrl: './penerima-kuasa-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class PenerimaKuasaDeleteDialogComponent {
  penerimaKuasa?: IPenerimaKuasa;

  constructor(
    protected penerimaKuasaService: PenerimaKuasaService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.penerimaKuasaService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
