import dayjs from 'dayjs/esm';

import { IPenerimaKuasa, NewPenerimaKuasa } from './penerima-kuasa.model';

export const sampleWithRequiredData: IPenerimaKuasa = {
  id: 5705,
};

export const sampleWithPartialData: IPenerimaKuasa = {
  id: 16406,
  titel: 'pish mussel',
  nama: 'uselessly',
  komparisi: 'reluctantly plagiarize',
  tanggalSk: dayjs('2023-12-14'),
  recordStatus: 31806,
  updateBy: 'paste community',
  updateOn: dayjs('2023-12-14T10:17'),
};

export const sampleWithFullData: IPenerimaKuasa = {
  id: 5972,
  titel: 'inasmuch passionate qua',
  nama: 'exfoliate',
  komparisi: 'decent without brook',
  tts: 'cradle pish',
  sk: 'versus',
  tanggalSk: dayjs('2023-12-15'),
  noKontak: 'ellipse geez',
  recordStatus: 862,
  updateBy: 'untimely bite-sized gadzooks',
  updateOn: dayjs('2023-12-14T17:54'),
};

export const sampleWithNewData: NewPenerimaKuasa = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
