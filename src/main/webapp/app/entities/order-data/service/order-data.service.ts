import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOrderData, NewOrderData } from '../order-data.model';

export type PartialUpdateOrderData = Partial<IOrderData> & Pick<IOrderData, 'id'>;

type RestOf<T extends IOrderData | NewOrderData> = Omit<T, 'ppkTanggal' | 'tanggal'> & {
  ppkTanggal?: string | null;
  tanggal?: string | null;
};

export type RestOrderData = RestOf<IOrderData>;

export type NewRestOrderData = RestOf<NewOrderData>;

export type PartialUpdateRestOrderData = RestOf<PartialUpdateOrderData>;

export type EntityResponseType = HttpResponse<IOrderData>;
export type EntityArrayResponseType = HttpResponse<IOrderData[]>;

@Injectable({ providedIn: 'root' })
export class OrderDataService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/order-data');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(orderData: NewOrderData): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderData);
    return this.http
      .post<RestOrderData>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(orderData: IOrderData): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderData);
    return this.http
      .put<RestOrderData>(`${this.resourceUrl}/${this.getOrderDataIdentifier(orderData)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(orderData: PartialUpdateOrderData): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderData);
    return this.http
      .patch<RestOrderData>(`${this.resourceUrl}/${this.getOrderDataIdentifier(orderData)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestOrderData>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestOrderData[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getOrderDataIdentifier(orderData: Pick<IOrderData, 'id'>): number {
    return orderData.id;
  }

  compareOrderData(o1: Pick<IOrderData, 'id'> | null, o2: Pick<IOrderData, 'id'> | null): boolean {
    return o1 && o2 ? this.getOrderDataIdentifier(o1) === this.getOrderDataIdentifier(o2) : o1 === o2;
  }

  addOrderDataToCollectionIfMissing<Type extends Pick<IOrderData, 'id'>>(
    orderDataCollection: Type[],
    ...orderDataToCheck: (Type | null | undefined)[]
  ): Type[] {
    const orderData: Type[] = orderDataToCheck.filter(isPresent);
    if (orderData.length > 0) {
      const orderDataCollectionIdentifiers = orderDataCollection.map(orderDataItem => this.getOrderDataIdentifier(orderDataItem)!);
      const orderDataToAdd = orderData.filter(orderDataItem => {
        const orderDataIdentifier = this.getOrderDataIdentifier(orderDataItem);
        if (orderDataCollectionIdentifiers.includes(orderDataIdentifier)) {
          return false;
        }
        orderDataCollectionIdentifiers.push(orderDataIdentifier);
        return true;
      });
      return [...orderDataToAdd, ...orderDataCollection];
    }
    return orderDataCollection;
  }

  protected convertDateFromClient<T extends IOrderData | NewOrderData | PartialUpdateOrderData>(orderData: T): RestOf<T> {
    return {
      ...orderData,
      ppkTanggal: orderData.ppkTanggal?.format(DATE_FORMAT) ?? null,
      tanggal: orderData.tanggal?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restOrderData: RestOrderData): IOrderData {
    return {
      ...restOrderData,
      ppkTanggal: restOrderData.ppkTanggal ? dayjs(restOrderData.ppkTanggal) : undefined,
      tanggal: restOrderData.tanggal ? dayjs(restOrderData.tanggal) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestOrderData>): HttpResponse<IOrderData> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestOrderData[]>): HttpResponse<IOrderData[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
