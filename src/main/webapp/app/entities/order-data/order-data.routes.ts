import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { OrderDataComponent } from './list/order-data.component';
import { OrderDataDetailComponent } from './detail/order-data-detail.component';
import { OrderDataUpdateComponent } from './update/order-data-update.component';
import OrderDataResolve from './route/order-data-routing-resolve.service';

const orderDataRoute: Routes = [
  {
    path: '',
    component: OrderDataComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrderDataDetailComponent,
    resolve: {
      orderData: OrderDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrderDataUpdateComponent,
    resolve: {
      orderData: OrderDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrderDataUpdateComponent,
    resolve: {
      orderData: OrderDataResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default orderDataRoute;
