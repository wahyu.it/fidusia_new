import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { OrderDataDetailComponent } from './order-data-detail.component';

describe('OrderData Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OrderDataDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: OrderDataDetailComponent,
              resolve: { orderData: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(OrderDataDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load orderData on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', OrderDataDetailComponent);

      // THEN
      expect(instance.orderData).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
