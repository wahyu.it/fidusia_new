import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IOrderData, NewOrderData } from '../order-data.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IOrderData for edit and NewOrderDataFormGroupInput for create.
 */
type OrderDataFormGroupInput = IOrderData | PartialWithRequiredKeyOf<NewOrderData>;

type OrderDataFormDefaults = Pick<NewOrderData, 'id'>;

type OrderDataFormGroupContent = {
  id: FormControl<IOrderData['id'] | NewOrderData['id']>;
  ppkNomor: FormControl<IOrderData['ppkNomor']>;
  ppkTanggal: FormControl<IOrderData['ppkTanggal']>;
  nasabahNama: FormControl<IOrderData['nasabahNama']>;
  nasabahJenis: FormControl<IOrderData['nasabahJenis']>;
  tanggal: FormControl<IOrderData['tanggal']>;
  status: FormControl<IOrderData['status']>;
  type: FormControl<IOrderData['type']>;
  rawData: FormControl<IOrderData['rawData']>;
  skSubtitusi: FormControl<IOrderData['skSubtitusi']>;
  berkas: FormControl<IOrderData['berkas']>;
  cabang: FormControl<IOrderData['cabang']>;
};

export type OrderDataFormGroup = FormGroup<OrderDataFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class OrderDataFormService {
  createOrderDataFormGroup(orderData: OrderDataFormGroupInput = { id: null }): OrderDataFormGroup {
    const orderDataRawValue = {
      ...this.getFormDefaults(),
      ...orderData,
    };
    return new FormGroup<OrderDataFormGroupContent>({
      id: new FormControl(
        { value: orderDataRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      ppkNomor: new FormControl(orderDataRawValue.ppkNomor),
      ppkTanggal: new FormControl(orderDataRawValue.ppkTanggal),
      nasabahNama: new FormControl(orderDataRawValue.nasabahNama),
      nasabahJenis: new FormControl(orderDataRawValue.nasabahJenis),
      tanggal: new FormControl(orderDataRawValue.tanggal),
      status: new FormControl(orderDataRawValue.status),
      type: new FormControl(orderDataRawValue.type),
      rawData: new FormControl(orderDataRawValue.rawData),
      skSubtitusi: new FormControl(orderDataRawValue.skSubtitusi),
      berkas: new FormControl(orderDataRawValue.berkas),
      cabang: new FormControl(orderDataRawValue.cabang),
    });
  }

  getOrderData(form: OrderDataFormGroup): IOrderData | NewOrderData {
    return form.getRawValue() as IOrderData | NewOrderData;
  }

  resetForm(form: OrderDataFormGroup, orderData: OrderDataFormGroupInput): void {
    const orderDataRawValue = { ...this.getFormDefaults(), ...orderData };
    form.reset(
      {
        ...orderDataRawValue,
        id: { value: orderDataRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): OrderDataFormDefaults {
    return {
      id: null,
    };
  }
}
