import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IRawData } from 'app/entities/raw-data/raw-data.model';
import { RawDataService } from 'app/entities/raw-data/service/raw-data.service';
import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { SkSubtitusiService } from 'app/entities/sk-subtitusi/service/sk-subtitusi.service';
import { IBerkas } from 'app/entities/berkas/berkas.model';
import { BerkasService } from 'app/entities/berkas/service/berkas.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { IOrderData } from '../order-data.model';
import { OrderDataService } from '../service/order-data.service';
import { OrderDataFormService } from './order-data-form.service';

import { OrderDataUpdateComponent } from './order-data-update.component';

describe('OrderData Management Update Component', () => {
  let comp: OrderDataUpdateComponent;
  let fixture: ComponentFixture<OrderDataUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let orderDataFormService: OrderDataFormService;
  let orderDataService: OrderDataService;
  let rawDataService: RawDataService;
  let skSubtitusiService: SkSubtitusiService;
  let berkasService: BerkasService;
  let cabangService: CabangService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), OrderDataUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(OrderDataUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OrderDataUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    orderDataFormService = TestBed.inject(OrderDataFormService);
    orderDataService = TestBed.inject(OrderDataService);
    rawDataService = TestBed.inject(RawDataService);
    skSubtitusiService = TestBed.inject(SkSubtitusiService);
    berkasService = TestBed.inject(BerkasService);
    cabangService = TestBed.inject(CabangService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call RawData query and add missing value', () => {
      const orderData: IOrderData = { id: 456 };
      const rawData: IRawData = { id: 29254 };
      orderData.rawData = rawData;

      const rawDataCollection: IRawData[] = [{ id: 16939 }];
      jest.spyOn(rawDataService, 'query').mockReturnValue(of(new HttpResponse({ body: rawDataCollection })));
      const additionalRawData = [rawData];
      const expectedCollection: IRawData[] = [...additionalRawData, ...rawDataCollection];
      jest.spyOn(rawDataService, 'addRawDataToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderData });
      comp.ngOnInit();

      expect(rawDataService.query).toHaveBeenCalled();
      expect(rawDataService.addRawDataToCollectionIfMissing).toHaveBeenCalledWith(
        rawDataCollection,
        ...additionalRawData.map(expect.objectContaining),
      );
      expect(comp.rawDataSharedCollection).toEqual(expectedCollection);
    });

    it('Should call SkSubtitusi query and add missing value', () => {
      const orderData: IOrderData = { id: 456 };
      const skSubtitusi: ISkSubtitusi = { id: 17160 };
      orderData.skSubtitusi = skSubtitusi;

      const skSubtitusiCollection: ISkSubtitusi[] = [{ id: 28147 }];
      jest.spyOn(skSubtitusiService, 'query').mockReturnValue(of(new HttpResponse({ body: skSubtitusiCollection })));
      const additionalSkSubtitusis = [skSubtitusi];
      const expectedCollection: ISkSubtitusi[] = [...additionalSkSubtitusis, ...skSubtitusiCollection];
      jest.spyOn(skSubtitusiService, 'addSkSubtitusiToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderData });
      comp.ngOnInit();

      expect(skSubtitusiService.query).toHaveBeenCalled();
      expect(skSubtitusiService.addSkSubtitusiToCollectionIfMissing).toHaveBeenCalledWith(
        skSubtitusiCollection,
        ...additionalSkSubtitusis.map(expect.objectContaining),
      );
      expect(comp.skSubtitusisSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Berkas query and add missing value', () => {
      const orderData: IOrderData = { id: 456 };
      const berkas: IBerkas = { id: 21107 };
      orderData.berkas = berkas;

      const berkasCollection: IBerkas[] = [{ id: 14611 }];
      jest.spyOn(berkasService, 'query').mockReturnValue(of(new HttpResponse({ body: berkasCollection })));
      const additionalBerkas = [berkas];
      const expectedCollection: IBerkas[] = [...additionalBerkas, ...berkasCollection];
      jest.spyOn(berkasService, 'addBerkasToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderData });
      comp.ngOnInit();

      expect(berkasService.query).toHaveBeenCalled();
      expect(berkasService.addBerkasToCollectionIfMissing).toHaveBeenCalledWith(
        berkasCollection,
        ...additionalBerkas.map(expect.objectContaining),
      );
      expect(comp.berkasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Cabang query and add missing value', () => {
      const orderData: IOrderData = { id: 456 };
      const cabang: ICabang = { id: 15397 };
      orderData.cabang = cabang;

      const cabangCollection: ICabang[] = [{ id: 1194 }];
      jest.spyOn(cabangService, 'query').mockReturnValue(of(new HttpResponse({ body: cabangCollection })));
      const additionalCabangs = [cabang];
      const expectedCollection: ICabang[] = [...additionalCabangs, ...cabangCollection];
      jest.spyOn(cabangService, 'addCabangToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderData });
      comp.ngOnInit();

      expect(cabangService.query).toHaveBeenCalled();
      expect(cabangService.addCabangToCollectionIfMissing).toHaveBeenCalledWith(
        cabangCollection,
        ...additionalCabangs.map(expect.objectContaining),
      );
      expect(comp.cabangsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const orderData: IOrderData = { id: 456 };
      const rawData: IRawData = { id: 12950 };
      orderData.rawData = rawData;
      const skSubtitusi: ISkSubtitusi = { id: 30497 };
      orderData.skSubtitusi = skSubtitusi;
      const berkas: IBerkas = { id: 20528 };
      orderData.berkas = berkas;
      const cabang: ICabang = { id: 19442 };
      orderData.cabang = cabang;

      activatedRoute.data = of({ orderData });
      comp.ngOnInit();

      expect(comp.rawDataSharedCollection).toContain(rawData);
      expect(comp.skSubtitusisSharedCollection).toContain(skSubtitusi);
      expect(comp.berkasSharedCollection).toContain(berkas);
      expect(comp.cabangsSharedCollection).toContain(cabang);
      expect(comp.orderData).toEqual(orderData);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderData>>();
      const orderData = { id: 123 };
      jest.spyOn(orderDataFormService, 'getOrderData').mockReturnValue(orderData);
      jest.spyOn(orderDataService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderData });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orderData }));
      saveSubject.complete();

      // THEN
      expect(orderDataFormService.getOrderData).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(orderDataService.update).toHaveBeenCalledWith(expect.objectContaining(orderData));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderData>>();
      const orderData = { id: 123 };
      jest.spyOn(orderDataFormService, 'getOrderData').mockReturnValue({ id: null });
      jest.spyOn(orderDataService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderData: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orderData }));
      saveSubject.complete();

      // THEN
      expect(orderDataFormService.getOrderData).toHaveBeenCalled();
      expect(orderDataService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderData>>();
      const orderData = { id: 123 };
      jest.spyOn(orderDataService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderData });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(orderDataService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareRawData', () => {
      it('Should forward to rawDataService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(rawDataService, 'compareRawData');
        comp.compareRawData(entity, entity2);
        expect(rawDataService.compareRawData).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareSkSubtitusi', () => {
      it('Should forward to skSubtitusiService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(skSubtitusiService, 'compareSkSubtitusi');
        comp.compareSkSubtitusi(entity, entity2);
        expect(skSubtitusiService.compareSkSubtitusi).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareBerkas', () => {
      it('Should forward to berkasService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(berkasService, 'compareBerkas');
        comp.compareBerkas(entity, entity2);
        expect(berkasService.compareBerkas).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareCabang', () => {
      it('Should forward to cabangService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(cabangService, 'compareCabang');
        comp.compareCabang(entity, entity2);
        expect(cabangService.compareCabang).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
