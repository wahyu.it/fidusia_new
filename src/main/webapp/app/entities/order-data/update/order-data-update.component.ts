import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IRawData } from 'app/entities/raw-data/raw-data.model';
import { RawDataService } from 'app/entities/raw-data/service/raw-data.service';
import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { SkSubtitusiService } from 'app/entities/sk-subtitusi/service/sk-subtitusi.service';
import { IBerkas } from 'app/entities/berkas/berkas.model';
import { BerkasService } from 'app/entities/berkas/service/berkas.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { OrderDataService } from '../service/order-data.service';
import { IOrderData } from '../order-data.model';
import { OrderDataFormService, OrderDataFormGroup } from './order-data-form.service';

@Component({
  standalone: true,
  selector: 'jhi-order-data-update',
  templateUrl: './order-data-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class OrderDataUpdateComponent implements OnInit {
  isSaving = false;
  orderData: IOrderData | null = null;

  rawDataSharedCollection: IRawData[] = [];
  skSubtitusisSharedCollection: ISkSubtitusi[] = [];
  berkasSharedCollection: IBerkas[] = [];
  cabangsSharedCollection: ICabang[] = [];

  editForm: OrderDataFormGroup = this.orderDataFormService.createOrderDataFormGroup();

  constructor(
    protected orderDataService: OrderDataService,
    protected orderDataFormService: OrderDataFormService,
    protected rawDataService: RawDataService,
    protected skSubtitusiService: SkSubtitusiService,
    protected berkasService: BerkasService,
    protected cabangService: CabangService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareRawData = (o1: IRawData | null, o2: IRawData | null): boolean => this.rawDataService.compareRawData(o1, o2);

  compareSkSubtitusi = (o1: ISkSubtitusi | null, o2: ISkSubtitusi | null): boolean => this.skSubtitusiService.compareSkSubtitusi(o1, o2);

  compareBerkas = (o1: IBerkas | null, o2: IBerkas | null): boolean => this.berkasService.compareBerkas(o1, o2);

  compareCabang = (o1: ICabang | null, o2: ICabang | null): boolean => this.cabangService.compareCabang(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderData }) => {
      this.orderData = orderData;
      if (orderData) {
        this.updateForm(orderData);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderData = this.orderDataFormService.getOrderData(this.editForm);
    if (orderData.id !== null) {
      this.subscribeToSaveResponse(this.orderDataService.update(orderData));
    } else {
      this.subscribeToSaveResponse(this.orderDataService.create(orderData));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderData>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(orderData: IOrderData): void {
    this.orderData = orderData;
    this.orderDataFormService.resetForm(this.editForm, orderData);

    this.rawDataSharedCollection = this.rawDataService.addRawDataToCollectionIfMissing<IRawData>(
      this.rawDataSharedCollection,
      orderData.rawData,
    );
    this.skSubtitusisSharedCollection = this.skSubtitusiService.addSkSubtitusiToCollectionIfMissing<ISkSubtitusi>(
      this.skSubtitusisSharedCollection,
      orderData.skSubtitusi,
    );
    this.berkasSharedCollection = this.berkasService.addBerkasToCollectionIfMissing<IBerkas>(this.berkasSharedCollection, orderData.berkas);
    this.cabangsSharedCollection = this.cabangService.addCabangToCollectionIfMissing<ICabang>(
      this.cabangsSharedCollection,
      orderData.cabang,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.rawDataService
      .query()
      .pipe(map((res: HttpResponse<IRawData[]>) => res.body ?? []))
      .pipe(map((rawData: IRawData[]) => this.rawDataService.addRawDataToCollectionIfMissing<IRawData>(rawData, this.orderData?.rawData)))
      .subscribe((rawData: IRawData[]) => (this.rawDataSharedCollection = rawData));

    this.skSubtitusiService
      .query()
      .pipe(map((res: HttpResponse<ISkSubtitusi[]>) => res.body ?? []))
      .pipe(
        map((skSubtitusis: ISkSubtitusi[]) =>
          this.skSubtitusiService.addSkSubtitusiToCollectionIfMissing<ISkSubtitusi>(skSubtitusis, this.orderData?.skSubtitusi),
        ),
      )
      .subscribe((skSubtitusis: ISkSubtitusi[]) => (this.skSubtitusisSharedCollection = skSubtitusis));

    this.berkasService
      .query()
      .pipe(map((res: HttpResponse<IBerkas[]>) => res.body ?? []))
      .pipe(map((berkas: IBerkas[]) => this.berkasService.addBerkasToCollectionIfMissing<IBerkas>(berkas, this.orderData?.berkas)))
      .subscribe((berkas: IBerkas[]) => (this.berkasSharedCollection = berkas));

    this.cabangService
      .query()
      .pipe(map((res: HttpResponse<ICabang[]>) => res.body ?? []))
      .pipe(map((cabangs: ICabang[]) => this.cabangService.addCabangToCollectionIfMissing<ICabang>(cabangs, this.orderData?.cabang)))
      .subscribe((cabangs: ICabang[]) => (this.cabangsSharedCollection = cabangs));
  }
}
