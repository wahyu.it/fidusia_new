import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOrderData } from '../order-data.model';
import { OrderDataService } from '../service/order-data.service';

export const orderDataResolve = (route: ActivatedRouteSnapshot): Observable<null | IOrderData> => {
  const id = route.params['id'];
  if (id) {
    return inject(OrderDataService)
      .find(id)
      .pipe(
        mergeMap((orderData: HttpResponse<IOrderData>) => {
          if (orderData.body) {
            return of(orderData.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default orderDataResolve;
