import dayjs from 'dayjs/esm';

import { IOrderData, NewOrderData } from './order-data.model';

export const sampleWithRequiredData: IOrderData = {
  id: 20027,
};

export const sampleWithPartialData: IOrderData = {
  id: 2838,
  ppkTanggal: dayjs('2023-12-14'),
  nasabahNama: 'doting lovingly interval',
  status: 20717,
};

export const sampleWithFullData: IOrderData = {
  id: 581,
  ppkNomor: 'even lovingly for',
  ppkTanggal: dayjs('2023-12-14'),
  nasabahNama: 'apud handmade briefing',
  nasabahJenis: 8760,
  tanggal: dayjs('2023-12-14'),
  status: 16554,
  type: 4278,
};

export const sampleWithNewData: NewOrderData = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
