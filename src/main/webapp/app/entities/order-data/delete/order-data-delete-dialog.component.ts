import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IOrderData } from '../order-data.model';
import { OrderDataService } from '../service/order-data.service';

@Component({
  standalone: true,
  templateUrl: './order-data-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class OrderDataDeleteDialogComponent {
  orderData?: IOrderData;

  constructor(
    protected orderDataService: OrderDataService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orderDataService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
