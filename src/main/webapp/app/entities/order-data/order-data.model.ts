import dayjs from 'dayjs/esm';
import { IRawData } from 'app/entities/raw-data/raw-data.model';
import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { IBerkas } from 'app/entities/berkas/berkas.model';
import { ICabang } from 'app/entities/cabang/cabang.model';

export interface IOrderData {
  id: number;
  ppkNomor?: string | null;
  ppkTanggal?: dayjs.Dayjs | null;
  nasabahNama?: string | null;
  nasabahJenis?: number | null;
  tanggal?: dayjs.Dayjs | null;
  status?: number | null;
  type?: number | null;
  rawData?: IRawData | null;
  skSubtitusi?: ISkSubtitusi | null;
  berkas?: IBerkas | null;
  cabang?: ICabang | null;
}

export type NewOrderData = Omit<IOrderData, 'id'> & { id: null };
