import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';
import { PenerimaKuasaService } from 'app/entities/penerima-kuasa/service/penerima-kuasa.service';
import { NotarisService } from '../service/notaris.service';
import { INotaris } from '../notaris.model';
import { NotarisFormService } from './notaris-form.service';

import { NotarisUpdateComponent } from './notaris-update.component';

describe('Notaris Management Update Component', () => {
  let comp: NotarisUpdateComponent;
  let fixture: ComponentFixture<NotarisUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let notarisFormService: NotarisFormService;
  let notarisService: NotarisService;
  let penerimaKuasaService: PenerimaKuasaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), NotarisUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(NotarisUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NotarisUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    notarisFormService = TestBed.inject(NotarisFormService);
    notarisService = TestBed.inject(NotarisService);
    penerimaKuasaService = TestBed.inject(PenerimaKuasaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call penerimaKuasa query and add missing value', () => {
      const notaris: INotaris = { id: 456 };
      const penerimaKuasa: IPenerimaKuasa = { id: 22589 };
      notaris.penerimaKuasa = penerimaKuasa;

      const penerimaKuasaCollection: IPenerimaKuasa[] = [{ id: 2892 }];
      jest.spyOn(penerimaKuasaService, 'query').mockReturnValue(of(new HttpResponse({ body: penerimaKuasaCollection })));
      const expectedCollection: IPenerimaKuasa[] = [penerimaKuasa, ...penerimaKuasaCollection];
      jest.spyOn(penerimaKuasaService, 'addPenerimaKuasaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ notaris });
      comp.ngOnInit();

      expect(penerimaKuasaService.query).toHaveBeenCalled();
      expect(penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing).toHaveBeenCalledWith(penerimaKuasaCollection, penerimaKuasa);
      expect(comp.penerimaKuasasCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const notaris: INotaris = { id: 456 };
      const penerimaKuasa: IPenerimaKuasa = { id: 16793 };
      notaris.penerimaKuasa = penerimaKuasa;

      activatedRoute.data = of({ notaris });
      comp.ngOnInit();

      expect(comp.penerimaKuasasCollection).toContain(penerimaKuasa);
      expect(comp.notaris).toEqual(notaris);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INotaris>>();
      const notaris = { id: 123 };
      jest.spyOn(notarisFormService, 'getNotaris').mockReturnValue(notaris);
      jest.spyOn(notarisService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ notaris });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: notaris }));
      saveSubject.complete();

      // THEN
      expect(notarisFormService.getNotaris).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(notarisService.update).toHaveBeenCalledWith(expect.objectContaining(notaris));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INotaris>>();
      const notaris = { id: 123 };
      jest.spyOn(notarisFormService, 'getNotaris').mockReturnValue({ id: null });
      jest.spyOn(notarisService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ notaris: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: notaris }));
      saveSubject.complete();

      // THEN
      expect(notarisFormService.getNotaris).toHaveBeenCalled();
      expect(notarisService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INotaris>>();
      const notaris = { id: 123 };
      jest.spyOn(notarisService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ notaris });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(notarisService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePenerimaKuasa', () => {
      it('Should forward to penerimaKuasaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(penerimaKuasaService, 'comparePenerimaKuasa');
        comp.comparePenerimaKuasa(entity, entity2);
        expect(penerimaKuasaService.comparePenerimaKuasa).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
