import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../notaris.test-samples';

import { NotarisFormService } from './notaris-form.service';

describe('Notaris Form Service', () => {
  let service: NotarisFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotarisFormService);
  });

  describe('Service methods', () => {
    describe('createNotarisFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createNotarisFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nama: expect.any(Object),
            namaShort: expect.any(Object),
            titelShortOne: expect.any(Object),
            titelShortTwo: expect.any(Object),
            titelLongOne: expect.any(Object),
            titelLongTwo: expect.any(Object),
            wilayahKerja: expect.any(Object),
            kedudukan: expect.any(Object),
            sk: expect.any(Object),
            tglSk: expect.any(Object),
            jenisHariKerja: expect.any(Object),
            jamKerjaAwalOne: expect.any(Object),
            jamKerjaAkhirOne: expect.any(Object),
            jamKerjaAwalTwo: expect.any(Object),
            jamKerjaAkhirTwo: expect.any(Object),
            selisihPukul: expect.any(Object),
            jamRestAwalOne: expect.any(Object),
            jamRestAkhirOne: expect.any(Object),
            jamRestAwalTwo: expect.any(Object),
            jamRestAkhirTwo: expect.any(Object),
            maksAkta: expect.any(Object),
            maksOrderHarian: expect.any(Object),
            lembarSk: expect.any(Object),
            lembarSumpah: expect.any(Object),
            alamatKantor: expect.any(Object),
            alamatEmail: expect.any(Object),
            noKontak: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            penerimaKuasa: expect.any(Object),
          }),
        );
      });

      it('passing INotaris should create a new form with FormGroup', () => {
        const formGroup = service.createNotarisFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nama: expect.any(Object),
            namaShort: expect.any(Object),
            titelShortOne: expect.any(Object),
            titelShortTwo: expect.any(Object),
            titelLongOne: expect.any(Object),
            titelLongTwo: expect.any(Object),
            wilayahKerja: expect.any(Object),
            kedudukan: expect.any(Object),
            sk: expect.any(Object),
            tglSk: expect.any(Object),
            jenisHariKerja: expect.any(Object),
            jamKerjaAwalOne: expect.any(Object),
            jamKerjaAkhirOne: expect.any(Object),
            jamKerjaAwalTwo: expect.any(Object),
            jamKerjaAkhirTwo: expect.any(Object),
            selisihPukul: expect.any(Object),
            jamRestAwalOne: expect.any(Object),
            jamRestAkhirOne: expect.any(Object),
            jamRestAwalTwo: expect.any(Object),
            jamRestAkhirTwo: expect.any(Object),
            maksAkta: expect.any(Object),
            maksOrderHarian: expect.any(Object),
            lembarSk: expect.any(Object),
            lembarSumpah: expect.any(Object),
            alamatKantor: expect.any(Object),
            alamatEmail: expect.any(Object),
            noKontak: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            penerimaKuasa: expect.any(Object),
          }),
        );
      });
    });

    describe('getNotaris', () => {
      it('should return NewNotaris for default Notaris initial value', () => {
        const formGroup = service.createNotarisFormGroup(sampleWithNewData);

        const notaris = service.getNotaris(formGroup) as any;

        expect(notaris).toMatchObject(sampleWithNewData);
      });

      it('should return NewNotaris for empty Notaris initial value', () => {
        const formGroup = service.createNotarisFormGroup();

        const notaris = service.getNotaris(formGroup) as any;

        expect(notaris).toMatchObject({});
      });

      it('should return INotaris', () => {
        const formGroup = service.createNotarisFormGroup(sampleWithRequiredData);

        const notaris = service.getNotaris(formGroup) as any;

        expect(notaris).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing INotaris should not enable id FormControl', () => {
        const formGroup = service.createNotarisFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewNotaris should disable id FormControl', () => {
        const formGroup = service.createNotarisFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
