import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';
import { PenerimaKuasaService } from 'app/entities/penerima-kuasa/service/penerima-kuasa.service';
import { INotaris } from '../notaris.model';
import { NotarisService } from '../service/notaris.service';
import { NotarisFormService, NotarisFormGroup } from './notaris-form.service';

@Component({
  standalone: true,
  selector: 'jhi-notaris-update',
  templateUrl: './notaris-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class NotarisUpdateComponent implements OnInit {
  isSaving = false;
  notaris: INotaris | null = null;

  penerimaKuasasCollection: IPenerimaKuasa[] = [];

  editForm: NotarisFormGroup = this.notarisFormService.createNotarisFormGroup();

  constructor(
    protected notarisService: NotarisService,
    protected notarisFormService: NotarisFormService,
    protected penerimaKuasaService: PenerimaKuasaService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  comparePenerimaKuasa = (o1: IPenerimaKuasa | null, o2: IPenerimaKuasa | null): boolean =>
    this.penerimaKuasaService.comparePenerimaKuasa(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ notaris }) => {
      this.notaris = notaris;
      if (notaris) {
        this.updateForm(notaris);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const notaris = this.notarisFormService.getNotaris(this.editForm);
    if (notaris.id !== null) {
      this.subscribeToSaveResponse(this.notarisService.update(notaris));
    } else {
      this.subscribeToSaveResponse(this.notarisService.create(notaris));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INotaris>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(notaris: INotaris): void {
    this.notaris = notaris;
    this.notarisFormService.resetForm(this.editForm, notaris);

    this.penerimaKuasasCollection = this.penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing<IPenerimaKuasa>(
      this.penerimaKuasasCollection,
      notaris.penerimaKuasa,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.penerimaKuasaService
      .query({ filter: 'notaris-is-null' })
      .pipe(map((res: HttpResponse<IPenerimaKuasa[]>) => res.body ?? []))
      .pipe(
        map((penerimaKuasas: IPenerimaKuasa[]) =>
          this.penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing<IPenerimaKuasa>(penerimaKuasas, this.notaris?.penerimaKuasa),
        ),
      )
      .subscribe((penerimaKuasas: IPenerimaKuasa[]) => (this.penerimaKuasasCollection = penerimaKuasas));
  }
}
