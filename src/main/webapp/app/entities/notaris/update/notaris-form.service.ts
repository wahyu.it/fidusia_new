import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { INotaris, NewNotaris } from '../notaris.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts INotaris for edit and NewNotarisFormGroupInput for create.
 */
type NotarisFormGroupInput = INotaris | PartialWithRequiredKeyOf<NewNotaris>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends INotaris | NewNotaris> = Omit<
  T,
  | 'jamKerjaAwalOne'
  | 'jamKerjaAkhirOne'
  | 'jamKerjaAwalTwo'
  | 'jamKerjaAkhirTwo'
  | 'jamRestAwalOne'
  | 'jamRestAkhirOne'
  | 'jamRestAwalTwo'
  | 'jamRestAkhirTwo'
  | 'updateOn'
> & {
  jamKerjaAwalOne?: string | null;
  jamKerjaAkhirOne?: string | null;
  jamKerjaAwalTwo?: string | null;
  jamKerjaAkhirTwo?: string | null;
  jamRestAwalOne?: string | null;
  jamRestAkhirOne?: string | null;
  jamRestAwalTwo?: string | null;
  jamRestAkhirTwo?: string | null;
  updateOn?: string | null;
};

type NotarisFormRawValue = FormValueOf<INotaris>;

type NewNotarisFormRawValue = FormValueOf<NewNotaris>;

type NotarisFormDefaults = Pick<
  NewNotaris,
  | 'id'
  | 'jamKerjaAwalOne'
  | 'jamKerjaAkhirOne'
  | 'jamKerjaAwalTwo'
  | 'jamKerjaAkhirTwo'
  | 'jamRestAwalOne'
  | 'jamRestAkhirOne'
  | 'jamRestAwalTwo'
  | 'jamRestAkhirTwo'
  | 'updateOn'
>;

type NotarisFormGroupContent = {
  id: FormControl<NotarisFormRawValue['id'] | NewNotaris['id']>;
  nama: FormControl<NotarisFormRawValue['nama']>;
  namaShort: FormControl<NotarisFormRawValue['namaShort']>;
  titelShortOne: FormControl<NotarisFormRawValue['titelShortOne']>;
  titelShortTwo: FormControl<NotarisFormRawValue['titelShortTwo']>;
  titelLongOne: FormControl<NotarisFormRawValue['titelLongOne']>;
  titelLongTwo: FormControl<NotarisFormRawValue['titelLongTwo']>;
  wilayahKerja: FormControl<NotarisFormRawValue['wilayahKerja']>;
  kedudukan: FormControl<NotarisFormRawValue['kedudukan']>;
  sk: FormControl<NotarisFormRawValue['sk']>;
  tglSk: FormControl<NotarisFormRawValue['tglSk']>;
  jenisHariKerja: FormControl<NotarisFormRawValue['jenisHariKerja']>;
  jamKerjaAwalOne: FormControl<NotarisFormRawValue['jamKerjaAwalOne']>;
  jamKerjaAkhirOne: FormControl<NotarisFormRawValue['jamKerjaAkhirOne']>;
  jamKerjaAwalTwo: FormControl<NotarisFormRawValue['jamKerjaAwalTwo']>;
  jamKerjaAkhirTwo: FormControl<NotarisFormRawValue['jamKerjaAkhirTwo']>;
  selisihPukul: FormControl<NotarisFormRawValue['selisihPukul']>;
  jamRestAwalOne: FormControl<NotarisFormRawValue['jamRestAwalOne']>;
  jamRestAkhirOne: FormControl<NotarisFormRawValue['jamRestAkhirOne']>;
  jamRestAwalTwo: FormControl<NotarisFormRawValue['jamRestAwalTwo']>;
  jamRestAkhirTwo: FormControl<NotarisFormRawValue['jamRestAkhirTwo']>;
  maksAkta: FormControl<NotarisFormRawValue['maksAkta']>;
  maksOrderHarian: FormControl<NotarisFormRawValue['maksOrderHarian']>;
  lembarSk: FormControl<NotarisFormRawValue['lembarSk']>;
  lembarSumpah: FormControl<NotarisFormRawValue['lembarSumpah']>;
  alamatKantor: FormControl<NotarisFormRawValue['alamatKantor']>;
  alamatEmail: FormControl<NotarisFormRawValue['alamatEmail']>;
  noKontak: FormControl<NotarisFormRawValue['noKontak']>;
  recordStatus: FormControl<NotarisFormRawValue['recordStatus']>;
  updateBy: FormControl<NotarisFormRawValue['updateBy']>;
  updateOn: FormControl<NotarisFormRawValue['updateOn']>;
  penerimaKuasa: FormControl<NotarisFormRawValue['penerimaKuasa']>;
};

export type NotarisFormGroup = FormGroup<NotarisFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class NotarisFormService {
  createNotarisFormGroup(notaris: NotarisFormGroupInput = { id: null }): NotarisFormGroup {
    const notarisRawValue = this.convertNotarisToNotarisRawValue({
      ...this.getFormDefaults(),
      ...notaris,
    });
    return new FormGroup<NotarisFormGroupContent>({
      id: new FormControl(
        { value: notarisRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      nama: new FormControl(notarisRawValue.nama),
      namaShort: new FormControl(notarisRawValue.namaShort),
      titelShortOne: new FormControl(notarisRawValue.titelShortOne),
      titelShortTwo: new FormControl(notarisRawValue.titelShortTwo),
      titelLongOne: new FormControl(notarisRawValue.titelLongOne),
      titelLongTwo: new FormControl(notarisRawValue.titelLongTwo),
      wilayahKerja: new FormControl(notarisRawValue.wilayahKerja),
      kedudukan: new FormControl(notarisRawValue.kedudukan),
      sk: new FormControl(notarisRawValue.sk),
      tglSk: new FormControl(notarisRawValue.tglSk),
      jenisHariKerja: new FormControl(notarisRawValue.jenisHariKerja),
      jamKerjaAwalOne: new FormControl(notarisRawValue.jamKerjaAwalOne),
      jamKerjaAkhirOne: new FormControl(notarisRawValue.jamKerjaAkhirOne),
      jamKerjaAwalTwo: new FormControl(notarisRawValue.jamKerjaAwalTwo),
      jamKerjaAkhirTwo: new FormControl(notarisRawValue.jamKerjaAkhirTwo),
      selisihPukul: new FormControl(notarisRawValue.selisihPukul),
      jamRestAwalOne: new FormControl(notarisRawValue.jamRestAwalOne),
      jamRestAkhirOne: new FormControl(notarisRawValue.jamRestAkhirOne),
      jamRestAwalTwo: new FormControl(notarisRawValue.jamRestAwalTwo),
      jamRestAkhirTwo: new FormControl(notarisRawValue.jamRestAkhirTwo),
      maksAkta: new FormControl(notarisRawValue.maksAkta),
      maksOrderHarian: new FormControl(notarisRawValue.maksOrderHarian),
      lembarSk: new FormControl(notarisRawValue.lembarSk),
      lembarSumpah: new FormControl(notarisRawValue.lembarSumpah),
      alamatKantor: new FormControl(notarisRawValue.alamatKantor),
      alamatEmail: new FormControl(notarisRawValue.alamatEmail),
      noKontak: new FormControl(notarisRawValue.noKontak),
      recordStatus: new FormControl(notarisRawValue.recordStatus),
      updateBy: new FormControl(notarisRawValue.updateBy),
      updateOn: new FormControl(notarisRawValue.updateOn),
      penerimaKuasa: new FormControl(notarisRawValue.penerimaKuasa),
    });
  }

  getNotaris(form: NotarisFormGroup): INotaris | NewNotaris {
    return this.convertNotarisRawValueToNotaris(form.getRawValue() as NotarisFormRawValue | NewNotarisFormRawValue);
  }

  resetForm(form: NotarisFormGroup, notaris: NotarisFormGroupInput): void {
    const notarisRawValue = this.convertNotarisToNotarisRawValue({ ...this.getFormDefaults(), ...notaris });
    form.reset(
      {
        ...notarisRawValue,
        id: { value: notarisRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): NotarisFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      jamKerjaAwalOne: currentTime,
      jamKerjaAkhirOne: currentTime,
      jamKerjaAwalTwo: currentTime,
      jamKerjaAkhirTwo: currentTime,
      jamRestAwalOne: currentTime,
      jamRestAkhirOne: currentTime,
      jamRestAwalTwo: currentTime,
      jamRestAkhirTwo: currentTime,
      updateOn: currentTime,
    };
  }

  private convertNotarisRawValueToNotaris(rawNotaris: NotarisFormRawValue | NewNotarisFormRawValue): INotaris | NewNotaris {
    return {
      ...rawNotaris,
      jamKerjaAwalOne: dayjs(rawNotaris.jamKerjaAwalOne, DATE_TIME_FORMAT),
      jamKerjaAkhirOne: dayjs(rawNotaris.jamKerjaAkhirOne, DATE_TIME_FORMAT),
      jamKerjaAwalTwo: dayjs(rawNotaris.jamKerjaAwalTwo, DATE_TIME_FORMAT),
      jamKerjaAkhirTwo: dayjs(rawNotaris.jamKerjaAkhirTwo, DATE_TIME_FORMAT),
      jamRestAwalOne: dayjs(rawNotaris.jamRestAwalOne, DATE_TIME_FORMAT),
      jamRestAkhirOne: dayjs(rawNotaris.jamRestAkhirOne, DATE_TIME_FORMAT),
      jamRestAwalTwo: dayjs(rawNotaris.jamRestAwalTwo, DATE_TIME_FORMAT),
      jamRestAkhirTwo: dayjs(rawNotaris.jamRestAkhirTwo, DATE_TIME_FORMAT),
      updateOn: dayjs(rawNotaris.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertNotarisToNotarisRawValue(
    notaris: INotaris | (Partial<NewNotaris> & NotarisFormDefaults),
  ): NotarisFormRawValue | PartialWithRequiredKeyOf<NewNotarisFormRawValue> {
    return {
      ...notaris,
      jamKerjaAwalOne: notaris.jamKerjaAwalOne ? notaris.jamKerjaAwalOne.format(DATE_TIME_FORMAT) : undefined,
      jamKerjaAkhirOne: notaris.jamKerjaAkhirOne ? notaris.jamKerjaAkhirOne.format(DATE_TIME_FORMAT) : undefined,
      jamKerjaAwalTwo: notaris.jamKerjaAwalTwo ? notaris.jamKerjaAwalTwo.format(DATE_TIME_FORMAT) : undefined,
      jamKerjaAkhirTwo: notaris.jamKerjaAkhirTwo ? notaris.jamKerjaAkhirTwo.format(DATE_TIME_FORMAT) : undefined,
      jamRestAwalOne: notaris.jamRestAwalOne ? notaris.jamRestAwalOne.format(DATE_TIME_FORMAT) : undefined,
      jamRestAkhirOne: notaris.jamRestAkhirOne ? notaris.jamRestAkhirOne.format(DATE_TIME_FORMAT) : undefined,
      jamRestAwalTwo: notaris.jamRestAwalTwo ? notaris.jamRestAwalTwo.format(DATE_TIME_FORMAT) : undefined,
      jamRestAkhirTwo: notaris.jamRestAkhirTwo ? notaris.jamRestAkhirTwo.format(DATE_TIME_FORMAT) : undefined,
      updateOn: notaris.updateOn ? notaris.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
