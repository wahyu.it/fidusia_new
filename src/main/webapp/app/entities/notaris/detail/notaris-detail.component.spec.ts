import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { NotarisDetailComponent } from './notaris-detail.component';

describe('Notaris Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NotarisDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: NotarisDetailComponent,
              resolve: { notaris: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(NotarisDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load notaris on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', NotarisDetailComponent);

      // THEN
      expect(instance.notaris).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
