import dayjs from 'dayjs/esm';

import { INotaris, NewNotaris } from './notaris.model';

export const sampleWithRequiredData: INotaris = {
  id: 17500,
};

export const sampleWithPartialData: INotaris = {
  id: 3398,
  nama: 'at swim',
  titelShortTwo: 'during',
  titelLongOne: 'so imaginary frigid',
  kedudukan: 'than',
  tglSk: dayjs('2023-12-14'),
  jamKerjaAwalOne: dayjs('2023-12-14T18:56'),
  jamKerjaAkhirTwo: dayjs('2023-12-15T00:55'),
  selisihPukul: 7698,
  jamRestAkhirTwo: dayjs('2023-12-14T02:44'),
  maksOrderHarian: 29001,
  lembarSk: 'because',
  lembarSumpah: 'educated however gee',
  alamatKantor: 'inside',
  recordStatus: 12879,
};

export const sampleWithFullData: INotaris = {
  id: 8603,
  nama: 'except',
  namaShort: 'tamale',
  titelShortOne: 'accompanist by',
  titelShortTwo: 'concerning audit',
  titelLongOne: 'qua quietly',
  titelLongTwo: 'out terribly before',
  wilayahKerja: 'yum objection ack',
  kedudukan: 'trek advantage',
  sk: 'through',
  tglSk: dayjs('2023-12-14'),
  jenisHariKerja: 9187,
  jamKerjaAwalOne: dayjs('2023-12-14T20:50'),
  jamKerjaAkhirOne: dayjs('2023-12-14T22:02'),
  jamKerjaAwalTwo: dayjs('2023-12-14T05:48'),
  jamKerjaAkhirTwo: dayjs('2023-12-14T09:35'),
  selisihPukul: 13485,
  jamRestAwalOne: dayjs('2023-12-14T08:41'),
  jamRestAkhirOne: dayjs('2023-12-14T19:20'),
  jamRestAwalTwo: dayjs('2023-12-15T00:24'),
  jamRestAkhirTwo: dayjs('2023-12-14T10:27'),
  maksAkta: 20296,
  maksOrderHarian: 22567,
  lembarSk: 'actually',
  lembarSumpah: 'far',
  alamatKantor: 'muted blah',
  alamatEmail: 'italicise anti spiral',
  noKontak: 'provided once seldom',
  recordStatus: 23284,
  updateBy: 'really indelible',
  updateOn: dayjs('2023-12-14T17:28'),
};

export const sampleWithNewData: NewNotaris = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
