import dayjs from 'dayjs/esm';
import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';

export interface INotaris {
  id: number;
  nama?: string | null;
  namaShort?: string | null;
  titelShortOne?: string | null;
  titelShortTwo?: string | null;
  titelLongOne?: string | null;
  titelLongTwo?: string | null;
  wilayahKerja?: string | null;
  kedudukan?: string | null;
  sk?: string | null;
  tglSk?: dayjs.Dayjs | null;
  jenisHariKerja?: number | null;
  jamKerjaAwalOne?: dayjs.Dayjs | null;
  jamKerjaAkhirOne?: dayjs.Dayjs | null;
  jamKerjaAwalTwo?: dayjs.Dayjs | null;
  jamKerjaAkhirTwo?: dayjs.Dayjs | null;
  selisihPukul?: number | null;
  jamRestAwalOne?: dayjs.Dayjs | null;
  jamRestAkhirOne?: dayjs.Dayjs | null;
  jamRestAwalTwo?: dayjs.Dayjs | null;
  jamRestAkhirTwo?: dayjs.Dayjs | null;
  maksAkta?: number | null;
  maksOrderHarian?: number | null;
  lembarSk?: string | null;
  lembarSumpah?: string | null;
  alamatKantor?: string | null;
  alamatEmail?: string | null;
  noKontak?: string | null;
  recordStatus?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  penerimaKuasa?: IPenerimaKuasa | null;
}

export type NewNotaris = Omit<INotaris, 'id'> & { id: null };
