import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INotaris } from '../notaris.model';
import { NotarisService } from '../service/notaris.service';

export const notarisResolve = (route: ActivatedRouteSnapshot): Observable<null | INotaris> => {
  const id = route.params['id'];
  if (id) {
    return inject(NotarisService)
      .find(id)
      .pipe(
        mergeMap((notaris: HttpResponse<INotaris>) => {
          if (notaris.body) {
            return of(notaris.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default notarisResolve;
