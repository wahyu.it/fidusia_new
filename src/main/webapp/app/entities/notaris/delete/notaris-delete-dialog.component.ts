import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { INotaris } from '../notaris.model';
import { NotarisService } from '../service/notaris.service';

@Component({
  standalone: true,
  templateUrl: './notaris-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class NotarisDeleteDialogComponent {
  notaris?: INotaris;

  constructor(
    protected notarisService: NotarisService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.notarisService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
