import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { NotarisComponent } from './list/notaris.component';
import { NotarisDetailComponent } from './detail/notaris-detail.component';
import { NotarisUpdateComponent } from './update/notaris-update.component';
import NotarisResolve from './route/notaris-routing-resolve.service';

const notarisRoute: Routes = [
  {
    path: '',
    component: NotarisComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NotarisDetailComponent,
    resolve: {
      notaris: NotarisResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NotarisUpdateComponent,
    resolve: {
      notaris: NotarisResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NotarisUpdateComponent,
    resolve: {
      notaris: NotarisResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default notarisRoute;
