import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { INotaris } from '../notaris.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../notaris.test-samples';

import { NotarisService, RestNotaris } from './notaris.service';

const requireRestSample: RestNotaris = {
  ...sampleWithRequiredData,
  tglSk: sampleWithRequiredData.tglSk?.format(DATE_FORMAT),
  jamKerjaAwalOne: sampleWithRequiredData.jamKerjaAwalOne?.toJSON(),
  jamKerjaAkhirOne: sampleWithRequiredData.jamKerjaAkhirOne?.toJSON(),
  jamKerjaAwalTwo: sampleWithRequiredData.jamKerjaAwalTwo?.toJSON(),
  jamKerjaAkhirTwo: sampleWithRequiredData.jamKerjaAkhirTwo?.toJSON(),
  jamRestAwalOne: sampleWithRequiredData.jamRestAwalOne?.toJSON(),
  jamRestAkhirOne: sampleWithRequiredData.jamRestAkhirOne?.toJSON(),
  jamRestAwalTwo: sampleWithRequiredData.jamRestAwalTwo?.toJSON(),
  jamRestAkhirTwo: sampleWithRequiredData.jamRestAkhirTwo?.toJSON(),
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
};

describe('Notaris Service', () => {
  let service: NotarisService;
  let httpMock: HttpTestingController;
  let expectedResult: INotaris | INotaris[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NotarisService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Notaris', () => {
      const notaris = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(notaris).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Notaris', () => {
      const notaris = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(notaris).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Notaris', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Notaris', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Notaris', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addNotarisToCollectionIfMissing', () => {
      it('should add a Notaris to an empty array', () => {
        const notaris: INotaris = sampleWithRequiredData;
        expectedResult = service.addNotarisToCollectionIfMissing([], notaris);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(notaris);
      });

      it('should not add a Notaris to an array that contains it', () => {
        const notaris: INotaris = sampleWithRequiredData;
        const notarisCollection: INotaris[] = [
          {
            ...notaris,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addNotarisToCollectionIfMissing(notarisCollection, notaris);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Notaris to an array that doesn't contain it", () => {
        const notaris: INotaris = sampleWithRequiredData;
        const notarisCollection: INotaris[] = [sampleWithPartialData];
        expectedResult = service.addNotarisToCollectionIfMissing(notarisCollection, notaris);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(notaris);
      });

      it('should add only unique Notaris to an array', () => {
        const notarisArray: INotaris[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const notarisCollection: INotaris[] = [sampleWithRequiredData];
        expectedResult = service.addNotarisToCollectionIfMissing(notarisCollection, ...notarisArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const notaris: INotaris = sampleWithRequiredData;
        const notaris2: INotaris = sampleWithPartialData;
        expectedResult = service.addNotarisToCollectionIfMissing([], notaris, notaris2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(notaris);
        expect(expectedResult).toContain(notaris2);
      });

      it('should accept null and undefined values', () => {
        const notaris: INotaris = sampleWithRequiredData;
        expectedResult = service.addNotarisToCollectionIfMissing([], null, notaris, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(notaris);
      });

      it('should return initial array if no Notaris is added', () => {
        const notarisCollection: INotaris[] = [sampleWithRequiredData];
        expectedResult = service.addNotarisToCollectionIfMissing(notarisCollection, undefined, null);
        expect(expectedResult).toEqual(notarisCollection);
      });
    });

    describe('compareNotaris', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareNotaris(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareNotaris(entity1, entity2);
        const compareResult2 = service.compareNotaris(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareNotaris(entity1, entity2);
        const compareResult2 = service.compareNotaris(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareNotaris(entity1, entity2);
        const compareResult2 = service.compareNotaris(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
