import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INotaris, NewNotaris } from '../notaris.model';

export type PartialUpdateNotaris = Partial<INotaris> & Pick<INotaris, 'id'>;

type RestOf<T extends INotaris | NewNotaris> = Omit<
  T,
  | 'tglSk'
  | 'jamKerjaAwalOne'
  | 'jamKerjaAkhirOne'
  | 'jamKerjaAwalTwo'
  | 'jamKerjaAkhirTwo'
  | 'jamRestAwalOne'
  | 'jamRestAkhirOne'
  | 'jamRestAwalTwo'
  | 'jamRestAkhirTwo'
  | 'updateOn'
> & {
  tglSk?: string | null;
  jamKerjaAwalOne?: string | null;
  jamKerjaAkhirOne?: string | null;
  jamKerjaAwalTwo?: string | null;
  jamKerjaAkhirTwo?: string | null;
  jamRestAwalOne?: string | null;
  jamRestAkhirOne?: string | null;
  jamRestAwalTwo?: string | null;
  jamRestAkhirTwo?: string | null;
  updateOn?: string | null;
};

export type RestNotaris = RestOf<INotaris>;

export type NewRestNotaris = RestOf<NewNotaris>;

export type PartialUpdateRestNotaris = RestOf<PartialUpdateNotaris>;

export type EntityResponseType = HttpResponse<INotaris>;
export type EntityArrayResponseType = HttpResponse<INotaris[]>;

@Injectable({ providedIn: 'root' })
export class NotarisService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/notarises');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(notaris: NewNotaris): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notaris);
    return this.http
      .post<RestNotaris>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(notaris: INotaris): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notaris);
    return this.http
      .put<RestNotaris>(`${this.resourceUrl}/${this.getNotarisIdentifier(notaris)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(notaris: PartialUpdateNotaris): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notaris);
    return this.http
      .patch<RestNotaris>(`${this.resourceUrl}/${this.getNotarisIdentifier(notaris)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestNotaris>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestNotaris[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getNotarisIdentifier(notaris: Pick<INotaris, 'id'>): number {
    return notaris.id;
  }

  compareNotaris(o1: Pick<INotaris, 'id'> | null, o2: Pick<INotaris, 'id'> | null): boolean {
    return o1 && o2 ? this.getNotarisIdentifier(o1) === this.getNotarisIdentifier(o2) : o1 === o2;
  }

  addNotarisToCollectionIfMissing<Type extends Pick<INotaris, 'id'>>(
    notarisCollection: Type[],
    ...notarisesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const notarises: Type[] = notarisesToCheck.filter(isPresent);
    if (notarises.length > 0) {
      const notarisCollectionIdentifiers = notarisCollection.map(notarisItem => this.getNotarisIdentifier(notarisItem)!);
      const notarisesToAdd = notarises.filter(notarisItem => {
        const notarisIdentifier = this.getNotarisIdentifier(notarisItem);
        if (notarisCollectionIdentifiers.includes(notarisIdentifier)) {
          return false;
        }
        notarisCollectionIdentifiers.push(notarisIdentifier);
        return true;
      });
      return [...notarisesToAdd, ...notarisCollection];
    }
    return notarisCollection;
  }

  protected convertDateFromClient<T extends INotaris | NewNotaris | PartialUpdateNotaris>(notaris: T): RestOf<T> {
    return {
      ...notaris,
      tglSk: notaris.tglSk?.format(DATE_FORMAT) ?? null,
      jamKerjaAwalOne: notaris.jamKerjaAwalOne?.toJSON() ?? null,
      jamKerjaAkhirOne: notaris.jamKerjaAkhirOne?.toJSON() ?? null,
      jamKerjaAwalTwo: notaris.jamKerjaAwalTwo?.toJSON() ?? null,
      jamKerjaAkhirTwo: notaris.jamKerjaAkhirTwo?.toJSON() ?? null,
      jamRestAwalOne: notaris.jamRestAwalOne?.toJSON() ?? null,
      jamRestAkhirOne: notaris.jamRestAkhirOne?.toJSON() ?? null,
      jamRestAwalTwo: notaris.jamRestAwalTwo?.toJSON() ?? null,
      jamRestAkhirTwo: notaris.jamRestAkhirTwo?.toJSON() ?? null,
      updateOn: notaris.updateOn?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restNotaris: RestNotaris): INotaris {
    return {
      ...restNotaris,
      tglSk: restNotaris.tglSk ? dayjs(restNotaris.tglSk) : undefined,
      jamKerjaAwalOne: restNotaris.jamKerjaAwalOne ? dayjs(restNotaris.jamKerjaAwalOne) : undefined,
      jamKerjaAkhirOne: restNotaris.jamKerjaAkhirOne ? dayjs(restNotaris.jamKerjaAkhirOne) : undefined,
      jamKerjaAwalTwo: restNotaris.jamKerjaAwalTwo ? dayjs(restNotaris.jamKerjaAwalTwo) : undefined,
      jamKerjaAkhirTwo: restNotaris.jamKerjaAkhirTwo ? dayjs(restNotaris.jamKerjaAkhirTwo) : undefined,
      jamRestAwalOne: restNotaris.jamRestAwalOne ? dayjs(restNotaris.jamRestAwalOne) : undefined,
      jamRestAkhirOne: restNotaris.jamRestAkhirOne ? dayjs(restNotaris.jamRestAkhirOne) : undefined,
      jamRestAwalTwo: restNotaris.jamRestAwalTwo ? dayjs(restNotaris.jamRestAwalTwo) : undefined,
      jamRestAkhirTwo: restNotaris.jamRestAkhirTwo ? dayjs(restNotaris.jamRestAkhirTwo) : undefined,
      updateOn: restNotaris.updateOn ? dayjs(restNotaris.updateOn) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestNotaris>): HttpResponse<INotaris> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestNotaris[]>): HttpResponse<INotaris[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
