import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { SkSubtitusiDetailComponent } from './sk-subtitusi-detail.component';

describe('SkSubtitusi Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SkSubtitusiDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: SkSubtitusiDetailComponent,
              resolve: { skSubtitusi: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(SkSubtitusiDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load skSubtitusi on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', SkSubtitusiDetailComponent);

      // THEN
      expect(instance.skSubtitusi).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
