import { Component, Input } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { ISkSubtitusi } from '../sk-subtitusi.model';

@Component({
  standalone: true,
  selector: 'jhi-sk-subtitusi-detail',
  templateUrl: './sk-subtitusi-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class SkSubtitusiDetailComponent {
  @Input() skSubtitusi: ISkSubtitusi | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  previousState(): void {
    window.history.back();
  }
}
