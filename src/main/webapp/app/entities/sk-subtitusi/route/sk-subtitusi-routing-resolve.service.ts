import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISkSubtitusi } from '../sk-subtitusi.model';
import { SkSubtitusiService } from '../service/sk-subtitusi.service';

export const skSubtitusiResolve = (route: ActivatedRouteSnapshot): Observable<null | ISkSubtitusi> => {
  const id = route.params['id'];
  if (id) {
    return inject(SkSubtitusiService)
      .find(id)
      .pipe(
        mergeMap((skSubtitusi: HttpResponse<ISkSubtitusi>) => {
          if (skSubtitusi.body) {
            return of(skSubtitusi.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default skSubtitusiResolve;
