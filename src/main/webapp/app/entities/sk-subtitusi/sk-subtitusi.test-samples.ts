import dayjs from 'dayjs/esm';

import { ISkSubtitusi, NewSkSubtitusi } from './sk-subtitusi.model';

export const sampleWithRequiredData: ISkSubtitusi = {
  id: 12353,
};

export const sampleWithPartialData: ISkSubtitusi = {
  id: 19127,
  updateBy: 'than deliberately',
  updateOn: dayjs('2023-12-14T08:32'),
  fileNameLamp: 'phew modern amazing',
  statusSign: 8472,
};

export const sampleWithFullData: ISkSubtitusi = {
  id: 25397,
  nomor: 'upbeat young till',
  tanggal: dayjs('2023-12-14'),
  recordStatus: 28214,
  updateBy: 'climb successfully yippee',
  updateOn: dayjs('2023-12-14T21:13'),
  fileNameSk: 'wavy above levitate',
  fileNameLamp: 'pan throughout psst',
  statusSign: 26529,
};

export const sampleWithNewData: NewSkSubtitusi = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
