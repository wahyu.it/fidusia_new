import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { SkSubtitusiComponent } from './list/sk-subtitusi.component';
import { SkSubtitusiDetailComponent } from './detail/sk-subtitusi-detail.component';
import { SkSubtitusiUpdateComponent } from './update/sk-subtitusi-update.component';
import SkSubtitusiResolve from './route/sk-subtitusi-routing-resolve.service';

const skSubtitusiRoute: Routes = [
  {
    path: '',
    component: SkSubtitusiComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SkSubtitusiDetailComponent,
    resolve: {
      skSubtitusi: SkSubtitusiResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SkSubtitusiUpdateComponent,
    resolve: {
      skSubtitusi: SkSubtitusiResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SkSubtitusiUpdateComponent,
    resolve: {
      skSubtitusi: SkSubtitusiResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default skSubtitusiRoute;
