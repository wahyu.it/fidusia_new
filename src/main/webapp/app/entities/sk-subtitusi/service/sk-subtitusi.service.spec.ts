import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ISkSubtitusi } from '../sk-subtitusi.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../sk-subtitusi.test-samples';

import { SkSubtitusiService, RestSkSubtitusi } from './sk-subtitusi.service';

const requireRestSample: RestSkSubtitusi = {
  ...sampleWithRequiredData,
  tanggal: sampleWithRequiredData.tanggal?.format(DATE_FORMAT),
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
};

describe('SkSubtitusi Service', () => {
  let service: SkSubtitusiService;
  let httpMock: HttpTestingController;
  let expectedResult: ISkSubtitusi | ISkSubtitusi[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SkSubtitusiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a SkSubtitusi', () => {
      const skSubtitusi = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(skSubtitusi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a SkSubtitusi', () => {
      const skSubtitusi = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(skSubtitusi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a SkSubtitusi', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of SkSubtitusi', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a SkSubtitusi', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addSkSubtitusiToCollectionIfMissing', () => {
      it('should add a SkSubtitusi to an empty array', () => {
        const skSubtitusi: ISkSubtitusi = sampleWithRequiredData;
        expectedResult = service.addSkSubtitusiToCollectionIfMissing([], skSubtitusi);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(skSubtitusi);
      });

      it('should not add a SkSubtitusi to an array that contains it', () => {
        const skSubtitusi: ISkSubtitusi = sampleWithRequiredData;
        const skSubtitusiCollection: ISkSubtitusi[] = [
          {
            ...skSubtitusi,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addSkSubtitusiToCollectionIfMissing(skSubtitusiCollection, skSubtitusi);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a SkSubtitusi to an array that doesn't contain it", () => {
        const skSubtitusi: ISkSubtitusi = sampleWithRequiredData;
        const skSubtitusiCollection: ISkSubtitusi[] = [sampleWithPartialData];
        expectedResult = service.addSkSubtitusiToCollectionIfMissing(skSubtitusiCollection, skSubtitusi);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(skSubtitusi);
      });

      it('should add only unique SkSubtitusi to an array', () => {
        const skSubtitusiArray: ISkSubtitusi[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const skSubtitusiCollection: ISkSubtitusi[] = [sampleWithRequiredData];
        expectedResult = service.addSkSubtitusiToCollectionIfMissing(skSubtitusiCollection, ...skSubtitusiArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const skSubtitusi: ISkSubtitusi = sampleWithRequiredData;
        const skSubtitusi2: ISkSubtitusi = sampleWithPartialData;
        expectedResult = service.addSkSubtitusiToCollectionIfMissing([], skSubtitusi, skSubtitusi2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(skSubtitusi);
        expect(expectedResult).toContain(skSubtitusi2);
      });

      it('should accept null and undefined values', () => {
        const skSubtitusi: ISkSubtitusi = sampleWithRequiredData;
        expectedResult = service.addSkSubtitusiToCollectionIfMissing([], null, skSubtitusi, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(skSubtitusi);
      });

      it('should return initial array if no SkSubtitusi is added', () => {
        const skSubtitusiCollection: ISkSubtitusi[] = [sampleWithRequiredData];
        expectedResult = service.addSkSubtitusiToCollectionIfMissing(skSubtitusiCollection, undefined, null);
        expect(expectedResult).toEqual(skSubtitusiCollection);
      });
    });

    describe('compareSkSubtitusi', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareSkSubtitusi(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareSkSubtitusi(entity1, entity2);
        const compareResult2 = service.compareSkSubtitusi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareSkSubtitusi(entity1, entity2);
        const compareResult2 = service.compareSkSubtitusi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareSkSubtitusi(entity1, entity2);
        const compareResult2 = service.compareSkSubtitusi(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
