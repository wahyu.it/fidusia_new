import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISkSubtitusi, NewSkSubtitusi } from '../sk-subtitusi.model';

export type PartialUpdateSkSubtitusi = Partial<ISkSubtitusi> & Pick<ISkSubtitusi, 'id'>;

type RestOf<T extends ISkSubtitusi | NewSkSubtitusi> = Omit<T, 'tanggal' | 'updateOn'> & {
  tanggal?: string | null;
  updateOn?: string | null;
};

export type RestSkSubtitusi = RestOf<ISkSubtitusi>;

export type NewRestSkSubtitusi = RestOf<NewSkSubtitusi>;

export type PartialUpdateRestSkSubtitusi = RestOf<PartialUpdateSkSubtitusi>;

export type EntityResponseType = HttpResponse<ISkSubtitusi>;
export type EntityArrayResponseType = HttpResponse<ISkSubtitusi[]>;

@Injectable({ providedIn: 'root' })
export class SkSubtitusiService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/sk-subtitusis');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(skSubtitusi: NewSkSubtitusi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(skSubtitusi);
    return this.http
      .post<RestSkSubtitusi>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(skSubtitusi: ISkSubtitusi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(skSubtitusi);
    return this.http
      .put<RestSkSubtitusi>(`${this.resourceUrl}/${this.getSkSubtitusiIdentifier(skSubtitusi)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(skSubtitusi: PartialUpdateSkSubtitusi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(skSubtitusi);
    return this.http
      .patch<RestSkSubtitusi>(`${this.resourceUrl}/${this.getSkSubtitusiIdentifier(skSubtitusi)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestSkSubtitusi>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestSkSubtitusi[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSkSubtitusiIdentifier(skSubtitusi: Pick<ISkSubtitusi, 'id'>): number {
    return skSubtitusi.id;
  }

  compareSkSubtitusi(o1: Pick<ISkSubtitusi, 'id'> | null, o2: Pick<ISkSubtitusi, 'id'> | null): boolean {
    return o1 && o2 ? this.getSkSubtitusiIdentifier(o1) === this.getSkSubtitusiIdentifier(o2) : o1 === o2;
  }

  addSkSubtitusiToCollectionIfMissing<Type extends Pick<ISkSubtitusi, 'id'>>(
    skSubtitusiCollection: Type[],
    ...skSubtitusisToCheck: (Type | null | undefined)[]
  ): Type[] {
    const skSubtitusis: Type[] = skSubtitusisToCheck.filter(isPresent);
    if (skSubtitusis.length > 0) {
      const skSubtitusiCollectionIdentifiers = skSubtitusiCollection.map(
        skSubtitusiItem => this.getSkSubtitusiIdentifier(skSubtitusiItem)!,
      );
      const skSubtitusisToAdd = skSubtitusis.filter(skSubtitusiItem => {
        const skSubtitusiIdentifier = this.getSkSubtitusiIdentifier(skSubtitusiItem);
        if (skSubtitusiCollectionIdentifiers.includes(skSubtitusiIdentifier)) {
          return false;
        }
        skSubtitusiCollectionIdentifiers.push(skSubtitusiIdentifier);
        return true;
      });
      return [...skSubtitusisToAdd, ...skSubtitusiCollection];
    }
    return skSubtitusiCollection;
  }

  protected convertDateFromClient<T extends ISkSubtitusi | NewSkSubtitusi | PartialUpdateSkSubtitusi>(skSubtitusi: T): RestOf<T> {
    return {
      ...skSubtitusi,
      tanggal: skSubtitusi.tanggal?.format(DATE_FORMAT) ?? null,
      updateOn: skSubtitusi.updateOn?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restSkSubtitusi: RestSkSubtitusi): ISkSubtitusi {
    return {
      ...restSkSubtitusi,
      tanggal: restSkSubtitusi.tanggal ? dayjs(restSkSubtitusi.tanggal) : undefined,
      updateOn: restSkSubtitusi.updateOn ? dayjs(restSkSubtitusi.updateOn) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestSkSubtitusi>): HttpResponse<ISkSubtitusi> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestSkSubtitusi[]>): HttpResponse<ISkSubtitusi[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
