import dayjs from 'dayjs/esm';

export interface ISkSubtitusi {
  id: number;
  nomor?: string | null;
  tanggal?: dayjs.Dayjs | null;
  recordStatus?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  fileNameSk?: string | null;
  fileNameLamp?: string | null;
  statusSign?: number | null;
}

export type NewSkSubtitusi = Omit<ISkSubtitusi, 'id'> & { id: null };
