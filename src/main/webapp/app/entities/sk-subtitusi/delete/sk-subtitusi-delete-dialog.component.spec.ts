jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { SkSubtitusiService } from '../service/sk-subtitusi.service';

import { SkSubtitusiDeleteDialogComponent } from './sk-subtitusi-delete-dialog.component';

describe('SkSubtitusi Management Delete Component', () => {
  let comp: SkSubtitusiDeleteDialogComponent;
  let fixture: ComponentFixture<SkSubtitusiDeleteDialogComponent>;
  let service: SkSubtitusiService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SkSubtitusiDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(SkSubtitusiDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SkSubtitusiDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(SkSubtitusiService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      }),
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});
