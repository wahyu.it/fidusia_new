import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { ISkSubtitusi } from '../sk-subtitusi.model';
import { SkSubtitusiService } from '../service/sk-subtitusi.service';

@Component({
  standalone: true,
  templateUrl: './sk-subtitusi-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class SkSubtitusiDeleteDialogComponent {
  skSubtitusi?: ISkSubtitusi;

  constructor(
    protected skSubtitusiService: SkSubtitusiService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.skSubtitusiService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
