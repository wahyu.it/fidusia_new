import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SkSubtitusiService } from '../service/sk-subtitusi.service';
import { ISkSubtitusi } from '../sk-subtitusi.model';
import { SkSubtitusiFormService } from './sk-subtitusi-form.service';

import { SkSubtitusiUpdateComponent } from './sk-subtitusi-update.component';

describe('SkSubtitusi Management Update Component', () => {
  let comp: SkSubtitusiUpdateComponent;
  let fixture: ComponentFixture<SkSubtitusiUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let skSubtitusiFormService: SkSubtitusiFormService;
  let skSubtitusiService: SkSubtitusiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), SkSubtitusiUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SkSubtitusiUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SkSubtitusiUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    skSubtitusiFormService = TestBed.inject(SkSubtitusiFormService);
    skSubtitusiService = TestBed.inject(SkSubtitusiService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const skSubtitusi: ISkSubtitusi = { id: 456 };

      activatedRoute.data = of({ skSubtitusi });
      comp.ngOnInit();

      expect(comp.skSubtitusi).toEqual(skSubtitusi);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISkSubtitusi>>();
      const skSubtitusi = { id: 123 };
      jest.spyOn(skSubtitusiFormService, 'getSkSubtitusi').mockReturnValue(skSubtitusi);
      jest.spyOn(skSubtitusiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ skSubtitusi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: skSubtitusi }));
      saveSubject.complete();

      // THEN
      expect(skSubtitusiFormService.getSkSubtitusi).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(skSubtitusiService.update).toHaveBeenCalledWith(expect.objectContaining(skSubtitusi));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISkSubtitusi>>();
      const skSubtitusi = { id: 123 };
      jest.spyOn(skSubtitusiFormService, 'getSkSubtitusi').mockReturnValue({ id: null });
      jest.spyOn(skSubtitusiService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ skSubtitusi: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: skSubtitusi }));
      saveSubject.complete();

      // THEN
      expect(skSubtitusiFormService.getSkSubtitusi).toHaveBeenCalled();
      expect(skSubtitusiService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISkSubtitusi>>();
      const skSubtitusi = { id: 123 };
      jest.spyOn(skSubtitusiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ skSubtitusi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(skSubtitusiService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
