import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ISkSubtitusi, NewSkSubtitusi } from '../sk-subtitusi.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISkSubtitusi for edit and NewSkSubtitusiFormGroupInput for create.
 */
type SkSubtitusiFormGroupInput = ISkSubtitusi | PartialWithRequiredKeyOf<NewSkSubtitusi>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ISkSubtitusi | NewSkSubtitusi> = Omit<T, 'updateOn'> & {
  updateOn?: string | null;
};

type SkSubtitusiFormRawValue = FormValueOf<ISkSubtitusi>;

type NewSkSubtitusiFormRawValue = FormValueOf<NewSkSubtitusi>;

type SkSubtitusiFormDefaults = Pick<NewSkSubtitusi, 'id' | 'updateOn'>;

type SkSubtitusiFormGroupContent = {
  id: FormControl<SkSubtitusiFormRawValue['id'] | NewSkSubtitusi['id']>;
  nomor: FormControl<SkSubtitusiFormRawValue['nomor']>;
  tanggal: FormControl<SkSubtitusiFormRawValue['tanggal']>;
  recordStatus: FormControl<SkSubtitusiFormRawValue['recordStatus']>;
  updateBy: FormControl<SkSubtitusiFormRawValue['updateBy']>;
  updateOn: FormControl<SkSubtitusiFormRawValue['updateOn']>;
  fileNameSk: FormControl<SkSubtitusiFormRawValue['fileNameSk']>;
  fileNameLamp: FormControl<SkSubtitusiFormRawValue['fileNameLamp']>;
  statusSign: FormControl<SkSubtitusiFormRawValue['statusSign']>;
};

export type SkSubtitusiFormGroup = FormGroup<SkSubtitusiFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SkSubtitusiFormService {
  createSkSubtitusiFormGroup(skSubtitusi: SkSubtitusiFormGroupInput = { id: null }): SkSubtitusiFormGroup {
    const skSubtitusiRawValue = this.convertSkSubtitusiToSkSubtitusiRawValue({
      ...this.getFormDefaults(),
      ...skSubtitusi,
    });
    return new FormGroup<SkSubtitusiFormGroupContent>({
      id: new FormControl(
        { value: skSubtitusiRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      nomor: new FormControl(skSubtitusiRawValue.nomor),
      tanggal: new FormControl(skSubtitusiRawValue.tanggal),
      recordStatus: new FormControl(skSubtitusiRawValue.recordStatus),
      updateBy: new FormControl(skSubtitusiRawValue.updateBy),
      updateOn: new FormControl(skSubtitusiRawValue.updateOn),
      fileNameSk: new FormControl(skSubtitusiRawValue.fileNameSk),
      fileNameLamp: new FormControl(skSubtitusiRawValue.fileNameLamp),
      statusSign: new FormControl(skSubtitusiRawValue.statusSign),
    });
  }

  getSkSubtitusi(form: SkSubtitusiFormGroup): ISkSubtitusi | NewSkSubtitusi {
    return this.convertSkSubtitusiRawValueToSkSubtitusi(form.getRawValue() as SkSubtitusiFormRawValue | NewSkSubtitusiFormRawValue);
  }

  resetForm(form: SkSubtitusiFormGroup, skSubtitusi: SkSubtitusiFormGroupInput): void {
    const skSubtitusiRawValue = this.convertSkSubtitusiToSkSubtitusiRawValue({ ...this.getFormDefaults(), ...skSubtitusi });
    form.reset(
      {
        ...skSubtitusiRawValue,
        id: { value: skSubtitusiRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): SkSubtitusiFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      updateOn: currentTime,
    };
  }

  private convertSkSubtitusiRawValueToSkSubtitusi(
    rawSkSubtitusi: SkSubtitusiFormRawValue | NewSkSubtitusiFormRawValue,
  ): ISkSubtitusi | NewSkSubtitusi {
    return {
      ...rawSkSubtitusi,
      updateOn: dayjs(rawSkSubtitusi.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertSkSubtitusiToSkSubtitusiRawValue(
    skSubtitusi: ISkSubtitusi | (Partial<NewSkSubtitusi> & SkSubtitusiFormDefaults),
  ): SkSubtitusiFormRawValue | PartialWithRequiredKeyOf<NewSkSubtitusiFormRawValue> {
    return {
      ...skSubtitusi,
      updateOn: skSubtitusi.updateOn ? skSubtitusi.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
