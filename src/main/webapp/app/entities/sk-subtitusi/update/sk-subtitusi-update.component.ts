import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ISkSubtitusi } from '../sk-subtitusi.model';
import { SkSubtitusiService } from '../service/sk-subtitusi.service';
import { SkSubtitusiFormService, SkSubtitusiFormGroup } from './sk-subtitusi-form.service';

@Component({
  standalone: true,
  selector: 'jhi-sk-subtitusi-update',
  templateUrl: './sk-subtitusi-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class SkSubtitusiUpdateComponent implements OnInit {
  isSaving = false;
  skSubtitusi: ISkSubtitusi | null = null;

  editForm: SkSubtitusiFormGroup = this.skSubtitusiFormService.createSkSubtitusiFormGroup();

  constructor(
    protected skSubtitusiService: SkSubtitusiService,
    protected skSubtitusiFormService: SkSubtitusiFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ skSubtitusi }) => {
      this.skSubtitusi = skSubtitusi;
      if (skSubtitusi) {
        this.updateForm(skSubtitusi);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const skSubtitusi = this.skSubtitusiFormService.getSkSubtitusi(this.editForm);
    if (skSubtitusi.id !== null) {
      this.subscribeToSaveResponse(this.skSubtitusiService.update(skSubtitusi));
    } else {
      this.subscribeToSaveResponse(this.skSubtitusiService.create(skSubtitusi));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISkSubtitusi>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(skSubtitusi: ISkSubtitusi): void {
    this.skSubtitusi = skSubtitusi;
    this.skSubtitusiFormService.resetForm(this.editForm, skSubtitusi);
  }
}
