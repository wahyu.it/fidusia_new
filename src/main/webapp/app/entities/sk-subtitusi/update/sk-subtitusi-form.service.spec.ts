import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../sk-subtitusi.test-samples';

import { SkSubtitusiFormService } from './sk-subtitusi-form.service';

describe('SkSubtitusi Form Service', () => {
  let service: SkSubtitusiFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SkSubtitusiFormService);
  });

  describe('Service methods', () => {
    describe('createSkSubtitusiFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createSkSubtitusiFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nomor: expect.any(Object),
            tanggal: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            fileNameSk: expect.any(Object),
            fileNameLamp: expect.any(Object),
            statusSign: expect.any(Object),
          }),
        );
      });

      it('passing ISkSubtitusi should create a new form with FormGroup', () => {
        const formGroup = service.createSkSubtitusiFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nomor: expect.any(Object),
            tanggal: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            fileNameSk: expect.any(Object),
            fileNameLamp: expect.any(Object),
            statusSign: expect.any(Object),
          }),
        );
      });
    });

    describe('getSkSubtitusi', () => {
      it('should return NewSkSubtitusi for default SkSubtitusi initial value', () => {
        const formGroup = service.createSkSubtitusiFormGroup(sampleWithNewData);

        const skSubtitusi = service.getSkSubtitusi(formGroup) as any;

        expect(skSubtitusi).toMatchObject(sampleWithNewData);
      });

      it('should return NewSkSubtitusi for empty SkSubtitusi initial value', () => {
        const formGroup = service.createSkSubtitusiFormGroup();

        const skSubtitusi = service.getSkSubtitusi(formGroup) as any;

        expect(skSubtitusi).toMatchObject({});
      });

      it('should return ISkSubtitusi', () => {
        const formGroup = service.createSkSubtitusiFormGroup(sampleWithRequiredData);

        const skSubtitusi = service.getSkSubtitusi(formGroup) as any;

        expect(skSubtitusi).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ISkSubtitusi should not enable id FormControl', () => {
        const formGroup = service.createSkSubtitusiFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewSkSubtitusi should disable id FormControl', () => {
        const formGroup = service.createSkSubtitusiFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
