import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { PpkComponent } from './list/ppk.component';
import { PpkDetailComponent } from './detail/ppk-detail.component';
import { PpkUpdateComponent } from './update/ppk-update.component';
import PpkResolve from './route/ppk-routing-resolve.service';

const ppkRoute: Routes = [
  {
    path: '',
    component: PpkComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PpkDetailComponent,
    resolve: {
      ppk: PpkResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PpkUpdateComponent,
    resolve: {
      ppk: PpkResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PpkUpdateComponent,
    resolve: {
      ppk: PpkResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default ppkRoute;
