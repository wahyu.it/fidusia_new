import dayjs from 'dayjs/esm';
import { IOrang } from 'app/entities/orang/orang.model';
import { IBadan } from 'app/entities/badan/badan.model';
import { IAkta } from 'app/entities/akta/akta.model';

export interface IPpk {
  id: number;
  nomor?: string | null;
  nama?: string | null;
  jenis?: string | null;
  tanggal?: dayjs.Dayjs | null;
  tglSk?: dayjs.Dayjs | null;
  nilaiHutangPokok?: number | null;
  nilaiPenjaminan?: number | null;
  periodeTenor?: number | null;
  tglCicilAwal?: dayjs.Dayjs | null;
  tglCicilAkhir?: dayjs.Dayjs | null;
  nsbNamaDebitur?: string | null;
  nsbNama?: string | null;
  nsbJenis?: number | null;
  orang?: IOrang | null;
  badan?: IBadan | null;
  akta?: IAkta | null;
}

export type NewPpk = Omit<IPpk, 'id'> & { id: null };
