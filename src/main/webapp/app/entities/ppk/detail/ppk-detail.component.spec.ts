import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PpkDetailComponent } from './ppk-detail.component';

describe('Ppk Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PpkDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: PpkDetailComponent,
              resolve: { ppk: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(PpkDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load ppk on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', PpkDetailComponent);

      // THEN
      expect(instance.ppk).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
