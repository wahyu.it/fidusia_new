import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPpk, NewPpk } from '../ppk.model';

export type PartialUpdatePpk = Partial<IPpk> & Pick<IPpk, 'id'>;

type RestOf<T extends IPpk | NewPpk> = Omit<T, 'tanggal' | 'tglSk' | 'tglCicilAwal' | 'tglCicilAkhir'> & {
  tanggal?: string | null;
  tglSk?: string | null;
  tglCicilAwal?: string | null;
  tglCicilAkhir?: string | null;
};

export type RestPpk = RestOf<IPpk>;

export type NewRestPpk = RestOf<NewPpk>;

export type PartialUpdateRestPpk = RestOf<PartialUpdatePpk>;

export type EntityResponseType = HttpResponse<IPpk>;
export type EntityArrayResponseType = HttpResponse<IPpk[]>;

@Injectable({ providedIn: 'root' })
export class PpkService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/ppks');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(ppk: NewPpk): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ppk);
    return this.http.post<RestPpk>(this.resourceUrl, copy, { observe: 'response' }).pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(ppk: IPpk): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ppk);
    return this.http
      .put<RestPpk>(`${this.resourceUrl}/${this.getPpkIdentifier(ppk)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(ppk: PartialUpdatePpk): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ppk);
    return this.http
      .patch<RestPpk>(`${this.resourceUrl}/${this.getPpkIdentifier(ppk)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestPpk>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestPpk[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPpkIdentifier(ppk: Pick<IPpk, 'id'>): number {
    return ppk.id;
  }

  comparePpk(o1: Pick<IPpk, 'id'> | null, o2: Pick<IPpk, 'id'> | null): boolean {
    return o1 && o2 ? this.getPpkIdentifier(o1) === this.getPpkIdentifier(o2) : o1 === o2;
  }

  addPpkToCollectionIfMissing<Type extends Pick<IPpk, 'id'>>(ppkCollection: Type[], ...ppksToCheck: (Type | null | undefined)[]): Type[] {
    const ppks: Type[] = ppksToCheck.filter(isPresent);
    if (ppks.length > 0) {
      const ppkCollectionIdentifiers = ppkCollection.map(ppkItem => this.getPpkIdentifier(ppkItem)!);
      const ppksToAdd = ppks.filter(ppkItem => {
        const ppkIdentifier = this.getPpkIdentifier(ppkItem);
        if (ppkCollectionIdentifiers.includes(ppkIdentifier)) {
          return false;
        }
        ppkCollectionIdentifiers.push(ppkIdentifier);
        return true;
      });
      return [...ppksToAdd, ...ppkCollection];
    }
    return ppkCollection;
  }

  protected convertDateFromClient<T extends IPpk | NewPpk | PartialUpdatePpk>(ppk: T): RestOf<T> {
    return {
      ...ppk,
      tanggal: ppk.tanggal?.format(DATE_FORMAT) ?? null,
      tglSk: ppk.tglSk?.format(DATE_FORMAT) ?? null,
      tglCicilAwal: ppk.tglCicilAwal?.format(DATE_FORMAT) ?? null,
      tglCicilAkhir: ppk.tglCicilAkhir?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restPpk: RestPpk): IPpk {
    return {
      ...restPpk,
      tanggal: restPpk.tanggal ? dayjs(restPpk.tanggal) : undefined,
      tglSk: restPpk.tglSk ? dayjs(restPpk.tglSk) : undefined,
      tglCicilAwal: restPpk.tglCicilAwal ? dayjs(restPpk.tglCicilAwal) : undefined,
      tglCicilAkhir: restPpk.tglCicilAkhir ? dayjs(restPpk.tglCicilAkhir) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestPpk>): HttpResponse<IPpk> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestPpk[]>): HttpResponse<IPpk[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
