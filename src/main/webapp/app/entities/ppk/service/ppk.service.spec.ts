import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IPpk } from '../ppk.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../ppk.test-samples';

import { PpkService, RestPpk } from './ppk.service';

const requireRestSample: RestPpk = {
  ...sampleWithRequiredData,
  tanggal: sampleWithRequiredData.tanggal?.format(DATE_FORMAT),
  tglSk: sampleWithRequiredData.tglSk?.format(DATE_FORMAT),
  tglCicilAwal: sampleWithRequiredData.tglCicilAwal?.format(DATE_FORMAT),
  tglCicilAkhir: sampleWithRequiredData.tglCicilAkhir?.format(DATE_FORMAT),
};

describe('Ppk Service', () => {
  let service: PpkService;
  let httpMock: HttpTestingController;
  let expectedResult: IPpk | IPpk[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PpkService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Ppk', () => {
      const ppk = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(ppk).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Ppk', () => {
      const ppk = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(ppk).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Ppk', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Ppk', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Ppk', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addPpkToCollectionIfMissing', () => {
      it('should add a Ppk to an empty array', () => {
        const ppk: IPpk = sampleWithRequiredData;
        expectedResult = service.addPpkToCollectionIfMissing([], ppk);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(ppk);
      });

      it('should not add a Ppk to an array that contains it', () => {
        const ppk: IPpk = sampleWithRequiredData;
        const ppkCollection: IPpk[] = [
          {
            ...ppk,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addPpkToCollectionIfMissing(ppkCollection, ppk);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Ppk to an array that doesn't contain it", () => {
        const ppk: IPpk = sampleWithRequiredData;
        const ppkCollection: IPpk[] = [sampleWithPartialData];
        expectedResult = service.addPpkToCollectionIfMissing(ppkCollection, ppk);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(ppk);
      });

      it('should add only unique Ppk to an array', () => {
        const ppkArray: IPpk[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const ppkCollection: IPpk[] = [sampleWithRequiredData];
        expectedResult = service.addPpkToCollectionIfMissing(ppkCollection, ...ppkArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const ppk: IPpk = sampleWithRequiredData;
        const ppk2: IPpk = sampleWithPartialData;
        expectedResult = service.addPpkToCollectionIfMissing([], ppk, ppk2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(ppk);
        expect(expectedResult).toContain(ppk2);
      });

      it('should accept null and undefined values', () => {
        const ppk: IPpk = sampleWithRequiredData;
        expectedResult = service.addPpkToCollectionIfMissing([], null, ppk, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(ppk);
      });

      it('should return initial array if no Ppk is added', () => {
        const ppkCollection: IPpk[] = [sampleWithRequiredData];
        expectedResult = service.addPpkToCollectionIfMissing(ppkCollection, undefined, null);
        expect(expectedResult).toEqual(ppkCollection);
      });
    });

    describe('comparePpk', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.comparePpk(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.comparePpk(entity1, entity2);
        const compareResult2 = service.comparePpk(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.comparePpk(entity1, entity2);
        const compareResult2 = service.comparePpk(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.comparePpk(entity1, entity2);
        const compareResult2 = service.comparePpk(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
