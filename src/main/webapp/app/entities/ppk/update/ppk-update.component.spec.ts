import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IOrang } from 'app/entities/orang/orang.model';
import { OrangService } from 'app/entities/orang/service/orang.service';
import { IBadan } from 'app/entities/badan/badan.model';
import { BadanService } from 'app/entities/badan/service/badan.service';
import { IPpk } from '../ppk.model';
import { PpkService } from '../service/ppk.service';
import { PpkFormService } from './ppk-form.service';

import { PpkUpdateComponent } from './ppk-update.component';

describe('Ppk Management Update Component', () => {
  let comp: PpkUpdateComponent;
  let fixture: ComponentFixture<PpkUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let ppkFormService: PpkFormService;
  let ppkService: PpkService;
  let orangService: OrangService;
  let badanService: BadanService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), PpkUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PpkUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PpkUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    ppkFormService = TestBed.inject(PpkFormService);
    ppkService = TestBed.inject(PpkService);
    orangService = TestBed.inject(OrangService);
    badanService = TestBed.inject(BadanService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call orang query and add missing value', () => {
      const ppk: IPpk = { id: 456 };
      const orang: IOrang = { id: 20756 };
      ppk.orang = orang;

      const orangCollection: IOrang[] = [{ id: 20598 }];
      jest.spyOn(orangService, 'query').mockReturnValue(of(new HttpResponse({ body: orangCollection })));
      const expectedCollection: IOrang[] = [orang, ...orangCollection];
      jest.spyOn(orangService, 'addOrangToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ ppk });
      comp.ngOnInit();

      expect(orangService.query).toHaveBeenCalled();
      expect(orangService.addOrangToCollectionIfMissing).toHaveBeenCalledWith(orangCollection, orang);
      expect(comp.orangsCollection).toEqual(expectedCollection);
    });

    it('Should call badan query and add missing value', () => {
      const ppk: IPpk = { id: 456 };
      const badan: IBadan = { id: 9978 };
      ppk.badan = badan;

      const badanCollection: IBadan[] = [{ id: 10398 }];
      jest.spyOn(badanService, 'query').mockReturnValue(of(new HttpResponse({ body: badanCollection })));
      const expectedCollection: IBadan[] = [badan, ...badanCollection];
      jest.spyOn(badanService, 'addBadanToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ ppk });
      comp.ngOnInit();

      expect(badanService.query).toHaveBeenCalled();
      expect(badanService.addBadanToCollectionIfMissing).toHaveBeenCalledWith(badanCollection, badan);
      expect(comp.badansCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const ppk: IPpk = { id: 456 };
      const orang: IOrang = { id: 30737 };
      ppk.orang = orang;
      const badan: IBadan = { id: 15232 };
      ppk.badan = badan;

      activatedRoute.data = of({ ppk });
      comp.ngOnInit();

      expect(comp.orangsCollection).toContain(orang);
      expect(comp.badansCollection).toContain(badan);
      expect(comp.ppk).toEqual(ppk);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPpk>>();
      const ppk = { id: 123 };
      jest.spyOn(ppkFormService, 'getPpk').mockReturnValue(ppk);
      jest.spyOn(ppkService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ppk });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ppk }));
      saveSubject.complete();

      // THEN
      expect(ppkFormService.getPpk).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(ppkService.update).toHaveBeenCalledWith(expect.objectContaining(ppk));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPpk>>();
      const ppk = { id: 123 };
      jest.spyOn(ppkFormService, 'getPpk').mockReturnValue({ id: null });
      jest.spyOn(ppkService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ppk: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ppk }));
      saveSubject.complete();

      // THEN
      expect(ppkFormService.getPpk).toHaveBeenCalled();
      expect(ppkService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPpk>>();
      const ppk = { id: 123 };
      jest.spyOn(ppkService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ppk });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(ppkService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareOrang', () => {
      it('Should forward to orangService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(orangService, 'compareOrang');
        comp.compareOrang(entity, entity2);
        expect(orangService.compareOrang).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareBadan', () => {
      it('Should forward to badanService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(badanService, 'compareBadan');
        comp.compareBadan(entity, entity2);
        expect(badanService.compareBadan).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
