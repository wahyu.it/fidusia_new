import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPpk, NewPpk } from '../ppk.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPpk for edit and NewPpkFormGroupInput for create.
 */
type PpkFormGroupInput = IPpk | PartialWithRequiredKeyOf<NewPpk>;

type PpkFormDefaults = Pick<NewPpk, 'id'>;

type PpkFormGroupContent = {
  id: FormControl<IPpk['id'] | NewPpk['id']>;
  nomor: FormControl<IPpk['nomor']>;
  nama: FormControl<IPpk['nama']>;
  jenis: FormControl<IPpk['jenis']>;
  tanggal: FormControl<IPpk['tanggal']>;
  tglSk: FormControl<IPpk['tglSk']>;
  nilaiHutangPokok: FormControl<IPpk['nilaiHutangPokok']>;
  nilaiPenjaminan: FormControl<IPpk['nilaiPenjaminan']>;
  periodeTenor: FormControl<IPpk['periodeTenor']>;
  tglCicilAwal: FormControl<IPpk['tglCicilAwal']>;
  tglCicilAkhir: FormControl<IPpk['tglCicilAkhir']>;
  nsbNamaDebitur: FormControl<IPpk['nsbNamaDebitur']>;
  nsbNama: FormControl<IPpk['nsbNama']>;
  nsbJenis: FormControl<IPpk['nsbJenis']>;
  orang: FormControl<IPpk['orang']>;
  badan: FormControl<IPpk['badan']>;
};

export type PpkFormGroup = FormGroup<PpkFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PpkFormService {
  createPpkFormGroup(ppk: PpkFormGroupInput = { id: null }): PpkFormGroup {
    const ppkRawValue = {
      ...this.getFormDefaults(),
      ...ppk,
    };
    return new FormGroup<PpkFormGroupContent>({
      id: new FormControl(
        { value: ppkRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      nomor: new FormControl(ppkRawValue.nomor),
      nama: new FormControl(ppkRawValue.nama),
      jenis: new FormControl(ppkRawValue.jenis),
      tanggal: new FormControl(ppkRawValue.tanggal),
      tglSk: new FormControl(ppkRawValue.tglSk),
      nilaiHutangPokok: new FormControl(ppkRawValue.nilaiHutangPokok),
      nilaiPenjaminan: new FormControl(ppkRawValue.nilaiPenjaminan),
      periodeTenor: new FormControl(ppkRawValue.periodeTenor),
      tglCicilAwal: new FormControl(ppkRawValue.tglCicilAwal),
      tglCicilAkhir: new FormControl(ppkRawValue.tglCicilAkhir),
      nsbNamaDebitur: new FormControl(ppkRawValue.nsbNamaDebitur),
      nsbNama: new FormControl(ppkRawValue.nsbNama),
      nsbJenis: new FormControl(ppkRawValue.nsbJenis),
      orang: new FormControl(ppkRawValue.orang),
      badan: new FormControl(ppkRawValue.badan),
    });
  }

  getPpk(form: PpkFormGroup): IPpk | NewPpk {
    return form.getRawValue() as IPpk | NewPpk;
  }

  resetForm(form: PpkFormGroup, ppk: PpkFormGroupInput): void {
    const ppkRawValue = { ...this.getFormDefaults(), ...ppk };
    form.reset(
      {
        ...ppkRawValue,
        id: { value: ppkRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): PpkFormDefaults {
    return {
      id: null,
    };
  }
}
