import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IOrang } from 'app/entities/orang/orang.model';
import { OrangService } from 'app/entities/orang/service/orang.service';
import { IBadan } from 'app/entities/badan/badan.model';
import { BadanService } from 'app/entities/badan/service/badan.service';
import { PpkService } from '../service/ppk.service';
import { IPpk } from '../ppk.model';
import { PpkFormService, PpkFormGroup } from './ppk-form.service';

@Component({
  standalone: true,
  selector: 'jhi-ppk-update',
  templateUrl: './ppk-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class PpkUpdateComponent implements OnInit {
  isSaving = false;
  ppk: IPpk | null = null;

  orangsCollection: IOrang[] = [];
  badansCollection: IBadan[] = [];

  editForm: PpkFormGroup = this.ppkFormService.createPpkFormGroup();

  constructor(
    protected ppkService: PpkService,
    protected ppkFormService: PpkFormService,
    protected orangService: OrangService,
    protected badanService: BadanService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareOrang = (o1: IOrang | null, o2: IOrang | null): boolean => this.orangService.compareOrang(o1, o2);

  compareBadan = (o1: IBadan | null, o2: IBadan | null): boolean => this.badanService.compareBadan(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ppk }) => {
      this.ppk = ppk;
      if (ppk) {
        this.updateForm(ppk);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ppk = this.ppkFormService.getPpk(this.editForm);
    if (ppk.id !== null) {
      this.subscribeToSaveResponse(this.ppkService.update(ppk));
    } else {
      this.subscribeToSaveResponse(this.ppkService.create(ppk));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPpk>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(ppk: IPpk): void {
    this.ppk = ppk;
    this.ppkFormService.resetForm(this.editForm, ppk);

    this.orangsCollection = this.orangService.addOrangToCollectionIfMissing<IOrang>(this.orangsCollection, ppk.orang);
    this.badansCollection = this.badanService.addBadanToCollectionIfMissing<IBadan>(this.badansCollection, ppk.badan);
  }

  protected loadRelationshipsOptions(): void {
    this.orangService
      .query({ filter: 'ppk-is-null' })
      .pipe(map((res: HttpResponse<IOrang[]>) => res.body ?? []))
      .pipe(map((orangs: IOrang[]) => this.orangService.addOrangToCollectionIfMissing<IOrang>(orangs, this.ppk?.orang)))
      .subscribe((orangs: IOrang[]) => (this.orangsCollection = orangs));

    this.badanService
      .query({ filter: 'ppk-is-null' })
      .pipe(map((res: HttpResponse<IBadan[]>) => res.body ?? []))
      .pipe(map((badans: IBadan[]) => this.badanService.addBadanToCollectionIfMissing<IBadan>(badans, this.ppk?.badan)))
      .subscribe((badans: IBadan[]) => (this.badansCollection = badans));
  }
}
