import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../ppk.test-samples';

import { PpkFormService } from './ppk-form.service';

describe('Ppk Form Service', () => {
  let service: PpkFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PpkFormService);
  });

  describe('Service methods', () => {
    describe('createPpkFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createPpkFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nomor: expect.any(Object),
            nama: expect.any(Object),
            jenis: expect.any(Object),
            tanggal: expect.any(Object),
            tglSk: expect.any(Object),
            nilaiHutangPokok: expect.any(Object),
            nilaiPenjaminan: expect.any(Object),
            periodeTenor: expect.any(Object),
            tglCicilAwal: expect.any(Object),
            tglCicilAkhir: expect.any(Object),
            nsbNamaDebitur: expect.any(Object),
            nsbNama: expect.any(Object),
            nsbJenis: expect.any(Object),
            orang: expect.any(Object),
            badan: expect.any(Object),
          }),
        );
      });

      it('passing IPpk should create a new form with FormGroup', () => {
        const formGroup = service.createPpkFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nomor: expect.any(Object),
            nama: expect.any(Object),
            jenis: expect.any(Object),
            tanggal: expect.any(Object),
            tglSk: expect.any(Object),
            nilaiHutangPokok: expect.any(Object),
            nilaiPenjaminan: expect.any(Object),
            periodeTenor: expect.any(Object),
            tglCicilAwal: expect.any(Object),
            tglCicilAkhir: expect.any(Object),
            nsbNamaDebitur: expect.any(Object),
            nsbNama: expect.any(Object),
            nsbJenis: expect.any(Object),
            orang: expect.any(Object),
            badan: expect.any(Object),
          }),
        );
      });
    });

    describe('getPpk', () => {
      it('should return NewPpk for default Ppk initial value', () => {
        const formGroup = service.createPpkFormGroup(sampleWithNewData);

        const ppk = service.getPpk(formGroup) as any;

        expect(ppk).toMatchObject(sampleWithNewData);
      });

      it('should return NewPpk for empty Ppk initial value', () => {
        const formGroup = service.createPpkFormGroup();

        const ppk = service.getPpk(formGroup) as any;

        expect(ppk).toMatchObject({});
      });

      it('should return IPpk', () => {
        const formGroup = service.createPpkFormGroup(sampleWithRequiredData);

        const ppk = service.getPpk(formGroup) as any;

        expect(ppk).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IPpk should not enable id FormControl', () => {
        const formGroup = service.createPpkFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewPpk should disable id FormControl', () => {
        const formGroup = service.createPpkFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
