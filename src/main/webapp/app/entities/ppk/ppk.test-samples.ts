import dayjs from 'dayjs/esm';

import { IPpk, NewPpk } from './ppk.model';

export const sampleWithRequiredData: IPpk = {
  id: 8749,
};

export const sampleWithPartialData: IPpk = {
  id: 9480,
  nomor: 'total through',
  nama: 'lest apud',
  jenis: 'shareholder',
  tanggal: dayjs('2023-12-14'),
  nilaiPenjaminan: 4281,
  periodeTenor: 19524,
  tglCicilAwal: dayjs('2023-12-14'),
  nsbNama: 'defensive usually',
};

export const sampleWithFullData: IPpk = {
  id: 22125,
  nomor: 'as bruised',
  nama: 'assassinate frankly',
  jenis: 'between',
  tanggal: dayjs('2023-12-14'),
  tglSk: dayjs('2023-12-14'),
  nilaiHutangPokok: 27597,
  nilaiPenjaminan: 5733,
  periodeTenor: 5059,
  tglCicilAwal: dayjs('2023-12-14'),
  tglCicilAkhir: dayjs('2023-12-14'),
  nsbNamaDebitur: 'afore below',
  nsbNama: 'fatally circa jot',
  nsbJenis: 11294,
};

export const sampleWithNewData: NewPpk = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
