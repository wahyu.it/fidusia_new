import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPpk } from '../ppk.model';
import { PpkService } from '../service/ppk.service';

export const ppkResolve = (route: ActivatedRouteSnapshot): Observable<null | IPpk> => {
  const id = route.params['id'];
  if (id) {
    return inject(PpkService)
      .find(id)
      .pipe(
        mergeMap((ppk: HttpResponse<IPpk>) => {
          if (ppk.body) {
            return of(ppk.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default ppkResolve;
