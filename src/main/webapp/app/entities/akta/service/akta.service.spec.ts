import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IAkta } from '../akta.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../akta.test-samples';

import { AktaService, RestAkta } from './akta.service';

const requireRestSample: RestAkta = {
  ...sampleWithRequiredData,
  tglOrder: sampleWithRequiredData.tglOrder?.format(DATE_FORMAT),
  pukulAwal: sampleWithRequiredData.pukulAwal?.toJSON(),
  pukulAkhir: sampleWithRequiredData.pukulAkhir?.toJSON(),
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
};

describe('Akta Service', () => {
  let service: AktaService;
  let httpMock: HttpTestingController;
  let expectedResult: IAkta | IAkta[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AktaService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Akta', () => {
      const akta = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(akta).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Akta', () => {
      const akta = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(akta).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Akta', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Akta', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Akta', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addAktaToCollectionIfMissing', () => {
      it('should add a Akta to an empty array', () => {
        const akta: IAkta = sampleWithRequiredData;
        expectedResult = service.addAktaToCollectionIfMissing([], akta);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(akta);
      });

      it('should not add a Akta to an array that contains it', () => {
        const akta: IAkta = sampleWithRequiredData;
        const aktaCollection: IAkta[] = [
          {
            ...akta,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAktaToCollectionIfMissing(aktaCollection, akta);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Akta to an array that doesn't contain it", () => {
        const akta: IAkta = sampleWithRequiredData;
        const aktaCollection: IAkta[] = [sampleWithPartialData];
        expectedResult = service.addAktaToCollectionIfMissing(aktaCollection, akta);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(akta);
      });

      it('should add only unique Akta to an array', () => {
        const aktaArray: IAkta[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const aktaCollection: IAkta[] = [sampleWithRequiredData];
        expectedResult = service.addAktaToCollectionIfMissing(aktaCollection, ...aktaArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const akta: IAkta = sampleWithRequiredData;
        const akta2: IAkta = sampleWithPartialData;
        expectedResult = service.addAktaToCollectionIfMissing([], akta, akta2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(akta);
        expect(expectedResult).toContain(akta2);
      });

      it('should accept null and undefined values', () => {
        const akta: IAkta = sampleWithRequiredData;
        expectedResult = service.addAktaToCollectionIfMissing([], null, akta, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(akta);
      });

      it('should return initial array if no Akta is added', () => {
        const aktaCollection: IAkta[] = [sampleWithRequiredData];
        expectedResult = service.addAktaToCollectionIfMissing(aktaCollection, undefined, null);
        expect(expectedResult).toEqual(aktaCollection);
      });
    });

    describe('compareAkta', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAkta(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAkta(entity1, entity2);
        const compareResult2 = service.compareAkta(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAkta(entity1, entity2);
        const compareResult2 = service.compareAkta(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAkta(entity1, entity2);
        const compareResult2 = service.compareAkta(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
