import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAkta, NewAkta } from '../akta.model';

export type PartialUpdateAkta = Partial<IAkta> & Pick<IAkta, 'id'>;

type RestOf<T extends IAkta | NewAkta> = Omit<T, 'tglOrder' | 'pukulAwal' | 'pukulAkhir' | 'updateOn'> & {
  tglOrder?: string | null;
  pukulAwal?: string | null;
  pukulAkhir?: string | null;
  updateOn?: string | null;
};

export type RestAkta = RestOf<IAkta>;

export type NewRestAkta = RestOf<NewAkta>;

export type PartialUpdateRestAkta = RestOf<PartialUpdateAkta>;

export type EntityResponseType = HttpResponse<IAkta>;
export type EntityArrayResponseType = HttpResponse<IAkta[]>;

@Injectable({ providedIn: 'root' })
export class AktaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/aktas');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(akta: NewAkta): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(akta);
    return this.http.post<RestAkta>(this.resourceUrl, copy, { observe: 'response' }).pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(akta: IAkta): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(akta);
    return this.http
      .put<RestAkta>(`${this.resourceUrl}/${this.getAktaIdentifier(akta)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(akta: PartialUpdateAkta): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(akta);
    return this.http
      .patch<RestAkta>(`${this.resourceUrl}/${this.getAktaIdentifier(akta)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestAkta>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestAkta[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAktaIdentifier(akta: Pick<IAkta, 'id'>): number {
    return akta.id;
  }

  compareAkta(o1: Pick<IAkta, 'id'> | null, o2: Pick<IAkta, 'id'> | null): boolean {
    return o1 && o2 ? this.getAktaIdentifier(o1) === this.getAktaIdentifier(o2) : o1 === o2;
  }

  addAktaToCollectionIfMissing<Type extends Pick<IAkta, 'id'>>(
    aktaCollection: Type[],
    ...aktasToCheck: (Type | null | undefined)[]
  ): Type[] {
    const aktas: Type[] = aktasToCheck.filter(isPresent);
    if (aktas.length > 0) {
      const aktaCollectionIdentifiers = aktaCollection.map(aktaItem => this.getAktaIdentifier(aktaItem)!);
      const aktasToAdd = aktas.filter(aktaItem => {
        const aktaIdentifier = this.getAktaIdentifier(aktaItem);
        if (aktaCollectionIdentifiers.includes(aktaIdentifier)) {
          return false;
        }
        aktaCollectionIdentifiers.push(aktaIdentifier);
        return true;
      });
      return [...aktasToAdd, ...aktaCollection];
    }
    return aktaCollection;
  }

  protected convertDateFromClient<T extends IAkta | NewAkta | PartialUpdateAkta>(akta: T): RestOf<T> {
    return {
      ...akta,
      tglOrder: akta.tglOrder?.format(DATE_FORMAT) ?? null,
      pukulAwal: akta.pukulAwal?.toJSON() ?? null,
      pukulAkhir: akta.pukulAkhir?.toJSON() ?? null,
      updateOn: akta.updateOn?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restAkta: RestAkta): IAkta {
    return {
      ...restAkta,
      tglOrder: restAkta.tglOrder ? dayjs(restAkta.tglOrder) : undefined,
      pukulAwal: restAkta.pukulAwal ? dayjs(restAkta.pukulAwal) : undefined,
      pukulAkhir: restAkta.pukulAkhir ? dayjs(restAkta.pukulAkhir) : undefined,
      updateOn: restAkta.updateOn ? dayjs(restAkta.updateOn) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestAkta>): HttpResponse<IAkta> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestAkta[]>): HttpResponse<IAkta[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
