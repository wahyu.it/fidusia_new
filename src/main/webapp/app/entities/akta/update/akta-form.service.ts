import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IAkta, NewAkta } from '../akta.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAkta for edit and NewAktaFormGroupInput for create.
 */
type AktaFormGroupInput = IAkta | PartialWithRequiredKeyOf<NewAkta>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IAkta | NewAkta> = Omit<T, 'pukulAwal' | 'pukulAkhir' | 'updateOn'> & {
  pukulAwal?: string | null;
  pukulAkhir?: string | null;
  updateOn?: string | null;
};

type AktaFormRawValue = FormValueOf<IAkta>;

type NewAktaFormRawValue = FormValueOf<NewAkta>;

type AktaFormDefaults = Pick<NewAkta, 'id' | 'pukulAwal' | 'pukulAkhir' | 'updateOn'>;

type AktaFormGroupContent = {
  id: FormControl<AktaFormRawValue['id'] | NewAkta['id']>;
  tglOrder: FormControl<AktaFormRawValue['tglOrder']>;
  orderType: FormControl<AktaFormRawValue['orderType']>;
  kode: FormControl<AktaFormRawValue['kode']>;
  nomor: FormControl<AktaFormRawValue['nomor']>;
  tanggal: FormControl<AktaFormRawValue['tanggal']>;
  pukulAwal: FormControl<AktaFormRawValue['pukulAwal']>;
  pukulAkhir: FormControl<AktaFormRawValue['pukulAkhir']>;
  status: FormControl<AktaFormRawValue['status']>;
  updateBy: FormControl<AktaFormRawValue['updateBy']>;
  updateOn: FormControl<AktaFormRawValue['updateOn']>;
  ppk: FormControl<AktaFormRawValue['ppk']>;
  notaris: FormControl<AktaFormRawValue['notaris']>;
  penerimaKuasa: FormControl<AktaFormRawValue['penerimaKuasa']>;
  saksiOne: FormControl<AktaFormRawValue['saksiOne']>;
  saksiTwo: FormControl<AktaFormRawValue['saksiTwo']>;
  cabang: FormControl<AktaFormRawValue['cabang']>;
  skSubtitusi: FormControl<AktaFormRawValue['skSubtitusi']>;
};

export type AktaFormGroup = FormGroup<AktaFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AktaFormService {
  createAktaFormGroup(akta: AktaFormGroupInput = { id: null }): AktaFormGroup {
    const aktaRawValue = this.convertAktaToAktaRawValue({
      ...this.getFormDefaults(),
      ...akta,
    });
    return new FormGroup<AktaFormGroupContent>({
      id: new FormControl(
        { value: aktaRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      tglOrder: new FormControl(aktaRawValue.tglOrder),
      orderType: new FormControl(aktaRawValue.orderType),
      kode: new FormControl(aktaRawValue.kode),
      nomor: new FormControl(aktaRawValue.nomor),
      tanggal: new FormControl(aktaRawValue.tanggal),
      pukulAwal: new FormControl(aktaRawValue.pukulAwal),
      pukulAkhir: new FormControl(aktaRawValue.pukulAkhir),
      status: new FormControl(aktaRawValue.status),
      updateBy: new FormControl(aktaRawValue.updateBy),
      updateOn: new FormControl(aktaRawValue.updateOn),
      ppk: new FormControl(aktaRawValue.ppk),
      notaris: new FormControl(aktaRawValue.notaris),
      penerimaKuasa: new FormControl(aktaRawValue.penerimaKuasa),
      saksiOne: new FormControl(aktaRawValue.saksiOne),
      saksiTwo: new FormControl(aktaRawValue.saksiTwo),
      cabang: new FormControl(aktaRawValue.cabang),
      skSubtitusi: new FormControl(aktaRawValue.skSubtitusi),
    });
  }

  getAkta(form: AktaFormGroup): IAkta | NewAkta {
    return this.convertAktaRawValueToAkta(form.getRawValue() as AktaFormRawValue | NewAktaFormRawValue);
  }

  resetForm(form: AktaFormGroup, akta: AktaFormGroupInput): void {
    const aktaRawValue = this.convertAktaToAktaRawValue({ ...this.getFormDefaults(), ...akta });
    form.reset(
      {
        ...aktaRawValue,
        id: { value: aktaRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): AktaFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      pukulAwal: currentTime,
      pukulAkhir: currentTime,
      updateOn: currentTime,
    };
  }

  private convertAktaRawValueToAkta(rawAkta: AktaFormRawValue | NewAktaFormRawValue): IAkta | NewAkta {
    return {
      ...rawAkta,
      pukulAwal: dayjs(rawAkta.pukulAwal, DATE_TIME_FORMAT),
      pukulAkhir: dayjs(rawAkta.pukulAkhir, DATE_TIME_FORMAT),
      updateOn: dayjs(rawAkta.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertAktaToAktaRawValue(
    akta: IAkta | (Partial<NewAkta> & AktaFormDefaults),
  ): AktaFormRawValue | PartialWithRequiredKeyOf<NewAktaFormRawValue> {
    return {
      ...akta,
      pukulAwal: akta.pukulAwal ? akta.pukulAwal.format(DATE_TIME_FORMAT) : undefined,
      pukulAkhir: akta.pukulAkhir ? akta.pukulAkhir.format(DATE_TIME_FORMAT) : undefined,
      updateOn: akta.updateOn ? akta.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
