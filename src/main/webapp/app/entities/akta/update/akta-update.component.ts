import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPpk } from 'app/entities/ppk/ppk.model';
import { PpkService } from 'app/entities/ppk/service/ppk.service';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';
import { PenerimaKuasaService } from 'app/entities/penerima-kuasa/service/penerima-kuasa.service';
import { ISaksi } from 'app/entities/saksi/saksi.model';
import { SaksiService } from 'app/entities/saksi/service/saksi.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { SkSubtitusiService } from 'app/entities/sk-subtitusi/service/sk-subtitusi.service';
import { AktaService } from '../service/akta.service';
import { IAkta } from '../akta.model';
import { AktaFormService, AktaFormGroup } from './akta-form.service';

@Component({
  standalone: true,
  selector: 'jhi-akta-update',
  templateUrl: './akta-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class AktaUpdateComponent implements OnInit {
  isSaving = false;
  akta: IAkta | null = null;

  ppksCollection: IPpk[] = [];
  notarisesSharedCollection: INotaris[] = [];
  penerimaKuasasSharedCollection: IPenerimaKuasa[] = [];
  saksisSharedCollection: ISaksi[] = [];
  cabangsSharedCollection: ICabang[] = [];
  skSubtitusisSharedCollection: ISkSubtitusi[] = [];

  editForm: AktaFormGroup = this.aktaFormService.createAktaFormGroup();

  constructor(
    protected aktaService: AktaService,
    protected aktaFormService: AktaFormService,
    protected ppkService: PpkService,
    protected notarisService: NotarisService,
    protected penerimaKuasaService: PenerimaKuasaService,
    protected saksiService: SaksiService,
    protected cabangService: CabangService,
    protected skSubtitusiService: SkSubtitusiService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  comparePpk = (o1: IPpk | null, o2: IPpk | null): boolean => this.ppkService.comparePpk(o1, o2);

  compareNotaris = (o1: INotaris | null, o2: INotaris | null): boolean => this.notarisService.compareNotaris(o1, o2);

  comparePenerimaKuasa = (o1: IPenerimaKuasa | null, o2: IPenerimaKuasa | null): boolean =>
    this.penerimaKuasaService.comparePenerimaKuasa(o1, o2);

  compareSaksi = (o1: ISaksi | null, o2: ISaksi | null): boolean => this.saksiService.compareSaksi(o1, o2);

  compareCabang = (o1: ICabang | null, o2: ICabang | null): boolean => this.cabangService.compareCabang(o1, o2);

  compareSkSubtitusi = (o1: ISkSubtitusi | null, o2: ISkSubtitusi | null): boolean => this.skSubtitusiService.compareSkSubtitusi(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ akta }) => {
      this.akta = akta;
      if (akta) {
        this.updateForm(akta);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const akta = this.aktaFormService.getAkta(this.editForm);
    if (akta.id !== null) {
      this.subscribeToSaveResponse(this.aktaService.update(akta));
    } else {
      this.subscribeToSaveResponse(this.aktaService.create(akta));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAkta>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(akta: IAkta): void {
    this.akta = akta;
    this.aktaFormService.resetForm(this.editForm, akta);

    this.ppksCollection = this.ppkService.addPpkToCollectionIfMissing<IPpk>(this.ppksCollection, akta.ppk);
    this.notarisesSharedCollection = this.notarisService.addNotarisToCollectionIfMissing<INotaris>(
      this.notarisesSharedCollection,
      akta.notaris,
    );
    this.penerimaKuasasSharedCollection = this.penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing<IPenerimaKuasa>(
      this.penerimaKuasasSharedCollection,
      akta.penerimaKuasa,
    );
    this.saksisSharedCollection = this.saksiService.addSaksiToCollectionIfMissing<ISaksi>(
      this.saksisSharedCollection,
      akta.saksiOne,
      akta.saksiTwo,
    );
    this.cabangsSharedCollection = this.cabangService.addCabangToCollectionIfMissing<ICabang>(this.cabangsSharedCollection, akta.cabang);
    this.skSubtitusisSharedCollection = this.skSubtitusiService.addSkSubtitusiToCollectionIfMissing<ISkSubtitusi>(
      this.skSubtitusisSharedCollection,
      akta.skSubtitusi,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.ppkService
      .query({ filter: 'akta-is-null' })
      .pipe(map((res: HttpResponse<IPpk[]>) => res.body ?? []))
      .pipe(map((ppks: IPpk[]) => this.ppkService.addPpkToCollectionIfMissing<IPpk>(ppks, this.akta?.ppk)))
      .subscribe((ppks: IPpk[]) => (this.ppksCollection = ppks));

    this.notarisService
      .query()
      .pipe(map((res: HttpResponse<INotaris[]>) => res.body ?? []))
      .pipe(map((notarises: INotaris[]) => this.notarisService.addNotarisToCollectionIfMissing<INotaris>(notarises, this.akta?.notaris)))
      .subscribe((notarises: INotaris[]) => (this.notarisesSharedCollection = notarises));

    this.penerimaKuasaService
      .query()
      .pipe(map((res: HttpResponse<IPenerimaKuasa[]>) => res.body ?? []))
      .pipe(
        map((penerimaKuasas: IPenerimaKuasa[]) =>
          this.penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing<IPenerimaKuasa>(penerimaKuasas, this.akta?.penerimaKuasa),
        ),
      )
      .subscribe((penerimaKuasas: IPenerimaKuasa[]) => (this.penerimaKuasasSharedCollection = penerimaKuasas));

    this.saksiService
      .query()
      .pipe(map((res: HttpResponse<ISaksi[]>) => res.body ?? []))
      .pipe(
        map((saksis: ISaksi[]) =>
          this.saksiService.addSaksiToCollectionIfMissing<ISaksi>(saksis, this.akta?.saksiOne, this.akta?.saksiTwo),
        ),
      )
      .subscribe((saksis: ISaksi[]) => (this.saksisSharedCollection = saksis));

    this.cabangService
      .query()
      .pipe(map((res: HttpResponse<ICabang[]>) => res.body ?? []))
      .pipe(map((cabangs: ICabang[]) => this.cabangService.addCabangToCollectionIfMissing<ICabang>(cabangs, this.akta?.cabang)))
      .subscribe((cabangs: ICabang[]) => (this.cabangsSharedCollection = cabangs));

    this.skSubtitusiService
      .query()
      .pipe(map((res: HttpResponse<ISkSubtitusi[]>) => res.body ?? []))
      .pipe(
        map((skSubtitusis: ISkSubtitusi[]) =>
          this.skSubtitusiService.addSkSubtitusiToCollectionIfMissing<ISkSubtitusi>(skSubtitusis, this.akta?.skSubtitusi),
        ),
      )
      .subscribe((skSubtitusis: ISkSubtitusi[]) => (this.skSubtitusisSharedCollection = skSubtitusis));
  }
}
