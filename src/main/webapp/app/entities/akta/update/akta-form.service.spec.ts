import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../akta.test-samples';

import { AktaFormService } from './akta-form.service';

describe('Akta Form Service', () => {
  let service: AktaFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AktaFormService);
  });

  describe('Service methods', () => {
    describe('createAktaFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAktaFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            tglOrder: expect.any(Object),
            orderType: expect.any(Object),
            kode: expect.any(Object),
            nomor: expect.any(Object),
            tanggal: expect.any(Object),
            pukulAwal: expect.any(Object),
            pukulAkhir: expect.any(Object),
            status: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            ppk: expect.any(Object),
            notaris: expect.any(Object),
            penerimaKuasa: expect.any(Object),
            saksiOne: expect.any(Object),
            saksiTwo: expect.any(Object),
            cabang: expect.any(Object),
            skSubtitusi: expect.any(Object),
          }),
        );
      });

      it('passing IAkta should create a new form with FormGroup', () => {
        const formGroup = service.createAktaFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            tglOrder: expect.any(Object),
            orderType: expect.any(Object),
            kode: expect.any(Object),
            nomor: expect.any(Object),
            tanggal: expect.any(Object),
            pukulAwal: expect.any(Object),
            pukulAkhir: expect.any(Object),
            status: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            ppk: expect.any(Object),
            notaris: expect.any(Object),
            penerimaKuasa: expect.any(Object),
            saksiOne: expect.any(Object),
            saksiTwo: expect.any(Object),
            cabang: expect.any(Object),
            skSubtitusi: expect.any(Object),
          }),
        );
      });
    });

    describe('getAkta', () => {
      it('should return NewAkta for default Akta initial value', () => {
        const formGroup = service.createAktaFormGroup(sampleWithNewData);

        const akta = service.getAkta(formGroup) as any;

        expect(akta).toMatchObject(sampleWithNewData);
      });

      it('should return NewAkta for empty Akta initial value', () => {
        const formGroup = service.createAktaFormGroup();

        const akta = service.getAkta(formGroup) as any;

        expect(akta).toMatchObject({});
      });

      it('should return IAkta', () => {
        const formGroup = service.createAktaFormGroup(sampleWithRequiredData);

        const akta = service.getAkta(formGroup) as any;

        expect(akta).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAkta should not enable id FormControl', () => {
        const formGroup = service.createAktaFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAkta should disable id FormControl', () => {
        const formGroup = service.createAktaFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
