import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IPpk } from 'app/entities/ppk/ppk.model';
import { PpkService } from 'app/entities/ppk/service/ppk.service';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';
import { PenerimaKuasaService } from 'app/entities/penerima-kuasa/service/penerima-kuasa.service';
import { ISaksi } from 'app/entities/saksi/saksi.model';
import { SaksiService } from 'app/entities/saksi/service/saksi.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { SkSubtitusiService } from 'app/entities/sk-subtitusi/service/sk-subtitusi.service';
import { IAkta } from '../akta.model';
import { AktaService } from '../service/akta.service';
import { AktaFormService } from './akta-form.service';

import { AktaUpdateComponent } from './akta-update.component';

describe('Akta Management Update Component', () => {
  let comp: AktaUpdateComponent;
  let fixture: ComponentFixture<AktaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let aktaFormService: AktaFormService;
  let aktaService: AktaService;
  let ppkService: PpkService;
  let notarisService: NotarisService;
  let penerimaKuasaService: PenerimaKuasaService;
  let saksiService: SaksiService;
  let cabangService: CabangService;
  let skSubtitusiService: SkSubtitusiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), AktaUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AktaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AktaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    aktaFormService = TestBed.inject(AktaFormService);
    aktaService = TestBed.inject(AktaService);
    ppkService = TestBed.inject(PpkService);
    notarisService = TestBed.inject(NotarisService);
    penerimaKuasaService = TestBed.inject(PenerimaKuasaService);
    saksiService = TestBed.inject(SaksiService);
    cabangService = TestBed.inject(CabangService);
    skSubtitusiService = TestBed.inject(SkSubtitusiService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ppk query and add missing value', () => {
      const akta: IAkta = { id: 456 };
      const ppk: IPpk = { id: 19126 };
      akta.ppk = ppk;

      const ppkCollection: IPpk[] = [{ id: 20248 }];
      jest.spyOn(ppkService, 'query').mockReturnValue(of(new HttpResponse({ body: ppkCollection })));
      const expectedCollection: IPpk[] = [ppk, ...ppkCollection];
      jest.spyOn(ppkService, 'addPpkToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      expect(ppkService.query).toHaveBeenCalled();
      expect(ppkService.addPpkToCollectionIfMissing).toHaveBeenCalledWith(ppkCollection, ppk);
      expect(comp.ppksCollection).toEqual(expectedCollection);
    });

    it('Should call Notaris query and add missing value', () => {
      const akta: IAkta = { id: 456 };
      const notaris: INotaris = { id: 7109 };
      akta.notaris = notaris;

      const notarisCollection: INotaris[] = [{ id: 6844 }];
      jest.spyOn(notarisService, 'query').mockReturnValue(of(new HttpResponse({ body: notarisCollection })));
      const additionalNotarises = [notaris];
      const expectedCollection: INotaris[] = [...additionalNotarises, ...notarisCollection];
      jest.spyOn(notarisService, 'addNotarisToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      expect(notarisService.query).toHaveBeenCalled();
      expect(notarisService.addNotarisToCollectionIfMissing).toHaveBeenCalledWith(
        notarisCollection,
        ...additionalNotarises.map(expect.objectContaining),
      );
      expect(comp.notarisesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call PenerimaKuasa query and add missing value', () => {
      const akta: IAkta = { id: 456 };
      const penerimaKuasa: IPenerimaKuasa = { id: 17955 };
      akta.penerimaKuasa = penerimaKuasa;

      const penerimaKuasaCollection: IPenerimaKuasa[] = [{ id: 14204 }];
      jest.spyOn(penerimaKuasaService, 'query').mockReturnValue(of(new HttpResponse({ body: penerimaKuasaCollection })));
      const additionalPenerimaKuasas = [penerimaKuasa];
      const expectedCollection: IPenerimaKuasa[] = [...additionalPenerimaKuasas, ...penerimaKuasaCollection];
      jest.spyOn(penerimaKuasaService, 'addPenerimaKuasaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      expect(penerimaKuasaService.query).toHaveBeenCalled();
      expect(penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing).toHaveBeenCalledWith(
        penerimaKuasaCollection,
        ...additionalPenerimaKuasas.map(expect.objectContaining),
      );
      expect(comp.penerimaKuasasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Saksi query and add missing value', () => {
      const akta: IAkta = { id: 456 };
      const saksiOne: ISaksi = { id: 3318 };
      akta.saksiOne = saksiOne;
      const saksiTwo: ISaksi = { id: 31421 };
      akta.saksiTwo = saksiTwo;

      const saksiCollection: ISaksi[] = [{ id: 23563 }];
      jest.spyOn(saksiService, 'query').mockReturnValue(of(new HttpResponse({ body: saksiCollection })));
      const additionalSaksis = [saksiOne, saksiTwo];
      const expectedCollection: ISaksi[] = [...additionalSaksis, ...saksiCollection];
      jest.spyOn(saksiService, 'addSaksiToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      expect(saksiService.query).toHaveBeenCalled();
      expect(saksiService.addSaksiToCollectionIfMissing).toHaveBeenCalledWith(
        saksiCollection,
        ...additionalSaksis.map(expect.objectContaining),
      );
      expect(comp.saksisSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Cabang query and add missing value', () => {
      const akta: IAkta = { id: 456 };
      const cabang: ICabang = { id: 5219 };
      akta.cabang = cabang;

      const cabangCollection: ICabang[] = [{ id: 5942 }];
      jest.spyOn(cabangService, 'query').mockReturnValue(of(new HttpResponse({ body: cabangCollection })));
      const additionalCabangs = [cabang];
      const expectedCollection: ICabang[] = [...additionalCabangs, ...cabangCollection];
      jest.spyOn(cabangService, 'addCabangToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      expect(cabangService.query).toHaveBeenCalled();
      expect(cabangService.addCabangToCollectionIfMissing).toHaveBeenCalledWith(
        cabangCollection,
        ...additionalCabangs.map(expect.objectContaining),
      );
      expect(comp.cabangsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call SkSubtitusi query and add missing value', () => {
      const akta: IAkta = { id: 456 };
      const skSubtitusi: ISkSubtitusi = { id: 2498 };
      akta.skSubtitusi = skSubtitusi;

      const skSubtitusiCollection: ISkSubtitusi[] = [{ id: 31097 }];
      jest.spyOn(skSubtitusiService, 'query').mockReturnValue(of(new HttpResponse({ body: skSubtitusiCollection })));
      const additionalSkSubtitusis = [skSubtitusi];
      const expectedCollection: ISkSubtitusi[] = [...additionalSkSubtitusis, ...skSubtitusiCollection];
      jest.spyOn(skSubtitusiService, 'addSkSubtitusiToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      expect(skSubtitusiService.query).toHaveBeenCalled();
      expect(skSubtitusiService.addSkSubtitusiToCollectionIfMissing).toHaveBeenCalledWith(
        skSubtitusiCollection,
        ...additionalSkSubtitusis.map(expect.objectContaining),
      );
      expect(comp.skSubtitusisSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const akta: IAkta = { id: 456 };
      const ppk: IPpk = { id: 29379 };
      akta.ppk = ppk;
      const notaris: INotaris = { id: 1828 };
      akta.notaris = notaris;
      const penerimaKuasa: IPenerimaKuasa = { id: 16764 };
      akta.penerimaKuasa = penerimaKuasa;
      const saksiOne: ISaksi = { id: 15121 };
      akta.saksiOne = saksiOne;
      const saksiTwo: ISaksi = { id: 28869 };
      akta.saksiTwo = saksiTwo;
      const cabang: ICabang = { id: 8542 };
      akta.cabang = cabang;
      const skSubtitusi: ISkSubtitusi = { id: 25732 };
      akta.skSubtitusi = skSubtitusi;

      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      expect(comp.ppksCollection).toContain(ppk);
      expect(comp.notarisesSharedCollection).toContain(notaris);
      expect(comp.penerimaKuasasSharedCollection).toContain(penerimaKuasa);
      expect(comp.saksisSharedCollection).toContain(saksiOne);
      expect(comp.saksisSharedCollection).toContain(saksiTwo);
      expect(comp.cabangsSharedCollection).toContain(cabang);
      expect(comp.skSubtitusisSharedCollection).toContain(skSubtitusi);
      expect(comp.akta).toEqual(akta);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAkta>>();
      const akta = { id: 123 };
      jest.spyOn(aktaFormService, 'getAkta').mockReturnValue(akta);
      jest.spyOn(aktaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: akta }));
      saveSubject.complete();

      // THEN
      expect(aktaFormService.getAkta).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(aktaService.update).toHaveBeenCalledWith(expect.objectContaining(akta));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAkta>>();
      const akta = { id: 123 };
      jest.spyOn(aktaFormService, 'getAkta').mockReturnValue({ id: null });
      jest.spyOn(aktaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ akta: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: akta }));
      saveSubject.complete();

      // THEN
      expect(aktaFormService.getAkta).toHaveBeenCalled();
      expect(aktaService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAkta>>();
      const akta = { id: 123 };
      jest.spyOn(aktaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ akta });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(aktaService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePpk', () => {
      it('Should forward to ppkService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(ppkService, 'comparePpk');
        comp.comparePpk(entity, entity2);
        expect(ppkService.comparePpk).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareNotaris', () => {
      it('Should forward to notarisService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(notarisService, 'compareNotaris');
        comp.compareNotaris(entity, entity2);
        expect(notarisService.compareNotaris).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('comparePenerimaKuasa', () => {
      it('Should forward to penerimaKuasaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(penerimaKuasaService, 'comparePenerimaKuasa');
        comp.comparePenerimaKuasa(entity, entity2);
        expect(penerimaKuasaService.comparePenerimaKuasa).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareSaksi', () => {
      it('Should forward to saksiService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(saksiService, 'compareSaksi');
        comp.compareSaksi(entity, entity2);
        expect(saksiService.compareSaksi).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareCabang', () => {
      it('Should forward to cabangService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(cabangService, 'compareCabang');
        comp.compareCabang(entity, entity2);
        expect(cabangService.compareCabang).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareSkSubtitusi', () => {
      it('Should forward to skSubtitusiService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(skSubtitusiService, 'compareSkSubtitusi');
        comp.compareSkSubtitusi(entity, entity2);
        expect(skSubtitusiService.compareSkSubtitusi).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
