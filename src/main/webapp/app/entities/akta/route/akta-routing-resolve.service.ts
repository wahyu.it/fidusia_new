import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAkta } from '../akta.model';
import { AktaService } from '../service/akta.service';

export const aktaResolve = (route: ActivatedRouteSnapshot): Observable<null | IAkta> => {
  const id = route.params['id'];
  if (id) {
    return inject(AktaService)
      .find(id)
      .pipe(
        mergeMap((akta: HttpResponse<IAkta>) => {
          if (akta.body) {
            return of(akta.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default aktaResolve;
