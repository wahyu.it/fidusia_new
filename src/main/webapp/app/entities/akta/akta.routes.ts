import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { AktaComponent } from './list/akta.component';
import { AktaDetailComponent } from './detail/akta-detail.component';
import { AktaUpdateComponent } from './update/akta-update.component';
import AktaResolve from './route/akta-routing-resolve.service';

const aktaRoute: Routes = [
  {
    path: '',
    component: AktaComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AktaDetailComponent,
    resolve: {
      akta: AktaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AktaUpdateComponent,
    resolve: {
      akta: AktaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AktaUpdateComponent,
    resolve: {
      akta: AktaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default aktaRoute;
