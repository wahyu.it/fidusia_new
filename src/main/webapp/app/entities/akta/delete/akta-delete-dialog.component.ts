import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IAkta } from '../akta.model';
import { AktaService } from '../service/akta.service';

@Component({
  standalone: true,
  templateUrl: './akta-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class AktaDeleteDialogComponent {
  akta?: IAkta;

  constructor(
    protected aktaService: AktaService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.aktaService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
