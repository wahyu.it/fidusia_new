import dayjs from 'dayjs/esm';
import { IPpk } from 'app/entities/ppk/ppk.model';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';
import { ISaksi } from 'app/entities/saksi/saksi.model';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';

export interface IAkta {
  id: number;
  tglOrder?: dayjs.Dayjs | null;
  orderType?: number | null;
  kode?: string | null;
  nomor?: string | null;
  tanggal?: string | null;
  pukulAwal?: dayjs.Dayjs | null;
  pukulAkhir?: dayjs.Dayjs | null;
  status?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  ppk?: IPpk | null;
  notaris?: INotaris | null;
  penerimaKuasa?: IPenerimaKuasa | null;
  saksiOne?: ISaksi | null;
  saksiTwo?: ISaksi | null;
  cabang?: ICabang | null;
  skSubtitusi?: ISkSubtitusi | null;
}

export type NewAkta = Omit<IAkta, 'id'> & { id: null };
