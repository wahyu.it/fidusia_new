import dayjs from 'dayjs/esm';

import { IAkta, NewAkta } from './akta.model';

export const sampleWithRequiredData: IAkta = {
  id: 7740,
};

export const sampleWithPartialData: IAkta = {
  id: 19702,
  orderType: 27093,
  nomor: 'whoever especially',
  tanggal: 'unto till previous',
  pukulAwal: dayjs('2023-12-14T09:59'),
};

export const sampleWithFullData: IAkta = {
  id: 13630,
  tglOrder: dayjs('2023-12-14'),
  orderType: 22884,
  kode: 'room but amnesty',
  nomor: 'throughout recant glare',
  tanggal: 'however wreathe',
  pukulAwal: dayjs('2023-12-14T16:10'),
  pukulAkhir: dayjs('2023-12-14T10:48'),
  status: 29932,
  updateBy: 'dollar',
  updateOn: dayjs('2023-12-14T05:08'),
};

export const sampleWithNewData: NewAkta = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
