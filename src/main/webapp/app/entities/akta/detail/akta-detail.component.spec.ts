import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AktaDetailComponent } from './akta-detail.component';

describe('Akta Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AktaDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: AktaDetailComponent,
              resolve: { akta: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(AktaDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load akta on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', AktaDetailComponent);

      // THEN
      expect(instance.akta).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
