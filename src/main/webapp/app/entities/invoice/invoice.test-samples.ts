import dayjs from 'dayjs/esm';

import { IInvoice, NewInvoice } from './invoice.model';

export const sampleWithRequiredData: IInvoice = {
  id: 17014,
};

export const sampleWithPartialData: IInvoice = {
  id: 30592,
  type: 25045,
  tanggal: dayjs('2023-12-14'),
  noProformaPnbp: 'abnormally drat stool',
  noProformaJasa: 'buckle but owlishly',
  noBillpymPnbp: 'reinforce',
  noBillpymJasa: 'high-level extra-large',
  amountPnbp: 10428,
  amountJasa: 12922,
  vatJasa: 9909,
  taxJasa: 12307,
  totalAmountJasa: 17812,
  fileProformaJasa: 'wherever safeguard times',
  fileBillpymPnbp: 'marvelous',
  fileTxt: 'realize beneath',
};

export const sampleWithFullData: IInvoice = {
  id: 17746,
  type: 14638,
  tanggal: dayjs('2023-12-14'),
  tglOrder: dayjs('2023-12-14'),
  jmlOrder: 16307,
  jmlOrderCancel: 28007,
  noProformaPnbp: 'scruple',
  noProformaJasa: 'roar reliable',
  noBillpymPnbp: 'witch',
  noBillpymJasa: 'rouse',
  amountPnbp: 6014,
  taxPnbp: 528,
  totalAmountPnbp: 27505,
  amountJasa: 6441,
  vatJasa: 21239,
  taxJasa: 564,
  totalAmountJasa: 10688,
  fileProformaPnbp: 'hm while',
  fileProformaJasa: 'revive redistribute',
  fileBillpymPnbp: 'snappy mixer',
  fileBillpymJasa: 'vivaciously',
  fileBulk: 'corps',
  fileTax: 'washtub inlay phew',
  fileTxt: 'infatuated shuttle',
  status: 27668,
  updateBy: 'voluntarily which fussy',
  updateOn: dayjs('2023-12-14T06:16'),
};

export const sampleWithNewData: NewInvoice = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
