import dayjs from 'dayjs/esm';

export interface IInvoice {
  id: number;
  type?: number | null;
  tanggal?: dayjs.Dayjs | null;
  tglOrder?: dayjs.Dayjs | null;
  jmlOrder?: number | null;
  jmlOrderCancel?: number | null;
  noProformaPnbp?: string | null;
  noProformaJasa?: string | null;
  noBillpymPnbp?: string | null;
  noBillpymJasa?: string | null;
  amountPnbp?: number | null;
  taxPnbp?: number | null;
  totalAmountPnbp?: number | null;
  amountJasa?: number | null;
  vatJasa?: number | null;
  taxJasa?: number | null;
  totalAmountJasa?: number | null;
  fileProformaPnbp?: string | null;
  fileProformaJasa?: string | null;
  fileBillpymPnbp?: string | null;
  fileBillpymJasa?: string | null;
  fileBulk?: string | null;
  fileTax?: string | null;
  fileTxt?: string | null;
  status?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
}

export type NewInvoice = Omit<IInvoice, 'id'> & { id: null };
