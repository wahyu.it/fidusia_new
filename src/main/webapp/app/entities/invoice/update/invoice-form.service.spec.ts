import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../invoice.test-samples';

import { InvoiceFormService } from './invoice-form.service';

describe('Invoice Form Service', () => {
  let service: InvoiceFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InvoiceFormService);
  });

  describe('Service methods', () => {
    describe('createInvoiceFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createInvoiceFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            type: expect.any(Object),
            tanggal: expect.any(Object),
            tglOrder: expect.any(Object),
            jmlOrder: expect.any(Object),
            jmlOrderCancel: expect.any(Object),
            noProformaPnbp: expect.any(Object),
            noProformaJasa: expect.any(Object),
            noBillpymPnbp: expect.any(Object),
            noBillpymJasa: expect.any(Object),
            amountPnbp: expect.any(Object),
            taxPnbp: expect.any(Object),
            totalAmountPnbp: expect.any(Object),
            amountJasa: expect.any(Object),
            vatJasa: expect.any(Object),
            taxJasa: expect.any(Object),
            totalAmountJasa: expect.any(Object),
            fileProformaPnbp: expect.any(Object),
            fileProformaJasa: expect.any(Object),
            fileBillpymPnbp: expect.any(Object),
            fileBillpymJasa: expect.any(Object),
            fileBulk: expect.any(Object),
            fileTax: expect.any(Object),
            fileTxt: expect.any(Object),
            status: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
          }),
        );
      });

      it('passing IInvoice should create a new form with FormGroup', () => {
        const formGroup = service.createInvoiceFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            type: expect.any(Object),
            tanggal: expect.any(Object),
            tglOrder: expect.any(Object),
            jmlOrder: expect.any(Object),
            jmlOrderCancel: expect.any(Object),
            noProformaPnbp: expect.any(Object),
            noProformaJasa: expect.any(Object),
            noBillpymPnbp: expect.any(Object),
            noBillpymJasa: expect.any(Object),
            amountPnbp: expect.any(Object),
            taxPnbp: expect.any(Object),
            totalAmountPnbp: expect.any(Object),
            amountJasa: expect.any(Object),
            vatJasa: expect.any(Object),
            taxJasa: expect.any(Object),
            totalAmountJasa: expect.any(Object),
            fileProformaPnbp: expect.any(Object),
            fileProformaJasa: expect.any(Object),
            fileBillpymPnbp: expect.any(Object),
            fileBillpymJasa: expect.any(Object),
            fileBulk: expect.any(Object),
            fileTax: expect.any(Object),
            fileTxt: expect.any(Object),
            status: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
          }),
        );
      });
    });

    describe('getInvoice', () => {
      it('should return NewInvoice for default Invoice initial value', () => {
        const formGroup = service.createInvoiceFormGroup(sampleWithNewData);

        const invoice = service.getInvoice(formGroup) as any;

        expect(invoice).toMatchObject(sampleWithNewData);
      });

      it('should return NewInvoice for empty Invoice initial value', () => {
        const formGroup = service.createInvoiceFormGroup();

        const invoice = service.getInvoice(formGroup) as any;

        expect(invoice).toMatchObject({});
      });

      it('should return IInvoice', () => {
        const formGroup = service.createInvoiceFormGroup(sampleWithRequiredData);

        const invoice = service.getInvoice(formGroup) as any;

        expect(invoice).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IInvoice should not enable id FormControl', () => {
        const formGroup = service.createInvoiceFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewInvoice should disable id FormControl', () => {
        const formGroup = service.createInvoiceFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
