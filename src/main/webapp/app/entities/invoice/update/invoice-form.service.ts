import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IInvoice, NewInvoice } from '../invoice.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IInvoice for edit and NewInvoiceFormGroupInput for create.
 */
type InvoiceFormGroupInput = IInvoice | PartialWithRequiredKeyOf<NewInvoice>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IInvoice | NewInvoice> = Omit<T, 'updateOn'> & {
  updateOn?: string | null;
};

type InvoiceFormRawValue = FormValueOf<IInvoice>;

type NewInvoiceFormRawValue = FormValueOf<NewInvoice>;

type InvoiceFormDefaults = Pick<NewInvoice, 'id' | 'updateOn'>;

type InvoiceFormGroupContent = {
  id: FormControl<InvoiceFormRawValue['id'] | NewInvoice['id']>;
  type: FormControl<InvoiceFormRawValue['type']>;
  tanggal: FormControl<InvoiceFormRawValue['tanggal']>;
  tglOrder: FormControl<InvoiceFormRawValue['tglOrder']>;
  jmlOrder: FormControl<InvoiceFormRawValue['jmlOrder']>;
  jmlOrderCancel: FormControl<InvoiceFormRawValue['jmlOrderCancel']>;
  noProformaPnbp: FormControl<InvoiceFormRawValue['noProformaPnbp']>;
  noProformaJasa: FormControl<InvoiceFormRawValue['noProformaJasa']>;
  noBillpymPnbp: FormControl<InvoiceFormRawValue['noBillpymPnbp']>;
  noBillpymJasa: FormControl<InvoiceFormRawValue['noBillpymJasa']>;
  amountPnbp: FormControl<InvoiceFormRawValue['amountPnbp']>;
  taxPnbp: FormControl<InvoiceFormRawValue['taxPnbp']>;
  totalAmountPnbp: FormControl<InvoiceFormRawValue['totalAmountPnbp']>;
  amountJasa: FormControl<InvoiceFormRawValue['amountJasa']>;
  vatJasa: FormControl<InvoiceFormRawValue['vatJasa']>;
  taxJasa: FormControl<InvoiceFormRawValue['taxJasa']>;
  totalAmountJasa: FormControl<InvoiceFormRawValue['totalAmountJasa']>;
  fileProformaPnbp: FormControl<InvoiceFormRawValue['fileProformaPnbp']>;
  fileProformaJasa: FormControl<InvoiceFormRawValue['fileProformaJasa']>;
  fileBillpymPnbp: FormControl<InvoiceFormRawValue['fileBillpymPnbp']>;
  fileBillpymJasa: FormControl<InvoiceFormRawValue['fileBillpymJasa']>;
  fileBulk: FormControl<InvoiceFormRawValue['fileBulk']>;
  fileTax: FormControl<InvoiceFormRawValue['fileTax']>;
  fileTxt: FormControl<InvoiceFormRawValue['fileTxt']>;
  status: FormControl<InvoiceFormRawValue['status']>;
  updateBy: FormControl<InvoiceFormRawValue['updateBy']>;
  updateOn: FormControl<InvoiceFormRawValue['updateOn']>;
};

export type InvoiceFormGroup = FormGroup<InvoiceFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class InvoiceFormService {
  createInvoiceFormGroup(invoice: InvoiceFormGroupInput = { id: null }): InvoiceFormGroup {
    const invoiceRawValue = this.convertInvoiceToInvoiceRawValue({
      ...this.getFormDefaults(),
      ...invoice,
    });
    return new FormGroup<InvoiceFormGroupContent>({
      id: new FormControl(
        { value: invoiceRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      type: new FormControl(invoiceRawValue.type),
      tanggal: new FormControl(invoiceRawValue.tanggal),
      tglOrder: new FormControl(invoiceRawValue.tglOrder),
      jmlOrder: new FormControl(invoiceRawValue.jmlOrder),
      jmlOrderCancel: new FormControl(invoiceRawValue.jmlOrderCancel),
      noProformaPnbp: new FormControl(invoiceRawValue.noProformaPnbp),
      noProformaJasa: new FormControl(invoiceRawValue.noProformaJasa),
      noBillpymPnbp: new FormControl(invoiceRawValue.noBillpymPnbp),
      noBillpymJasa: new FormControl(invoiceRawValue.noBillpymJasa),
      amountPnbp: new FormControl(invoiceRawValue.amountPnbp),
      taxPnbp: new FormControl(invoiceRawValue.taxPnbp),
      totalAmountPnbp: new FormControl(invoiceRawValue.totalAmountPnbp),
      amountJasa: new FormControl(invoiceRawValue.amountJasa),
      vatJasa: new FormControl(invoiceRawValue.vatJasa),
      taxJasa: new FormControl(invoiceRawValue.taxJasa),
      totalAmountJasa: new FormControl(invoiceRawValue.totalAmountJasa),
      fileProformaPnbp: new FormControl(invoiceRawValue.fileProformaPnbp),
      fileProformaJasa: new FormControl(invoiceRawValue.fileProformaJasa),
      fileBillpymPnbp: new FormControl(invoiceRawValue.fileBillpymPnbp),
      fileBillpymJasa: new FormControl(invoiceRawValue.fileBillpymJasa),
      fileBulk: new FormControl(invoiceRawValue.fileBulk),
      fileTax: new FormControl(invoiceRawValue.fileTax),
      fileTxt: new FormControl(invoiceRawValue.fileTxt),
      status: new FormControl(invoiceRawValue.status),
      updateBy: new FormControl(invoiceRawValue.updateBy),
      updateOn: new FormControl(invoiceRawValue.updateOn),
    });
  }

  getInvoice(form: InvoiceFormGroup): IInvoice | NewInvoice {
    return this.convertInvoiceRawValueToInvoice(form.getRawValue() as InvoiceFormRawValue | NewInvoiceFormRawValue);
  }

  resetForm(form: InvoiceFormGroup, invoice: InvoiceFormGroupInput): void {
    const invoiceRawValue = this.convertInvoiceToInvoiceRawValue({ ...this.getFormDefaults(), ...invoice });
    form.reset(
      {
        ...invoiceRawValue,
        id: { value: invoiceRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): InvoiceFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      updateOn: currentTime,
    };
  }

  private convertInvoiceRawValueToInvoice(rawInvoice: InvoiceFormRawValue | NewInvoiceFormRawValue): IInvoice | NewInvoice {
    return {
      ...rawInvoice,
      updateOn: dayjs(rawInvoice.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertInvoiceToInvoiceRawValue(
    invoice: IInvoice | (Partial<NewInvoice> & InvoiceFormDefaults),
  ): InvoiceFormRawValue | PartialWithRequiredKeyOf<NewInvoiceFormRawValue> {
    return {
      ...invoice,
      updateOn: invoice.updateOn ? invoice.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
