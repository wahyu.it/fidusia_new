import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ICabang, NewCabang } from '../cabang.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ICabang for edit and NewCabangFormGroupInput for create.
 */
type CabangFormGroupInput = ICabang | PartialWithRequiredKeyOf<NewCabang>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ICabang | NewCabang> = Omit<T, 'updateOn'> & {
  updateOn?: string | null;
};

type CabangFormRawValue = FormValueOf<ICabang>;

type NewCabangFormRawValue = FormValueOf<NewCabang>;

type CabangFormDefaults = Pick<NewCabang, 'id' | 'updateOn'>;

type CabangFormGroupContent = {
  id: FormControl<CabangFormRawValue['id'] | NewCabang['id']>;
  kode: FormControl<CabangFormRawValue['kode']>;
  nama: FormControl<CabangFormRawValue['nama']>;
  alamat: FormControl<CabangFormRawValue['alamat']>;
  kota: FormControl<CabangFormRawValue['kota']>;
  provinsi: FormControl<CabangFormRawValue['provinsi']>;
  pengadilanNegeri: FormControl<CabangFormRawValue['pengadilanNegeri']>;
  noKontak: FormControl<CabangFormRawValue['noKontak']>;
  recordStatus: FormControl<CabangFormRawValue['recordStatus']>;
  updateBy: FormControl<CabangFormRawValue['updateBy']>;
  updateOn: FormControl<CabangFormRawValue['updateOn']>;
  startDate: FormControl<CabangFormRawValue['startDate']>;
  leasing: FormControl<CabangFormRawValue['leasing']>;
};

export type CabangFormGroup = FormGroup<CabangFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class CabangFormService {
  createCabangFormGroup(cabang: CabangFormGroupInput = { id: null }): CabangFormGroup {
    const cabangRawValue = this.convertCabangToCabangRawValue({
      ...this.getFormDefaults(),
      ...cabang,
    });
    return new FormGroup<CabangFormGroupContent>({
      id: new FormControl(
        { value: cabangRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      kode: new FormControl(cabangRawValue.kode),
      nama: new FormControl(cabangRawValue.nama),
      alamat: new FormControl(cabangRawValue.alamat),
      kota: new FormControl(cabangRawValue.kota),
      provinsi: new FormControl(cabangRawValue.provinsi),
      pengadilanNegeri: new FormControl(cabangRawValue.pengadilanNegeri),
      noKontak: new FormControl(cabangRawValue.noKontak),
      recordStatus: new FormControl(cabangRawValue.recordStatus),
      updateBy: new FormControl(cabangRawValue.updateBy),
      updateOn: new FormControl(cabangRawValue.updateOn),
      startDate: new FormControl(cabangRawValue.startDate),
      leasing: new FormControl(cabangRawValue.leasing),
    });
  }

  getCabang(form: CabangFormGroup): ICabang | NewCabang {
    return this.convertCabangRawValueToCabang(form.getRawValue() as CabangFormRawValue | NewCabangFormRawValue);
  }

  resetForm(form: CabangFormGroup, cabang: CabangFormGroupInput): void {
    const cabangRawValue = this.convertCabangToCabangRawValue({ ...this.getFormDefaults(), ...cabang });
    form.reset(
      {
        ...cabangRawValue,
        id: { value: cabangRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): CabangFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      updateOn: currentTime,
    };
  }

  private convertCabangRawValueToCabang(rawCabang: CabangFormRawValue | NewCabangFormRawValue): ICabang | NewCabang {
    return {
      ...rawCabang,
      updateOn: dayjs(rawCabang.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertCabangToCabangRawValue(
    cabang: ICabang | (Partial<NewCabang> & CabangFormDefaults),
  ): CabangFormRawValue | PartialWithRequiredKeyOf<NewCabangFormRawValue> {
    return {
      ...cabang,
      updateOn: cabang.updateOn ? cabang.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
