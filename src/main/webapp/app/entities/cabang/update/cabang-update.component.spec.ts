import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ILeasing } from 'app/entities/leasing/leasing.model';
import { LeasingService } from 'app/entities/leasing/service/leasing.service';
import { CabangService } from '../service/cabang.service';
import { ICabang } from '../cabang.model';
import { CabangFormService } from './cabang-form.service';

import { CabangUpdateComponent } from './cabang-update.component';

describe('Cabang Management Update Component', () => {
  let comp: CabangUpdateComponent;
  let fixture: ComponentFixture<CabangUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let cabangFormService: CabangFormService;
  let cabangService: CabangService;
  let leasingService: LeasingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), CabangUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CabangUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CabangUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    cabangFormService = TestBed.inject(CabangFormService);
    cabangService = TestBed.inject(CabangService);
    leasingService = TestBed.inject(LeasingService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Leasing query and add missing value', () => {
      const cabang: ICabang = { id: 456 };
      const leasing: ILeasing = { id: 1767 };
      cabang.leasing = leasing;

      const leasingCollection: ILeasing[] = [{ id: 24418 }];
      jest.spyOn(leasingService, 'query').mockReturnValue(of(new HttpResponse({ body: leasingCollection })));
      const additionalLeasings = [leasing];
      const expectedCollection: ILeasing[] = [...additionalLeasings, ...leasingCollection];
      jest.spyOn(leasingService, 'addLeasingToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ cabang });
      comp.ngOnInit();

      expect(leasingService.query).toHaveBeenCalled();
      expect(leasingService.addLeasingToCollectionIfMissing).toHaveBeenCalledWith(
        leasingCollection,
        ...additionalLeasings.map(expect.objectContaining),
      );
      expect(comp.leasingsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const cabang: ICabang = { id: 456 };
      const leasing: ILeasing = { id: 26060 };
      cabang.leasing = leasing;

      activatedRoute.data = of({ cabang });
      comp.ngOnInit();

      expect(comp.leasingsSharedCollection).toContain(leasing);
      expect(comp.cabang).toEqual(cabang);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICabang>>();
      const cabang = { id: 123 };
      jest.spyOn(cabangFormService, 'getCabang').mockReturnValue(cabang);
      jest.spyOn(cabangService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cabang });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: cabang }));
      saveSubject.complete();

      // THEN
      expect(cabangFormService.getCabang).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(cabangService.update).toHaveBeenCalledWith(expect.objectContaining(cabang));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICabang>>();
      const cabang = { id: 123 };
      jest.spyOn(cabangFormService, 'getCabang').mockReturnValue({ id: null });
      jest.spyOn(cabangService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cabang: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: cabang }));
      saveSubject.complete();

      // THEN
      expect(cabangFormService.getCabang).toHaveBeenCalled();
      expect(cabangService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICabang>>();
      const cabang = { id: 123 };
      jest.spyOn(cabangService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cabang });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(cabangService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareLeasing', () => {
      it('Should forward to leasingService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(leasingService, 'compareLeasing');
        comp.compareLeasing(entity, entity2);
        expect(leasingService.compareLeasing).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
