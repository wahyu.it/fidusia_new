import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ILeasing } from 'app/entities/leasing/leasing.model';
import { LeasingService } from 'app/entities/leasing/service/leasing.service';
import { ICabang } from '../cabang.model';
import { CabangService } from '../service/cabang.service';
import { CabangFormService, CabangFormGroup } from './cabang-form.service';

@Component({
  standalone: true,
  selector: 'jhi-cabang-update',
  templateUrl: './cabang-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class CabangUpdateComponent implements OnInit {
  isSaving = false;
  cabang: ICabang | null = null;

  leasingsSharedCollection: ILeasing[] = [];

  editForm: CabangFormGroup = this.cabangFormService.createCabangFormGroup();

  constructor(
    protected cabangService: CabangService,
    protected cabangFormService: CabangFormService,
    protected leasingService: LeasingService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareLeasing = (o1: ILeasing | null, o2: ILeasing | null): boolean => this.leasingService.compareLeasing(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cabang }) => {
      this.cabang = cabang;
      if (cabang) {
        this.updateForm(cabang);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cabang = this.cabangFormService.getCabang(this.editForm);
    if (cabang.id !== null) {
      this.subscribeToSaveResponse(this.cabangService.update(cabang));
    } else {
      this.subscribeToSaveResponse(this.cabangService.create(cabang));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICabang>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(cabang: ICabang): void {
    this.cabang = cabang;
    this.cabangFormService.resetForm(this.editForm, cabang);

    this.leasingsSharedCollection = this.leasingService.addLeasingToCollectionIfMissing<ILeasing>(
      this.leasingsSharedCollection,
      cabang.leasing,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.leasingService
      .query()
      .pipe(map((res: HttpResponse<ILeasing[]>) => res.body ?? []))
      .pipe(map((leasings: ILeasing[]) => this.leasingService.addLeasingToCollectionIfMissing<ILeasing>(leasings, this.cabang?.leasing)))
      .subscribe((leasings: ILeasing[]) => (this.leasingsSharedCollection = leasings));
  }
}
