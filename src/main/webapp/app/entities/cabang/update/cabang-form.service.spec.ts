import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../cabang.test-samples';

import { CabangFormService } from './cabang-form.service';

describe('Cabang Form Service', () => {
  let service: CabangFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CabangFormService);
  });

  describe('Service methods', () => {
    describe('createCabangFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createCabangFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kode: expect.any(Object),
            nama: expect.any(Object),
            alamat: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            pengadilanNegeri: expect.any(Object),
            noKontak: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            startDate: expect.any(Object),
            leasing: expect.any(Object),
          }),
        );
      });

      it('passing ICabang should create a new form with FormGroup', () => {
        const formGroup = service.createCabangFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kode: expect.any(Object),
            nama: expect.any(Object),
            alamat: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            pengadilanNegeri: expect.any(Object),
            noKontak: expect.any(Object),
            recordStatus: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            startDate: expect.any(Object),
            leasing: expect.any(Object),
          }),
        );
      });
    });

    describe('getCabang', () => {
      it('should return NewCabang for default Cabang initial value', () => {
        const formGroup = service.createCabangFormGroup(sampleWithNewData);

        const cabang = service.getCabang(formGroup) as any;

        expect(cabang).toMatchObject(sampleWithNewData);
      });

      it('should return NewCabang for empty Cabang initial value', () => {
        const formGroup = service.createCabangFormGroup();

        const cabang = service.getCabang(formGroup) as any;

        expect(cabang).toMatchObject({});
      });

      it('should return ICabang', () => {
        const formGroup = service.createCabangFormGroup(sampleWithRequiredData);

        const cabang = service.getCabang(formGroup) as any;

        expect(cabang).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ICabang should not enable id FormControl', () => {
        const formGroup = service.createCabangFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewCabang should disable id FormControl', () => {
        const formGroup = service.createCabangFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
