import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { CabangComponent } from './list/cabang.component';
import { CabangDetailComponent } from './detail/cabang-detail.component';
import { CabangUpdateComponent } from './update/cabang-update.component';
import CabangResolve from './route/cabang-routing-resolve.service';

const cabangRoute: Routes = [
  {
    path: '',
    component: CabangComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CabangDetailComponent,
    resolve: {
      cabang: CabangResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CabangUpdateComponent,
    resolve: {
      cabang: CabangResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CabangUpdateComponent,
    resolve: {
      cabang: CabangResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default cabangRoute;
