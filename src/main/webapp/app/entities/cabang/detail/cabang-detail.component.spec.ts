import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { CabangDetailComponent } from './cabang-detail.component';

describe('Cabang Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CabangDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: CabangDetailComponent,
              resolve: { cabang: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(CabangDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load cabang on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', CabangDetailComponent);

      // THEN
      expect(instance.cabang).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
