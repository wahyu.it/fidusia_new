import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICabang, NewCabang } from '../cabang.model';

export type PartialUpdateCabang = Partial<ICabang> & Pick<ICabang, 'id'>;

type RestOf<T extends ICabang | NewCabang> = Omit<T, 'updateOn' | 'startDate'> & {
  updateOn?: string | null;
  startDate?: string | null;
};

export type RestCabang = RestOf<ICabang>;

export type NewRestCabang = RestOf<NewCabang>;

export type PartialUpdateRestCabang = RestOf<PartialUpdateCabang>;

export type EntityResponseType = HttpResponse<ICabang>;
export type EntityArrayResponseType = HttpResponse<ICabang[]>;

@Injectable({ providedIn: 'root' })
export class CabangService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/cabangs');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(cabang: NewCabang): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cabang);
    return this.http
      .post<RestCabang>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(cabang: ICabang): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cabang);
    return this.http
      .put<RestCabang>(`${this.resourceUrl}/${this.getCabangIdentifier(cabang)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(cabang: PartialUpdateCabang): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cabang);
    return this.http
      .patch<RestCabang>(`${this.resourceUrl}/${this.getCabangIdentifier(cabang)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestCabang>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestCabang[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getCabangIdentifier(cabang: Pick<ICabang, 'id'>): number {
    return cabang.id;
  }

  compareCabang(o1: Pick<ICabang, 'id'> | null, o2: Pick<ICabang, 'id'> | null): boolean {
    return o1 && o2 ? this.getCabangIdentifier(o1) === this.getCabangIdentifier(o2) : o1 === o2;
  }

  addCabangToCollectionIfMissing<Type extends Pick<ICabang, 'id'>>(
    cabangCollection: Type[],
    ...cabangsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const cabangs: Type[] = cabangsToCheck.filter(isPresent);
    if (cabangs.length > 0) {
      const cabangCollectionIdentifiers = cabangCollection.map(cabangItem => this.getCabangIdentifier(cabangItem)!);
      const cabangsToAdd = cabangs.filter(cabangItem => {
        const cabangIdentifier = this.getCabangIdentifier(cabangItem);
        if (cabangCollectionIdentifiers.includes(cabangIdentifier)) {
          return false;
        }
        cabangCollectionIdentifiers.push(cabangIdentifier);
        return true;
      });
      return [...cabangsToAdd, ...cabangCollection];
    }
    return cabangCollection;
  }

  protected convertDateFromClient<T extends ICabang | NewCabang | PartialUpdateCabang>(cabang: T): RestOf<T> {
    return {
      ...cabang,
      updateOn: cabang.updateOn?.toJSON() ?? null,
      startDate: cabang.startDate?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restCabang: RestCabang): ICabang {
    return {
      ...restCabang,
      updateOn: restCabang.updateOn ? dayjs(restCabang.updateOn) : undefined,
      startDate: restCabang.startDate ? dayjs(restCabang.startDate) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestCabang>): HttpResponse<ICabang> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestCabang[]>): HttpResponse<ICabang[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
