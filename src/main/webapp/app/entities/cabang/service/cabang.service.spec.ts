import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ICabang } from '../cabang.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../cabang.test-samples';

import { CabangService, RestCabang } from './cabang.service';

const requireRestSample: RestCabang = {
  ...sampleWithRequiredData,
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
  startDate: sampleWithRequiredData.startDate?.format(DATE_FORMAT),
};

describe('Cabang Service', () => {
  let service: CabangService;
  let httpMock: HttpTestingController;
  let expectedResult: ICabang | ICabang[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CabangService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Cabang', () => {
      const cabang = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(cabang).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Cabang', () => {
      const cabang = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(cabang).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Cabang', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Cabang', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Cabang', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addCabangToCollectionIfMissing', () => {
      it('should add a Cabang to an empty array', () => {
        const cabang: ICabang = sampleWithRequiredData;
        expectedResult = service.addCabangToCollectionIfMissing([], cabang);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(cabang);
      });

      it('should not add a Cabang to an array that contains it', () => {
        const cabang: ICabang = sampleWithRequiredData;
        const cabangCollection: ICabang[] = [
          {
            ...cabang,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addCabangToCollectionIfMissing(cabangCollection, cabang);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Cabang to an array that doesn't contain it", () => {
        const cabang: ICabang = sampleWithRequiredData;
        const cabangCollection: ICabang[] = [sampleWithPartialData];
        expectedResult = service.addCabangToCollectionIfMissing(cabangCollection, cabang);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(cabang);
      });

      it('should add only unique Cabang to an array', () => {
        const cabangArray: ICabang[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const cabangCollection: ICabang[] = [sampleWithRequiredData];
        expectedResult = service.addCabangToCollectionIfMissing(cabangCollection, ...cabangArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const cabang: ICabang = sampleWithRequiredData;
        const cabang2: ICabang = sampleWithPartialData;
        expectedResult = service.addCabangToCollectionIfMissing([], cabang, cabang2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(cabang);
        expect(expectedResult).toContain(cabang2);
      });

      it('should accept null and undefined values', () => {
        const cabang: ICabang = sampleWithRequiredData;
        expectedResult = service.addCabangToCollectionIfMissing([], null, cabang, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(cabang);
      });

      it('should return initial array if no Cabang is added', () => {
        const cabangCollection: ICabang[] = [sampleWithRequiredData];
        expectedResult = service.addCabangToCollectionIfMissing(cabangCollection, undefined, null);
        expect(expectedResult).toEqual(cabangCollection);
      });
    });

    describe('compareCabang', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareCabang(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareCabang(entity1, entity2);
        const compareResult2 = service.compareCabang(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareCabang(entity1, entity2);
        const compareResult2 = service.compareCabang(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareCabang(entity1, entity2);
        const compareResult2 = service.compareCabang(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
