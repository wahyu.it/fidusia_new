import dayjs from 'dayjs/esm';
import { ILeasing } from 'app/entities/leasing/leasing.model';

export interface ICabang {
  id: number;
  kode?: string | null;
  nama?: string | null;
  alamat?: string | null;
  kota?: string | null;
  provinsi?: string | null;
  pengadilanNegeri?: string | null;
  noKontak?: string | null;
  recordStatus?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  startDate?: dayjs.Dayjs | null;
  leasing?: ILeasing | null;
}

export type NewCabang = Omit<ICabang, 'id'> & { id: null };
