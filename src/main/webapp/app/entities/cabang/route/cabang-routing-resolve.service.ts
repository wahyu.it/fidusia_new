import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICabang } from '../cabang.model';
import { CabangService } from '../service/cabang.service';

export const cabangResolve = (route: ActivatedRouteSnapshot): Observable<null | ICabang> => {
  const id = route.params['id'];
  if (id) {
    return inject(CabangService)
      .find(id)
      .pipe(
        mergeMap((cabang: HttpResponse<ICabang>) => {
          if (cabang.body) {
            return of(cabang.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default cabangResolve;
