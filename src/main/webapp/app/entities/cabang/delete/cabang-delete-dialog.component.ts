import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { ICabang } from '../cabang.model';
import { CabangService } from '../service/cabang.service';

@Component({
  standalone: true,
  templateUrl: './cabang-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class CabangDeleteDialogComponent {
  cabang?: ICabang;

  constructor(
    protected cabangService: CabangService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cabangService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
