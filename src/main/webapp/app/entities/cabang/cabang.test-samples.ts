import dayjs from 'dayjs/esm';

import { ICabang, NewCabang } from './cabang.model';

export const sampleWithRequiredData: ICabang = {
  id: 11120,
};

export const sampleWithPartialData: ICabang = {
  id: 12052,
  nama: 'viciously rigidly',
  kota: 'section',
  pengadilanNegeri: 'gosh woo sled',
  noKontak: 'sweetly',
};

export const sampleWithFullData: ICabang = {
  id: 4777,
  kode: 'beanie anenst',
  nama: 'knotty',
  alamat: 'mellow transport privilege',
  kota: 'neat oh',
  provinsi: 'defenseless what maximise',
  pengadilanNegeri: 'cop-out vacantly',
  noKontak: 'consequently',
  recordStatus: 805,
  updateBy: 'incorporate ha',
  updateOn: dayjs('2023-12-14T12:32'),
  startDate: dayjs('2023-12-14'),
};

export const sampleWithNewData: NewCabang = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
