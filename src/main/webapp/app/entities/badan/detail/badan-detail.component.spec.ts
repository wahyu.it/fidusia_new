import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { BadanDetailComponent } from './badan-detail.component';

describe('Badan Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BadanDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: BadanDetailComponent,
              resolve: { badan: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(BadanDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load badan on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', BadanDetailComponent);

      // THEN
      expect(instance.badan).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
