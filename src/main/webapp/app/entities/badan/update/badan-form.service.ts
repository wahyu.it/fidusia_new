import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IBadan, NewBadan } from '../badan.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IBadan for edit and NewBadanFormGroupInput for create.
 */
type BadanFormGroupInput = IBadan | PartialWithRequiredKeyOf<NewBadan>;

type BadanFormDefaults = Pick<NewBadan, 'id'>;

type BadanFormGroupContent = {
  id: FormControl<IBadan['id'] | NewBadan['id']>;
  golonganUsaha: FormControl<IBadan['golonganUsaha']>;
  jenis: FormControl<IBadan['jenis']>;
  nama: FormControl<IBadan['nama']>;
  kedudukan: FormControl<IBadan['kedudukan']>;
  noAkta: FormControl<IBadan['noAkta']>;
  tglAkta: FormControl<IBadan['tglAkta']>;
  namaNotaris: FormControl<IBadan['namaNotaris']>;
  wilKerjaNotaris: FormControl<IBadan['wilKerjaNotaris']>;
  skNotaris: FormControl<IBadan['skNotaris']>;
  alamat: FormControl<IBadan['alamat']>;
  rt: FormControl<IBadan['rt']>;
  rw: FormControl<IBadan['rw']>;
  kelurahan: FormControl<IBadan['kelurahan']>;
  kecamatan: FormControl<IBadan['kecamatan']>;
  kota: FormControl<IBadan['kota']>;
  provinsi: FormControl<IBadan['provinsi']>;
  kodePos: FormControl<IBadan['kodePos']>;
  noKontak: FormControl<IBadan['noKontak']>;
  namaSk: FormControl<IBadan['namaSk']>;
  nomorSk: FormControl<IBadan['nomorSk']>;
  tglSk: FormControl<IBadan['tglSk']>;
  npwp: FormControl<IBadan['npwp']>;
  pjNama: FormControl<IBadan['pjNama']>;
  pjJenisKelamin: FormControl<IBadan['pjJenisKelamin']>;
  pjStatusKawin: FormControl<IBadan['pjStatusKawin']>;
  pjKelahiran: FormControl<IBadan['pjKelahiran']>;
  pjTglLahir: FormControl<IBadan['pjTglLahir']>;
  pjPekerjaan: FormControl<IBadan['pjPekerjaan']>;
  pjWargaNegara: FormControl<IBadan['pjWargaNegara']>;
  pjJenisId: FormControl<IBadan['pjJenisId']>;
  pjNoId: FormControl<IBadan['pjNoId']>;
  pjAlamat: FormControl<IBadan['pjAlamat']>;
  pjRt: FormControl<IBadan['pjRt']>;
  pjRw: FormControl<IBadan['pjRw']>;
  pjKelurahan: FormControl<IBadan['pjKelurahan']>;
  pjKecamatan: FormControl<IBadan['pjKecamatan']>;
  pjKota: FormControl<IBadan['pjKota']>;
  pjProvinsi: FormControl<IBadan['pjProvinsi']>;
  pjKodePos: FormControl<IBadan['pjKodePos']>;
  template: FormControl<IBadan['template']>;
  komparisi: FormControl<IBadan['komparisi']>;
};

export type BadanFormGroup = FormGroup<BadanFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class BadanFormService {
  createBadanFormGroup(badan: BadanFormGroupInput = { id: null }): BadanFormGroup {
    const badanRawValue = {
      ...this.getFormDefaults(),
      ...badan,
    };
    return new FormGroup<BadanFormGroupContent>({
      id: new FormControl(
        { value: badanRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      golonganUsaha: new FormControl(badanRawValue.golonganUsaha),
      jenis: new FormControl(badanRawValue.jenis),
      nama: new FormControl(badanRawValue.nama),
      kedudukan: new FormControl(badanRawValue.kedudukan),
      noAkta: new FormControl(badanRawValue.noAkta),
      tglAkta: new FormControl(badanRawValue.tglAkta),
      namaNotaris: new FormControl(badanRawValue.namaNotaris),
      wilKerjaNotaris: new FormControl(badanRawValue.wilKerjaNotaris),
      skNotaris: new FormControl(badanRawValue.skNotaris),
      alamat: new FormControl(badanRawValue.alamat),
      rt: new FormControl(badanRawValue.rt),
      rw: new FormControl(badanRawValue.rw),
      kelurahan: new FormControl(badanRawValue.kelurahan),
      kecamatan: new FormControl(badanRawValue.kecamatan),
      kota: new FormControl(badanRawValue.kota),
      provinsi: new FormControl(badanRawValue.provinsi),
      kodePos: new FormControl(badanRawValue.kodePos),
      noKontak: new FormControl(badanRawValue.noKontak),
      namaSk: new FormControl(badanRawValue.namaSk),
      nomorSk: new FormControl(badanRawValue.nomorSk),
      tglSk: new FormControl(badanRawValue.tglSk),
      npwp: new FormControl(badanRawValue.npwp),
      pjNama: new FormControl(badanRawValue.pjNama),
      pjJenisKelamin: new FormControl(badanRawValue.pjJenisKelamin),
      pjStatusKawin: new FormControl(badanRawValue.pjStatusKawin),
      pjKelahiran: new FormControl(badanRawValue.pjKelahiran),
      pjTglLahir: new FormControl(badanRawValue.pjTglLahir),
      pjPekerjaan: new FormControl(badanRawValue.pjPekerjaan),
      pjWargaNegara: new FormControl(badanRawValue.pjWargaNegara),
      pjJenisId: new FormControl(badanRawValue.pjJenisId),
      pjNoId: new FormControl(badanRawValue.pjNoId),
      pjAlamat: new FormControl(badanRawValue.pjAlamat),
      pjRt: new FormControl(badanRawValue.pjRt),
      pjRw: new FormControl(badanRawValue.pjRw),
      pjKelurahan: new FormControl(badanRawValue.pjKelurahan),
      pjKecamatan: new FormControl(badanRawValue.pjKecamatan),
      pjKota: new FormControl(badanRawValue.pjKota),
      pjProvinsi: new FormControl(badanRawValue.pjProvinsi),
      pjKodePos: new FormControl(badanRawValue.pjKodePos),
      template: new FormControl(badanRawValue.template),
      komparisi: new FormControl(badanRawValue.komparisi),
    });
  }

  getBadan(form: BadanFormGroup): IBadan | NewBadan {
    return form.getRawValue() as IBadan | NewBadan;
  }

  resetForm(form: BadanFormGroup, badan: BadanFormGroupInput): void {
    const badanRawValue = { ...this.getFormDefaults(), ...badan };
    form.reset(
      {
        ...badanRawValue,
        id: { value: badanRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): BadanFormDefaults {
    return {
      id: null,
    };
  }
}
