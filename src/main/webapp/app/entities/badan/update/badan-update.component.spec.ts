import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { BadanService } from '../service/badan.service';
import { IBadan } from '../badan.model';
import { BadanFormService } from './badan-form.service';

import { BadanUpdateComponent } from './badan-update.component';

describe('Badan Management Update Component', () => {
  let comp: BadanUpdateComponent;
  let fixture: ComponentFixture<BadanUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let badanFormService: BadanFormService;
  let badanService: BadanService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), BadanUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BadanUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BadanUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    badanFormService = TestBed.inject(BadanFormService);
    badanService = TestBed.inject(BadanService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const badan: IBadan = { id: 456 };

      activatedRoute.data = of({ badan });
      comp.ngOnInit();

      expect(comp.badan).toEqual(badan);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBadan>>();
      const badan = { id: 123 };
      jest.spyOn(badanFormService, 'getBadan').mockReturnValue(badan);
      jest.spyOn(badanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ badan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: badan }));
      saveSubject.complete();

      // THEN
      expect(badanFormService.getBadan).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(badanService.update).toHaveBeenCalledWith(expect.objectContaining(badan));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBadan>>();
      const badan = { id: 123 };
      jest.spyOn(badanFormService, 'getBadan').mockReturnValue({ id: null });
      jest.spyOn(badanService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ badan: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: badan }));
      saveSubject.complete();

      // THEN
      expect(badanFormService.getBadan).toHaveBeenCalled();
      expect(badanService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBadan>>();
      const badan = { id: 123 };
      jest.spyOn(badanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ badan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(badanService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
