import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../badan.test-samples';

import { BadanFormService } from './badan-form.service';

describe('Badan Form Service', () => {
  let service: BadanFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BadanFormService);
  });

  describe('Service methods', () => {
    describe('createBadanFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createBadanFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            golonganUsaha: expect.any(Object),
            jenis: expect.any(Object),
            nama: expect.any(Object),
            kedudukan: expect.any(Object),
            noAkta: expect.any(Object),
            tglAkta: expect.any(Object),
            namaNotaris: expect.any(Object),
            wilKerjaNotaris: expect.any(Object),
            skNotaris: expect.any(Object),
            alamat: expect.any(Object),
            rt: expect.any(Object),
            rw: expect.any(Object),
            kelurahan: expect.any(Object),
            kecamatan: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            kodePos: expect.any(Object),
            noKontak: expect.any(Object),
            namaSk: expect.any(Object),
            nomorSk: expect.any(Object),
            tglSk: expect.any(Object),
            npwp: expect.any(Object),
            pjNama: expect.any(Object),
            pjJenisKelamin: expect.any(Object),
            pjStatusKawin: expect.any(Object),
            pjKelahiran: expect.any(Object),
            pjTglLahir: expect.any(Object),
            pjPekerjaan: expect.any(Object),
            pjWargaNegara: expect.any(Object),
            pjJenisId: expect.any(Object),
            pjNoId: expect.any(Object),
            pjAlamat: expect.any(Object),
            pjRt: expect.any(Object),
            pjRw: expect.any(Object),
            pjKelurahan: expect.any(Object),
            pjKecamatan: expect.any(Object),
            pjKota: expect.any(Object),
            pjProvinsi: expect.any(Object),
            pjKodePos: expect.any(Object),
            template: expect.any(Object),
            komparisi: expect.any(Object),
          }),
        );
      });

      it('passing IBadan should create a new form with FormGroup', () => {
        const formGroup = service.createBadanFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            golonganUsaha: expect.any(Object),
            jenis: expect.any(Object),
            nama: expect.any(Object),
            kedudukan: expect.any(Object),
            noAkta: expect.any(Object),
            tglAkta: expect.any(Object),
            namaNotaris: expect.any(Object),
            wilKerjaNotaris: expect.any(Object),
            skNotaris: expect.any(Object),
            alamat: expect.any(Object),
            rt: expect.any(Object),
            rw: expect.any(Object),
            kelurahan: expect.any(Object),
            kecamatan: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            kodePos: expect.any(Object),
            noKontak: expect.any(Object),
            namaSk: expect.any(Object),
            nomorSk: expect.any(Object),
            tglSk: expect.any(Object),
            npwp: expect.any(Object),
            pjNama: expect.any(Object),
            pjJenisKelamin: expect.any(Object),
            pjStatusKawin: expect.any(Object),
            pjKelahiran: expect.any(Object),
            pjTglLahir: expect.any(Object),
            pjPekerjaan: expect.any(Object),
            pjWargaNegara: expect.any(Object),
            pjJenisId: expect.any(Object),
            pjNoId: expect.any(Object),
            pjAlamat: expect.any(Object),
            pjRt: expect.any(Object),
            pjRw: expect.any(Object),
            pjKelurahan: expect.any(Object),
            pjKecamatan: expect.any(Object),
            pjKota: expect.any(Object),
            pjProvinsi: expect.any(Object),
            pjKodePos: expect.any(Object),
            template: expect.any(Object),
            komparisi: expect.any(Object),
          }),
        );
      });
    });

    describe('getBadan', () => {
      it('should return NewBadan for default Badan initial value', () => {
        const formGroup = service.createBadanFormGroup(sampleWithNewData);

        const badan = service.getBadan(formGroup) as any;

        expect(badan).toMatchObject(sampleWithNewData);
      });

      it('should return NewBadan for empty Badan initial value', () => {
        const formGroup = service.createBadanFormGroup();

        const badan = service.getBadan(formGroup) as any;

        expect(badan).toMatchObject({});
      });

      it('should return IBadan', () => {
        const formGroup = service.createBadanFormGroup(sampleWithRequiredData);

        const badan = service.getBadan(formGroup) as any;

        expect(badan).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IBadan should not enable id FormControl', () => {
        const formGroup = service.createBadanFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewBadan should disable id FormControl', () => {
        const formGroup = service.createBadanFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
