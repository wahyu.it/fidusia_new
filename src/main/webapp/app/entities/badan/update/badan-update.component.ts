import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IBadan } from '../badan.model';
import { BadanService } from '../service/badan.service';
import { BadanFormService, BadanFormGroup } from './badan-form.service';

@Component({
  standalone: true,
  selector: 'jhi-badan-update',
  templateUrl: './badan-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class BadanUpdateComponent implements OnInit {
  isSaving = false;
  badan: IBadan | null = null;

  editForm: BadanFormGroup = this.badanFormService.createBadanFormGroup();

  constructor(
    protected badanService: BadanService,
    protected badanFormService: BadanFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ badan }) => {
      this.badan = badan;
      if (badan) {
        this.updateForm(badan);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const badan = this.badanFormService.getBadan(this.editForm);
    if (badan.id !== null) {
      this.subscribeToSaveResponse(this.badanService.update(badan));
    } else {
      this.subscribeToSaveResponse(this.badanService.create(badan));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBadan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(badan: IBadan): void {
    this.badan = badan;
    this.badanFormService.resetForm(this.editForm, badan);
  }
}
