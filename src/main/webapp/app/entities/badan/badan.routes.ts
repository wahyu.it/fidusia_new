import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { BadanComponent } from './list/badan.component';
import { BadanDetailComponent } from './detail/badan-detail.component';
import { BadanUpdateComponent } from './update/badan-update.component';
import BadanResolve from './route/badan-routing-resolve.service';

const badanRoute: Routes = [
  {
    path: '',
    component: BadanComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BadanDetailComponent,
    resolve: {
      badan: BadanResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BadanUpdateComponent,
    resolve: {
      badan: BadanResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BadanUpdateComponent,
    resolve: {
      badan: BadanResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default badanRoute;
