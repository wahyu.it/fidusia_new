import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBadan } from '../badan.model';
import { BadanService } from '../service/badan.service';

export const badanResolve = (route: ActivatedRouteSnapshot): Observable<null | IBadan> => {
  const id = route.params['id'];
  if (id) {
    return inject(BadanService)
      .find(id)
      .pipe(
        mergeMap((badan: HttpResponse<IBadan>) => {
          if (badan.body) {
            return of(badan.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default badanResolve;
