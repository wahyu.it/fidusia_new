import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IBadan } from '../badan.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../badan.test-samples';

import { BadanService, RestBadan } from './badan.service';

const requireRestSample: RestBadan = {
  ...sampleWithRequiredData,
  tglAkta: sampleWithRequiredData.tglAkta?.format(DATE_FORMAT),
  pjTglLahir: sampleWithRequiredData.pjTglLahir?.format(DATE_FORMAT),
};

describe('Badan Service', () => {
  let service: BadanService;
  let httpMock: HttpTestingController;
  let expectedResult: IBadan | IBadan[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(BadanService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Badan', () => {
      const badan = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(badan).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Badan', () => {
      const badan = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(badan).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Badan', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Badan', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Badan', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addBadanToCollectionIfMissing', () => {
      it('should add a Badan to an empty array', () => {
        const badan: IBadan = sampleWithRequiredData;
        expectedResult = service.addBadanToCollectionIfMissing([], badan);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(badan);
      });

      it('should not add a Badan to an array that contains it', () => {
        const badan: IBadan = sampleWithRequiredData;
        const badanCollection: IBadan[] = [
          {
            ...badan,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addBadanToCollectionIfMissing(badanCollection, badan);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Badan to an array that doesn't contain it", () => {
        const badan: IBadan = sampleWithRequiredData;
        const badanCollection: IBadan[] = [sampleWithPartialData];
        expectedResult = service.addBadanToCollectionIfMissing(badanCollection, badan);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(badan);
      });

      it('should add only unique Badan to an array', () => {
        const badanArray: IBadan[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const badanCollection: IBadan[] = [sampleWithRequiredData];
        expectedResult = service.addBadanToCollectionIfMissing(badanCollection, ...badanArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const badan: IBadan = sampleWithRequiredData;
        const badan2: IBadan = sampleWithPartialData;
        expectedResult = service.addBadanToCollectionIfMissing([], badan, badan2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(badan);
        expect(expectedResult).toContain(badan2);
      });

      it('should accept null and undefined values', () => {
        const badan: IBadan = sampleWithRequiredData;
        expectedResult = service.addBadanToCollectionIfMissing([], null, badan, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(badan);
      });

      it('should return initial array if no Badan is added', () => {
        const badanCollection: IBadan[] = [sampleWithRequiredData];
        expectedResult = service.addBadanToCollectionIfMissing(badanCollection, undefined, null);
        expect(expectedResult).toEqual(badanCollection);
      });
    });

    describe('compareBadan', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareBadan(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareBadan(entity1, entity2);
        const compareResult2 = service.compareBadan(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareBadan(entity1, entity2);
        const compareResult2 = service.compareBadan(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareBadan(entity1, entity2);
        const compareResult2 = service.compareBadan(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
