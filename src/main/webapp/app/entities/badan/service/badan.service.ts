import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBadan, NewBadan } from '../badan.model';

export type PartialUpdateBadan = Partial<IBadan> & Pick<IBadan, 'id'>;

type RestOf<T extends IBadan | NewBadan> = Omit<T, 'tglAkta' | 'pjTglLahir'> & {
  tglAkta?: string | null;
  pjTglLahir?: string | null;
};

export type RestBadan = RestOf<IBadan>;

export type NewRestBadan = RestOf<NewBadan>;

export type PartialUpdateRestBadan = RestOf<PartialUpdateBadan>;

export type EntityResponseType = HttpResponse<IBadan>;
export type EntityArrayResponseType = HttpResponse<IBadan[]>;

@Injectable({ providedIn: 'root' })
export class BadanService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/badans');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(badan: NewBadan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(badan);
    return this.http.post<RestBadan>(this.resourceUrl, copy, { observe: 'response' }).pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(badan: IBadan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(badan);
    return this.http
      .put<RestBadan>(`${this.resourceUrl}/${this.getBadanIdentifier(badan)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(badan: PartialUpdateBadan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(badan);
    return this.http
      .patch<RestBadan>(`${this.resourceUrl}/${this.getBadanIdentifier(badan)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestBadan>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestBadan[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getBadanIdentifier(badan: Pick<IBadan, 'id'>): number {
    return badan.id;
  }

  compareBadan(o1: Pick<IBadan, 'id'> | null, o2: Pick<IBadan, 'id'> | null): boolean {
    return o1 && o2 ? this.getBadanIdentifier(o1) === this.getBadanIdentifier(o2) : o1 === o2;
  }

  addBadanToCollectionIfMissing<Type extends Pick<IBadan, 'id'>>(
    badanCollection: Type[],
    ...badansToCheck: (Type | null | undefined)[]
  ): Type[] {
    const badans: Type[] = badansToCheck.filter(isPresent);
    if (badans.length > 0) {
      const badanCollectionIdentifiers = badanCollection.map(badanItem => this.getBadanIdentifier(badanItem)!);
      const badansToAdd = badans.filter(badanItem => {
        const badanIdentifier = this.getBadanIdentifier(badanItem);
        if (badanCollectionIdentifiers.includes(badanIdentifier)) {
          return false;
        }
        badanCollectionIdentifiers.push(badanIdentifier);
        return true;
      });
      return [...badansToAdd, ...badanCollection];
    }
    return badanCollection;
  }

  protected convertDateFromClient<T extends IBadan | NewBadan | PartialUpdateBadan>(badan: T): RestOf<T> {
    return {
      ...badan,
      tglAkta: badan.tglAkta?.format(DATE_FORMAT) ?? null,
      pjTglLahir: badan.pjTglLahir?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restBadan: RestBadan): IBadan {
    return {
      ...restBadan,
      tglAkta: restBadan.tglAkta ? dayjs(restBadan.tglAkta) : undefined,
      pjTglLahir: restBadan.pjTglLahir ? dayjs(restBadan.pjTglLahir) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestBadan>): HttpResponse<IBadan> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestBadan[]>): HttpResponse<IBadan[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
