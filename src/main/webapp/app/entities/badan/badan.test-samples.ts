import dayjs from 'dayjs/esm';

import { IBadan, NewBadan } from './badan.model';

export const sampleWithRequiredData: IBadan = {
  id: 20009,
};

export const sampleWithPartialData: IBadan = {
  id: 25886,
  jenis: 'up beyond',
  nama: 'curiously an',
  namaNotaris: 'impress',
  wilKerjaNotaris: 'beyond seethe',
  alamat: 'yum',
  rt: 'drat',
  kelurahan: 'given ritualize',
  provinsi: 'boring trade',
  nomorSk: 'what attentive trend',
  tglSk: 'alongside up',
  pjJenisKelamin: 'boo',
  pjTglLahir: dayjs('2023-12-14'),
  pjPekerjaan: 'inasmuch unimpressively coach',
  pjAlamat: 'solidly tract although',
  pjKecamatan: 'qua geez',
  pjKota: 'incidentally revoke er',
  pjKodePos: 'regionalism',
};

export const sampleWithFullData: IBadan = {
  id: 25841,
  golonganUsaha: 'upon',
  jenis: 'scenery heaven',
  nama: 'bakeware cough notwithstanding',
  kedudukan: 'palm',
  noAkta: 'late',
  tglAkta: dayjs('2023-12-14'),
  namaNotaris: 'disentangle rake kindly',
  wilKerjaNotaris: 'upright wherever aftershave',
  skNotaris: 'aside',
  alamat: 'every highly',
  rt: 'wherever',
  rw: 'between pier',
  kelurahan: 'yet until through',
  kecamatan: 'bitterly',
  kota: 'stair',
  provinsi: 'ick because',
  kodePos: 'including meh',
  noKontak: 'versus strong',
  namaSk: 'past gosh',
  nomorSk: 'expert than',
  tglSk: 'innocently nervous',
  npwp: 'brown wearily',
  pjNama: 'vice vestment',
  pjJenisKelamin: 'willfully',
  pjStatusKawin: 'medium until yet',
  pjKelahiran: 'as hm unabashedly',
  pjTglLahir: dayjs('2023-12-15'),
  pjPekerjaan: 'vigorous neat oof',
  pjWargaNegara: 'ha',
  pjJenisId: 'cavernous precious quizzically',
  pjNoId: 'angry as wide-eyed',
  pjAlamat: 'swarm till witty',
  pjRt: 'gadzooks blah meh',
  pjRw: 'dead waterbed',
  pjKelurahan: 'pfft',
  pjKecamatan: 'safely',
  pjKota: 'dense than',
  pjProvinsi: 'savor',
  pjKodePos: 'mozzarella',
  template: 'thankful meaty',
  komparisi: 'incidentally inasmuch whoa',
};

export const sampleWithNewData: NewBadan = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
