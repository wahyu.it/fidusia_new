import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOrderKerja, NewOrderKerja } from '../order-kerja.model';

export type PartialUpdateOrderKerja = Partial<IOrderKerja> & Pick<IOrderKerja, 'id'>;

type RestOf<T extends IOrderKerja | NewOrderKerja> = Omit<T, 'tanggal' | 'tanggalTerima' | 'updateOn' | 'approveOn'> & {
  tanggal?: string | null;
  tanggalTerima?: string | null;
  updateOn?: string | null;
  approveOn?: string | null;
};

export type RestOrderKerja = RestOf<IOrderKerja>;

export type NewRestOrderKerja = RestOf<NewOrderKerja>;

export type PartialUpdateRestOrderKerja = RestOf<PartialUpdateOrderKerja>;

export type EntityResponseType = HttpResponse<IOrderKerja>;
export type EntityArrayResponseType = HttpResponse<IOrderKerja[]>;

@Injectable({ providedIn: 'root' })
export class OrderKerjaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/order-kerjas');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(orderKerja: NewOrderKerja): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderKerja);
    return this.http
      .post<RestOrderKerja>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(orderKerja: IOrderKerja): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderKerja);
    return this.http
      .put<RestOrderKerja>(`${this.resourceUrl}/${this.getOrderKerjaIdentifier(orderKerja)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(orderKerja: PartialUpdateOrderKerja): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderKerja);
    return this.http
      .patch<RestOrderKerja>(`${this.resourceUrl}/${this.getOrderKerjaIdentifier(orderKerja)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestOrderKerja>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestOrderKerja[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getOrderKerjaIdentifier(orderKerja: Pick<IOrderKerja, 'id'>): number {
    return orderKerja.id;
  }

  compareOrderKerja(o1: Pick<IOrderKerja, 'id'> | null, o2: Pick<IOrderKerja, 'id'> | null): boolean {
    return o1 && o2 ? this.getOrderKerjaIdentifier(o1) === this.getOrderKerjaIdentifier(o2) : o1 === o2;
  }

  addOrderKerjaToCollectionIfMissing<Type extends Pick<IOrderKerja, 'id'>>(
    orderKerjaCollection: Type[],
    ...orderKerjasToCheck: (Type | null | undefined)[]
  ): Type[] {
    const orderKerjas: Type[] = orderKerjasToCheck.filter(isPresent);
    if (orderKerjas.length > 0) {
      const orderKerjaCollectionIdentifiers = orderKerjaCollection.map(orderKerjaItem => this.getOrderKerjaIdentifier(orderKerjaItem)!);
      const orderKerjasToAdd = orderKerjas.filter(orderKerjaItem => {
        const orderKerjaIdentifier = this.getOrderKerjaIdentifier(orderKerjaItem);
        if (orderKerjaCollectionIdentifiers.includes(orderKerjaIdentifier)) {
          return false;
        }
        orderKerjaCollectionIdentifiers.push(orderKerjaIdentifier);
        return true;
      });
      return [...orderKerjasToAdd, ...orderKerjaCollection];
    }
    return orderKerjaCollection;
  }

  protected convertDateFromClient<T extends IOrderKerja | NewOrderKerja | PartialUpdateOrderKerja>(orderKerja: T): RestOf<T> {
    return {
      ...orderKerja,
      tanggal: orderKerja.tanggal?.format(DATE_FORMAT) ?? null,
      tanggalTerima: orderKerja.tanggalTerima?.format(DATE_FORMAT) ?? null,
      updateOn: orderKerja.updateOn?.toJSON() ?? null,
      approveOn: orderKerja.approveOn?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restOrderKerja: RestOrderKerja): IOrderKerja {
    return {
      ...restOrderKerja,
      tanggal: restOrderKerja.tanggal ? dayjs(restOrderKerja.tanggal) : undefined,
      tanggalTerima: restOrderKerja.tanggalTerima ? dayjs(restOrderKerja.tanggalTerima) : undefined,
      updateOn: restOrderKerja.updateOn ? dayjs(restOrderKerja.updateOn) : undefined,
      approveOn: restOrderKerja.approveOn ? dayjs(restOrderKerja.approveOn) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestOrderKerja>): HttpResponse<IOrderKerja> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestOrderKerja[]>): HttpResponse<IOrderKerja[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
