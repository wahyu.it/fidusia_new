import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IOrderKerja } from '../order-kerja.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../order-kerja.test-samples';

import { OrderKerjaService, RestOrderKerja } from './order-kerja.service';

const requireRestSample: RestOrderKerja = {
  ...sampleWithRequiredData,
  tanggal: sampleWithRequiredData.tanggal?.format(DATE_FORMAT),
  tanggalTerima: sampleWithRequiredData.tanggalTerima?.format(DATE_FORMAT),
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
  approveOn: sampleWithRequiredData.approveOn?.toJSON(),
};

describe('OrderKerja Service', () => {
  let service: OrderKerjaService;
  let httpMock: HttpTestingController;
  let expectedResult: IOrderKerja | IOrderKerja[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(OrderKerjaService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a OrderKerja', () => {
      const orderKerja = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(orderKerja).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a OrderKerja', () => {
      const orderKerja = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(orderKerja).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a OrderKerja', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of OrderKerja', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a OrderKerja', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addOrderKerjaToCollectionIfMissing', () => {
      it('should add a OrderKerja to an empty array', () => {
        const orderKerja: IOrderKerja = sampleWithRequiredData;
        expectedResult = service.addOrderKerjaToCollectionIfMissing([], orderKerja);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(orderKerja);
      });

      it('should not add a OrderKerja to an array that contains it', () => {
        const orderKerja: IOrderKerja = sampleWithRequiredData;
        const orderKerjaCollection: IOrderKerja[] = [
          {
            ...orderKerja,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addOrderKerjaToCollectionIfMissing(orderKerjaCollection, orderKerja);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a OrderKerja to an array that doesn't contain it", () => {
        const orderKerja: IOrderKerja = sampleWithRequiredData;
        const orderKerjaCollection: IOrderKerja[] = [sampleWithPartialData];
        expectedResult = service.addOrderKerjaToCollectionIfMissing(orderKerjaCollection, orderKerja);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(orderKerja);
      });

      it('should add only unique OrderKerja to an array', () => {
        const orderKerjaArray: IOrderKerja[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const orderKerjaCollection: IOrderKerja[] = [sampleWithRequiredData];
        expectedResult = service.addOrderKerjaToCollectionIfMissing(orderKerjaCollection, ...orderKerjaArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const orderKerja: IOrderKerja = sampleWithRequiredData;
        const orderKerja2: IOrderKerja = sampleWithPartialData;
        expectedResult = service.addOrderKerjaToCollectionIfMissing([], orderKerja, orderKerja2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(orderKerja);
        expect(expectedResult).toContain(orderKerja2);
      });

      it('should accept null and undefined values', () => {
        const orderKerja: IOrderKerja = sampleWithRequiredData;
        expectedResult = service.addOrderKerjaToCollectionIfMissing([], null, orderKerja, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(orderKerja);
      });

      it('should return initial array if no OrderKerja is added', () => {
        const orderKerjaCollection: IOrderKerja[] = [sampleWithRequiredData];
        expectedResult = service.addOrderKerjaToCollectionIfMissing(orderKerjaCollection, undefined, null);
        expect(expectedResult).toEqual(orderKerjaCollection);
      });
    });

    describe('compareOrderKerja', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareOrderKerja(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareOrderKerja(entity1, entity2);
        const compareResult2 = service.compareOrderKerja(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareOrderKerja(entity1, entity2);
        const compareResult2 = service.compareOrderKerja(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareOrderKerja(entity1, entity2);
        const compareResult2 = service.compareOrderKerja(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
