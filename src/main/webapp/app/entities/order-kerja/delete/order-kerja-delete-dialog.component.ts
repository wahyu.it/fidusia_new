import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IOrderKerja } from '../order-kerja.model';
import { OrderKerjaService } from '../service/order-kerja.service';

@Component({
  standalone: true,
  templateUrl: './order-kerja-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class OrderKerjaDeleteDialogComponent {
  orderKerja?: IOrderKerja;

  constructor(
    protected orderKerjaService: OrderKerjaService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orderKerjaService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
