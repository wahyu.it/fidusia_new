import dayjs from 'dayjs/esm';
import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';

export interface IOrderKerja {
  id: number;
  nomor?: string | null;
  tanggal?: dayjs.Dayjs | null;
  kategori?: number | null;
  tanggalTerima?: dayjs.Dayjs | null;
  jumlahAkta?: number | null;
  jumlahCancel?: number | null;
  jumlahCetakAkta?: number | null;
  uploadFile?: string | null;
  status?: number | null;
  statusSigned?: boolean | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  approveBy?: string | null;
  approveOn?: dayjs.Dayjs | null;
  skSubtitusi?: ISkSubtitusi | null;
  notaris?: INotaris | null;
  penerimaKuasa?: IPenerimaKuasa | null;
}

export type NewOrderKerja = Omit<IOrderKerja, 'id'> & { id: null };
