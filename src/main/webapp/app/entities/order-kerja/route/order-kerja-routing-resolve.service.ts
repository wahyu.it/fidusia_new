import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOrderKerja } from '../order-kerja.model';
import { OrderKerjaService } from '../service/order-kerja.service';

export const orderKerjaResolve = (route: ActivatedRouteSnapshot): Observable<null | IOrderKerja> => {
  const id = route.params['id'];
  if (id) {
    return inject(OrderKerjaService)
      .find(id)
      .pipe(
        mergeMap((orderKerja: HttpResponse<IOrderKerja>) => {
          if (orderKerja.body) {
            return of(orderKerja.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default orderKerjaResolve;
