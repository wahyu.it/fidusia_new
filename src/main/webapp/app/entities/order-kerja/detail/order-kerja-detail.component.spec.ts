import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { OrderKerjaDetailComponent } from './order-kerja-detail.component';

describe('OrderKerja Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OrderKerjaDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: OrderKerjaDetailComponent,
              resolve: { orderKerja: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(OrderKerjaDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load orderKerja on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', OrderKerjaDetailComponent);

      // THEN
      expect(instance.orderKerja).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
