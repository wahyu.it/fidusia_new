import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { OrderKerjaComponent } from './list/order-kerja.component';
import { OrderKerjaDetailComponent } from './detail/order-kerja-detail.component';
import { OrderKerjaUpdateComponent } from './update/order-kerja-update.component';
import OrderKerjaResolve from './route/order-kerja-routing-resolve.service';

const orderKerjaRoute: Routes = [
  {
    path: '',
    component: OrderKerjaComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrderKerjaDetailComponent,
    resolve: {
      orderKerja: OrderKerjaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrderKerjaUpdateComponent,
    resolve: {
      orderKerja: OrderKerjaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrderKerjaUpdateComponent,
    resolve: {
      orderKerja: OrderKerjaResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default orderKerjaRoute;
