import dayjs from 'dayjs/esm';

import { IOrderKerja, NewOrderKerja } from './order-kerja.model';

export const sampleWithRequiredData: IOrderKerja = {
  id: 31502,
};

export const sampleWithPartialData: IOrderKerja = {
  id: 5755,
  tanggal: dayjs('2023-12-14'),
  kategori: 25439,
  tanggalTerima: dayjs('2023-12-14'),
  jumlahAkta: 25161,
  jumlahCancel: 1058,
  statusSigned: false,
  updateBy: 'section',
  updateOn: dayjs('2023-12-14T12:21'),
  approveOn: dayjs('2023-12-14T07:55'),
};

export const sampleWithFullData: IOrderKerja = {
  id: 15922,
  nomor: 'sans',
  tanggal: dayjs('2023-12-14'),
  kategori: 17590,
  tanggalTerima: dayjs('2023-12-14'),
  jumlahAkta: 20449,
  jumlahCancel: 4060,
  jumlahCetakAkta: 23695,
  uploadFile: 'child concerning rudely',
  status: 196,
  statusSigned: true,
  updateBy: 'elaborate quietly meh',
  updateOn: dayjs('2023-12-15T01:06'),
  approveBy: 'vague till',
  approveOn: dayjs('2023-12-14T23:38'),
};

export const sampleWithNewData: NewOrderKerja = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
