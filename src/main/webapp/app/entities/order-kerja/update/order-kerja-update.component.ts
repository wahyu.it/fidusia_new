import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { SkSubtitusiService } from 'app/entities/sk-subtitusi/service/sk-subtitusi.service';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';
import { PenerimaKuasaService } from 'app/entities/penerima-kuasa/service/penerima-kuasa.service';
import { OrderKerjaService } from '../service/order-kerja.service';
import { IOrderKerja } from '../order-kerja.model';
import { OrderKerjaFormService, OrderKerjaFormGroup } from './order-kerja-form.service';

@Component({
  standalone: true,
  selector: 'jhi-order-kerja-update',
  templateUrl: './order-kerja-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class OrderKerjaUpdateComponent implements OnInit {
  isSaving = false;
  orderKerja: IOrderKerja | null = null;

  skSubtitusisSharedCollection: ISkSubtitusi[] = [];
  notarisesSharedCollection: INotaris[] = [];
  penerimaKuasasSharedCollection: IPenerimaKuasa[] = [];

  editForm: OrderKerjaFormGroup = this.orderKerjaFormService.createOrderKerjaFormGroup();

  constructor(
    protected orderKerjaService: OrderKerjaService,
    protected orderKerjaFormService: OrderKerjaFormService,
    protected skSubtitusiService: SkSubtitusiService,
    protected notarisService: NotarisService,
    protected penerimaKuasaService: PenerimaKuasaService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareSkSubtitusi = (o1: ISkSubtitusi | null, o2: ISkSubtitusi | null): boolean => this.skSubtitusiService.compareSkSubtitusi(o1, o2);

  compareNotaris = (o1: INotaris | null, o2: INotaris | null): boolean => this.notarisService.compareNotaris(o1, o2);

  comparePenerimaKuasa = (o1: IPenerimaKuasa | null, o2: IPenerimaKuasa | null): boolean =>
    this.penerimaKuasaService.comparePenerimaKuasa(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderKerja }) => {
      this.orderKerja = orderKerja;
      if (orderKerja) {
        this.updateForm(orderKerja);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderKerja = this.orderKerjaFormService.getOrderKerja(this.editForm);
    if (orderKerja.id !== null) {
      this.subscribeToSaveResponse(this.orderKerjaService.update(orderKerja));
    } else {
      this.subscribeToSaveResponse(this.orderKerjaService.create(orderKerja));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderKerja>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(orderKerja: IOrderKerja): void {
    this.orderKerja = orderKerja;
    this.orderKerjaFormService.resetForm(this.editForm, orderKerja);

    this.skSubtitusisSharedCollection = this.skSubtitusiService.addSkSubtitusiToCollectionIfMissing<ISkSubtitusi>(
      this.skSubtitusisSharedCollection,
      orderKerja.skSubtitusi,
    );
    this.notarisesSharedCollection = this.notarisService.addNotarisToCollectionIfMissing<INotaris>(
      this.notarisesSharedCollection,
      orderKerja.notaris,
    );
    this.penerimaKuasasSharedCollection = this.penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing<IPenerimaKuasa>(
      this.penerimaKuasasSharedCollection,
      orderKerja.penerimaKuasa,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.skSubtitusiService
      .query()
      .pipe(map((res: HttpResponse<ISkSubtitusi[]>) => res.body ?? []))
      .pipe(
        map((skSubtitusis: ISkSubtitusi[]) =>
          this.skSubtitusiService.addSkSubtitusiToCollectionIfMissing<ISkSubtitusi>(skSubtitusis, this.orderKerja?.skSubtitusi),
        ),
      )
      .subscribe((skSubtitusis: ISkSubtitusi[]) => (this.skSubtitusisSharedCollection = skSubtitusis));

    this.notarisService
      .query()
      .pipe(map((res: HttpResponse<INotaris[]>) => res.body ?? []))
      .pipe(
        map((notarises: INotaris[]) => this.notarisService.addNotarisToCollectionIfMissing<INotaris>(notarises, this.orderKerja?.notaris)),
      )
      .subscribe((notarises: INotaris[]) => (this.notarisesSharedCollection = notarises));

    this.penerimaKuasaService
      .query()
      .pipe(map((res: HttpResponse<IPenerimaKuasa[]>) => res.body ?? []))
      .pipe(
        map((penerimaKuasas: IPenerimaKuasa[]) =>
          this.penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing<IPenerimaKuasa>(penerimaKuasas, this.orderKerja?.penerimaKuasa),
        ),
      )
      .subscribe((penerimaKuasas: IPenerimaKuasa[]) => (this.penerimaKuasasSharedCollection = penerimaKuasas));
  }
}
