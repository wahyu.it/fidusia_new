import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IOrderKerja, NewOrderKerja } from '../order-kerja.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IOrderKerja for edit and NewOrderKerjaFormGroupInput for create.
 */
type OrderKerjaFormGroupInput = IOrderKerja | PartialWithRequiredKeyOf<NewOrderKerja>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IOrderKerja | NewOrderKerja> = Omit<T, 'updateOn' | 'approveOn'> & {
  updateOn?: string | null;
  approveOn?: string | null;
};

type OrderKerjaFormRawValue = FormValueOf<IOrderKerja>;

type NewOrderKerjaFormRawValue = FormValueOf<NewOrderKerja>;

type OrderKerjaFormDefaults = Pick<NewOrderKerja, 'id' | 'statusSigned' | 'updateOn' | 'approveOn'>;

type OrderKerjaFormGroupContent = {
  id: FormControl<OrderKerjaFormRawValue['id'] | NewOrderKerja['id']>;
  nomor: FormControl<OrderKerjaFormRawValue['nomor']>;
  tanggal: FormControl<OrderKerjaFormRawValue['tanggal']>;
  kategori: FormControl<OrderKerjaFormRawValue['kategori']>;
  tanggalTerima: FormControl<OrderKerjaFormRawValue['tanggalTerima']>;
  jumlahAkta: FormControl<OrderKerjaFormRawValue['jumlahAkta']>;
  jumlahCancel: FormControl<OrderKerjaFormRawValue['jumlahCancel']>;
  jumlahCetakAkta: FormControl<OrderKerjaFormRawValue['jumlahCetakAkta']>;
  uploadFile: FormControl<OrderKerjaFormRawValue['uploadFile']>;
  status: FormControl<OrderKerjaFormRawValue['status']>;
  statusSigned: FormControl<OrderKerjaFormRawValue['statusSigned']>;
  updateBy: FormControl<OrderKerjaFormRawValue['updateBy']>;
  updateOn: FormControl<OrderKerjaFormRawValue['updateOn']>;
  approveBy: FormControl<OrderKerjaFormRawValue['approveBy']>;
  approveOn: FormControl<OrderKerjaFormRawValue['approveOn']>;
  skSubtitusi: FormControl<OrderKerjaFormRawValue['skSubtitusi']>;
  notaris: FormControl<OrderKerjaFormRawValue['notaris']>;
  penerimaKuasa: FormControl<OrderKerjaFormRawValue['penerimaKuasa']>;
};

export type OrderKerjaFormGroup = FormGroup<OrderKerjaFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class OrderKerjaFormService {
  createOrderKerjaFormGroup(orderKerja: OrderKerjaFormGroupInput = { id: null }): OrderKerjaFormGroup {
    const orderKerjaRawValue = this.convertOrderKerjaToOrderKerjaRawValue({
      ...this.getFormDefaults(),
      ...orderKerja,
    });
    return new FormGroup<OrderKerjaFormGroupContent>({
      id: new FormControl(
        { value: orderKerjaRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      nomor: new FormControl(orderKerjaRawValue.nomor),
      tanggal: new FormControl(orderKerjaRawValue.tanggal),
      kategori: new FormControl(orderKerjaRawValue.kategori),
      tanggalTerima: new FormControl(orderKerjaRawValue.tanggalTerima),
      jumlahAkta: new FormControl(orderKerjaRawValue.jumlahAkta),
      jumlahCancel: new FormControl(orderKerjaRawValue.jumlahCancel),
      jumlahCetakAkta: new FormControl(orderKerjaRawValue.jumlahCetakAkta),
      uploadFile: new FormControl(orderKerjaRawValue.uploadFile),
      status: new FormControl(orderKerjaRawValue.status),
      statusSigned: new FormControl(orderKerjaRawValue.statusSigned),
      updateBy: new FormControl(orderKerjaRawValue.updateBy),
      updateOn: new FormControl(orderKerjaRawValue.updateOn),
      approveBy: new FormControl(orderKerjaRawValue.approveBy),
      approveOn: new FormControl(orderKerjaRawValue.approveOn),
      skSubtitusi: new FormControl(orderKerjaRawValue.skSubtitusi),
      notaris: new FormControl(orderKerjaRawValue.notaris),
      penerimaKuasa: new FormControl(orderKerjaRawValue.penerimaKuasa),
    });
  }

  getOrderKerja(form: OrderKerjaFormGroup): IOrderKerja | NewOrderKerja {
    return this.convertOrderKerjaRawValueToOrderKerja(form.getRawValue() as OrderKerjaFormRawValue | NewOrderKerjaFormRawValue);
  }

  resetForm(form: OrderKerjaFormGroup, orderKerja: OrderKerjaFormGroupInput): void {
    const orderKerjaRawValue = this.convertOrderKerjaToOrderKerjaRawValue({ ...this.getFormDefaults(), ...orderKerja });
    form.reset(
      {
        ...orderKerjaRawValue,
        id: { value: orderKerjaRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): OrderKerjaFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      statusSigned: false,
      updateOn: currentTime,
      approveOn: currentTime,
    };
  }

  private convertOrderKerjaRawValueToOrderKerja(
    rawOrderKerja: OrderKerjaFormRawValue | NewOrderKerjaFormRawValue,
  ): IOrderKerja | NewOrderKerja {
    return {
      ...rawOrderKerja,
      updateOn: dayjs(rawOrderKerja.updateOn, DATE_TIME_FORMAT),
      approveOn: dayjs(rawOrderKerja.approveOn, DATE_TIME_FORMAT),
    };
  }

  private convertOrderKerjaToOrderKerjaRawValue(
    orderKerja: IOrderKerja | (Partial<NewOrderKerja> & OrderKerjaFormDefaults),
  ): OrderKerjaFormRawValue | PartialWithRequiredKeyOf<NewOrderKerjaFormRawValue> {
    return {
      ...orderKerja,
      updateOn: orderKerja.updateOn ? orderKerja.updateOn.format(DATE_TIME_FORMAT) : undefined,
      approveOn: orderKerja.approveOn ? orderKerja.approveOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
