import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ISkSubtitusi } from 'app/entities/sk-subtitusi/sk-subtitusi.model';
import { SkSubtitusiService } from 'app/entities/sk-subtitusi/service/sk-subtitusi.service';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { IPenerimaKuasa } from 'app/entities/penerima-kuasa/penerima-kuasa.model';
import { PenerimaKuasaService } from 'app/entities/penerima-kuasa/service/penerima-kuasa.service';
import { IOrderKerja } from '../order-kerja.model';
import { OrderKerjaService } from '../service/order-kerja.service';
import { OrderKerjaFormService } from './order-kerja-form.service';

import { OrderKerjaUpdateComponent } from './order-kerja-update.component';

describe('OrderKerja Management Update Component', () => {
  let comp: OrderKerjaUpdateComponent;
  let fixture: ComponentFixture<OrderKerjaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let orderKerjaFormService: OrderKerjaFormService;
  let orderKerjaService: OrderKerjaService;
  let skSubtitusiService: SkSubtitusiService;
  let notarisService: NotarisService;
  let penerimaKuasaService: PenerimaKuasaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), OrderKerjaUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(OrderKerjaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OrderKerjaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    orderKerjaFormService = TestBed.inject(OrderKerjaFormService);
    orderKerjaService = TestBed.inject(OrderKerjaService);
    skSubtitusiService = TestBed.inject(SkSubtitusiService);
    notarisService = TestBed.inject(NotarisService);
    penerimaKuasaService = TestBed.inject(PenerimaKuasaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call SkSubtitusi query and add missing value', () => {
      const orderKerja: IOrderKerja = { id: 456 };
      const skSubtitusi: ISkSubtitusi = { id: 5554 };
      orderKerja.skSubtitusi = skSubtitusi;

      const skSubtitusiCollection: ISkSubtitusi[] = [{ id: 12656 }];
      jest.spyOn(skSubtitusiService, 'query').mockReturnValue(of(new HttpResponse({ body: skSubtitusiCollection })));
      const additionalSkSubtitusis = [skSubtitusi];
      const expectedCollection: ISkSubtitusi[] = [...additionalSkSubtitusis, ...skSubtitusiCollection];
      jest.spyOn(skSubtitusiService, 'addSkSubtitusiToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderKerja });
      comp.ngOnInit();

      expect(skSubtitusiService.query).toHaveBeenCalled();
      expect(skSubtitusiService.addSkSubtitusiToCollectionIfMissing).toHaveBeenCalledWith(
        skSubtitusiCollection,
        ...additionalSkSubtitusis.map(expect.objectContaining),
      );
      expect(comp.skSubtitusisSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Notaris query and add missing value', () => {
      const orderKerja: IOrderKerja = { id: 456 };
      const notaris: INotaris = { id: 2692 };
      orderKerja.notaris = notaris;

      const notarisCollection: INotaris[] = [{ id: 7621 }];
      jest.spyOn(notarisService, 'query').mockReturnValue(of(new HttpResponse({ body: notarisCollection })));
      const additionalNotarises = [notaris];
      const expectedCollection: INotaris[] = [...additionalNotarises, ...notarisCollection];
      jest.spyOn(notarisService, 'addNotarisToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderKerja });
      comp.ngOnInit();

      expect(notarisService.query).toHaveBeenCalled();
      expect(notarisService.addNotarisToCollectionIfMissing).toHaveBeenCalledWith(
        notarisCollection,
        ...additionalNotarises.map(expect.objectContaining),
      );
      expect(comp.notarisesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call PenerimaKuasa query and add missing value', () => {
      const orderKerja: IOrderKerja = { id: 456 };
      const penerimaKuasa: IPenerimaKuasa = { id: 3566 };
      orderKerja.penerimaKuasa = penerimaKuasa;

      const penerimaKuasaCollection: IPenerimaKuasa[] = [{ id: 422 }];
      jest.spyOn(penerimaKuasaService, 'query').mockReturnValue(of(new HttpResponse({ body: penerimaKuasaCollection })));
      const additionalPenerimaKuasas = [penerimaKuasa];
      const expectedCollection: IPenerimaKuasa[] = [...additionalPenerimaKuasas, ...penerimaKuasaCollection];
      jest.spyOn(penerimaKuasaService, 'addPenerimaKuasaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderKerja });
      comp.ngOnInit();

      expect(penerimaKuasaService.query).toHaveBeenCalled();
      expect(penerimaKuasaService.addPenerimaKuasaToCollectionIfMissing).toHaveBeenCalledWith(
        penerimaKuasaCollection,
        ...additionalPenerimaKuasas.map(expect.objectContaining),
      );
      expect(comp.penerimaKuasasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const orderKerja: IOrderKerja = { id: 456 };
      const skSubtitusi: ISkSubtitusi = { id: 17095 };
      orderKerja.skSubtitusi = skSubtitusi;
      const notaris: INotaris = { id: 488 };
      orderKerja.notaris = notaris;
      const penerimaKuasa: IPenerimaKuasa = { id: 31017 };
      orderKerja.penerimaKuasa = penerimaKuasa;

      activatedRoute.data = of({ orderKerja });
      comp.ngOnInit();

      expect(comp.skSubtitusisSharedCollection).toContain(skSubtitusi);
      expect(comp.notarisesSharedCollection).toContain(notaris);
      expect(comp.penerimaKuasasSharedCollection).toContain(penerimaKuasa);
      expect(comp.orderKerja).toEqual(orderKerja);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderKerja>>();
      const orderKerja = { id: 123 };
      jest.spyOn(orderKerjaFormService, 'getOrderKerja').mockReturnValue(orderKerja);
      jest.spyOn(orderKerjaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderKerja });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orderKerja }));
      saveSubject.complete();

      // THEN
      expect(orderKerjaFormService.getOrderKerja).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(orderKerjaService.update).toHaveBeenCalledWith(expect.objectContaining(orderKerja));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderKerja>>();
      const orderKerja = { id: 123 };
      jest.spyOn(orderKerjaFormService, 'getOrderKerja').mockReturnValue({ id: null });
      jest.spyOn(orderKerjaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderKerja: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orderKerja }));
      saveSubject.complete();

      // THEN
      expect(orderKerjaFormService.getOrderKerja).toHaveBeenCalled();
      expect(orderKerjaService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderKerja>>();
      const orderKerja = { id: 123 };
      jest.spyOn(orderKerjaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderKerja });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(orderKerjaService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareSkSubtitusi', () => {
      it('Should forward to skSubtitusiService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(skSubtitusiService, 'compareSkSubtitusi');
        comp.compareSkSubtitusi(entity, entity2);
        expect(skSubtitusiService.compareSkSubtitusi).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareNotaris', () => {
      it('Should forward to notarisService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(notarisService, 'compareNotaris');
        comp.compareNotaris(entity, entity2);
        expect(notarisService.compareNotaris).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('comparePenerimaKuasa', () => {
      it('Should forward to penerimaKuasaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(penerimaKuasaService, 'comparePenerimaKuasa');
        comp.comparePenerimaKuasa(entity, entity2);
        expect(penerimaKuasaService.comparePenerimaKuasa).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
