import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../order-kerja.test-samples';

import { OrderKerjaFormService } from './order-kerja-form.service';

describe('OrderKerja Form Service', () => {
  let service: OrderKerjaFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrderKerjaFormService);
  });

  describe('Service methods', () => {
    describe('createOrderKerjaFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createOrderKerjaFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nomor: expect.any(Object),
            tanggal: expect.any(Object),
            kategori: expect.any(Object),
            tanggalTerima: expect.any(Object),
            jumlahAkta: expect.any(Object),
            jumlahCancel: expect.any(Object),
            jumlahCetakAkta: expect.any(Object),
            uploadFile: expect.any(Object),
            status: expect.any(Object),
            statusSigned: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            approveBy: expect.any(Object),
            approveOn: expect.any(Object),
            skSubtitusi: expect.any(Object),
            notaris: expect.any(Object),
            penerimaKuasa: expect.any(Object),
          }),
        );
      });

      it('passing IOrderKerja should create a new form with FormGroup', () => {
        const formGroup = service.createOrderKerjaFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            nomor: expect.any(Object),
            tanggal: expect.any(Object),
            kategori: expect.any(Object),
            tanggalTerima: expect.any(Object),
            jumlahAkta: expect.any(Object),
            jumlahCancel: expect.any(Object),
            jumlahCetakAkta: expect.any(Object),
            uploadFile: expect.any(Object),
            status: expect.any(Object),
            statusSigned: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            approveBy: expect.any(Object),
            approveOn: expect.any(Object),
            skSubtitusi: expect.any(Object),
            notaris: expect.any(Object),
            penerimaKuasa: expect.any(Object),
          }),
        );
      });
    });

    describe('getOrderKerja', () => {
      it('should return NewOrderKerja for default OrderKerja initial value', () => {
        const formGroup = service.createOrderKerjaFormGroup(sampleWithNewData);

        const orderKerja = service.getOrderKerja(formGroup) as any;

        expect(orderKerja).toMatchObject(sampleWithNewData);
      });

      it('should return NewOrderKerja for empty OrderKerja initial value', () => {
        const formGroup = service.createOrderKerjaFormGroup();

        const orderKerja = service.getOrderKerja(formGroup) as any;

        expect(orderKerja).toMatchObject({});
      });

      it('should return IOrderKerja', () => {
        const formGroup = service.createOrderKerjaFormGroup(sampleWithRequiredData);

        const orderKerja = service.getOrderKerja(formGroup) as any;

        expect(orderKerja).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IOrderKerja should not enable id FormControl', () => {
        const formGroup = service.createOrderKerjaFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewOrderKerja should disable id FormControl', () => {
        const formGroup = service.createOrderKerjaFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
