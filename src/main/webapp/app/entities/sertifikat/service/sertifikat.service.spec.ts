import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ISertifikat } from '../sertifikat.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../sertifikat.test-samples';

import { SertifikatService, RestSertifikat } from './sertifikat.service';

const requireRestSample: RestSertifikat = {
  ...sampleWithRequiredData,
  tglVoucher: sampleWithRequiredData.tglVoucher?.toJSON(),
  tglSertifikat: sampleWithRequiredData.tglSertifikat?.toJSON(),
  updateOn: sampleWithRequiredData.updateOn?.toJSON(),
  tglOrder: sampleWithRequiredData.tglOrder?.format(DATE_FORMAT),
  tglCancel: sampleWithRequiredData.tglCancel?.format(DATE_FORMAT),
};

describe('Sertifikat Service', () => {
  let service: SertifikatService;
  let httpMock: HttpTestingController;
  let expectedResult: ISertifikat | ISertifikat[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SertifikatService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Sertifikat', () => {
      const sertifikat = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(sertifikat).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Sertifikat', () => {
      const sertifikat = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(sertifikat).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Sertifikat', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Sertifikat', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Sertifikat', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addSertifikatToCollectionIfMissing', () => {
      it('should add a Sertifikat to an empty array', () => {
        const sertifikat: ISertifikat = sampleWithRequiredData;
        expectedResult = service.addSertifikatToCollectionIfMissing([], sertifikat);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sertifikat);
      });

      it('should not add a Sertifikat to an array that contains it', () => {
        const sertifikat: ISertifikat = sampleWithRequiredData;
        const sertifikatCollection: ISertifikat[] = [
          {
            ...sertifikat,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addSertifikatToCollectionIfMissing(sertifikatCollection, sertifikat);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Sertifikat to an array that doesn't contain it", () => {
        const sertifikat: ISertifikat = sampleWithRequiredData;
        const sertifikatCollection: ISertifikat[] = [sampleWithPartialData];
        expectedResult = service.addSertifikatToCollectionIfMissing(sertifikatCollection, sertifikat);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sertifikat);
      });

      it('should add only unique Sertifikat to an array', () => {
        const sertifikatArray: ISertifikat[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const sertifikatCollection: ISertifikat[] = [sampleWithRequiredData];
        expectedResult = service.addSertifikatToCollectionIfMissing(sertifikatCollection, ...sertifikatArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const sertifikat: ISertifikat = sampleWithRequiredData;
        const sertifikat2: ISertifikat = sampleWithPartialData;
        expectedResult = service.addSertifikatToCollectionIfMissing([], sertifikat, sertifikat2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sertifikat);
        expect(expectedResult).toContain(sertifikat2);
      });

      it('should accept null and undefined values', () => {
        const sertifikat: ISertifikat = sampleWithRequiredData;
        expectedResult = service.addSertifikatToCollectionIfMissing([], null, sertifikat, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sertifikat);
      });

      it('should return initial array if no Sertifikat is added', () => {
        const sertifikatCollection: ISertifikat[] = [sampleWithRequiredData];
        expectedResult = service.addSertifikatToCollectionIfMissing(sertifikatCollection, undefined, null);
        expect(expectedResult).toEqual(sertifikatCollection);
      });
    });

    describe('compareSertifikat', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareSertifikat(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareSertifikat(entity1, entity2);
        const compareResult2 = service.compareSertifikat(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareSertifikat(entity1, entity2);
        const compareResult2 = service.compareSertifikat(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareSertifikat(entity1, entity2);
        const compareResult2 = service.compareSertifikat(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
