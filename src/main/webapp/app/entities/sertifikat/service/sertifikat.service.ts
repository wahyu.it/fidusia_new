import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISertifikat, NewSertifikat } from '../sertifikat.model';

export type PartialUpdateSertifikat = Partial<ISertifikat> & Pick<ISertifikat, 'id'>;

type RestOf<T extends ISertifikat | NewSertifikat> = Omit<T, 'tglVoucher' | 'tglSertifikat' | 'updateOn' | 'tglOrder' | 'tglCancel'> & {
  tglVoucher?: string | null;
  tglSertifikat?: string | null;
  updateOn?: string | null;
  tglOrder?: string | null;
  tglCancel?: string | null;
};

export type RestSertifikat = RestOf<ISertifikat>;

export type NewRestSertifikat = RestOf<NewSertifikat>;

export type PartialUpdateRestSertifikat = RestOf<PartialUpdateSertifikat>;

export type EntityResponseType = HttpResponse<ISertifikat>;
export type EntityArrayResponseType = HttpResponse<ISertifikat[]>;

@Injectable({ providedIn: 'root' })
export class SertifikatService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/sertifikats');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(sertifikat: NewSertifikat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sertifikat);
    return this.http
      .post<RestSertifikat>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(sertifikat: ISertifikat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sertifikat);
    return this.http
      .put<RestSertifikat>(`${this.resourceUrl}/${this.getSertifikatIdentifier(sertifikat)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(sertifikat: PartialUpdateSertifikat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sertifikat);
    return this.http
      .patch<RestSertifikat>(`${this.resourceUrl}/${this.getSertifikatIdentifier(sertifikat)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestSertifikat>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestSertifikat[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSertifikatIdentifier(sertifikat: Pick<ISertifikat, 'id'>): number {
    return sertifikat.id;
  }

  compareSertifikat(o1: Pick<ISertifikat, 'id'> | null, o2: Pick<ISertifikat, 'id'> | null): boolean {
    return o1 && o2 ? this.getSertifikatIdentifier(o1) === this.getSertifikatIdentifier(o2) : o1 === o2;
  }

  addSertifikatToCollectionIfMissing<Type extends Pick<ISertifikat, 'id'>>(
    sertifikatCollection: Type[],
    ...sertifikatsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const sertifikats: Type[] = sertifikatsToCheck.filter(isPresent);
    if (sertifikats.length > 0) {
      const sertifikatCollectionIdentifiers = sertifikatCollection.map(sertifikatItem => this.getSertifikatIdentifier(sertifikatItem)!);
      const sertifikatsToAdd = sertifikats.filter(sertifikatItem => {
        const sertifikatIdentifier = this.getSertifikatIdentifier(sertifikatItem);
        if (sertifikatCollectionIdentifiers.includes(sertifikatIdentifier)) {
          return false;
        }
        sertifikatCollectionIdentifiers.push(sertifikatIdentifier);
        return true;
      });
      return [...sertifikatsToAdd, ...sertifikatCollection];
    }
    return sertifikatCollection;
  }

  protected convertDateFromClient<T extends ISertifikat | NewSertifikat | PartialUpdateSertifikat>(sertifikat: T): RestOf<T> {
    return {
      ...sertifikat,
      tglVoucher: sertifikat.tglVoucher?.toJSON() ?? null,
      tglSertifikat: sertifikat.tglSertifikat?.toJSON() ?? null,
      updateOn: sertifikat.updateOn?.toJSON() ?? null,
      tglOrder: sertifikat.tglOrder?.format(DATE_FORMAT) ?? null,
      tglCancel: sertifikat.tglCancel?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restSertifikat: RestSertifikat): ISertifikat {
    return {
      ...restSertifikat,
      tglVoucher: restSertifikat.tglVoucher ? dayjs(restSertifikat.tglVoucher) : undefined,
      tglSertifikat: restSertifikat.tglSertifikat ? dayjs(restSertifikat.tglSertifikat) : undefined,
      updateOn: restSertifikat.updateOn ? dayjs(restSertifikat.updateOn) : undefined,
      tglOrder: restSertifikat.tglOrder ? dayjs(restSertifikat.tglOrder) : undefined,
      tglCancel: restSertifikat.tglCancel ? dayjs(restSertifikat.tglCancel) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestSertifikat>): HttpResponse<ISertifikat> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestSertifikat[]>): HttpResponse<ISertifikat[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
