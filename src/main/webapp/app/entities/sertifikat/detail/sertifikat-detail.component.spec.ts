import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { SertifikatDetailComponent } from './sertifikat-detail.component';

describe('Sertifikat Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SertifikatDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: SertifikatDetailComponent,
              resolve: { sertifikat: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(SertifikatDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load sertifikat on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', SertifikatDetailComponent);

      // THEN
      expect(instance.sertifikat).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
