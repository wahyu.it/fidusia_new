import dayjs from 'dayjs/esm';

import { ISertifikat, NewSertifikat } from './sertifikat.model';

export const sampleWithRequiredData: ISertifikat = {
  id: 27006,
};

export const sampleWithPartialData: ISertifikat = {
  id: 8658,
  noSertifikat: 'frightened over sunlamp',
  updateBy: 'kissingly geez gently',
  updateOn: dayjs('2023-12-14T07:13'),
  registrationId: 'up anxiously partially',
  tglOrder: dayjs('2023-12-14'),
  biayaJasa: 25679,
  reportSubmitted: true,
};

export const sampleWithFullData: ISertifikat = {
  id: 23913,
  kodeVoucher: 'next pricey italicise',
  tglVoucher: dayjs('2023-12-14T20:01'),
  noSertifikat: 'questioningly elephant',
  tglSertifikat: dayjs('2023-12-14T03:33'),
  biayaPnbp: 15372,
  noReferensiBni: 'unto',
  status: 10128,
  updateBy: 'spectrograph',
  updateOn: dayjs('2023-12-15T01:13'),
  registrationId: 'bronze er yippee',
  tglOrder: dayjs('2023-12-14'),
  tglCancel: dayjs('2023-12-15'),
  biayaJasa: 4368,
  invoiceSubmitted: false,
  reportSubmitted: true,
};

export const sampleWithNewData: NewSertifikat = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
