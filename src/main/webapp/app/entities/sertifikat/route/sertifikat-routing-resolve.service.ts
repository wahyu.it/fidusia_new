import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISertifikat } from '../sertifikat.model';
import { SertifikatService } from '../service/sertifikat.service';

export const sertifikatResolve = (route: ActivatedRouteSnapshot): Observable<null | ISertifikat> => {
  const id = route.params['id'];
  if (id) {
    return inject(SertifikatService)
      .find(id)
      .pipe(
        mergeMap((sertifikat: HttpResponse<ISertifikat>) => {
          if (sertifikat.body) {
            return of(sertifikat.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default sertifikatResolve;
