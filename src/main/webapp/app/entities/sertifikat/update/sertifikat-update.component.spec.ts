import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IAkta } from 'app/entities/akta/akta.model';
import { AktaService } from 'app/entities/akta/service/akta.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { IInvoice } from 'app/entities/invoice/invoice.model';
import { InvoiceService } from 'app/entities/invoice/service/invoice.service';
import { ISertifikat } from '../sertifikat.model';
import { SertifikatService } from '../service/sertifikat.service';
import { SertifikatFormService } from './sertifikat-form.service';

import { SertifikatUpdateComponent } from './sertifikat-update.component';

describe('Sertifikat Management Update Component', () => {
  let comp: SertifikatUpdateComponent;
  let fixture: ComponentFixture<SertifikatUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let sertifikatFormService: SertifikatFormService;
  let sertifikatService: SertifikatService;
  let aktaService: AktaService;
  let cabangService: CabangService;
  let invoiceService: InvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), SertifikatUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SertifikatUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SertifikatUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    sertifikatFormService = TestBed.inject(SertifikatFormService);
    sertifikatService = TestBed.inject(SertifikatService);
    aktaService = TestBed.inject(AktaService);
    cabangService = TestBed.inject(CabangService);
    invoiceService = TestBed.inject(InvoiceService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Akta query and add missing value', () => {
      const sertifikat: ISertifikat = { id: 456 };
      const akta: IAkta = { id: 15773 };
      sertifikat.akta = akta;

      const aktaCollection: IAkta[] = [{ id: 16002 }];
      jest.spyOn(aktaService, 'query').mockReturnValue(of(new HttpResponse({ body: aktaCollection })));
      const additionalAktas = [akta];
      const expectedCollection: IAkta[] = [...additionalAktas, ...aktaCollection];
      jest.spyOn(aktaService, 'addAktaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ sertifikat });
      comp.ngOnInit();

      expect(aktaService.query).toHaveBeenCalled();
      expect(aktaService.addAktaToCollectionIfMissing).toHaveBeenCalledWith(
        aktaCollection,
        ...additionalAktas.map(expect.objectContaining),
      );
      expect(comp.aktasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Cabang query and add missing value', () => {
      const sertifikat: ISertifikat = { id: 456 };
      const cabang: ICabang = { id: 2268 };
      sertifikat.cabang = cabang;

      const cabangCollection: ICabang[] = [{ id: 19212 }];
      jest.spyOn(cabangService, 'query').mockReturnValue(of(new HttpResponse({ body: cabangCollection })));
      const additionalCabangs = [cabang];
      const expectedCollection: ICabang[] = [...additionalCabangs, ...cabangCollection];
      jest.spyOn(cabangService, 'addCabangToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ sertifikat });
      comp.ngOnInit();

      expect(cabangService.query).toHaveBeenCalled();
      expect(cabangService.addCabangToCollectionIfMissing).toHaveBeenCalledWith(
        cabangCollection,
        ...additionalCabangs.map(expect.objectContaining),
      );
      expect(comp.cabangsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Invoice query and add missing value', () => {
      const sertifikat: ISertifikat = { id: 456 };
      const invoice: IInvoice = { id: 3340 };
      sertifikat.invoice = invoice;

      const invoiceCollection: IInvoice[] = [{ id: 16462 }];
      jest.spyOn(invoiceService, 'query').mockReturnValue(of(new HttpResponse({ body: invoiceCollection })));
      const additionalInvoices = [invoice];
      const expectedCollection: IInvoice[] = [...additionalInvoices, ...invoiceCollection];
      jest.spyOn(invoiceService, 'addInvoiceToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ sertifikat });
      comp.ngOnInit();

      expect(invoiceService.query).toHaveBeenCalled();
      expect(invoiceService.addInvoiceToCollectionIfMissing).toHaveBeenCalledWith(
        invoiceCollection,
        ...additionalInvoices.map(expect.objectContaining),
      );
      expect(comp.invoicesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const sertifikat: ISertifikat = { id: 456 };
      const akta: IAkta = { id: 508 };
      sertifikat.akta = akta;
      const cabang: ICabang = { id: 25899 };
      sertifikat.cabang = cabang;
      const invoice: IInvoice = { id: 738 };
      sertifikat.invoice = invoice;

      activatedRoute.data = of({ sertifikat });
      comp.ngOnInit();

      expect(comp.aktasSharedCollection).toContain(akta);
      expect(comp.cabangsSharedCollection).toContain(cabang);
      expect(comp.invoicesSharedCollection).toContain(invoice);
      expect(comp.sertifikat).toEqual(sertifikat);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISertifikat>>();
      const sertifikat = { id: 123 };
      jest.spyOn(sertifikatFormService, 'getSertifikat').mockReturnValue(sertifikat);
      jest.spyOn(sertifikatService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sertifikat });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sertifikat }));
      saveSubject.complete();

      // THEN
      expect(sertifikatFormService.getSertifikat).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(sertifikatService.update).toHaveBeenCalledWith(expect.objectContaining(sertifikat));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISertifikat>>();
      const sertifikat = { id: 123 };
      jest.spyOn(sertifikatFormService, 'getSertifikat').mockReturnValue({ id: null });
      jest.spyOn(sertifikatService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sertifikat: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sertifikat }));
      saveSubject.complete();

      // THEN
      expect(sertifikatFormService.getSertifikat).toHaveBeenCalled();
      expect(sertifikatService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISertifikat>>();
      const sertifikat = { id: 123 };
      jest.spyOn(sertifikatService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sertifikat });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(sertifikatService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareAkta', () => {
      it('Should forward to aktaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(aktaService, 'compareAkta');
        comp.compareAkta(entity, entity2);
        expect(aktaService.compareAkta).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareCabang', () => {
      it('Should forward to cabangService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(cabangService, 'compareCabang');
        comp.compareCabang(entity, entity2);
        expect(cabangService.compareCabang).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareInvoice', () => {
      it('Should forward to invoiceService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(invoiceService, 'compareInvoice');
        comp.compareInvoice(entity, entity2);
        expect(invoiceService.compareInvoice).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
