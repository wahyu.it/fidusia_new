import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ISertifikat, NewSertifikat } from '../sertifikat.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISertifikat for edit and NewSertifikatFormGroupInput for create.
 */
type SertifikatFormGroupInput = ISertifikat | PartialWithRequiredKeyOf<NewSertifikat>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ISertifikat | NewSertifikat> = Omit<T, 'tglVoucher' | 'tglSertifikat' | 'updateOn'> & {
  tglVoucher?: string | null;
  tglSertifikat?: string | null;
  updateOn?: string | null;
};

type SertifikatFormRawValue = FormValueOf<ISertifikat>;

type NewSertifikatFormRawValue = FormValueOf<NewSertifikat>;

type SertifikatFormDefaults = Pick<
  NewSertifikat,
  'id' | 'tglVoucher' | 'tglSertifikat' | 'updateOn' | 'invoiceSubmitted' | 'reportSubmitted'
>;

type SertifikatFormGroupContent = {
  id: FormControl<SertifikatFormRawValue['id'] | NewSertifikat['id']>;
  kodeVoucher: FormControl<SertifikatFormRawValue['kodeVoucher']>;
  tglVoucher: FormControl<SertifikatFormRawValue['tglVoucher']>;
  noSertifikat: FormControl<SertifikatFormRawValue['noSertifikat']>;
  tglSertifikat: FormControl<SertifikatFormRawValue['tglSertifikat']>;
  biayaPnbp: FormControl<SertifikatFormRawValue['biayaPnbp']>;
  noReferensiBni: FormControl<SertifikatFormRawValue['noReferensiBni']>;
  status: FormControl<SertifikatFormRawValue['status']>;
  updateBy: FormControl<SertifikatFormRawValue['updateBy']>;
  updateOn: FormControl<SertifikatFormRawValue['updateOn']>;
  registrationId: FormControl<SertifikatFormRawValue['registrationId']>;
  tglOrder: FormControl<SertifikatFormRawValue['tglOrder']>;
  tglCancel: FormControl<SertifikatFormRawValue['tglCancel']>;
  biayaJasa: FormControl<SertifikatFormRawValue['biayaJasa']>;
  invoiceSubmitted: FormControl<SertifikatFormRawValue['invoiceSubmitted']>;
  reportSubmitted: FormControl<SertifikatFormRawValue['reportSubmitted']>;
  akta: FormControl<SertifikatFormRawValue['akta']>;
  cabang: FormControl<SertifikatFormRawValue['cabang']>;
  invoice: FormControl<SertifikatFormRawValue['invoice']>;
};

export type SertifikatFormGroup = FormGroup<SertifikatFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SertifikatFormService {
  createSertifikatFormGroup(sertifikat: SertifikatFormGroupInput = { id: null }): SertifikatFormGroup {
    const sertifikatRawValue = this.convertSertifikatToSertifikatRawValue({
      ...this.getFormDefaults(),
      ...sertifikat,
    });
    return new FormGroup<SertifikatFormGroupContent>({
      id: new FormControl(
        { value: sertifikatRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      kodeVoucher: new FormControl(sertifikatRawValue.kodeVoucher),
      tglVoucher: new FormControl(sertifikatRawValue.tglVoucher),
      noSertifikat: new FormControl(sertifikatRawValue.noSertifikat),
      tglSertifikat: new FormControl(sertifikatRawValue.tglSertifikat),
      biayaPnbp: new FormControl(sertifikatRawValue.biayaPnbp),
      noReferensiBni: new FormControl(sertifikatRawValue.noReferensiBni),
      status: new FormControl(sertifikatRawValue.status),
      updateBy: new FormControl(sertifikatRawValue.updateBy),
      updateOn: new FormControl(sertifikatRawValue.updateOn),
      registrationId: new FormControl(sertifikatRawValue.registrationId),
      tglOrder: new FormControl(sertifikatRawValue.tglOrder),
      tglCancel: new FormControl(sertifikatRawValue.tglCancel),
      biayaJasa: new FormControl(sertifikatRawValue.biayaJasa),
      invoiceSubmitted: new FormControl(sertifikatRawValue.invoiceSubmitted),
      reportSubmitted: new FormControl(sertifikatRawValue.reportSubmitted),
      akta: new FormControl(sertifikatRawValue.akta),
      cabang: new FormControl(sertifikatRawValue.cabang),
      invoice: new FormControl(sertifikatRawValue.invoice),
    });
  }

  getSertifikat(form: SertifikatFormGroup): ISertifikat | NewSertifikat {
    return this.convertSertifikatRawValueToSertifikat(form.getRawValue() as SertifikatFormRawValue | NewSertifikatFormRawValue);
  }

  resetForm(form: SertifikatFormGroup, sertifikat: SertifikatFormGroupInput): void {
    const sertifikatRawValue = this.convertSertifikatToSertifikatRawValue({ ...this.getFormDefaults(), ...sertifikat });
    form.reset(
      {
        ...sertifikatRawValue,
        id: { value: sertifikatRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): SertifikatFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      tglVoucher: currentTime,
      tglSertifikat: currentTime,
      updateOn: currentTime,
      invoiceSubmitted: false,
      reportSubmitted: false,
    };
  }

  private convertSertifikatRawValueToSertifikat(
    rawSertifikat: SertifikatFormRawValue | NewSertifikatFormRawValue,
  ): ISertifikat | NewSertifikat {
    return {
      ...rawSertifikat,
      tglVoucher: dayjs(rawSertifikat.tglVoucher, DATE_TIME_FORMAT),
      tglSertifikat: dayjs(rawSertifikat.tglSertifikat, DATE_TIME_FORMAT),
      updateOn: dayjs(rawSertifikat.updateOn, DATE_TIME_FORMAT),
    };
  }

  private convertSertifikatToSertifikatRawValue(
    sertifikat: ISertifikat | (Partial<NewSertifikat> & SertifikatFormDefaults),
  ): SertifikatFormRawValue | PartialWithRequiredKeyOf<NewSertifikatFormRawValue> {
    return {
      ...sertifikat,
      tglVoucher: sertifikat.tglVoucher ? sertifikat.tglVoucher.format(DATE_TIME_FORMAT) : undefined,
      tglSertifikat: sertifikat.tglSertifikat ? sertifikat.tglSertifikat.format(DATE_TIME_FORMAT) : undefined,
      updateOn: sertifikat.updateOn ? sertifikat.updateOn.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
