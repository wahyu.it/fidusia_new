import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IAkta } from 'app/entities/akta/akta.model';
import { AktaService } from 'app/entities/akta/service/akta.service';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { CabangService } from 'app/entities/cabang/service/cabang.service';
import { IInvoice } from 'app/entities/invoice/invoice.model';
import { InvoiceService } from 'app/entities/invoice/service/invoice.service';
import { SertifikatService } from '../service/sertifikat.service';
import { ISertifikat } from '../sertifikat.model';
import { SertifikatFormService, SertifikatFormGroup } from './sertifikat-form.service';

@Component({
  standalone: true,
  selector: 'jhi-sertifikat-update',
  templateUrl: './sertifikat-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class SertifikatUpdateComponent implements OnInit {
  isSaving = false;
  sertifikat: ISertifikat | null = null;

  aktasSharedCollection: IAkta[] = [];
  cabangsSharedCollection: ICabang[] = [];
  invoicesSharedCollection: IInvoice[] = [];

  editForm: SertifikatFormGroup = this.sertifikatFormService.createSertifikatFormGroup();

  constructor(
    protected sertifikatService: SertifikatService,
    protected sertifikatFormService: SertifikatFormService,
    protected aktaService: AktaService,
    protected cabangService: CabangService,
    protected invoiceService: InvoiceService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareAkta = (o1: IAkta | null, o2: IAkta | null): boolean => this.aktaService.compareAkta(o1, o2);

  compareCabang = (o1: ICabang | null, o2: ICabang | null): boolean => this.cabangService.compareCabang(o1, o2);

  compareInvoice = (o1: IInvoice | null, o2: IInvoice | null): boolean => this.invoiceService.compareInvoice(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sertifikat }) => {
      this.sertifikat = sertifikat;
      if (sertifikat) {
        this.updateForm(sertifikat);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sertifikat = this.sertifikatFormService.getSertifikat(this.editForm);
    if (sertifikat.id !== null) {
      this.subscribeToSaveResponse(this.sertifikatService.update(sertifikat));
    } else {
      this.subscribeToSaveResponse(this.sertifikatService.create(sertifikat));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISertifikat>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(sertifikat: ISertifikat): void {
    this.sertifikat = sertifikat;
    this.sertifikatFormService.resetForm(this.editForm, sertifikat);

    this.aktasSharedCollection = this.aktaService.addAktaToCollectionIfMissing<IAkta>(this.aktasSharedCollection, sertifikat.akta);
    this.cabangsSharedCollection = this.cabangService.addCabangToCollectionIfMissing<ICabang>(
      this.cabangsSharedCollection,
      sertifikat.cabang,
    );
    this.invoicesSharedCollection = this.invoiceService.addInvoiceToCollectionIfMissing<IInvoice>(
      this.invoicesSharedCollection,
      sertifikat.invoice,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.aktaService
      .query()
      .pipe(map((res: HttpResponse<IAkta[]>) => res.body ?? []))
      .pipe(map((aktas: IAkta[]) => this.aktaService.addAktaToCollectionIfMissing<IAkta>(aktas, this.sertifikat?.akta)))
      .subscribe((aktas: IAkta[]) => (this.aktasSharedCollection = aktas));

    this.cabangService
      .query()
      .pipe(map((res: HttpResponse<ICabang[]>) => res.body ?? []))
      .pipe(map((cabangs: ICabang[]) => this.cabangService.addCabangToCollectionIfMissing<ICabang>(cabangs, this.sertifikat?.cabang)))
      .subscribe((cabangs: ICabang[]) => (this.cabangsSharedCollection = cabangs));

    this.invoiceService
      .query()
      .pipe(map((res: HttpResponse<IInvoice[]>) => res.body ?? []))
      .pipe(
        map((invoices: IInvoice[]) => this.invoiceService.addInvoiceToCollectionIfMissing<IInvoice>(invoices, this.sertifikat?.invoice)),
      )
      .subscribe((invoices: IInvoice[]) => (this.invoicesSharedCollection = invoices));
  }
}
