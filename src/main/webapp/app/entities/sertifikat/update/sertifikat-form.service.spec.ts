import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../sertifikat.test-samples';

import { SertifikatFormService } from './sertifikat-form.service';

describe('Sertifikat Form Service', () => {
  let service: SertifikatFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SertifikatFormService);
  });

  describe('Service methods', () => {
    describe('createSertifikatFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createSertifikatFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kodeVoucher: expect.any(Object),
            tglVoucher: expect.any(Object),
            noSertifikat: expect.any(Object),
            tglSertifikat: expect.any(Object),
            biayaPnbp: expect.any(Object),
            noReferensiBni: expect.any(Object),
            status: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            registrationId: expect.any(Object),
            tglOrder: expect.any(Object),
            tglCancel: expect.any(Object),
            biayaJasa: expect.any(Object),
            invoiceSubmitted: expect.any(Object),
            reportSubmitted: expect.any(Object),
            akta: expect.any(Object),
            cabang: expect.any(Object),
            invoice: expect.any(Object),
          }),
        );
      });

      it('passing ISertifikat should create a new form with FormGroup', () => {
        const formGroup = service.createSertifikatFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            kodeVoucher: expect.any(Object),
            tglVoucher: expect.any(Object),
            noSertifikat: expect.any(Object),
            tglSertifikat: expect.any(Object),
            biayaPnbp: expect.any(Object),
            noReferensiBni: expect.any(Object),
            status: expect.any(Object),
            updateBy: expect.any(Object),
            updateOn: expect.any(Object),
            registrationId: expect.any(Object),
            tglOrder: expect.any(Object),
            tglCancel: expect.any(Object),
            biayaJasa: expect.any(Object),
            invoiceSubmitted: expect.any(Object),
            reportSubmitted: expect.any(Object),
            akta: expect.any(Object),
            cabang: expect.any(Object),
            invoice: expect.any(Object),
          }),
        );
      });
    });

    describe('getSertifikat', () => {
      it('should return NewSertifikat for default Sertifikat initial value', () => {
        const formGroup = service.createSertifikatFormGroup(sampleWithNewData);

        const sertifikat = service.getSertifikat(formGroup) as any;

        expect(sertifikat).toMatchObject(sampleWithNewData);
      });

      it('should return NewSertifikat for empty Sertifikat initial value', () => {
        const formGroup = service.createSertifikatFormGroup();

        const sertifikat = service.getSertifikat(formGroup) as any;

        expect(sertifikat).toMatchObject({});
      });

      it('should return ISertifikat', () => {
        const formGroup = service.createSertifikatFormGroup(sampleWithRequiredData);

        const sertifikat = service.getSertifikat(formGroup) as any;

        expect(sertifikat).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ISertifikat should not enable id FormControl', () => {
        const formGroup = service.createSertifikatFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewSertifikat should disable id FormControl', () => {
        const formGroup = service.createSertifikatFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
