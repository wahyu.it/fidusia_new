import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { SertifikatComponent } from './list/sertifikat.component';
import { SertifikatDetailComponent } from './detail/sertifikat-detail.component';
import { SertifikatUpdateComponent } from './update/sertifikat-update.component';
import SertifikatResolve from './route/sertifikat-routing-resolve.service';

const sertifikatRoute: Routes = [
  {
    path: '',
    component: SertifikatComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SertifikatDetailComponent,
    resolve: {
      sertifikat: SertifikatResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SertifikatUpdateComponent,
    resolve: {
      sertifikat: SertifikatResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SertifikatUpdateComponent,
    resolve: {
      sertifikat: SertifikatResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default sertifikatRoute;
