import dayjs from 'dayjs/esm';
import { IAkta } from 'app/entities/akta/akta.model';
import { ICabang } from 'app/entities/cabang/cabang.model';
import { IInvoice } from 'app/entities/invoice/invoice.model';

export interface ISertifikat {
  id: number;
  kodeVoucher?: string | null;
  tglVoucher?: dayjs.Dayjs | null;
  noSertifikat?: string | null;
  tglSertifikat?: dayjs.Dayjs | null;
  biayaPnbp?: number | null;
  noReferensiBni?: string | null;
  status?: number | null;
  updateBy?: string | null;
  updateOn?: dayjs.Dayjs | null;
  registrationId?: string | null;
  tglOrder?: dayjs.Dayjs | null;
  tglCancel?: dayjs.Dayjs | null;
  biayaJasa?: number | null;
  invoiceSubmitted?: boolean | null;
  reportSubmitted?: boolean | null;
  akta?: IAkta | null;
  cabang?: ICabang | null;
  invoice?: IInvoice | null;
}

export type NewSertifikat = Omit<ISertifikat, 'id'> & { id: null };
