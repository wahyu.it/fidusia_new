import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { OrderNotarisDetailComponent } from './order-notaris-detail.component';

describe('OrderNotaris Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OrderNotarisDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: OrderNotarisDetailComponent,
              resolve: { orderNotaris: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(OrderNotarisDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load orderNotaris on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', OrderNotarisDetailComponent);

      // THEN
      expect(instance.orderNotaris).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
