import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOrderNotaris, NewOrderNotaris } from '../order-notaris.model';

export type PartialUpdateOrderNotaris = Partial<IOrderNotaris> & Pick<IOrderNotaris, 'id'>;

type RestOf<T extends IOrderNotaris | NewOrderNotaris> = Omit<T, 'tanggal'> & {
  tanggal?: string | null;
};

export type RestOrderNotaris = RestOf<IOrderNotaris>;

export type NewRestOrderNotaris = RestOf<NewOrderNotaris>;

export type PartialUpdateRestOrderNotaris = RestOf<PartialUpdateOrderNotaris>;

export type EntityResponseType = HttpResponse<IOrderNotaris>;
export type EntityArrayResponseType = HttpResponse<IOrderNotaris[]>;

@Injectable({ providedIn: 'root' })
export class OrderNotarisService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/order-notarises');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(orderNotaris: NewOrderNotaris): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderNotaris);
    return this.http
      .post<RestOrderNotaris>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(orderNotaris: IOrderNotaris): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderNotaris);
    return this.http
      .put<RestOrderNotaris>(`${this.resourceUrl}/${this.getOrderNotarisIdentifier(orderNotaris)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(orderNotaris: PartialUpdateOrderNotaris): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderNotaris);
    return this.http
      .patch<RestOrderNotaris>(`${this.resourceUrl}/${this.getOrderNotarisIdentifier(orderNotaris)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestOrderNotaris>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestOrderNotaris[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getOrderNotarisIdentifier(orderNotaris: Pick<IOrderNotaris, 'id'>): number {
    return orderNotaris.id;
  }

  compareOrderNotaris(o1: Pick<IOrderNotaris, 'id'> | null, o2: Pick<IOrderNotaris, 'id'> | null): boolean {
    return o1 && o2 ? this.getOrderNotarisIdentifier(o1) === this.getOrderNotarisIdentifier(o2) : o1 === o2;
  }

  addOrderNotarisToCollectionIfMissing<Type extends Pick<IOrderNotaris, 'id'>>(
    orderNotarisCollection: Type[],
    ...orderNotarisesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const orderNotarises: Type[] = orderNotarisesToCheck.filter(isPresent);
    if (orderNotarises.length > 0) {
      const orderNotarisCollectionIdentifiers = orderNotarisCollection.map(
        orderNotarisItem => this.getOrderNotarisIdentifier(orderNotarisItem)!,
      );
      const orderNotarisesToAdd = orderNotarises.filter(orderNotarisItem => {
        const orderNotarisIdentifier = this.getOrderNotarisIdentifier(orderNotarisItem);
        if (orderNotarisCollectionIdentifiers.includes(orderNotarisIdentifier)) {
          return false;
        }
        orderNotarisCollectionIdentifiers.push(orderNotarisIdentifier);
        return true;
      });
      return [...orderNotarisesToAdd, ...orderNotarisCollection];
    }
    return orderNotarisCollection;
  }

  protected convertDateFromClient<T extends IOrderNotaris | NewOrderNotaris | PartialUpdateOrderNotaris>(orderNotaris: T): RestOf<T> {
    return {
      ...orderNotaris,
      tanggal: orderNotaris.tanggal?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restOrderNotaris: RestOrderNotaris): IOrderNotaris {
    return {
      ...restOrderNotaris,
      tanggal: restOrderNotaris.tanggal ? dayjs(restOrderNotaris.tanggal) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestOrderNotaris>): HttpResponse<IOrderNotaris> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestOrderNotaris[]>): HttpResponse<IOrderNotaris[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
