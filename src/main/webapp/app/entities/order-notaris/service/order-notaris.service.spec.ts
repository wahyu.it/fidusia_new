import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IOrderNotaris } from '../order-notaris.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../order-notaris.test-samples';

import { OrderNotarisService, RestOrderNotaris } from './order-notaris.service';

const requireRestSample: RestOrderNotaris = {
  ...sampleWithRequiredData,
  tanggal: sampleWithRequiredData.tanggal?.format(DATE_FORMAT),
};

describe('OrderNotaris Service', () => {
  let service: OrderNotarisService;
  let httpMock: HttpTestingController;
  let expectedResult: IOrderNotaris | IOrderNotaris[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(OrderNotarisService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a OrderNotaris', () => {
      const orderNotaris = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(orderNotaris).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a OrderNotaris', () => {
      const orderNotaris = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(orderNotaris).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a OrderNotaris', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of OrderNotaris', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a OrderNotaris', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addOrderNotarisToCollectionIfMissing', () => {
      it('should add a OrderNotaris to an empty array', () => {
        const orderNotaris: IOrderNotaris = sampleWithRequiredData;
        expectedResult = service.addOrderNotarisToCollectionIfMissing([], orderNotaris);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(orderNotaris);
      });

      it('should not add a OrderNotaris to an array that contains it', () => {
        const orderNotaris: IOrderNotaris = sampleWithRequiredData;
        const orderNotarisCollection: IOrderNotaris[] = [
          {
            ...orderNotaris,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addOrderNotarisToCollectionIfMissing(orderNotarisCollection, orderNotaris);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a OrderNotaris to an array that doesn't contain it", () => {
        const orderNotaris: IOrderNotaris = sampleWithRequiredData;
        const orderNotarisCollection: IOrderNotaris[] = [sampleWithPartialData];
        expectedResult = service.addOrderNotarisToCollectionIfMissing(orderNotarisCollection, orderNotaris);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(orderNotaris);
      });

      it('should add only unique OrderNotaris to an array', () => {
        const orderNotarisArray: IOrderNotaris[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const orderNotarisCollection: IOrderNotaris[] = [sampleWithRequiredData];
        expectedResult = service.addOrderNotarisToCollectionIfMissing(orderNotarisCollection, ...orderNotarisArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const orderNotaris: IOrderNotaris = sampleWithRequiredData;
        const orderNotaris2: IOrderNotaris = sampleWithPartialData;
        expectedResult = service.addOrderNotarisToCollectionIfMissing([], orderNotaris, orderNotaris2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(orderNotaris);
        expect(expectedResult).toContain(orderNotaris2);
      });

      it('should accept null and undefined values', () => {
        const orderNotaris: IOrderNotaris = sampleWithRequiredData;
        expectedResult = service.addOrderNotarisToCollectionIfMissing([], null, orderNotaris, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(orderNotaris);
      });

      it('should return initial array if no OrderNotaris is added', () => {
        const orderNotarisCollection: IOrderNotaris[] = [sampleWithRequiredData];
        expectedResult = service.addOrderNotarisToCollectionIfMissing(orderNotarisCollection, undefined, null);
        expect(expectedResult).toEqual(orderNotarisCollection);
      });
    });

    describe('compareOrderNotaris', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareOrderNotaris(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareOrderNotaris(entity1, entity2);
        const compareResult2 = service.compareOrderNotaris(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareOrderNotaris(entity1, entity2);
        const compareResult2 = service.compareOrderNotaris(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareOrderNotaris(entity1, entity2);
        const compareResult2 = service.compareOrderNotaris(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
