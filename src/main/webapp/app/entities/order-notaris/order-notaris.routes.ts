import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { OrderNotarisComponent } from './list/order-notaris.component';
import { OrderNotarisDetailComponent } from './detail/order-notaris-detail.component';
import { OrderNotarisUpdateComponent } from './update/order-notaris-update.component';
import OrderNotarisResolve from './route/order-notaris-routing-resolve.service';

const orderNotarisRoute: Routes = [
  {
    path: '',
    component: OrderNotarisComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrderNotarisDetailComponent,
    resolve: {
      orderNotaris: OrderNotarisResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrderNotarisUpdateComponent,
    resolve: {
      orderNotaris: OrderNotarisResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrderNotarisUpdateComponent,
    resolve: {
      orderNotaris: OrderNotarisResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default orderNotarisRoute;
