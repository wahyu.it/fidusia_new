import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IOrderNotaris, NewOrderNotaris } from '../order-notaris.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IOrderNotaris for edit and NewOrderNotarisFormGroupInput for create.
 */
type OrderNotarisFormGroupInput = IOrderNotaris | PartialWithRequiredKeyOf<NewOrderNotaris>;

type OrderNotarisFormDefaults = Pick<NewOrderNotaris, 'id'>;

type OrderNotarisFormGroupContent = {
  id: FormControl<IOrderNotaris['id'] | NewOrderNotaris['id']>;
  tanggal: FormControl<IOrderNotaris['tanggal']>;
  status: FormControl<IOrderNotaris['status']>;
  orderData: FormControl<IOrderNotaris['orderData']>;
  notaris: FormControl<IOrderNotaris['notaris']>;
  akta: FormControl<IOrderNotaris['akta']>;
  orderKerja: FormControl<IOrderNotaris['orderKerja']>;
};

export type OrderNotarisFormGroup = FormGroup<OrderNotarisFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class OrderNotarisFormService {
  createOrderNotarisFormGroup(orderNotaris: OrderNotarisFormGroupInput = { id: null }): OrderNotarisFormGroup {
    const orderNotarisRawValue = {
      ...this.getFormDefaults(),
      ...orderNotaris,
    };
    return new FormGroup<OrderNotarisFormGroupContent>({
      id: new FormControl(
        { value: orderNotarisRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      tanggal: new FormControl(orderNotarisRawValue.tanggal),
      status: new FormControl(orderNotarisRawValue.status),
      orderData: new FormControl(orderNotarisRawValue.orderData),
      notaris: new FormControl(orderNotarisRawValue.notaris),
      akta: new FormControl(orderNotarisRawValue.akta),
      orderKerja: new FormControl(orderNotarisRawValue.orderKerja),
    });
  }

  getOrderNotaris(form: OrderNotarisFormGroup): IOrderNotaris | NewOrderNotaris {
    return form.getRawValue() as IOrderNotaris | NewOrderNotaris;
  }

  resetForm(form: OrderNotarisFormGroup, orderNotaris: OrderNotarisFormGroupInput): void {
    const orderNotarisRawValue = { ...this.getFormDefaults(), ...orderNotaris };
    form.reset(
      {
        ...orderNotarisRawValue,
        id: { value: orderNotarisRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): OrderNotarisFormDefaults {
    return {
      id: null,
    };
  }
}
