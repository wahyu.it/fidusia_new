import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../order-notaris.test-samples';

import { OrderNotarisFormService } from './order-notaris-form.service';

describe('OrderNotaris Form Service', () => {
  let service: OrderNotarisFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrderNotarisFormService);
  });

  describe('Service methods', () => {
    describe('createOrderNotarisFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createOrderNotarisFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            tanggal: expect.any(Object),
            status: expect.any(Object),
            orderData: expect.any(Object),
            notaris: expect.any(Object),
            akta: expect.any(Object),
            orderKerja: expect.any(Object),
          }),
        );
      });

      it('passing IOrderNotaris should create a new form with FormGroup', () => {
        const formGroup = service.createOrderNotarisFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            tanggal: expect.any(Object),
            status: expect.any(Object),
            orderData: expect.any(Object),
            notaris: expect.any(Object),
            akta: expect.any(Object),
            orderKerja: expect.any(Object),
          }),
        );
      });
    });

    describe('getOrderNotaris', () => {
      it('should return NewOrderNotaris for default OrderNotaris initial value', () => {
        const formGroup = service.createOrderNotarisFormGroup(sampleWithNewData);

        const orderNotaris = service.getOrderNotaris(formGroup) as any;

        expect(orderNotaris).toMatchObject(sampleWithNewData);
      });

      it('should return NewOrderNotaris for empty OrderNotaris initial value', () => {
        const formGroup = service.createOrderNotarisFormGroup();

        const orderNotaris = service.getOrderNotaris(formGroup) as any;

        expect(orderNotaris).toMatchObject({});
      });

      it('should return IOrderNotaris', () => {
        const formGroup = service.createOrderNotarisFormGroup(sampleWithRequiredData);

        const orderNotaris = service.getOrderNotaris(formGroup) as any;

        expect(orderNotaris).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IOrderNotaris should not enable id FormControl', () => {
        const formGroup = service.createOrderNotarisFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewOrderNotaris should disable id FormControl', () => {
        const formGroup = service.createOrderNotarisFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
