import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IOrderData } from 'app/entities/order-data/order-data.model';
import { OrderDataService } from 'app/entities/order-data/service/order-data.service';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { IAkta } from 'app/entities/akta/akta.model';
import { AktaService } from 'app/entities/akta/service/akta.service';
import { IOrderKerja } from 'app/entities/order-kerja/order-kerja.model';
import { OrderKerjaService } from 'app/entities/order-kerja/service/order-kerja.service';
import { IOrderNotaris } from '../order-notaris.model';
import { OrderNotarisService } from '../service/order-notaris.service';
import { OrderNotarisFormService } from './order-notaris-form.service';

import { OrderNotarisUpdateComponent } from './order-notaris-update.component';

describe('OrderNotaris Management Update Component', () => {
  let comp: OrderNotarisUpdateComponent;
  let fixture: ComponentFixture<OrderNotarisUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let orderNotarisFormService: OrderNotarisFormService;
  let orderNotarisService: OrderNotarisService;
  let orderDataService: OrderDataService;
  let notarisService: NotarisService;
  let aktaService: AktaService;
  let orderKerjaService: OrderKerjaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), OrderNotarisUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(OrderNotarisUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OrderNotarisUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    orderNotarisFormService = TestBed.inject(OrderNotarisFormService);
    orderNotarisService = TestBed.inject(OrderNotarisService);
    orderDataService = TestBed.inject(OrderDataService);
    notarisService = TestBed.inject(NotarisService);
    aktaService = TestBed.inject(AktaService);
    orderKerjaService = TestBed.inject(OrderKerjaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call OrderData query and add missing value', () => {
      const orderNotaris: IOrderNotaris = { id: 456 };
      const orderData: IOrderData = { id: 23309 };
      orderNotaris.orderData = orderData;

      const orderDataCollection: IOrderData[] = [{ id: 18600 }];
      jest.spyOn(orderDataService, 'query').mockReturnValue(of(new HttpResponse({ body: orderDataCollection })));
      const additionalOrderData = [orderData];
      const expectedCollection: IOrderData[] = [...additionalOrderData, ...orderDataCollection];
      jest.spyOn(orderDataService, 'addOrderDataToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderNotaris });
      comp.ngOnInit();

      expect(orderDataService.query).toHaveBeenCalled();
      expect(orderDataService.addOrderDataToCollectionIfMissing).toHaveBeenCalledWith(
        orderDataCollection,
        ...additionalOrderData.map(expect.objectContaining),
      );
      expect(comp.orderDataSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Notaris query and add missing value', () => {
      const orderNotaris: IOrderNotaris = { id: 456 };
      const notaris: INotaris = { id: 4254 };
      orderNotaris.notaris = notaris;

      const notarisCollection: INotaris[] = [{ id: 14150 }];
      jest.spyOn(notarisService, 'query').mockReturnValue(of(new HttpResponse({ body: notarisCollection })));
      const additionalNotarises = [notaris];
      const expectedCollection: INotaris[] = [...additionalNotarises, ...notarisCollection];
      jest.spyOn(notarisService, 'addNotarisToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderNotaris });
      comp.ngOnInit();

      expect(notarisService.query).toHaveBeenCalled();
      expect(notarisService.addNotarisToCollectionIfMissing).toHaveBeenCalledWith(
        notarisCollection,
        ...additionalNotarises.map(expect.objectContaining),
      );
      expect(comp.notarisesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Akta query and add missing value', () => {
      const orderNotaris: IOrderNotaris = { id: 456 };
      const akta: IAkta = { id: 28817 };
      orderNotaris.akta = akta;

      const aktaCollection: IAkta[] = [{ id: 22401 }];
      jest.spyOn(aktaService, 'query').mockReturnValue(of(new HttpResponse({ body: aktaCollection })));
      const additionalAktas = [akta];
      const expectedCollection: IAkta[] = [...additionalAktas, ...aktaCollection];
      jest.spyOn(aktaService, 'addAktaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderNotaris });
      comp.ngOnInit();

      expect(aktaService.query).toHaveBeenCalled();
      expect(aktaService.addAktaToCollectionIfMissing).toHaveBeenCalledWith(
        aktaCollection,
        ...additionalAktas.map(expect.objectContaining),
      );
      expect(comp.aktasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call OrderKerja query and add missing value', () => {
      const orderNotaris: IOrderNotaris = { id: 456 };
      const orderKerja: IOrderKerja = { id: 27490 };
      orderNotaris.orderKerja = orderKerja;

      const orderKerjaCollection: IOrderKerja[] = [{ id: 3333 }];
      jest.spyOn(orderKerjaService, 'query').mockReturnValue(of(new HttpResponse({ body: orderKerjaCollection })));
      const additionalOrderKerjas = [orderKerja];
      const expectedCollection: IOrderKerja[] = [...additionalOrderKerjas, ...orderKerjaCollection];
      jest.spyOn(orderKerjaService, 'addOrderKerjaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ orderNotaris });
      comp.ngOnInit();

      expect(orderKerjaService.query).toHaveBeenCalled();
      expect(orderKerjaService.addOrderKerjaToCollectionIfMissing).toHaveBeenCalledWith(
        orderKerjaCollection,
        ...additionalOrderKerjas.map(expect.objectContaining),
      );
      expect(comp.orderKerjasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const orderNotaris: IOrderNotaris = { id: 456 };
      const orderData: IOrderData = { id: 16904 };
      orderNotaris.orderData = orderData;
      const notaris: INotaris = { id: 11355 };
      orderNotaris.notaris = notaris;
      const akta: IAkta = { id: 1320 };
      orderNotaris.akta = akta;
      const orderKerja: IOrderKerja = { id: 31104 };
      orderNotaris.orderKerja = orderKerja;

      activatedRoute.data = of({ orderNotaris });
      comp.ngOnInit();

      expect(comp.orderDataSharedCollection).toContain(orderData);
      expect(comp.notarisesSharedCollection).toContain(notaris);
      expect(comp.aktasSharedCollection).toContain(akta);
      expect(comp.orderKerjasSharedCollection).toContain(orderKerja);
      expect(comp.orderNotaris).toEqual(orderNotaris);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderNotaris>>();
      const orderNotaris = { id: 123 };
      jest.spyOn(orderNotarisFormService, 'getOrderNotaris').mockReturnValue(orderNotaris);
      jest.spyOn(orderNotarisService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderNotaris });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orderNotaris }));
      saveSubject.complete();

      // THEN
      expect(orderNotarisFormService.getOrderNotaris).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(orderNotarisService.update).toHaveBeenCalledWith(expect.objectContaining(orderNotaris));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderNotaris>>();
      const orderNotaris = { id: 123 };
      jest.spyOn(orderNotarisFormService, 'getOrderNotaris').mockReturnValue({ id: null });
      jest.spyOn(orderNotarisService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderNotaris: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orderNotaris }));
      saveSubject.complete();

      // THEN
      expect(orderNotarisFormService.getOrderNotaris).toHaveBeenCalled();
      expect(orderNotarisService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrderNotaris>>();
      const orderNotaris = { id: 123 };
      jest.spyOn(orderNotarisService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orderNotaris });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(orderNotarisService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareOrderData', () => {
      it('Should forward to orderDataService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(orderDataService, 'compareOrderData');
        comp.compareOrderData(entity, entity2);
        expect(orderDataService.compareOrderData).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareNotaris', () => {
      it('Should forward to notarisService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(notarisService, 'compareNotaris');
        comp.compareNotaris(entity, entity2);
        expect(notarisService.compareNotaris).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareAkta', () => {
      it('Should forward to aktaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(aktaService, 'compareAkta');
        comp.compareAkta(entity, entity2);
        expect(aktaService.compareAkta).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareOrderKerja', () => {
      it('Should forward to orderKerjaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(orderKerjaService, 'compareOrderKerja');
        comp.compareOrderKerja(entity, entity2);
        expect(orderKerjaService.compareOrderKerja).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
