import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IOrderData } from 'app/entities/order-data/order-data.model';
import { OrderDataService } from 'app/entities/order-data/service/order-data.service';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { NotarisService } from 'app/entities/notaris/service/notaris.service';
import { IAkta } from 'app/entities/akta/akta.model';
import { AktaService } from 'app/entities/akta/service/akta.service';
import { IOrderKerja } from 'app/entities/order-kerja/order-kerja.model';
import { OrderKerjaService } from 'app/entities/order-kerja/service/order-kerja.service';
import { OrderNotarisService } from '../service/order-notaris.service';
import { IOrderNotaris } from '../order-notaris.model';
import { OrderNotarisFormService, OrderNotarisFormGroup } from './order-notaris-form.service';

@Component({
  standalone: true,
  selector: 'jhi-order-notaris-update',
  templateUrl: './order-notaris-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class OrderNotarisUpdateComponent implements OnInit {
  isSaving = false;
  orderNotaris: IOrderNotaris | null = null;

  orderDataSharedCollection: IOrderData[] = [];
  notarisesSharedCollection: INotaris[] = [];
  aktasSharedCollection: IAkta[] = [];
  orderKerjasSharedCollection: IOrderKerja[] = [];

  editForm: OrderNotarisFormGroup = this.orderNotarisFormService.createOrderNotarisFormGroup();

  constructor(
    protected orderNotarisService: OrderNotarisService,
    protected orderNotarisFormService: OrderNotarisFormService,
    protected orderDataService: OrderDataService,
    protected notarisService: NotarisService,
    protected aktaService: AktaService,
    protected orderKerjaService: OrderKerjaService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  compareOrderData = (o1: IOrderData | null, o2: IOrderData | null): boolean => this.orderDataService.compareOrderData(o1, o2);

  compareNotaris = (o1: INotaris | null, o2: INotaris | null): boolean => this.notarisService.compareNotaris(o1, o2);

  compareAkta = (o1: IAkta | null, o2: IAkta | null): boolean => this.aktaService.compareAkta(o1, o2);

  compareOrderKerja = (o1: IOrderKerja | null, o2: IOrderKerja | null): boolean => this.orderKerjaService.compareOrderKerja(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderNotaris }) => {
      this.orderNotaris = orderNotaris;
      if (orderNotaris) {
        this.updateForm(orderNotaris);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderNotaris = this.orderNotarisFormService.getOrderNotaris(this.editForm);
    if (orderNotaris.id !== null) {
      this.subscribeToSaveResponse(this.orderNotarisService.update(orderNotaris));
    } else {
      this.subscribeToSaveResponse(this.orderNotarisService.create(orderNotaris));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderNotaris>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(orderNotaris: IOrderNotaris): void {
    this.orderNotaris = orderNotaris;
    this.orderNotarisFormService.resetForm(this.editForm, orderNotaris);

    this.orderDataSharedCollection = this.orderDataService.addOrderDataToCollectionIfMissing<IOrderData>(
      this.orderDataSharedCollection,
      orderNotaris.orderData,
    );
    this.notarisesSharedCollection = this.notarisService.addNotarisToCollectionIfMissing<INotaris>(
      this.notarisesSharedCollection,
      orderNotaris.notaris,
    );
    this.aktasSharedCollection = this.aktaService.addAktaToCollectionIfMissing<IAkta>(this.aktasSharedCollection, orderNotaris.akta);
    this.orderKerjasSharedCollection = this.orderKerjaService.addOrderKerjaToCollectionIfMissing<IOrderKerja>(
      this.orderKerjasSharedCollection,
      orderNotaris.orderKerja,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.orderDataService
      .query()
      .pipe(map((res: HttpResponse<IOrderData[]>) => res.body ?? []))
      .pipe(
        map((orderData: IOrderData[]) =>
          this.orderDataService.addOrderDataToCollectionIfMissing<IOrderData>(orderData, this.orderNotaris?.orderData),
        ),
      )
      .subscribe((orderData: IOrderData[]) => (this.orderDataSharedCollection = orderData));

    this.notarisService
      .query()
      .pipe(map((res: HttpResponse<INotaris[]>) => res.body ?? []))
      .pipe(
        map((notarises: INotaris[]) =>
          this.notarisService.addNotarisToCollectionIfMissing<INotaris>(notarises, this.orderNotaris?.notaris),
        ),
      )
      .subscribe((notarises: INotaris[]) => (this.notarisesSharedCollection = notarises));

    this.aktaService
      .query()
      .pipe(map((res: HttpResponse<IAkta[]>) => res.body ?? []))
      .pipe(map((aktas: IAkta[]) => this.aktaService.addAktaToCollectionIfMissing<IAkta>(aktas, this.orderNotaris?.akta)))
      .subscribe((aktas: IAkta[]) => (this.aktasSharedCollection = aktas));

    this.orderKerjaService
      .query()
      .pipe(map((res: HttpResponse<IOrderKerja[]>) => res.body ?? []))
      .pipe(
        map((orderKerjas: IOrderKerja[]) =>
          this.orderKerjaService.addOrderKerjaToCollectionIfMissing<IOrderKerja>(orderKerjas, this.orderNotaris?.orderKerja),
        ),
      )
      .subscribe((orderKerjas: IOrderKerja[]) => (this.orderKerjasSharedCollection = orderKerjas));
  }
}
