import dayjs from 'dayjs/esm';
import { IOrderData } from 'app/entities/order-data/order-data.model';
import { INotaris } from 'app/entities/notaris/notaris.model';
import { IAkta } from 'app/entities/akta/akta.model';
import { IOrderKerja } from 'app/entities/order-kerja/order-kerja.model';

export interface IOrderNotaris {
  id: number;
  tanggal?: dayjs.Dayjs | null;
  status?: number | null;
  orderData?: IOrderData | null;
  notaris?: INotaris | null;
  akta?: IAkta | null;
  orderKerja?: IOrderKerja | null;
}

export type NewOrderNotaris = Omit<IOrderNotaris, 'id'> & { id: null };
