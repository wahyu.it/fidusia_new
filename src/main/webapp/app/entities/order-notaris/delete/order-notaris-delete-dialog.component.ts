import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IOrderNotaris } from '../order-notaris.model';
import { OrderNotarisService } from '../service/order-notaris.service';

@Component({
  standalone: true,
  templateUrl: './order-notaris-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class OrderNotarisDeleteDialogComponent {
  orderNotaris?: IOrderNotaris;

  constructor(
    protected orderNotarisService: OrderNotarisService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orderNotarisService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
