import dayjs from 'dayjs/esm';

import { IOrderNotaris, NewOrderNotaris } from './order-notaris.model';

export const sampleWithRequiredData: IOrderNotaris = {
  id: 24974,
};

export const sampleWithPartialData: IOrderNotaris = {
  id: 19732,
};

export const sampleWithFullData: IOrderNotaris = {
  id: 22526,
  tanggal: dayjs('2023-12-14'),
  status: 9129,
};

export const sampleWithNewData: NewOrderNotaris = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
