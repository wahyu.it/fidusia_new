import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOrderNotaris } from '../order-notaris.model';
import { OrderNotarisService } from '../service/order-notaris.service';

export const orderNotarisResolve = (route: ActivatedRouteSnapshot): Observable<null | IOrderNotaris> => {
  const id = route.params['id'];
  if (id) {
    return inject(OrderNotarisService)
      .find(id)
      .pipe(
        mergeMap((orderNotaris: HttpResponse<IOrderNotaris>) => {
          if (orderNotaris.body) {
            return of(orderNotaris.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default orderNotarisResolve;
