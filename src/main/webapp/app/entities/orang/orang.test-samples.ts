import dayjs from 'dayjs/esm';

import { IOrang, NewOrang } from './orang.model';

export const sampleWithRequiredData: IOrang = {
  id: 10710,
};

export const sampleWithPartialData: IOrang = {
  id: 7526,
  penggunaan: 'geez jubilant blockade',
  nama: 'in boohoo',
  kelahiran: 'mmm likewise',
  tglLahir: dayjs('2023-12-14'),
  wargaNegara: 'carter',
  jenisId: 'blah without whoa',
  noId: 'framework hostel',
  rw: 'eek',
  kecamatan: 'kiddingly',
  kodePos: 'oof',
  noKontak: 'gastronomy',
};

export const sampleWithFullData: IOrang = {
  id: 6710,
  penggunaan: 'whenever',
  nama: 'door philosophise',
  jenisKelamin: 'innocently whoever mmm',
  statusKawin: 'disrespect boldly',
  kelahiran: 'lock a while',
  tglLahir: dayjs('2023-12-14'),
  pekerjaan: 'whenever',
  wargaNegara: 'cashier',
  jenisId: 'consequently class',
  noId: 'deserted than',
  alamat: 'angry likewise off',
  rt: 'fooey assail',
  rw: 'fussy where',
  kelurahan: 'aside ugly upright',
  kecamatan: 'sermonize',
  kota: 'greatly',
  provinsi: 'far',
  kodePos: 'agitated gadzooks pitiful',
  noKontak: 'afore however',
};

export const sampleWithNewData: NewOrang = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
