import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { OrangDetailComponent } from './orang-detail.component';

describe('Orang Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OrangDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: OrangDetailComponent,
              resolve: { orang: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(OrangDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load orang on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', OrangDetailComponent);

      // THEN
      expect(instance.orang).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
