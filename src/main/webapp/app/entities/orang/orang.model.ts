import dayjs from 'dayjs/esm';
import { IPpk } from 'app/entities/ppk/ppk.model';

export interface IOrang {
  id: number;
  penggunaan?: string | null;
  nama?: string | null;
  jenisKelamin?: string | null;
  statusKawin?: string | null;
  kelahiran?: string | null;
  tglLahir?: dayjs.Dayjs | null;
  pekerjaan?: string | null;
  wargaNegara?: string | null;
  jenisId?: string | null;
  noId?: string | null;
  alamat?: string | null;
  rt?: string | null;
  rw?: string | null;
  kelurahan?: string | null;
  kecamatan?: string | null;
  kota?: string | null;
  provinsi?: string | null;
  kodePos?: string | null;
  noKontak?: string | null;
  ppk?: IPpk | null;
}

export type NewOrang = Omit<IOrang, 'id'> & { id: null };
