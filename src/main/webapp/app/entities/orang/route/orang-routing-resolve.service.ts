import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOrang } from '../orang.model';
import { OrangService } from '../service/orang.service';

export const orangResolve = (route: ActivatedRouteSnapshot): Observable<null | IOrang> => {
  const id = route.params['id'];
  if (id) {
    return inject(OrangService)
      .find(id)
      .pipe(
        mergeMap((orang: HttpResponse<IOrang>) => {
          if (orang.body) {
            return of(orang.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default orangResolve;
