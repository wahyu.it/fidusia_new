import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOrang, NewOrang } from '../orang.model';

export type PartialUpdateOrang = Partial<IOrang> & Pick<IOrang, 'id'>;

type RestOf<T extends IOrang | NewOrang> = Omit<T, 'tglLahir'> & {
  tglLahir?: string | null;
};

export type RestOrang = RestOf<IOrang>;

export type NewRestOrang = RestOf<NewOrang>;

export type PartialUpdateRestOrang = RestOf<PartialUpdateOrang>;

export type EntityResponseType = HttpResponse<IOrang>;
export type EntityArrayResponseType = HttpResponse<IOrang[]>;

@Injectable({ providedIn: 'root' })
export class OrangService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/orangs');

  constructor(
    protected http: HttpClient,
    protected applicationConfigService: ApplicationConfigService,
  ) {}

  create(orang: NewOrang): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orang);
    return this.http.post<RestOrang>(this.resourceUrl, copy, { observe: 'response' }).pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(orang: IOrang): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orang);
    return this.http
      .put<RestOrang>(`${this.resourceUrl}/${this.getOrangIdentifier(orang)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(orang: PartialUpdateOrang): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orang);
    return this.http
      .patch<RestOrang>(`${this.resourceUrl}/${this.getOrangIdentifier(orang)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestOrang>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestOrang[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getOrangIdentifier(orang: Pick<IOrang, 'id'>): number {
    return orang.id;
  }

  compareOrang(o1: Pick<IOrang, 'id'> | null, o2: Pick<IOrang, 'id'> | null): boolean {
    return o1 && o2 ? this.getOrangIdentifier(o1) === this.getOrangIdentifier(o2) : o1 === o2;
  }

  addOrangToCollectionIfMissing<Type extends Pick<IOrang, 'id'>>(
    orangCollection: Type[],
    ...orangsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const orangs: Type[] = orangsToCheck.filter(isPresent);
    if (orangs.length > 0) {
      const orangCollectionIdentifiers = orangCollection.map(orangItem => this.getOrangIdentifier(orangItem)!);
      const orangsToAdd = orangs.filter(orangItem => {
        const orangIdentifier = this.getOrangIdentifier(orangItem);
        if (orangCollectionIdentifiers.includes(orangIdentifier)) {
          return false;
        }
        orangCollectionIdentifiers.push(orangIdentifier);
        return true;
      });
      return [...orangsToAdd, ...orangCollection];
    }
    return orangCollection;
  }

  protected convertDateFromClient<T extends IOrang | NewOrang | PartialUpdateOrang>(orang: T): RestOf<T> {
    return {
      ...orang,
      tglLahir: orang.tglLahir?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restOrang: RestOrang): IOrang {
    return {
      ...restOrang,
      tglLahir: restOrang.tglLahir ? dayjs(restOrang.tglLahir) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestOrang>): HttpResponse<IOrang> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestOrang[]>): HttpResponse<IOrang[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
