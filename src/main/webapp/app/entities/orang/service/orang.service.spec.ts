import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IOrang } from '../orang.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../orang.test-samples';

import { OrangService, RestOrang } from './orang.service';

const requireRestSample: RestOrang = {
  ...sampleWithRequiredData,
  tglLahir: sampleWithRequiredData.tglLahir?.format(DATE_FORMAT),
};

describe('Orang Service', () => {
  let service: OrangService;
  let httpMock: HttpTestingController;
  let expectedResult: IOrang | IOrang[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(OrangService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Orang', () => {
      const orang = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(orang).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Orang', () => {
      const orang = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(orang).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Orang', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Orang', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Orang', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addOrangToCollectionIfMissing', () => {
      it('should add a Orang to an empty array', () => {
        const orang: IOrang = sampleWithRequiredData;
        expectedResult = service.addOrangToCollectionIfMissing([], orang);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(orang);
      });

      it('should not add a Orang to an array that contains it', () => {
        const orang: IOrang = sampleWithRequiredData;
        const orangCollection: IOrang[] = [
          {
            ...orang,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addOrangToCollectionIfMissing(orangCollection, orang);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Orang to an array that doesn't contain it", () => {
        const orang: IOrang = sampleWithRequiredData;
        const orangCollection: IOrang[] = [sampleWithPartialData];
        expectedResult = service.addOrangToCollectionIfMissing(orangCollection, orang);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(orang);
      });

      it('should add only unique Orang to an array', () => {
        const orangArray: IOrang[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const orangCollection: IOrang[] = [sampleWithRequiredData];
        expectedResult = service.addOrangToCollectionIfMissing(orangCollection, ...orangArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const orang: IOrang = sampleWithRequiredData;
        const orang2: IOrang = sampleWithPartialData;
        expectedResult = service.addOrangToCollectionIfMissing([], orang, orang2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(orang);
        expect(expectedResult).toContain(orang2);
      });

      it('should accept null and undefined values', () => {
        const orang: IOrang = sampleWithRequiredData;
        expectedResult = service.addOrangToCollectionIfMissing([], null, orang, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(orang);
      });

      it('should return initial array if no Orang is added', () => {
        const orangCollection: IOrang[] = [sampleWithRequiredData];
        expectedResult = service.addOrangToCollectionIfMissing(orangCollection, undefined, null);
        expect(expectedResult).toEqual(orangCollection);
      });
    });

    describe('compareOrang', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareOrang(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareOrang(entity1, entity2);
        const compareResult2 = service.compareOrang(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareOrang(entity1, entity2);
        const compareResult2 = service.compareOrang(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareOrang(entity1, entity2);
        const compareResult2 = service.compareOrang(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
