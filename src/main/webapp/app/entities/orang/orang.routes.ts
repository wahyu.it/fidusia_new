import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { OrangComponent } from './list/orang.component';
import { OrangDetailComponent } from './detail/orang-detail.component';
import { OrangUpdateComponent } from './update/orang-update.component';
import OrangResolve from './route/orang-routing-resolve.service';

const orangRoute: Routes = [
  {
    path: '',
    component: OrangComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrangDetailComponent,
    resolve: {
      orang: OrangResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrangUpdateComponent,
    resolve: {
      orang: OrangResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrangUpdateComponent,
    resolve: {
      orang: OrangResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default orangRoute;
