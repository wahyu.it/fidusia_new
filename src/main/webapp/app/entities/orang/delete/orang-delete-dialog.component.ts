import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IOrang } from '../orang.model';
import { OrangService } from '../service/orang.service';

@Component({
  standalone: true,
  templateUrl: './orang-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class OrangDeleteDialogComponent {
  orang?: IOrang;

  constructor(
    protected orangService: OrangService,
    protected activeModal: NgbActiveModal,
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orangService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
