import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../orang.test-samples';

import { OrangFormService } from './orang-form.service';

describe('Orang Form Service', () => {
  let service: OrangFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrangFormService);
  });

  describe('Service methods', () => {
    describe('createOrangFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createOrangFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            penggunaan: expect.any(Object),
            nama: expect.any(Object),
            jenisKelamin: expect.any(Object),
            statusKawin: expect.any(Object),
            kelahiran: expect.any(Object),
            tglLahir: expect.any(Object),
            pekerjaan: expect.any(Object),
            wargaNegara: expect.any(Object),
            jenisId: expect.any(Object),
            noId: expect.any(Object),
            alamat: expect.any(Object),
            rt: expect.any(Object),
            rw: expect.any(Object),
            kelurahan: expect.any(Object),
            kecamatan: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            kodePos: expect.any(Object),
            noKontak: expect.any(Object),
          }),
        );
      });

      it('passing IOrang should create a new form with FormGroup', () => {
        const formGroup = service.createOrangFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            penggunaan: expect.any(Object),
            nama: expect.any(Object),
            jenisKelamin: expect.any(Object),
            statusKawin: expect.any(Object),
            kelahiran: expect.any(Object),
            tglLahir: expect.any(Object),
            pekerjaan: expect.any(Object),
            wargaNegara: expect.any(Object),
            jenisId: expect.any(Object),
            noId: expect.any(Object),
            alamat: expect.any(Object),
            rt: expect.any(Object),
            rw: expect.any(Object),
            kelurahan: expect.any(Object),
            kecamatan: expect.any(Object),
            kota: expect.any(Object),
            provinsi: expect.any(Object),
            kodePos: expect.any(Object),
            noKontak: expect.any(Object),
          }),
        );
      });
    });

    describe('getOrang', () => {
      it('should return NewOrang for default Orang initial value', () => {
        const formGroup = service.createOrangFormGroup(sampleWithNewData);

        const orang = service.getOrang(formGroup) as any;

        expect(orang).toMatchObject(sampleWithNewData);
      });

      it('should return NewOrang for empty Orang initial value', () => {
        const formGroup = service.createOrangFormGroup();

        const orang = service.getOrang(formGroup) as any;

        expect(orang).toMatchObject({});
      });

      it('should return IOrang', () => {
        const formGroup = service.createOrangFormGroup(sampleWithRequiredData);

        const orang = service.getOrang(formGroup) as any;

        expect(orang).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IOrang should not enable id FormControl', () => {
        const formGroup = service.createOrangFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewOrang should disable id FormControl', () => {
        const formGroup = service.createOrangFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
