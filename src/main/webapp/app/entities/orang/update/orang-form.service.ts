import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IOrang, NewOrang } from '../orang.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IOrang for edit and NewOrangFormGroupInput for create.
 */
type OrangFormGroupInput = IOrang | PartialWithRequiredKeyOf<NewOrang>;

type OrangFormDefaults = Pick<NewOrang, 'id'>;

type OrangFormGroupContent = {
  id: FormControl<IOrang['id'] | NewOrang['id']>;
  penggunaan: FormControl<IOrang['penggunaan']>;
  nama: FormControl<IOrang['nama']>;
  jenisKelamin: FormControl<IOrang['jenisKelamin']>;
  statusKawin: FormControl<IOrang['statusKawin']>;
  kelahiran: FormControl<IOrang['kelahiran']>;
  tglLahir: FormControl<IOrang['tglLahir']>;
  pekerjaan: FormControl<IOrang['pekerjaan']>;
  wargaNegara: FormControl<IOrang['wargaNegara']>;
  jenisId: FormControl<IOrang['jenisId']>;
  noId: FormControl<IOrang['noId']>;
  alamat: FormControl<IOrang['alamat']>;
  rt: FormControl<IOrang['rt']>;
  rw: FormControl<IOrang['rw']>;
  kelurahan: FormControl<IOrang['kelurahan']>;
  kecamatan: FormControl<IOrang['kecamatan']>;
  kota: FormControl<IOrang['kota']>;
  provinsi: FormControl<IOrang['provinsi']>;
  kodePos: FormControl<IOrang['kodePos']>;
  noKontak: FormControl<IOrang['noKontak']>;
};

export type OrangFormGroup = FormGroup<OrangFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class OrangFormService {
  createOrangFormGroup(orang: OrangFormGroupInput = { id: null }): OrangFormGroup {
    const orangRawValue = {
      ...this.getFormDefaults(),
      ...orang,
    };
    return new FormGroup<OrangFormGroupContent>({
      id: new FormControl(
        { value: orangRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      penggunaan: new FormControl(orangRawValue.penggunaan),
      nama: new FormControl(orangRawValue.nama),
      jenisKelamin: new FormControl(orangRawValue.jenisKelamin),
      statusKawin: new FormControl(orangRawValue.statusKawin),
      kelahiran: new FormControl(orangRawValue.kelahiran),
      tglLahir: new FormControl(orangRawValue.tglLahir),
      pekerjaan: new FormControl(orangRawValue.pekerjaan),
      wargaNegara: new FormControl(orangRawValue.wargaNegara),
      jenisId: new FormControl(orangRawValue.jenisId),
      noId: new FormControl(orangRawValue.noId),
      alamat: new FormControl(orangRawValue.alamat),
      rt: new FormControl(orangRawValue.rt),
      rw: new FormControl(orangRawValue.rw),
      kelurahan: new FormControl(orangRawValue.kelurahan),
      kecamatan: new FormControl(orangRawValue.kecamatan),
      kota: new FormControl(orangRawValue.kota),
      provinsi: new FormControl(orangRawValue.provinsi),
      kodePos: new FormControl(orangRawValue.kodePos),
      noKontak: new FormControl(orangRawValue.noKontak),
    });
  }

  getOrang(form: OrangFormGroup): IOrang | NewOrang {
    return form.getRawValue() as IOrang | NewOrang;
  }

  resetForm(form: OrangFormGroup, orang: OrangFormGroupInput): void {
    const orangRawValue = { ...this.getFormDefaults(), ...orang };
    form.reset(
      {
        ...orangRawValue,
        id: { value: orangRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): OrangFormDefaults {
    return {
      id: null,
    };
  }
}
