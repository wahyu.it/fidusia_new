import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { OrangService } from '../service/orang.service';
import { IOrang } from '../orang.model';
import { OrangFormService } from './orang-form.service';

import { OrangUpdateComponent } from './orang-update.component';

describe('Orang Management Update Component', () => {
  let comp: OrangUpdateComponent;
  let fixture: ComponentFixture<OrangUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let orangFormService: OrangFormService;
  let orangService: OrangService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([]), OrangUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(OrangUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OrangUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    orangFormService = TestBed.inject(OrangFormService);
    orangService = TestBed.inject(OrangService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const orang: IOrang = { id: 456 };

      activatedRoute.data = of({ orang });
      comp.ngOnInit();

      expect(comp.orang).toEqual(orang);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrang>>();
      const orang = { id: 123 };
      jest.spyOn(orangFormService, 'getOrang').mockReturnValue(orang);
      jest.spyOn(orangService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orang });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orang }));
      saveSubject.complete();

      // THEN
      expect(orangFormService.getOrang).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(orangService.update).toHaveBeenCalledWith(expect.objectContaining(orang));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrang>>();
      const orang = { id: 123 };
      jest.spyOn(orangFormService, 'getOrang').mockReturnValue({ id: null });
      jest.spyOn(orangService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orang: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: orang }));
      saveSubject.complete();

      // THEN
      expect(orangFormService.getOrang).toHaveBeenCalled();
      expect(orangService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrang>>();
      const orang = { id: 123 };
      jest.spyOn(orangService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ orang });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(orangService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
