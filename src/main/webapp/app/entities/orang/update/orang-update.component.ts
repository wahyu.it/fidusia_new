import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IOrang } from '../orang.model';
import { OrangService } from '../service/orang.service';
import { OrangFormService, OrangFormGroup } from './orang-form.service';

@Component({
  standalone: true,
  selector: 'jhi-orang-update',
  templateUrl: './orang-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class OrangUpdateComponent implements OnInit {
  isSaving = false;
  orang: IOrang | null = null;

  editForm: OrangFormGroup = this.orangFormService.createOrangFormGroup();

  constructor(
    protected orangService: OrangService,
    protected orangFormService: OrangFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orang }) => {
      this.orang = orang;
      if (orang) {
        this.updateForm(orang);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orang = this.orangFormService.getOrang(this.editForm);
    if (orang.id !== null) {
      this.subscribeToSaveResponse(this.orangService.update(orang));
    } else {
      this.subscribeToSaveResponse(this.orangService.create(orang));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrang>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(orang: IOrang): void {
    this.orang = orang;
    this.orangFormService.resetForm(this.editForm, orang);
  }
}
