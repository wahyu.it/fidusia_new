import { Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'raw-data',
    data: { pageTitle: 'RawData' },
    loadChildren: () => import('./raw-data/raw-data.routes'),
  },
  {
    path: 'order-data',
    data: { pageTitle: 'OrderData' },
    loadChildren: () => import('./order-data/order-data.routes'),
  },
  {
    path: 'berkas',
    data: { pageTitle: 'Berkas' },
    loadChildren: () => import('./berkas/berkas.routes'),
  },
  {
    path: 'remark',
    data: { pageTitle: 'Remarks' },
    loadChildren: () => import('./remark/remark.routes'),
  },
  {
    path: 'sk-subtitusi',
    data: { pageTitle: 'SkSubtitusis' },
    loadChildren: () => import('./sk-subtitusi/sk-subtitusi.routes'),
  },
  {
    path: 'cabang',
    data: { pageTitle: 'Cabangs' },
    loadChildren: () => import('./cabang/cabang.routes'),
  },
  {
    path: 'leasing',
    data: { pageTitle: 'Leasings' },
    loadChildren: () => import('./leasing/leasing.routes'),
  },
  {
    path: 'order-notaris',
    data: { pageTitle: 'OrderNotarises' },
    loadChildren: () => import('./order-notaris/order-notaris.routes'),
  },
  {
    path: 'notaris',
    data: { pageTitle: 'Notarises' },
    loadChildren: () => import('./notaris/notaris.routes'),
  },
  {
    path: 'order-kerja',
    data: { pageTitle: 'OrderKerjas' },
    loadChildren: () => import('./order-kerja/order-kerja.routes'),
  },
  {
    path: 'ppk',
    data: { pageTitle: 'Ppks' },
    loadChildren: () => import('./ppk/ppk.routes'),
  },
  {
    path: 'orang',
    data: { pageTitle: 'Orangs' },
    loadChildren: () => import('./orang/orang.routes'),
  },
  {
    path: 'badan',
    data: { pageTitle: 'Badans' },
    loadChildren: () => import('./badan/badan.routes'),
  },
  {
    path: 'kendaraan',
    data: { pageTitle: 'Kendaraans' },
    loadChildren: () => import('./kendaraan/kendaraan.routes'),
  },
  {
    path: 'akta',
    data: { pageTitle: 'Aktas' },
    loadChildren: () => import('./akta/akta.routes'),
  },
  {
    path: 'penerima-kuasa',
    data: { pageTitle: 'PenerimaKuasas' },
    loadChildren: () => import('./penerima-kuasa/penerima-kuasa.routes'),
  },
  {
    path: 'saksi',
    data: { pageTitle: 'Saksis' },
    loadChildren: () => import('./saksi/saksi.routes'),
  },
  {
    path: 'sertifikat',
    data: { pageTitle: 'Sertifikats' },
    loadChildren: () => import('./sertifikat/sertifikat.routes'),
  },
  {
    path: 'invoice',
    data: { pageTitle: 'Invoices' },
    loadChildren: () => import('./invoice/invoice.routes'),
  },
  /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];

export default routes;
