package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Saksi;
import com.project.fidusia.repository.SaksiRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SaksiResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SaksiResourceIT {

    private static final String DEFAULT_TITEL = "AAAAAAAAAA";
    private static final String UPDATED_TITEL = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_KOMPARISI = "AAAAAAAAAA";
    private static final String UPDATED_KOMPARISI = "BBBBBBBBBB";

    private static final String DEFAULT_TTS = "AAAAAAAAAA";
    private static final String UPDATED_TTS = "BBBBBBBBBB";

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/saksis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SaksiRepository saksiRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSaksiMockMvc;

    private Saksi saksi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Saksi createEntity(EntityManager em) {
        Saksi saksi = new Saksi()
            .titel(DEFAULT_TITEL)
            .nama(DEFAULT_NAMA)
            .komparisi(DEFAULT_KOMPARISI)
            .tts(DEFAULT_TTS)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON);
        return saksi;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Saksi createUpdatedEntity(EntityManager em) {
        Saksi saksi = new Saksi()
            .titel(UPDATED_TITEL)
            .nama(UPDATED_NAMA)
            .komparisi(UPDATED_KOMPARISI)
            .tts(UPDATED_TTS)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);
        return saksi;
    }

    @BeforeEach
    public void initTest() {
        saksi = createEntity(em);
    }

    @Test
    @Transactional
    void createSaksi() throws Exception {
        int databaseSizeBeforeCreate = saksiRepository.findAll().size();
        // Create the Saksi
        restSaksiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(saksi)))
            .andExpect(status().isCreated());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeCreate + 1);
        Saksi testSaksi = saksiList.get(saksiList.size() - 1);
        assertThat(testSaksi.getTitel()).isEqualTo(DEFAULT_TITEL);
        assertThat(testSaksi.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testSaksi.getKomparisi()).isEqualTo(DEFAULT_KOMPARISI);
        assertThat(testSaksi.getTts()).isEqualTo(DEFAULT_TTS);
        assertThat(testSaksi.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testSaksi.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testSaksi.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    void createSaksiWithExistingId() throws Exception {
        // Create the Saksi with an existing ID
        saksi.setId(1L);

        int databaseSizeBeforeCreate = saksiRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaksiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(saksi)))
            .andExpect(status().isBadRequest());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSaksis() throws Exception {
        // Initialize the database
        saksiRepository.saveAndFlush(saksi);

        // Get all the saksiList
        restSaksiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saksi.getId().intValue())))
            .andExpect(jsonPath("$.[*].titel").value(hasItem(DEFAULT_TITEL)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].komparisi").value(hasItem(DEFAULT_KOMPARISI)))
            .andExpect(jsonPath("$.[*].tts").value(hasItem(DEFAULT_TTS)))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))));
    }

    @Test
    @Transactional
    void getSaksi() throws Exception {
        // Initialize the database
        saksiRepository.saveAndFlush(saksi);

        // Get the saksi
        restSaksiMockMvc
            .perform(get(ENTITY_API_URL_ID, saksi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(saksi.getId().intValue()))
            .andExpect(jsonPath("$.titel").value(DEFAULT_TITEL))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.komparisi").value(DEFAULT_KOMPARISI))
            .andExpect(jsonPath("$.tts").value(DEFAULT_TTS))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)));
    }

    @Test
    @Transactional
    void getNonExistingSaksi() throws Exception {
        // Get the saksi
        restSaksiMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSaksi() throws Exception {
        // Initialize the database
        saksiRepository.saveAndFlush(saksi);

        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();

        // Update the saksi
        Saksi updatedSaksi = saksiRepository.findById(saksi.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedSaksi are not directly saved in db
        em.detach(updatedSaksi);
        updatedSaksi
            .titel(UPDATED_TITEL)
            .nama(UPDATED_NAMA)
            .komparisi(UPDATED_KOMPARISI)
            .tts(UPDATED_TTS)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restSaksiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSaksi.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSaksi))
            )
            .andExpect(status().isOk());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
        Saksi testSaksi = saksiList.get(saksiList.size() - 1);
        assertThat(testSaksi.getTitel()).isEqualTo(UPDATED_TITEL);
        assertThat(testSaksi.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testSaksi.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
        assertThat(testSaksi.getTts()).isEqualTo(UPDATED_TTS);
        assertThat(testSaksi.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testSaksi.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testSaksi.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void putNonExistingSaksi() throws Exception {
        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();
        saksi.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSaksiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, saksi.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(saksi))
            )
            .andExpect(status().isBadRequest());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSaksi() throws Exception {
        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();
        saksi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSaksiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(saksi))
            )
            .andExpect(status().isBadRequest());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSaksi() throws Exception {
        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();
        saksi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSaksiMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(saksi)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSaksiWithPatch() throws Exception {
        // Initialize the database
        saksiRepository.saveAndFlush(saksi);

        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();

        // Update the saksi using partial update
        Saksi partialUpdatedSaksi = new Saksi();
        partialUpdatedSaksi.setId(saksi.getId());

        partialUpdatedSaksi.titel(UPDATED_TITEL).nama(UPDATED_NAMA).komparisi(UPDATED_KOMPARISI);

        restSaksiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSaksi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSaksi))
            )
            .andExpect(status().isOk());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
        Saksi testSaksi = saksiList.get(saksiList.size() - 1);
        assertThat(testSaksi.getTitel()).isEqualTo(UPDATED_TITEL);
        assertThat(testSaksi.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testSaksi.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
        assertThat(testSaksi.getTts()).isEqualTo(DEFAULT_TTS);
        assertThat(testSaksi.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testSaksi.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testSaksi.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    void fullUpdateSaksiWithPatch() throws Exception {
        // Initialize the database
        saksiRepository.saveAndFlush(saksi);

        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();

        // Update the saksi using partial update
        Saksi partialUpdatedSaksi = new Saksi();
        partialUpdatedSaksi.setId(saksi.getId());

        partialUpdatedSaksi
            .titel(UPDATED_TITEL)
            .nama(UPDATED_NAMA)
            .komparisi(UPDATED_KOMPARISI)
            .tts(UPDATED_TTS)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restSaksiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSaksi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSaksi))
            )
            .andExpect(status().isOk());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
        Saksi testSaksi = saksiList.get(saksiList.size() - 1);
        assertThat(testSaksi.getTitel()).isEqualTo(UPDATED_TITEL);
        assertThat(testSaksi.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testSaksi.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
        assertThat(testSaksi.getTts()).isEqualTo(UPDATED_TTS);
        assertThat(testSaksi.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testSaksi.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testSaksi.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void patchNonExistingSaksi() throws Exception {
        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();
        saksi.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSaksiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, saksi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(saksi))
            )
            .andExpect(status().isBadRequest());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSaksi() throws Exception {
        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();
        saksi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSaksiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(saksi))
            )
            .andExpect(status().isBadRequest());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSaksi() throws Exception {
        int databaseSizeBeforeUpdate = saksiRepository.findAll().size();
        saksi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSaksiMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(saksi)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Saksi in the database
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSaksi() throws Exception {
        // Initialize the database
        saksiRepository.saveAndFlush(saksi);

        int databaseSizeBeforeDelete = saksiRepository.findAll().size();

        // Delete the saksi
        restSaksiMockMvc
            .perform(delete(ENTITY_API_URL_ID, saksi.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Saksi> saksiList = saksiRepository.findAll();
        assertThat(saksiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
