package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Notaris;
import com.project.fidusia.repository.NotarisRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NotarisResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class NotarisResourceIT {

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_SHORT = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_SHORT = "BBBBBBBBBB";

    private static final String DEFAULT_TITEL_SHORT_ONE = "AAAAAAAAAA";
    private static final String UPDATED_TITEL_SHORT_ONE = "BBBBBBBBBB";

    private static final String DEFAULT_TITEL_SHORT_TWO = "AAAAAAAAAA";
    private static final String UPDATED_TITEL_SHORT_TWO = "BBBBBBBBBB";

    private static final String DEFAULT_TITEL_LONG_ONE = "AAAAAAAAAA";
    private static final String UPDATED_TITEL_LONG_ONE = "BBBBBBBBBB";

    private static final String DEFAULT_TITEL_LONG_TWO = "AAAAAAAAAA";
    private static final String UPDATED_TITEL_LONG_TWO = "BBBBBBBBBB";

    private static final String DEFAULT_WILAYAH_KERJA = "AAAAAAAAAA";
    private static final String UPDATED_WILAYAH_KERJA = "BBBBBBBBBB";

    private static final String DEFAULT_KEDUDUKAN = "AAAAAAAAAA";
    private static final String UPDATED_KEDUDUKAN = "BBBBBBBBBB";

    private static final String DEFAULT_SK = "AAAAAAAAAA";
    private static final String UPDATED_SK = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TGL_SK = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_SK = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_JENIS_HARI_KERJA = 1;
    private static final Integer UPDATED_JENIS_HARI_KERJA = 2;

    private static final ZonedDateTime DEFAULT_JAM_KERJA_AWAL_ONE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_KERJA_AWAL_ONE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_JAM_KERJA_AKHIR_ONE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_KERJA_AKHIR_ONE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_JAM_KERJA_AWAL_TWO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_KERJA_AWAL_TWO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_JAM_KERJA_AKHIR_TWO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_KERJA_AKHIR_TWO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_SELISIH_PUKUL = 1;
    private static final Integer UPDATED_SELISIH_PUKUL = 2;

    private static final ZonedDateTime DEFAULT_JAM_REST_AWAL_ONE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_REST_AWAL_ONE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_JAM_REST_AKHIR_ONE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_REST_AKHIR_ONE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_JAM_REST_AWAL_TWO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_REST_AWAL_TWO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_JAM_REST_AKHIR_TWO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM_REST_AKHIR_TWO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_MAKS_AKTA = 1;
    private static final Integer UPDATED_MAKS_AKTA = 2;

    private static final Integer DEFAULT_MAKS_ORDER_HARIAN = 1;
    private static final Integer UPDATED_MAKS_ORDER_HARIAN = 2;

    private static final String DEFAULT_LEMBAR_SK = "AAAAAAAAAA";
    private static final String UPDATED_LEMBAR_SK = "BBBBBBBBBB";

    private static final String DEFAULT_LEMBAR_SUMPAH = "AAAAAAAAAA";
    private static final String UPDATED_LEMBAR_SUMPAH = "BBBBBBBBBB";

    private static final String DEFAULT_ALAMAT_KANTOR = "AAAAAAAAAA";
    private static final String UPDATED_ALAMAT_KANTOR = "BBBBBBBBBB";

    private static final String DEFAULT_ALAMAT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_ALAMAT_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_NO_KONTAK = "BBBBBBBBBB";

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/notarises";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private NotarisRepository notarisRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNotarisMockMvc;

    private Notaris notaris;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notaris createEntity(EntityManager em) {
        Notaris notaris = new Notaris()
            .nama(DEFAULT_NAMA)
            .namaShort(DEFAULT_NAMA_SHORT)
            .titelShortOne(DEFAULT_TITEL_SHORT_ONE)
            .titelShortTwo(DEFAULT_TITEL_SHORT_TWO)
            .titelLongOne(DEFAULT_TITEL_LONG_ONE)
            .titelLongTwo(DEFAULT_TITEL_LONG_TWO)
            .wilayahKerja(DEFAULT_WILAYAH_KERJA)
            .kedudukan(DEFAULT_KEDUDUKAN)
            .sk(DEFAULT_SK)
            .tglSk(DEFAULT_TGL_SK)
            .jenisHariKerja(DEFAULT_JENIS_HARI_KERJA)
            .jamKerjaAwalOne(DEFAULT_JAM_KERJA_AWAL_ONE)
            .jamKerjaAkhirOne(DEFAULT_JAM_KERJA_AKHIR_ONE)
            .jamKerjaAwalTwo(DEFAULT_JAM_KERJA_AWAL_TWO)
            .jamKerjaAkhirTwo(DEFAULT_JAM_KERJA_AKHIR_TWO)
            .selisihPukul(DEFAULT_SELISIH_PUKUL)
            .jamRestAwalOne(DEFAULT_JAM_REST_AWAL_ONE)
            .jamRestAkhirOne(DEFAULT_JAM_REST_AKHIR_ONE)
            .jamRestAwalTwo(DEFAULT_JAM_REST_AWAL_TWO)
            .jamRestAkhirTwo(DEFAULT_JAM_REST_AKHIR_TWO)
            .maksAkta(DEFAULT_MAKS_AKTA)
            .maksOrderHarian(DEFAULT_MAKS_ORDER_HARIAN)
            .lembarSk(DEFAULT_LEMBAR_SK)
            .lembarSumpah(DEFAULT_LEMBAR_SUMPAH)
            .alamatKantor(DEFAULT_ALAMAT_KANTOR)
            .alamatEmail(DEFAULT_ALAMAT_EMAIL)
            .noKontak(DEFAULT_NO_KONTAK)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON);
        return notaris;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notaris createUpdatedEntity(EntityManager em) {
        Notaris notaris = new Notaris()
            .nama(UPDATED_NAMA)
            .namaShort(UPDATED_NAMA_SHORT)
            .titelShortOne(UPDATED_TITEL_SHORT_ONE)
            .titelShortTwo(UPDATED_TITEL_SHORT_TWO)
            .titelLongOne(UPDATED_TITEL_LONG_ONE)
            .titelLongTwo(UPDATED_TITEL_LONG_TWO)
            .wilayahKerja(UPDATED_WILAYAH_KERJA)
            .kedudukan(UPDATED_KEDUDUKAN)
            .sk(UPDATED_SK)
            .tglSk(UPDATED_TGL_SK)
            .jenisHariKerja(UPDATED_JENIS_HARI_KERJA)
            .jamKerjaAwalOne(UPDATED_JAM_KERJA_AWAL_ONE)
            .jamKerjaAkhirOne(UPDATED_JAM_KERJA_AKHIR_ONE)
            .jamKerjaAwalTwo(UPDATED_JAM_KERJA_AWAL_TWO)
            .jamKerjaAkhirTwo(UPDATED_JAM_KERJA_AKHIR_TWO)
            .selisihPukul(UPDATED_SELISIH_PUKUL)
            .jamRestAwalOne(UPDATED_JAM_REST_AWAL_ONE)
            .jamRestAkhirOne(UPDATED_JAM_REST_AKHIR_ONE)
            .jamRestAwalTwo(UPDATED_JAM_REST_AWAL_TWO)
            .jamRestAkhirTwo(UPDATED_JAM_REST_AKHIR_TWO)
            .maksAkta(UPDATED_MAKS_AKTA)
            .maksOrderHarian(UPDATED_MAKS_ORDER_HARIAN)
            .lembarSk(UPDATED_LEMBAR_SK)
            .lembarSumpah(UPDATED_LEMBAR_SUMPAH)
            .alamatKantor(UPDATED_ALAMAT_KANTOR)
            .alamatEmail(UPDATED_ALAMAT_EMAIL)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);
        return notaris;
    }

    @BeforeEach
    public void initTest() {
        notaris = createEntity(em);
    }

    @Test
    @Transactional
    void createNotaris() throws Exception {
        int databaseSizeBeforeCreate = notarisRepository.findAll().size();
        // Create the Notaris
        restNotarisMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notaris)))
            .andExpect(status().isCreated());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeCreate + 1);
        Notaris testNotaris = notarisList.get(notarisList.size() - 1);
        assertThat(testNotaris.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testNotaris.getNamaShort()).isEqualTo(DEFAULT_NAMA_SHORT);
        assertThat(testNotaris.getTitelShortOne()).isEqualTo(DEFAULT_TITEL_SHORT_ONE);
        assertThat(testNotaris.getTitelShortTwo()).isEqualTo(DEFAULT_TITEL_SHORT_TWO);
        assertThat(testNotaris.getTitelLongOne()).isEqualTo(DEFAULT_TITEL_LONG_ONE);
        assertThat(testNotaris.getTitelLongTwo()).isEqualTo(DEFAULT_TITEL_LONG_TWO);
        assertThat(testNotaris.getWilayahKerja()).isEqualTo(DEFAULT_WILAYAH_KERJA);
        assertThat(testNotaris.getKedudukan()).isEqualTo(DEFAULT_KEDUDUKAN);
        assertThat(testNotaris.getSk()).isEqualTo(DEFAULT_SK);
        assertThat(testNotaris.getTglSk()).isEqualTo(DEFAULT_TGL_SK);
        assertThat(testNotaris.getJenisHariKerja()).isEqualTo(DEFAULT_JENIS_HARI_KERJA);
        assertThat(testNotaris.getJamKerjaAwalOne()).isEqualTo(DEFAULT_JAM_KERJA_AWAL_ONE);
        assertThat(testNotaris.getJamKerjaAkhirOne()).isEqualTo(DEFAULT_JAM_KERJA_AKHIR_ONE);
        assertThat(testNotaris.getJamKerjaAwalTwo()).isEqualTo(DEFAULT_JAM_KERJA_AWAL_TWO);
        assertThat(testNotaris.getJamKerjaAkhirTwo()).isEqualTo(DEFAULT_JAM_KERJA_AKHIR_TWO);
        assertThat(testNotaris.getSelisihPukul()).isEqualTo(DEFAULT_SELISIH_PUKUL);
        assertThat(testNotaris.getJamRestAwalOne()).isEqualTo(DEFAULT_JAM_REST_AWAL_ONE);
        assertThat(testNotaris.getJamRestAkhirOne()).isEqualTo(DEFAULT_JAM_REST_AKHIR_ONE);
        assertThat(testNotaris.getJamRestAwalTwo()).isEqualTo(DEFAULT_JAM_REST_AWAL_TWO);
        assertThat(testNotaris.getJamRestAkhirTwo()).isEqualTo(DEFAULT_JAM_REST_AKHIR_TWO);
        assertThat(testNotaris.getMaksAkta()).isEqualTo(DEFAULT_MAKS_AKTA);
        assertThat(testNotaris.getMaksOrderHarian()).isEqualTo(DEFAULT_MAKS_ORDER_HARIAN);
        assertThat(testNotaris.getLembarSk()).isEqualTo(DEFAULT_LEMBAR_SK);
        assertThat(testNotaris.getLembarSumpah()).isEqualTo(DEFAULT_LEMBAR_SUMPAH);
        assertThat(testNotaris.getAlamatKantor()).isEqualTo(DEFAULT_ALAMAT_KANTOR);
        assertThat(testNotaris.getAlamatEmail()).isEqualTo(DEFAULT_ALAMAT_EMAIL);
        assertThat(testNotaris.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
        assertThat(testNotaris.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testNotaris.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testNotaris.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    void createNotarisWithExistingId() throws Exception {
        // Create the Notaris with an existing ID
        notaris.setId(1L);

        int databaseSizeBeforeCreate = notarisRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotarisMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notaris)))
            .andExpect(status().isBadRequest());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllNotarises() throws Exception {
        // Initialize the database
        notarisRepository.saveAndFlush(notaris);

        // Get all the notarisList
        restNotarisMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notaris.getId().intValue())))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].namaShort").value(hasItem(DEFAULT_NAMA_SHORT)))
            .andExpect(jsonPath("$.[*].titelShortOne").value(hasItem(DEFAULT_TITEL_SHORT_ONE)))
            .andExpect(jsonPath("$.[*].titelShortTwo").value(hasItem(DEFAULT_TITEL_SHORT_TWO)))
            .andExpect(jsonPath("$.[*].titelLongOne").value(hasItem(DEFAULT_TITEL_LONG_ONE)))
            .andExpect(jsonPath("$.[*].titelLongTwo").value(hasItem(DEFAULT_TITEL_LONG_TWO)))
            .andExpect(jsonPath("$.[*].wilayahKerja").value(hasItem(DEFAULT_WILAYAH_KERJA)))
            .andExpect(jsonPath("$.[*].kedudukan").value(hasItem(DEFAULT_KEDUDUKAN)))
            .andExpect(jsonPath("$.[*].sk").value(hasItem(DEFAULT_SK)))
            .andExpect(jsonPath("$.[*].tglSk").value(hasItem(DEFAULT_TGL_SK.toString())))
            .andExpect(jsonPath("$.[*].jenisHariKerja").value(hasItem(DEFAULT_JENIS_HARI_KERJA)))
            .andExpect(jsonPath("$.[*].jamKerjaAwalOne").value(hasItem(sameInstant(DEFAULT_JAM_KERJA_AWAL_ONE))))
            .andExpect(jsonPath("$.[*].jamKerjaAkhirOne").value(hasItem(sameInstant(DEFAULT_JAM_KERJA_AKHIR_ONE))))
            .andExpect(jsonPath("$.[*].jamKerjaAwalTwo").value(hasItem(sameInstant(DEFAULT_JAM_KERJA_AWAL_TWO))))
            .andExpect(jsonPath("$.[*].jamKerjaAkhirTwo").value(hasItem(sameInstant(DEFAULT_JAM_KERJA_AKHIR_TWO))))
            .andExpect(jsonPath("$.[*].selisihPukul").value(hasItem(DEFAULT_SELISIH_PUKUL)))
            .andExpect(jsonPath("$.[*].jamRestAwalOne").value(hasItem(sameInstant(DEFAULT_JAM_REST_AWAL_ONE))))
            .andExpect(jsonPath("$.[*].jamRestAkhirOne").value(hasItem(sameInstant(DEFAULT_JAM_REST_AKHIR_ONE))))
            .andExpect(jsonPath("$.[*].jamRestAwalTwo").value(hasItem(sameInstant(DEFAULT_JAM_REST_AWAL_TWO))))
            .andExpect(jsonPath("$.[*].jamRestAkhirTwo").value(hasItem(sameInstant(DEFAULT_JAM_REST_AKHIR_TWO))))
            .andExpect(jsonPath("$.[*].maksAkta").value(hasItem(DEFAULT_MAKS_AKTA)))
            .andExpect(jsonPath("$.[*].maksOrderHarian").value(hasItem(DEFAULT_MAKS_ORDER_HARIAN)))
            .andExpect(jsonPath("$.[*].lembarSk").value(hasItem(DEFAULT_LEMBAR_SK)))
            .andExpect(jsonPath("$.[*].lembarSumpah").value(hasItem(DEFAULT_LEMBAR_SUMPAH)))
            .andExpect(jsonPath("$.[*].alamatKantor").value(hasItem(DEFAULT_ALAMAT_KANTOR)))
            .andExpect(jsonPath("$.[*].alamatEmail").value(hasItem(DEFAULT_ALAMAT_EMAIL)))
            .andExpect(jsonPath("$.[*].noKontak").value(hasItem(DEFAULT_NO_KONTAK)))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))));
    }

    @Test
    @Transactional
    void getNotaris() throws Exception {
        // Initialize the database
        notarisRepository.saveAndFlush(notaris);

        // Get the notaris
        restNotarisMockMvc
            .perform(get(ENTITY_API_URL_ID, notaris.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(notaris.getId().intValue()))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.namaShort").value(DEFAULT_NAMA_SHORT))
            .andExpect(jsonPath("$.titelShortOne").value(DEFAULT_TITEL_SHORT_ONE))
            .andExpect(jsonPath("$.titelShortTwo").value(DEFAULT_TITEL_SHORT_TWO))
            .andExpect(jsonPath("$.titelLongOne").value(DEFAULT_TITEL_LONG_ONE))
            .andExpect(jsonPath("$.titelLongTwo").value(DEFAULT_TITEL_LONG_TWO))
            .andExpect(jsonPath("$.wilayahKerja").value(DEFAULT_WILAYAH_KERJA))
            .andExpect(jsonPath("$.kedudukan").value(DEFAULT_KEDUDUKAN))
            .andExpect(jsonPath("$.sk").value(DEFAULT_SK))
            .andExpect(jsonPath("$.tglSk").value(DEFAULT_TGL_SK.toString()))
            .andExpect(jsonPath("$.jenisHariKerja").value(DEFAULT_JENIS_HARI_KERJA))
            .andExpect(jsonPath("$.jamKerjaAwalOne").value(sameInstant(DEFAULT_JAM_KERJA_AWAL_ONE)))
            .andExpect(jsonPath("$.jamKerjaAkhirOne").value(sameInstant(DEFAULT_JAM_KERJA_AKHIR_ONE)))
            .andExpect(jsonPath("$.jamKerjaAwalTwo").value(sameInstant(DEFAULT_JAM_KERJA_AWAL_TWO)))
            .andExpect(jsonPath("$.jamKerjaAkhirTwo").value(sameInstant(DEFAULT_JAM_KERJA_AKHIR_TWO)))
            .andExpect(jsonPath("$.selisihPukul").value(DEFAULT_SELISIH_PUKUL))
            .andExpect(jsonPath("$.jamRestAwalOne").value(sameInstant(DEFAULT_JAM_REST_AWAL_ONE)))
            .andExpect(jsonPath("$.jamRestAkhirOne").value(sameInstant(DEFAULT_JAM_REST_AKHIR_ONE)))
            .andExpect(jsonPath("$.jamRestAwalTwo").value(sameInstant(DEFAULT_JAM_REST_AWAL_TWO)))
            .andExpect(jsonPath("$.jamRestAkhirTwo").value(sameInstant(DEFAULT_JAM_REST_AKHIR_TWO)))
            .andExpect(jsonPath("$.maksAkta").value(DEFAULT_MAKS_AKTA))
            .andExpect(jsonPath("$.maksOrderHarian").value(DEFAULT_MAKS_ORDER_HARIAN))
            .andExpect(jsonPath("$.lembarSk").value(DEFAULT_LEMBAR_SK))
            .andExpect(jsonPath("$.lembarSumpah").value(DEFAULT_LEMBAR_SUMPAH))
            .andExpect(jsonPath("$.alamatKantor").value(DEFAULT_ALAMAT_KANTOR))
            .andExpect(jsonPath("$.alamatEmail").value(DEFAULT_ALAMAT_EMAIL))
            .andExpect(jsonPath("$.noKontak").value(DEFAULT_NO_KONTAK))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)));
    }

    @Test
    @Transactional
    void getNonExistingNotaris() throws Exception {
        // Get the notaris
        restNotarisMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingNotaris() throws Exception {
        // Initialize the database
        notarisRepository.saveAndFlush(notaris);

        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();

        // Update the notaris
        Notaris updatedNotaris = notarisRepository.findById(notaris.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedNotaris are not directly saved in db
        em.detach(updatedNotaris);
        updatedNotaris
            .nama(UPDATED_NAMA)
            .namaShort(UPDATED_NAMA_SHORT)
            .titelShortOne(UPDATED_TITEL_SHORT_ONE)
            .titelShortTwo(UPDATED_TITEL_SHORT_TWO)
            .titelLongOne(UPDATED_TITEL_LONG_ONE)
            .titelLongTwo(UPDATED_TITEL_LONG_TWO)
            .wilayahKerja(UPDATED_WILAYAH_KERJA)
            .kedudukan(UPDATED_KEDUDUKAN)
            .sk(UPDATED_SK)
            .tglSk(UPDATED_TGL_SK)
            .jenisHariKerja(UPDATED_JENIS_HARI_KERJA)
            .jamKerjaAwalOne(UPDATED_JAM_KERJA_AWAL_ONE)
            .jamKerjaAkhirOne(UPDATED_JAM_KERJA_AKHIR_ONE)
            .jamKerjaAwalTwo(UPDATED_JAM_KERJA_AWAL_TWO)
            .jamKerjaAkhirTwo(UPDATED_JAM_KERJA_AKHIR_TWO)
            .selisihPukul(UPDATED_SELISIH_PUKUL)
            .jamRestAwalOne(UPDATED_JAM_REST_AWAL_ONE)
            .jamRestAkhirOne(UPDATED_JAM_REST_AKHIR_ONE)
            .jamRestAwalTwo(UPDATED_JAM_REST_AWAL_TWO)
            .jamRestAkhirTwo(UPDATED_JAM_REST_AKHIR_TWO)
            .maksAkta(UPDATED_MAKS_AKTA)
            .maksOrderHarian(UPDATED_MAKS_ORDER_HARIAN)
            .lembarSk(UPDATED_LEMBAR_SK)
            .lembarSumpah(UPDATED_LEMBAR_SUMPAH)
            .alamatKantor(UPDATED_ALAMAT_KANTOR)
            .alamatEmail(UPDATED_ALAMAT_EMAIL)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restNotarisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedNotaris.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedNotaris))
            )
            .andExpect(status().isOk());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
        Notaris testNotaris = notarisList.get(notarisList.size() - 1);
        assertThat(testNotaris.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testNotaris.getNamaShort()).isEqualTo(UPDATED_NAMA_SHORT);
        assertThat(testNotaris.getTitelShortOne()).isEqualTo(UPDATED_TITEL_SHORT_ONE);
        assertThat(testNotaris.getTitelShortTwo()).isEqualTo(UPDATED_TITEL_SHORT_TWO);
        assertThat(testNotaris.getTitelLongOne()).isEqualTo(UPDATED_TITEL_LONG_ONE);
        assertThat(testNotaris.getTitelLongTwo()).isEqualTo(UPDATED_TITEL_LONG_TWO);
        assertThat(testNotaris.getWilayahKerja()).isEqualTo(UPDATED_WILAYAH_KERJA);
        assertThat(testNotaris.getKedudukan()).isEqualTo(UPDATED_KEDUDUKAN);
        assertThat(testNotaris.getSk()).isEqualTo(UPDATED_SK);
        assertThat(testNotaris.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testNotaris.getJenisHariKerja()).isEqualTo(UPDATED_JENIS_HARI_KERJA);
        assertThat(testNotaris.getJamKerjaAwalOne()).isEqualTo(UPDATED_JAM_KERJA_AWAL_ONE);
        assertThat(testNotaris.getJamKerjaAkhirOne()).isEqualTo(UPDATED_JAM_KERJA_AKHIR_ONE);
        assertThat(testNotaris.getJamKerjaAwalTwo()).isEqualTo(UPDATED_JAM_KERJA_AWAL_TWO);
        assertThat(testNotaris.getJamKerjaAkhirTwo()).isEqualTo(UPDATED_JAM_KERJA_AKHIR_TWO);
        assertThat(testNotaris.getSelisihPukul()).isEqualTo(UPDATED_SELISIH_PUKUL);
        assertThat(testNotaris.getJamRestAwalOne()).isEqualTo(UPDATED_JAM_REST_AWAL_ONE);
        assertThat(testNotaris.getJamRestAkhirOne()).isEqualTo(UPDATED_JAM_REST_AKHIR_ONE);
        assertThat(testNotaris.getJamRestAwalTwo()).isEqualTo(UPDATED_JAM_REST_AWAL_TWO);
        assertThat(testNotaris.getJamRestAkhirTwo()).isEqualTo(UPDATED_JAM_REST_AKHIR_TWO);
        assertThat(testNotaris.getMaksAkta()).isEqualTo(UPDATED_MAKS_AKTA);
        assertThat(testNotaris.getMaksOrderHarian()).isEqualTo(UPDATED_MAKS_ORDER_HARIAN);
        assertThat(testNotaris.getLembarSk()).isEqualTo(UPDATED_LEMBAR_SK);
        assertThat(testNotaris.getLembarSumpah()).isEqualTo(UPDATED_LEMBAR_SUMPAH);
        assertThat(testNotaris.getAlamatKantor()).isEqualTo(UPDATED_ALAMAT_KANTOR);
        assertThat(testNotaris.getAlamatEmail()).isEqualTo(UPDATED_ALAMAT_EMAIL);
        assertThat(testNotaris.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testNotaris.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testNotaris.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testNotaris.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void putNonExistingNotaris() throws Exception {
        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();
        notaris.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotarisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, notaris.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchNotaris() throws Exception {
        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();
        notaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotarisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamNotaris() throws Exception {
        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();
        notaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotarisMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notaris)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateNotarisWithPatch() throws Exception {
        // Initialize the database
        notarisRepository.saveAndFlush(notaris);

        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();

        // Update the notaris using partial update
        Notaris partialUpdatedNotaris = new Notaris();
        partialUpdatedNotaris.setId(notaris.getId());

        partialUpdatedNotaris
            .nama(UPDATED_NAMA)
            .namaShort(UPDATED_NAMA_SHORT)
            .titelShortTwo(UPDATED_TITEL_SHORT_TWO)
            .titelLongTwo(UPDATED_TITEL_LONG_TWO)
            .wilayahKerja(UPDATED_WILAYAH_KERJA)
            .kedudukan(UPDATED_KEDUDUKAN)
            .tglSk(UPDATED_TGL_SK)
            .jenisHariKerja(UPDATED_JENIS_HARI_KERJA)
            .jamKerjaAkhirOne(UPDATED_JAM_KERJA_AKHIR_ONE)
            .jamRestAwalOne(UPDATED_JAM_REST_AWAL_ONE)
            .jamRestAwalTwo(UPDATED_JAM_REST_AWAL_TWO)
            .jamRestAkhirTwo(UPDATED_JAM_REST_AKHIR_TWO)
            .maksAkta(UPDATED_MAKS_AKTA)
            .maksOrderHarian(UPDATED_MAKS_ORDER_HARIAN)
            .lembarSk(UPDATED_LEMBAR_SK)
            .lembarSumpah(UPDATED_LEMBAR_SUMPAH)
            .updateBy(UPDATED_UPDATE_BY);

        restNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNotaris.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNotaris))
            )
            .andExpect(status().isOk());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
        Notaris testNotaris = notarisList.get(notarisList.size() - 1);
        assertThat(testNotaris.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testNotaris.getNamaShort()).isEqualTo(UPDATED_NAMA_SHORT);
        assertThat(testNotaris.getTitelShortOne()).isEqualTo(DEFAULT_TITEL_SHORT_ONE);
        assertThat(testNotaris.getTitelShortTwo()).isEqualTo(UPDATED_TITEL_SHORT_TWO);
        assertThat(testNotaris.getTitelLongOne()).isEqualTo(DEFAULT_TITEL_LONG_ONE);
        assertThat(testNotaris.getTitelLongTwo()).isEqualTo(UPDATED_TITEL_LONG_TWO);
        assertThat(testNotaris.getWilayahKerja()).isEqualTo(UPDATED_WILAYAH_KERJA);
        assertThat(testNotaris.getKedudukan()).isEqualTo(UPDATED_KEDUDUKAN);
        assertThat(testNotaris.getSk()).isEqualTo(DEFAULT_SK);
        assertThat(testNotaris.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testNotaris.getJenisHariKerja()).isEqualTo(UPDATED_JENIS_HARI_KERJA);
        assertThat(testNotaris.getJamKerjaAwalOne()).isEqualTo(DEFAULT_JAM_KERJA_AWAL_ONE);
        assertThat(testNotaris.getJamKerjaAkhirOne()).isEqualTo(UPDATED_JAM_KERJA_AKHIR_ONE);
        assertThat(testNotaris.getJamKerjaAwalTwo()).isEqualTo(DEFAULT_JAM_KERJA_AWAL_TWO);
        assertThat(testNotaris.getJamKerjaAkhirTwo()).isEqualTo(DEFAULT_JAM_KERJA_AKHIR_TWO);
        assertThat(testNotaris.getSelisihPukul()).isEqualTo(DEFAULT_SELISIH_PUKUL);
        assertThat(testNotaris.getJamRestAwalOne()).isEqualTo(UPDATED_JAM_REST_AWAL_ONE);
        assertThat(testNotaris.getJamRestAkhirOne()).isEqualTo(DEFAULT_JAM_REST_AKHIR_ONE);
        assertThat(testNotaris.getJamRestAwalTwo()).isEqualTo(UPDATED_JAM_REST_AWAL_TWO);
        assertThat(testNotaris.getJamRestAkhirTwo()).isEqualTo(UPDATED_JAM_REST_AKHIR_TWO);
        assertThat(testNotaris.getMaksAkta()).isEqualTo(UPDATED_MAKS_AKTA);
        assertThat(testNotaris.getMaksOrderHarian()).isEqualTo(UPDATED_MAKS_ORDER_HARIAN);
        assertThat(testNotaris.getLembarSk()).isEqualTo(UPDATED_LEMBAR_SK);
        assertThat(testNotaris.getLembarSumpah()).isEqualTo(UPDATED_LEMBAR_SUMPAH);
        assertThat(testNotaris.getAlamatKantor()).isEqualTo(DEFAULT_ALAMAT_KANTOR);
        assertThat(testNotaris.getAlamatEmail()).isEqualTo(DEFAULT_ALAMAT_EMAIL);
        assertThat(testNotaris.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
        assertThat(testNotaris.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testNotaris.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testNotaris.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    void fullUpdateNotarisWithPatch() throws Exception {
        // Initialize the database
        notarisRepository.saveAndFlush(notaris);

        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();

        // Update the notaris using partial update
        Notaris partialUpdatedNotaris = new Notaris();
        partialUpdatedNotaris.setId(notaris.getId());

        partialUpdatedNotaris
            .nama(UPDATED_NAMA)
            .namaShort(UPDATED_NAMA_SHORT)
            .titelShortOne(UPDATED_TITEL_SHORT_ONE)
            .titelShortTwo(UPDATED_TITEL_SHORT_TWO)
            .titelLongOne(UPDATED_TITEL_LONG_ONE)
            .titelLongTwo(UPDATED_TITEL_LONG_TWO)
            .wilayahKerja(UPDATED_WILAYAH_KERJA)
            .kedudukan(UPDATED_KEDUDUKAN)
            .sk(UPDATED_SK)
            .tglSk(UPDATED_TGL_SK)
            .jenisHariKerja(UPDATED_JENIS_HARI_KERJA)
            .jamKerjaAwalOne(UPDATED_JAM_KERJA_AWAL_ONE)
            .jamKerjaAkhirOne(UPDATED_JAM_KERJA_AKHIR_ONE)
            .jamKerjaAwalTwo(UPDATED_JAM_KERJA_AWAL_TWO)
            .jamKerjaAkhirTwo(UPDATED_JAM_KERJA_AKHIR_TWO)
            .selisihPukul(UPDATED_SELISIH_PUKUL)
            .jamRestAwalOne(UPDATED_JAM_REST_AWAL_ONE)
            .jamRestAkhirOne(UPDATED_JAM_REST_AKHIR_ONE)
            .jamRestAwalTwo(UPDATED_JAM_REST_AWAL_TWO)
            .jamRestAkhirTwo(UPDATED_JAM_REST_AKHIR_TWO)
            .maksAkta(UPDATED_MAKS_AKTA)
            .maksOrderHarian(UPDATED_MAKS_ORDER_HARIAN)
            .lembarSk(UPDATED_LEMBAR_SK)
            .lembarSumpah(UPDATED_LEMBAR_SUMPAH)
            .alamatKantor(UPDATED_ALAMAT_KANTOR)
            .alamatEmail(UPDATED_ALAMAT_EMAIL)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNotaris.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNotaris))
            )
            .andExpect(status().isOk());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
        Notaris testNotaris = notarisList.get(notarisList.size() - 1);
        assertThat(testNotaris.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testNotaris.getNamaShort()).isEqualTo(UPDATED_NAMA_SHORT);
        assertThat(testNotaris.getTitelShortOne()).isEqualTo(UPDATED_TITEL_SHORT_ONE);
        assertThat(testNotaris.getTitelShortTwo()).isEqualTo(UPDATED_TITEL_SHORT_TWO);
        assertThat(testNotaris.getTitelLongOne()).isEqualTo(UPDATED_TITEL_LONG_ONE);
        assertThat(testNotaris.getTitelLongTwo()).isEqualTo(UPDATED_TITEL_LONG_TWO);
        assertThat(testNotaris.getWilayahKerja()).isEqualTo(UPDATED_WILAYAH_KERJA);
        assertThat(testNotaris.getKedudukan()).isEqualTo(UPDATED_KEDUDUKAN);
        assertThat(testNotaris.getSk()).isEqualTo(UPDATED_SK);
        assertThat(testNotaris.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testNotaris.getJenisHariKerja()).isEqualTo(UPDATED_JENIS_HARI_KERJA);
        assertThat(testNotaris.getJamKerjaAwalOne()).isEqualTo(UPDATED_JAM_KERJA_AWAL_ONE);
        assertThat(testNotaris.getJamKerjaAkhirOne()).isEqualTo(UPDATED_JAM_KERJA_AKHIR_ONE);
        assertThat(testNotaris.getJamKerjaAwalTwo()).isEqualTo(UPDATED_JAM_KERJA_AWAL_TWO);
        assertThat(testNotaris.getJamKerjaAkhirTwo()).isEqualTo(UPDATED_JAM_KERJA_AKHIR_TWO);
        assertThat(testNotaris.getSelisihPukul()).isEqualTo(UPDATED_SELISIH_PUKUL);
        assertThat(testNotaris.getJamRestAwalOne()).isEqualTo(UPDATED_JAM_REST_AWAL_ONE);
        assertThat(testNotaris.getJamRestAkhirOne()).isEqualTo(UPDATED_JAM_REST_AKHIR_ONE);
        assertThat(testNotaris.getJamRestAwalTwo()).isEqualTo(UPDATED_JAM_REST_AWAL_TWO);
        assertThat(testNotaris.getJamRestAkhirTwo()).isEqualTo(UPDATED_JAM_REST_AKHIR_TWO);
        assertThat(testNotaris.getMaksAkta()).isEqualTo(UPDATED_MAKS_AKTA);
        assertThat(testNotaris.getMaksOrderHarian()).isEqualTo(UPDATED_MAKS_ORDER_HARIAN);
        assertThat(testNotaris.getLembarSk()).isEqualTo(UPDATED_LEMBAR_SK);
        assertThat(testNotaris.getLembarSumpah()).isEqualTo(UPDATED_LEMBAR_SUMPAH);
        assertThat(testNotaris.getAlamatKantor()).isEqualTo(UPDATED_ALAMAT_KANTOR);
        assertThat(testNotaris.getAlamatEmail()).isEqualTo(UPDATED_ALAMAT_EMAIL);
        assertThat(testNotaris.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testNotaris.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testNotaris.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testNotaris.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void patchNonExistingNotaris() throws Exception {
        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();
        notaris.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, notaris.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(notaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchNotaris() throws Exception {
        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();
        notaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(notaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamNotaris() throws Exception {
        int databaseSizeBeforeUpdate = notarisRepository.findAll().size();
        notaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotarisMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(notaris)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Notaris in the database
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteNotaris() throws Exception {
        // Initialize the database
        notarisRepository.saveAndFlush(notaris);

        int databaseSizeBeforeDelete = notarisRepository.findAll().size();

        // Delete the notaris
        restNotarisMockMvc
            .perform(delete(ENTITY_API_URL_ID, notaris.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Notaris> notarisList = notarisRepository.findAll();
        assertThat(notarisList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
