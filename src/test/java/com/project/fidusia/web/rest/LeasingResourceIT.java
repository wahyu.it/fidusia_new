package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Leasing;
import com.project.fidusia.repository.LeasingRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LeasingResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LeasingResourceIT {

    private static final String DEFAULT_KODE = "AAAAAAAAAA";
    private static final String UPDATED_KODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_ALAMAT = "AAAAAAAAAA";
    private static final String UPDATED_ALAMAT = "BBBBBBBBBB";

    private static final String DEFAULT_KOTA = "AAAAAAAAAA";
    private static final String UPDATED_KOTA = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINSI = "AAAAAAAAAA";
    private static final String UPDATED_PROVINSI = "BBBBBBBBBB";

    private static final String DEFAULT_KECAMATAN = "AAAAAAAAAA";
    private static final String UPDATED_KECAMATAN = "BBBBBBBBBB";

    private static final String DEFAULT_KELURAHAN = "AAAAAAAAAA";
    private static final String UPDATED_KELURAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_RT = "AAAAAAAAAA";
    private static final String UPDATED_RT = "BBBBBBBBBB";

    private static final String DEFAULT_RW = "AAAAAAAAAA";
    private static final String UPDATED_RW = "BBBBBBBBBB";

    private static final String DEFAULT_NPWP = "AAAAAAAAAA";
    private static final String UPDATED_NPWP = "BBBBBBBBBB";

    private static final String DEFAULT_KODE_POSE = "AAAAAAAAAA";
    private static final String UPDATED_KODE_POSE = "BBBBBBBBBB";

    private static final String DEFAULT_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_NO_KONTAK = "BBBBBBBBBB";

    private static final String DEFAULT_TECH_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_TECH_NO_KONTAK = "BBBBBBBBBB";

    private static final String DEFAULT_TECH_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_TECH_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ACCT_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_ACCT_NO_KONTAK = "BBBBBBBBBB";

    private static final String DEFAULT_ACCT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_ACCT_EMAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ONE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ONE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_SK = "AAAAAAAAAA";
    private static final String UPDATED_SK = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/leasings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LeasingRepository leasingRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLeasingMockMvc;

    private Leasing leasing;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Leasing createEntity(EntityManager em) {
        Leasing leasing = new Leasing()
            .kode(DEFAULT_KODE)
            .nama(DEFAULT_NAMA)
            .alamat(DEFAULT_ALAMAT)
            .kota(DEFAULT_KOTA)
            .provinsi(DEFAULT_PROVINSI)
            .kecamatan(DEFAULT_KECAMATAN)
            .kelurahan(DEFAULT_KELURAHAN)
            .rt(DEFAULT_RT)
            .rw(DEFAULT_RW)
            .npwp(DEFAULT_NPWP)
            .kodePose(DEFAULT_KODE_POSE)
            .noKontak(DEFAULT_NO_KONTAK)
            .techNoKontak(DEFAULT_TECH_NO_KONTAK)
            .techEmail(DEFAULT_TECH_EMAIL)
            .acctNoKontak(DEFAULT_ACCT_NO_KONTAK)
            .acctEmail(DEFAULT_ACCT_EMAIL)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOne(DEFAULT_UPDATE_ONE)
            .email(DEFAULT_EMAIL)
            .sk(DEFAULT_SK);
        return leasing;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Leasing createUpdatedEntity(EntityManager em) {
        Leasing leasing = new Leasing()
            .kode(UPDATED_KODE)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kecamatan(UPDATED_KECAMATAN)
            .kelurahan(UPDATED_KELURAHAN)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .npwp(UPDATED_NPWP)
            .kodePose(UPDATED_KODE_POSE)
            .noKontak(UPDATED_NO_KONTAK)
            .techNoKontak(UPDATED_TECH_NO_KONTAK)
            .techEmail(UPDATED_TECH_EMAIL)
            .acctNoKontak(UPDATED_ACCT_NO_KONTAK)
            .acctEmail(UPDATED_ACCT_EMAIL)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOne(UPDATED_UPDATE_ONE)
            .email(UPDATED_EMAIL)
            .sk(UPDATED_SK);
        return leasing;
    }

    @BeforeEach
    public void initTest() {
        leasing = createEntity(em);
    }

    @Test
    @Transactional
    void createLeasing() throws Exception {
        int databaseSizeBeforeCreate = leasingRepository.findAll().size();
        // Create the Leasing
        restLeasingMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(leasing)))
            .andExpect(status().isCreated());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeCreate + 1);
        Leasing testLeasing = leasingList.get(leasingList.size() - 1);
        assertThat(testLeasing.getKode()).isEqualTo(DEFAULT_KODE);
        assertThat(testLeasing.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testLeasing.getAlamat()).isEqualTo(DEFAULT_ALAMAT);
        assertThat(testLeasing.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testLeasing.getProvinsi()).isEqualTo(DEFAULT_PROVINSI);
        assertThat(testLeasing.getKecamatan()).isEqualTo(DEFAULT_KECAMATAN);
        assertThat(testLeasing.getKelurahan()).isEqualTo(DEFAULT_KELURAHAN);
        assertThat(testLeasing.getRt()).isEqualTo(DEFAULT_RT);
        assertThat(testLeasing.getRw()).isEqualTo(DEFAULT_RW);
        assertThat(testLeasing.getNpwp()).isEqualTo(DEFAULT_NPWP);
        assertThat(testLeasing.getKodePose()).isEqualTo(DEFAULT_KODE_POSE);
        assertThat(testLeasing.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
        assertThat(testLeasing.getTechNoKontak()).isEqualTo(DEFAULT_TECH_NO_KONTAK);
        assertThat(testLeasing.getTechEmail()).isEqualTo(DEFAULT_TECH_EMAIL);
        assertThat(testLeasing.getAcctNoKontak()).isEqualTo(DEFAULT_ACCT_NO_KONTAK);
        assertThat(testLeasing.getAcctEmail()).isEqualTo(DEFAULT_ACCT_EMAIL);
        assertThat(testLeasing.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testLeasing.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testLeasing.getUpdateOne()).isEqualTo(DEFAULT_UPDATE_ONE);
        assertThat(testLeasing.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testLeasing.getSk()).isEqualTo(DEFAULT_SK);
    }

    @Test
    @Transactional
    void createLeasingWithExistingId() throws Exception {
        // Create the Leasing with an existing ID
        leasing.setId(1L);

        int databaseSizeBeforeCreate = leasingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLeasingMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(leasing)))
            .andExpect(status().isBadRequest());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllLeasings() throws Exception {
        // Initialize the database
        leasingRepository.saveAndFlush(leasing);

        // Get all the leasingList
        restLeasingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(leasing.getId().intValue())))
            .andExpect(jsonPath("$.[*].kode").value(hasItem(DEFAULT_KODE)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].alamat").value(hasItem(DEFAULT_ALAMAT)))
            .andExpect(jsonPath("$.[*].kota").value(hasItem(DEFAULT_KOTA)))
            .andExpect(jsonPath("$.[*].provinsi").value(hasItem(DEFAULT_PROVINSI)))
            .andExpect(jsonPath("$.[*].kecamatan").value(hasItem(DEFAULT_KECAMATAN)))
            .andExpect(jsonPath("$.[*].kelurahan").value(hasItem(DEFAULT_KELURAHAN)))
            .andExpect(jsonPath("$.[*].rt").value(hasItem(DEFAULT_RT)))
            .andExpect(jsonPath("$.[*].rw").value(hasItem(DEFAULT_RW)))
            .andExpect(jsonPath("$.[*].npwp").value(hasItem(DEFAULT_NPWP)))
            .andExpect(jsonPath("$.[*].kodePose").value(hasItem(DEFAULT_KODE_POSE)))
            .andExpect(jsonPath("$.[*].noKontak").value(hasItem(DEFAULT_NO_KONTAK)))
            .andExpect(jsonPath("$.[*].techNoKontak").value(hasItem(DEFAULT_TECH_NO_KONTAK)))
            .andExpect(jsonPath("$.[*].techEmail").value(hasItem(DEFAULT_TECH_EMAIL)))
            .andExpect(jsonPath("$.[*].acctNoKontak").value(hasItem(DEFAULT_ACCT_NO_KONTAK)))
            .andExpect(jsonPath("$.[*].acctEmail").value(hasItem(DEFAULT_ACCT_EMAIL)))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOne").value(hasItem(sameInstant(DEFAULT_UPDATE_ONE))))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].sk").value(hasItem(DEFAULT_SK)));
    }

    @Test
    @Transactional
    void getLeasing() throws Exception {
        // Initialize the database
        leasingRepository.saveAndFlush(leasing);

        // Get the leasing
        restLeasingMockMvc
            .perform(get(ENTITY_API_URL_ID, leasing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(leasing.getId().intValue()))
            .andExpect(jsonPath("$.kode").value(DEFAULT_KODE))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.alamat").value(DEFAULT_ALAMAT))
            .andExpect(jsonPath("$.kota").value(DEFAULT_KOTA))
            .andExpect(jsonPath("$.provinsi").value(DEFAULT_PROVINSI))
            .andExpect(jsonPath("$.kecamatan").value(DEFAULT_KECAMATAN))
            .andExpect(jsonPath("$.kelurahan").value(DEFAULT_KELURAHAN))
            .andExpect(jsonPath("$.rt").value(DEFAULT_RT))
            .andExpect(jsonPath("$.rw").value(DEFAULT_RW))
            .andExpect(jsonPath("$.npwp").value(DEFAULT_NPWP))
            .andExpect(jsonPath("$.kodePose").value(DEFAULT_KODE_POSE))
            .andExpect(jsonPath("$.noKontak").value(DEFAULT_NO_KONTAK))
            .andExpect(jsonPath("$.techNoKontak").value(DEFAULT_TECH_NO_KONTAK))
            .andExpect(jsonPath("$.techEmail").value(DEFAULT_TECH_EMAIL))
            .andExpect(jsonPath("$.acctNoKontak").value(DEFAULT_ACCT_NO_KONTAK))
            .andExpect(jsonPath("$.acctEmail").value(DEFAULT_ACCT_EMAIL))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOne").value(sameInstant(DEFAULT_UPDATE_ONE)))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.sk").value(DEFAULT_SK));
    }

    @Test
    @Transactional
    void getNonExistingLeasing() throws Exception {
        // Get the leasing
        restLeasingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingLeasing() throws Exception {
        // Initialize the database
        leasingRepository.saveAndFlush(leasing);

        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();

        // Update the leasing
        Leasing updatedLeasing = leasingRepository.findById(leasing.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedLeasing are not directly saved in db
        em.detach(updatedLeasing);
        updatedLeasing
            .kode(UPDATED_KODE)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kecamatan(UPDATED_KECAMATAN)
            .kelurahan(UPDATED_KELURAHAN)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .npwp(UPDATED_NPWP)
            .kodePose(UPDATED_KODE_POSE)
            .noKontak(UPDATED_NO_KONTAK)
            .techNoKontak(UPDATED_TECH_NO_KONTAK)
            .techEmail(UPDATED_TECH_EMAIL)
            .acctNoKontak(UPDATED_ACCT_NO_KONTAK)
            .acctEmail(UPDATED_ACCT_EMAIL)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOne(UPDATED_UPDATE_ONE)
            .email(UPDATED_EMAIL)
            .sk(UPDATED_SK);

        restLeasingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedLeasing.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedLeasing))
            )
            .andExpect(status().isOk());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
        Leasing testLeasing = leasingList.get(leasingList.size() - 1);
        assertThat(testLeasing.getKode()).isEqualTo(UPDATED_KODE);
        assertThat(testLeasing.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testLeasing.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testLeasing.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testLeasing.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testLeasing.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testLeasing.getKelurahan()).isEqualTo(UPDATED_KELURAHAN);
        assertThat(testLeasing.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testLeasing.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testLeasing.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testLeasing.getKodePose()).isEqualTo(UPDATED_KODE_POSE);
        assertThat(testLeasing.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testLeasing.getTechNoKontak()).isEqualTo(UPDATED_TECH_NO_KONTAK);
        assertThat(testLeasing.getTechEmail()).isEqualTo(UPDATED_TECH_EMAIL);
        assertThat(testLeasing.getAcctNoKontak()).isEqualTo(UPDATED_ACCT_NO_KONTAK);
        assertThat(testLeasing.getAcctEmail()).isEqualTo(UPDATED_ACCT_EMAIL);
        assertThat(testLeasing.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testLeasing.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testLeasing.getUpdateOne()).isEqualTo(UPDATED_UPDATE_ONE);
        assertThat(testLeasing.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testLeasing.getSk()).isEqualTo(UPDATED_SK);
    }

    @Test
    @Transactional
    void putNonExistingLeasing() throws Exception {
        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();
        leasing.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLeasingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, leasing.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(leasing))
            )
            .andExpect(status().isBadRequest());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLeasing() throws Exception {
        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();
        leasing.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLeasingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(leasing))
            )
            .andExpect(status().isBadRequest());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLeasing() throws Exception {
        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();
        leasing.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLeasingMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(leasing)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLeasingWithPatch() throws Exception {
        // Initialize the database
        leasingRepository.saveAndFlush(leasing);

        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();

        // Update the leasing using partial update
        Leasing partialUpdatedLeasing = new Leasing();
        partialUpdatedLeasing.setId(leasing.getId());

        partialUpdatedLeasing
            .kode(UPDATED_KODE)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .rw(UPDATED_RW)
            .npwp(UPDATED_NPWP)
            .techEmail(UPDATED_TECH_EMAIL)
            .acctEmail(UPDATED_ACCT_EMAIL);

        restLeasingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLeasing.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLeasing))
            )
            .andExpect(status().isOk());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
        Leasing testLeasing = leasingList.get(leasingList.size() - 1);
        assertThat(testLeasing.getKode()).isEqualTo(UPDATED_KODE);
        assertThat(testLeasing.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testLeasing.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testLeasing.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testLeasing.getProvinsi()).isEqualTo(DEFAULT_PROVINSI);
        assertThat(testLeasing.getKecamatan()).isEqualTo(DEFAULT_KECAMATAN);
        assertThat(testLeasing.getKelurahan()).isEqualTo(DEFAULT_KELURAHAN);
        assertThat(testLeasing.getRt()).isEqualTo(DEFAULT_RT);
        assertThat(testLeasing.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testLeasing.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testLeasing.getKodePose()).isEqualTo(DEFAULT_KODE_POSE);
        assertThat(testLeasing.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
        assertThat(testLeasing.getTechNoKontak()).isEqualTo(DEFAULT_TECH_NO_KONTAK);
        assertThat(testLeasing.getTechEmail()).isEqualTo(UPDATED_TECH_EMAIL);
        assertThat(testLeasing.getAcctNoKontak()).isEqualTo(DEFAULT_ACCT_NO_KONTAK);
        assertThat(testLeasing.getAcctEmail()).isEqualTo(UPDATED_ACCT_EMAIL);
        assertThat(testLeasing.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testLeasing.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testLeasing.getUpdateOne()).isEqualTo(DEFAULT_UPDATE_ONE);
        assertThat(testLeasing.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testLeasing.getSk()).isEqualTo(DEFAULT_SK);
    }

    @Test
    @Transactional
    void fullUpdateLeasingWithPatch() throws Exception {
        // Initialize the database
        leasingRepository.saveAndFlush(leasing);

        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();

        // Update the leasing using partial update
        Leasing partialUpdatedLeasing = new Leasing();
        partialUpdatedLeasing.setId(leasing.getId());

        partialUpdatedLeasing
            .kode(UPDATED_KODE)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kecamatan(UPDATED_KECAMATAN)
            .kelurahan(UPDATED_KELURAHAN)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .npwp(UPDATED_NPWP)
            .kodePose(UPDATED_KODE_POSE)
            .noKontak(UPDATED_NO_KONTAK)
            .techNoKontak(UPDATED_TECH_NO_KONTAK)
            .techEmail(UPDATED_TECH_EMAIL)
            .acctNoKontak(UPDATED_ACCT_NO_KONTAK)
            .acctEmail(UPDATED_ACCT_EMAIL)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOne(UPDATED_UPDATE_ONE)
            .email(UPDATED_EMAIL)
            .sk(UPDATED_SK);

        restLeasingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLeasing.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLeasing))
            )
            .andExpect(status().isOk());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
        Leasing testLeasing = leasingList.get(leasingList.size() - 1);
        assertThat(testLeasing.getKode()).isEqualTo(UPDATED_KODE);
        assertThat(testLeasing.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testLeasing.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testLeasing.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testLeasing.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testLeasing.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testLeasing.getKelurahan()).isEqualTo(UPDATED_KELURAHAN);
        assertThat(testLeasing.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testLeasing.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testLeasing.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testLeasing.getKodePose()).isEqualTo(UPDATED_KODE_POSE);
        assertThat(testLeasing.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testLeasing.getTechNoKontak()).isEqualTo(UPDATED_TECH_NO_KONTAK);
        assertThat(testLeasing.getTechEmail()).isEqualTo(UPDATED_TECH_EMAIL);
        assertThat(testLeasing.getAcctNoKontak()).isEqualTo(UPDATED_ACCT_NO_KONTAK);
        assertThat(testLeasing.getAcctEmail()).isEqualTo(UPDATED_ACCT_EMAIL);
        assertThat(testLeasing.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testLeasing.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testLeasing.getUpdateOne()).isEqualTo(UPDATED_UPDATE_ONE);
        assertThat(testLeasing.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testLeasing.getSk()).isEqualTo(UPDATED_SK);
    }

    @Test
    @Transactional
    void patchNonExistingLeasing() throws Exception {
        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();
        leasing.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLeasingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, leasing.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(leasing))
            )
            .andExpect(status().isBadRequest());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLeasing() throws Exception {
        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();
        leasing.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLeasingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(leasing))
            )
            .andExpect(status().isBadRequest());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLeasing() throws Exception {
        int databaseSizeBeforeUpdate = leasingRepository.findAll().size();
        leasing.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLeasingMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(leasing)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Leasing in the database
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLeasing() throws Exception {
        // Initialize the database
        leasingRepository.saveAndFlush(leasing);

        int databaseSizeBeforeDelete = leasingRepository.findAll().size();

        // Delete the leasing
        restLeasingMockMvc
            .perform(delete(ENTITY_API_URL_ID, leasing.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Leasing> leasingList = leasingRepository.findAll();
        assertThat(leasingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
