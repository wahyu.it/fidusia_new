package com.project.fidusia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Orang;
import com.project.fidusia.repository.OrangRepository;
import jakarta.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OrangResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrangResourceIT {

    private static final String DEFAULT_PENGGUNAAN = "AAAAAAAAAA";
    private static final String UPDATED_PENGGUNAAN = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_JENIS_KELAMIN = "AAAAAAAAAA";
    private static final String UPDATED_JENIS_KELAMIN = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS_KAWIN = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_KAWIN = "BBBBBBBBBB";

    private static final String DEFAULT_KELAHIRAN = "AAAAAAAAAA";
    private static final String UPDATED_KELAHIRAN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TGL_LAHIR = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_LAHIR = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_PEKERJAAN = "AAAAAAAAAA";
    private static final String UPDATED_PEKERJAAN = "BBBBBBBBBB";

    private static final String DEFAULT_WARGA_NEGARA = "AAAAAAAAAA";
    private static final String UPDATED_WARGA_NEGARA = "BBBBBBBBBB";

    private static final String DEFAULT_JENIS_ID = "AAAAAAAAAA";
    private static final String UPDATED_JENIS_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NO_ID = "AAAAAAAAAA";
    private static final String UPDATED_NO_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ALAMAT = "AAAAAAAAAA";
    private static final String UPDATED_ALAMAT = "BBBBBBBBBB";

    private static final String DEFAULT_RT = "AAAAAAAAAA";
    private static final String UPDATED_RT = "BBBBBBBBBB";

    private static final String DEFAULT_RW = "AAAAAAAAAA";
    private static final String UPDATED_RW = "BBBBBBBBBB";

    private static final String DEFAULT_KELURAHAN = "AAAAAAAAAA";
    private static final String UPDATED_KELURAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_KECAMATAN = "AAAAAAAAAA";
    private static final String UPDATED_KECAMATAN = "BBBBBBBBBB";

    private static final String DEFAULT_KOTA = "AAAAAAAAAA";
    private static final String UPDATED_KOTA = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINSI = "AAAAAAAAAA";
    private static final String UPDATED_PROVINSI = "BBBBBBBBBB";

    private static final String DEFAULT_KODE_POS = "AAAAAAAAAA";
    private static final String UPDATED_KODE_POS = "BBBBBBBBBB";

    private static final String DEFAULT_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_NO_KONTAK = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/orangs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrangRepository orangRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrangMockMvc;

    private Orang orang;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Orang createEntity(EntityManager em) {
        Orang orang = new Orang()
            .penggunaan(DEFAULT_PENGGUNAAN)
            .nama(DEFAULT_NAMA)
            .jenisKelamin(DEFAULT_JENIS_KELAMIN)
            .statusKawin(DEFAULT_STATUS_KAWIN)
            .kelahiran(DEFAULT_KELAHIRAN)
            .tglLahir(DEFAULT_TGL_LAHIR)
            .pekerjaan(DEFAULT_PEKERJAAN)
            .wargaNegara(DEFAULT_WARGA_NEGARA)
            .jenisId(DEFAULT_JENIS_ID)
            .noId(DEFAULT_NO_ID)
            .alamat(DEFAULT_ALAMAT)
            .rt(DEFAULT_RT)
            .rw(DEFAULT_RW)
            .kelurahan(DEFAULT_KELURAHAN)
            .kecamatan(DEFAULT_KECAMATAN)
            .kota(DEFAULT_KOTA)
            .provinsi(DEFAULT_PROVINSI)
            .kodePos(DEFAULT_KODE_POS)
            .noKontak(DEFAULT_NO_KONTAK);
        return orang;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Orang createUpdatedEntity(EntityManager em) {
        Orang orang = new Orang()
            .penggunaan(UPDATED_PENGGUNAAN)
            .nama(UPDATED_NAMA)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .statusKawin(UPDATED_STATUS_KAWIN)
            .kelahiran(UPDATED_KELAHIRAN)
            .tglLahir(UPDATED_TGL_LAHIR)
            .pekerjaan(UPDATED_PEKERJAAN)
            .wargaNegara(UPDATED_WARGA_NEGARA)
            .jenisId(UPDATED_JENIS_ID)
            .noId(UPDATED_NO_ID)
            .alamat(UPDATED_ALAMAT)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .kelurahan(UPDATED_KELURAHAN)
            .kecamatan(UPDATED_KECAMATAN)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK);
        return orang;
    }

    @BeforeEach
    public void initTest() {
        orang = createEntity(em);
    }

    @Test
    @Transactional
    void createOrang() throws Exception {
        int databaseSizeBeforeCreate = orangRepository.findAll().size();
        // Create the Orang
        restOrangMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orang)))
            .andExpect(status().isCreated());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeCreate + 1);
        Orang testOrang = orangList.get(orangList.size() - 1);
        assertThat(testOrang.getPenggunaan()).isEqualTo(DEFAULT_PENGGUNAAN);
        assertThat(testOrang.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testOrang.getJenisKelamin()).isEqualTo(DEFAULT_JENIS_KELAMIN);
        assertThat(testOrang.getStatusKawin()).isEqualTo(DEFAULT_STATUS_KAWIN);
        assertThat(testOrang.getKelahiran()).isEqualTo(DEFAULT_KELAHIRAN);
        assertThat(testOrang.getTglLahir()).isEqualTo(DEFAULT_TGL_LAHIR);
        assertThat(testOrang.getPekerjaan()).isEqualTo(DEFAULT_PEKERJAAN);
        assertThat(testOrang.getWargaNegara()).isEqualTo(DEFAULT_WARGA_NEGARA);
        assertThat(testOrang.getJenisId()).isEqualTo(DEFAULT_JENIS_ID);
        assertThat(testOrang.getNoId()).isEqualTo(DEFAULT_NO_ID);
        assertThat(testOrang.getAlamat()).isEqualTo(DEFAULT_ALAMAT);
        assertThat(testOrang.getRt()).isEqualTo(DEFAULT_RT);
        assertThat(testOrang.getRw()).isEqualTo(DEFAULT_RW);
        assertThat(testOrang.getKelurahan()).isEqualTo(DEFAULT_KELURAHAN);
        assertThat(testOrang.getKecamatan()).isEqualTo(DEFAULT_KECAMATAN);
        assertThat(testOrang.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testOrang.getProvinsi()).isEqualTo(DEFAULT_PROVINSI);
        assertThat(testOrang.getKodePos()).isEqualTo(DEFAULT_KODE_POS);
        assertThat(testOrang.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
    }

    @Test
    @Transactional
    void createOrangWithExistingId() throws Exception {
        // Create the Orang with an existing ID
        orang.setId(1L);

        int databaseSizeBeforeCreate = orangRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrangMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orang)))
            .andExpect(status().isBadRequest());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOrangs() throws Exception {
        // Initialize the database
        orangRepository.saveAndFlush(orang);

        // Get all the orangList
        restOrangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orang.getId().intValue())))
            .andExpect(jsonPath("$.[*].penggunaan").value(hasItem(DEFAULT_PENGGUNAAN)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].jenisKelamin").value(hasItem(DEFAULT_JENIS_KELAMIN)))
            .andExpect(jsonPath("$.[*].statusKawin").value(hasItem(DEFAULT_STATUS_KAWIN)))
            .andExpect(jsonPath("$.[*].kelahiran").value(hasItem(DEFAULT_KELAHIRAN)))
            .andExpect(jsonPath("$.[*].tglLahir").value(hasItem(DEFAULT_TGL_LAHIR.toString())))
            .andExpect(jsonPath("$.[*].pekerjaan").value(hasItem(DEFAULT_PEKERJAAN)))
            .andExpect(jsonPath("$.[*].wargaNegara").value(hasItem(DEFAULT_WARGA_NEGARA)))
            .andExpect(jsonPath("$.[*].jenisId").value(hasItem(DEFAULT_JENIS_ID)))
            .andExpect(jsonPath("$.[*].noId").value(hasItem(DEFAULT_NO_ID)))
            .andExpect(jsonPath("$.[*].alamat").value(hasItem(DEFAULT_ALAMAT)))
            .andExpect(jsonPath("$.[*].rt").value(hasItem(DEFAULT_RT)))
            .andExpect(jsonPath("$.[*].rw").value(hasItem(DEFAULT_RW)))
            .andExpect(jsonPath("$.[*].kelurahan").value(hasItem(DEFAULT_KELURAHAN)))
            .andExpect(jsonPath("$.[*].kecamatan").value(hasItem(DEFAULT_KECAMATAN)))
            .andExpect(jsonPath("$.[*].kota").value(hasItem(DEFAULT_KOTA)))
            .andExpect(jsonPath("$.[*].provinsi").value(hasItem(DEFAULT_PROVINSI)))
            .andExpect(jsonPath("$.[*].kodePos").value(hasItem(DEFAULT_KODE_POS)))
            .andExpect(jsonPath("$.[*].noKontak").value(hasItem(DEFAULT_NO_KONTAK)));
    }

    @Test
    @Transactional
    void getOrang() throws Exception {
        // Initialize the database
        orangRepository.saveAndFlush(orang);

        // Get the orang
        restOrangMockMvc
            .perform(get(ENTITY_API_URL_ID, orang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orang.getId().intValue()))
            .andExpect(jsonPath("$.penggunaan").value(DEFAULT_PENGGUNAAN))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.jenisKelamin").value(DEFAULT_JENIS_KELAMIN))
            .andExpect(jsonPath("$.statusKawin").value(DEFAULT_STATUS_KAWIN))
            .andExpect(jsonPath("$.kelahiran").value(DEFAULT_KELAHIRAN))
            .andExpect(jsonPath("$.tglLahir").value(DEFAULT_TGL_LAHIR.toString()))
            .andExpect(jsonPath("$.pekerjaan").value(DEFAULT_PEKERJAAN))
            .andExpect(jsonPath("$.wargaNegara").value(DEFAULT_WARGA_NEGARA))
            .andExpect(jsonPath("$.jenisId").value(DEFAULT_JENIS_ID))
            .andExpect(jsonPath("$.noId").value(DEFAULT_NO_ID))
            .andExpect(jsonPath("$.alamat").value(DEFAULT_ALAMAT))
            .andExpect(jsonPath("$.rt").value(DEFAULT_RT))
            .andExpect(jsonPath("$.rw").value(DEFAULT_RW))
            .andExpect(jsonPath("$.kelurahan").value(DEFAULT_KELURAHAN))
            .andExpect(jsonPath("$.kecamatan").value(DEFAULT_KECAMATAN))
            .andExpect(jsonPath("$.kota").value(DEFAULT_KOTA))
            .andExpect(jsonPath("$.provinsi").value(DEFAULT_PROVINSI))
            .andExpect(jsonPath("$.kodePos").value(DEFAULT_KODE_POS))
            .andExpect(jsonPath("$.noKontak").value(DEFAULT_NO_KONTAK));
    }

    @Test
    @Transactional
    void getNonExistingOrang() throws Exception {
        // Get the orang
        restOrangMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingOrang() throws Exception {
        // Initialize the database
        orangRepository.saveAndFlush(orang);

        int databaseSizeBeforeUpdate = orangRepository.findAll().size();

        // Update the orang
        Orang updatedOrang = orangRepository.findById(orang.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedOrang are not directly saved in db
        em.detach(updatedOrang);
        updatedOrang
            .penggunaan(UPDATED_PENGGUNAAN)
            .nama(UPDATED_NAMA)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .statusKawin(UPDATED_STATUS_KAWIN)
            .kelahiran(UPDATED_KELAHIRAN)
            .tglLahir(UPDATED_TGL_LAHIR)
            .pekerjaan(UPDATED_PEKERJAAN)
            .wargaNegara(UPDATED_WARGA_NEGARA)
            .jenisId(UPDATED_JENIS_ID)
            .noId(UPDATED_NO_ID)
            .alamat(UPDATED_ALAMAT)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .kelurahan(UPDATED_KELURAHAN)
            .kecamatan(UPDATED_KECAMATAN)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK);

        restOrangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOrang.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOrang))
            )
            .andExpect(status().isOk());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
        Orang testOrang = orangList.get(orangList.size() - 1);
        assertThat(testOrang.getPenggunaan()).isEqualTo(UPDATED_PENGGUNAAN);
        assertThat(testOrang.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testOrang.getJenisKelamin()).isEqualTo(UPDATED_JENIS_KELAMIN);
        assertThat(testOrang.getStatusKawin()).isEqualTo(UPDATED_STATUS_KAWIN);
        assertThat(testOrang.getKelahiran()).isEqualTo(UPDATED_KELAHIRAN);
        assertThat(testOrang.getTglLahir()).isEqualTo(UPDATED_TGL_LAHIR);
        assertThat(testOrang.getPekerjaan()).isEqualTo(UPDATED_PEKERJAAN);
        assertThat(testOrang.getWargaNegara()).isEqualTo(UPDATED_WARGA_NEGARA);
        assertThat(testOrang.getJenisId()).isEqualTo(UPDATED_JENIS_ID);
        assertThat(testOrang.getNoId()).isEqualTo(UPDATED_NO_ID);
        assertThat(testOrang.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testOrang.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testOrang.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testOrang.getKelurahan()).isEqualTo(UPDATED_KELURAHAN);
        assertThat(testOrang.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testOrang.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testOrang.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testOrang.getKodePos()).isEqualTo(UPDATED_KODE_POS);
        assertThat(testOrang.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
    }

    @Test
    @Transactional
    void putNonExistingOrang() throws Exception {
        int databaseSizeBeforeUpdate = orangRepository.findAll().size();
        orang.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orang.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrang() throws Exception {
        int databaseSizeBeforeUpdate = orangRepository.findAll().size();
        orang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrang() throws Exception {
        int databaseSizeBeforeUpdate = orangRepository.findAll().size();
        orang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrangMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orang)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrangWithPatch() throws Exception {
        // Initialize the database
        orangRepository.saveAndFlush(orang);

        int databaseSizeBeforeUpdate = orangRepository.findAll().size();

        // Update the orang using partial update
        Orang partialUpdatedOrang = new Orang();
        partialUpdatedOrang.setId(orang.getId());

        partialUpdatedOrang
            .penggunaan(UPDATED_PENGGUNAAN)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .statusKawin(UPDATED_STATUS_KAWIN)
            .kelahiran(UPDATED_KELAHIRAN)
            .pekerjaan(UPDATED_PEKERJAAN)
            .wargaNegara(UPDATED_WARGA_NEGARA)
            .alamat(UPDATED_ALAMAT)
            .rt(UPDATED_RT)
            .kecamatan(UPDATED_KECAMATAN)
            .provinsi(UPDATED_PROVINSI)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK);

        restOrangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrang))
            )
            .andExpect(status().isOk());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
        Orang testOrang = orangList.get(orangList.size() - 1);
        assertThat(testOrang.getPenggunaan()).isEqualTo(UPDATED_PENGGUNAAN);
        assertThat(testOrang.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testOrang.getJenisKelamin()).isEqualTo(UPDATED_JENIS_KELAMIN);
        assertThat(testOrang.getStatusKawin()).isEqualTo(UPDATED_STATUS_KAWIN);
        assertThat(testOrang.getKelahiran()).isEqualTo(UPDATED_KELAHIRAN);
        assertThat(testOrang.getTglLahir()).isEqualTo(DEFAULT_TGL_LAHIR);
        assertThat(testOrang.getPekerjaan()).isEqualTo(UPDATED_PEKERJAAN);
        assertThat(testOrang.getWargaNegara()).isEqualTo(UPDATED_WARGA_NEGARA);
        assertThat(testOrang.getJenisId()).isEqualTo(DEFAULT_JENIS_ID);
        assertThat(testOrang.getNoId()).isEqualTo(DEFAULT_NO_ID);
        assertThat(testOrang.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testOrang.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testOrang.getRw()).isEqualTo(DEFAULT_RW);
        assertThat(testOrang.getKelurahan()).isEqualTo(DEFAULT_KELURAHAN);
        assertThat(testOrang.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testOrang.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testOrang.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testOrang.getKodePos()).isEqualTo(UPDATED_KODE_POS);
        assertThat(testOrang.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
    }

    @Test
    @Transactional
    void fullUpdateOrangWithPatch() throws Exception {
        // Initialize the database
        orangRepository.saveAndFlush(orang);

        int databaseSizeBeforeUpdate = orangRepository.findAll().size();

        // Update the orang using partial update
        Orang partialUpdatedOrang = new Orang();
        partialUpdatedOrang.setId(orang.getId());

        partialUpdatedOrang
            .penggunaan(UPDATED_PENGGUNAAN)
            .nama(UPDATED_NAMA)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .statusKawin(UPDATED_STATUS_KAWIN)
            .kelahiran(UPDATED_KELAHIRAN)
            .tglLahir(UPDATED_TGL_LAHIR)
            .pekerjaan(UPDATED_PEKERJAAN)
            .wargaNegara(UPDATED_WARGA_NEGARA)
            .jenisId(UPDATED_JENIS_ID)
            .noId(UPDATED_NO_ID)
            .alamat(UPDATED_ALAMAT)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .kelurahan(UPDATED_KELURAHAN)
            .kecamatan(UPDATED_KECAMATAN)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK);

        restOrangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrang))
            )
            .andExpect(status().isOk());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
        Orang testOrang = orangList.get(orangList.size() - 1);
        assertThat(testOrang.getPenggunaan()).isEqualTo(UPDATED_PENGGUNAAN);
        assertThat(testOrang.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testOrang.getJenisKelamin()).isEqualTo(UPDATED_JENIS_KELAMIN);
        assertThat(testOrang.getStatusKawin()).isEqualTo(UPDATED_STATUS_KAWIN);
        assertThat(testOrang.getKelahiran()).isEqualTo(UPDATED_KELAHIRAN);
        assertThat(testOrang.getTglLahir()).isEqualTo(UPDATED_TGL_LAHIR);
        assertThat(testOrang.getPekerjaan()).isEqualTo(UPDATED_PEKERJAAN);
        assertThat(testOrang.getWargaNegara()).isEqualTo(UPDATED_WARGA_NEGARA);
        assertThat(testOrang.getJenisId()).isEqualTo(UPDATED_JENIS_ID);
        assertThat(testOrang.getNoId()).isEqualTo(UPDATED_NO_ID);
        assertThat(testOrang.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testOrang.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testOrang.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testOrang.getKelurahan()).isEqualTo(UPDATED_KELURAHAN);
        assertThat(testOrang.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testOrang.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testOrang.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testOrang.getKodePos()).isEqualTo(UPDATED_KODE_POS);
        assertThat(testOrang.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
    }

    @Test
    @Transactional
    void patchNonExistingOrang() throws Exception {
        int databaseSizeBeforeUpdate = orangRepository.findAll().size();
        orang.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, orang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrang() throws Exception {
        int databaseSizeBeforeUpdate = orangRepository.findAll().size();
        orang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrang() throws Exception {
        int databaseSizeBeforeUpdate = orangRepository.findAll().size();
        orang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrangMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(orang)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Orang in the database
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrang() throws Exception {
        // Initialize the database
        orangRepository.saveAndFlush(orang);

        int databaseSizeBeforeDelete = orangRepository.findAll().size();

        // Delete the orang
        restOrangMockMvc
            .perform(delete(ENTITY_API_URL_ID, orang.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Orang> orangList = orangRepository.findAll();
        assertThat(orangList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
