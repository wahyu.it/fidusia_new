package com.project.fidusia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.OrderData;
import com.project.fidusia.repository.OrderDataRepository;
import jakarta.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OrderDataResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrderDataResourceIT {

    private static final String DEFAULT_PPK_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_PPK_NOMOR = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_PPK_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PPK_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NASABAH_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NASABAH_NAMA = "BBBBBBBBBB";

    private static final Integer DEFAULT_NASABAH_JENIS = 1;
    private static final Integer UPDATED_NASABAH_JENIS = 2;

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final String ENTITY_API_URL = "/api/order-data";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrderDataRepository orderDataRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderDataMockMvc;

    private OrderData orderData;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderData createEntity(EntityManager em) {
        OrderData orderData = new OrderData()
            .ppkNomor(DEFAULT_PPK_NOMOR)
            .ppkTanggal(DEFAULT_PPK_TANGGAL)
            .nasabahNama(DEFAULT_NASABAH_NAMA)
            .nasabahJenis(DEFAULT_NASABAH_JENIS)
            .tanggal(DEFAULT_TANGGAL)
            .status(DEFAULT_STATUS)
            .type(DEFAULT_TYPE);
        return orderData;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderData createUpdatedEntity(EntityManager em) {
        OrderData orderData = new OrderData()
            .ppkNomor(UPDATED_PPK_NOMOR)
            .ppkTanggal(UPDATED_PPK_TANGGAL)
            .nasabahNama(UPDATED_NASABAH_NAMA)
            .nasabahJenis(UPDATED_NASABAH_JENIS)
            .tanggal(UPDATED_TANGGAL)
            .status(UPDATED_STATUS)
            .type(UPDATED_TYPE);
        return orderData;
    }

    @BeforeEach
    public void initTest() {
        orderData = createEntity(em);
    }

    @Test
    @Transactional
    void createOrderData() throws Exception {
        int databaseSizeBeforeCreate = orderDataRepository.findAll().size();
        // Create the OrderData
        restOrderDataMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderData)))
            .andExpect(status().isCreated());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeCreate + 1);
        OrderData testOrderData = orderDataList.get(orderDataList.size() - 1);
        assertThat(testOrderData.getPpkNomor()).isEqualTo(DEFAULT_PPK_NOMOR);
        assertThat(testOrderData.getPpkTanggal()).isEqualTo(DEFAULT_PPK_TANGGAL);
        assertThat(testOrderData.getNasabahNama()).isEqualTo(DEFAULT_NASABAH_NAMA);
        assertThat(testOrderData.getNasabahJenis()).isEqualTo(DEFAULT_NASABAH_JENIS);
        assertThat(testOrderData.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testOrderData.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrderData.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    void createOrderDataWithExistingId() throws Exception {
        // Create the OrderData with an existing ID
        orderData.setId(1L);

        int databaseSizeBeforeCreate = orderDataRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderDataMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderData)))
            .andExpect(status().isBadRequest());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOrderData() throws Exception {
        // Initialize the database
        orderDataRepository.saveAndFlush(orderData);

        // Get all the orderDataList
        restOrderDataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderData.getId().intValue())))
            .andExpect(jsonPath("$.[*].ppkNomor").value(hasItem(DEFAULT_PPK_NOMOR)))
            .andExpect(jsonPath("$.[*].ppkTanggal").value(hasItem(DEFAULT_PPK_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].nasabahNama").value(hasItem(DEFAULT_NASABAH_NAMA)))
            .andExpect(jsonPath("$.[*].nasabahJenis").value(hasItem(DEFAULT_NASABAH_JENIS)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }

    @Test
    @Transactional
    void getOrderData() throws Exception {
        // Initialize the database
        orderDataRepository.saveAndFlush(orderData);

        // Get the orderData
        restOrderDataMockMvc
            .perform(get(ENTITY_API_URL_ID, orderData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderData.getId().intValue()))
            .andExpect(jsonPath("$.ppkNomor").value(DEFAULT_PPK_NOMOR))
            .andExpect(jsonPath("$.ppkTanggal").value(DEFAULT_PPK_TANGGAL.toString()))
            .andExpect(jsonPath("$.nasabahNama").value(DEFAULT_NASABAH_NAMA))
            .andExpect(jsonPath("$.nasabahJenis").value(DEFAULT_NASABAH_JENIS))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE));
    }

    @Test
    @Transactional
    void getNonExistingOrderData() throws Exception {
        // Get the orderData
        restOrderDataMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingOrderData() throws Exception {
        // Initialize the database
        orderDataRepository.saveAndFlush(orderData);

        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();

        // Update the orderData
        OrderData updatedOrderData = orderDataRepository.findById(orderData.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedOrderData are not directly saved in db
        em.detach(updatedOrderData);
        updatedOrderData
            .ppkNomor(UPDATED_PPK_NOMOR)
            .ppkTanggal(UPDATED_PPK_TANGGAL)
            .nasabahNama(UPDATED_NASABAH_NAMA)
            .nasabahJenis(UPDATED_NASABAH_JENIS)
            .tanggal(UPDATED_TANGGAL)
            .status(UPDATED_STATUS)
            .type(UPDATED_TYPE);

        restOrderDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOrderData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOrderData))
            )
            .andExpect(status().isOk());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
        OrderData testOrderData = orderDataList.get(orderDataList.size() - 1);
        assertThat(testOrderData.getPpkNomor()).isEqualTo(UPDATED_PPK_NOMOR);
        assertThat(testOrderData.getPpkTanggal()).isEqualTo(UPDATED_PPK_TANGGAL);
        assertThat(testOrderData.getNasabahNama()).isEqualTo(UPDATED_NASABAH_NAMA);
        assertThat(testOrderData.getNasabahJenis()).isEqualTo(UPDATED_NASABAH_JENIS);
        assertThat(testOrderData.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderData.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderData.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingOrderData() throws Exception {
        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();
        orderData.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orderData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderData))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrderData() throws Exception {
        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();
        orderData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderData))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrderData() throws Exception {
        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();
        orderData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderDataMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderData)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrderDataWithPatch() throws Exception {
        // Initialize the database
        orderDataRepository.saveAndFlush(orderData);

        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();

        // Update the orderData using partial update
        OrderData partialUpdatedOrderData = new OrderData();
        partialUpdatedOrderData.setId(orderData.getId());

        partialUpdatedOrderData
            .ppkNomor(UPDATED_PPK_NOMOR)
            .ppkTanggal(UPDATED_PPK_TANGGAL)
            .nasabahNama(UPDATED_NASABAH_NAMA)
            .tanggal(UPDATED_TANGGAL)
            .status(UPDATED_STATUS)
            .type(UPDATED_TYPE);

        restOrderDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderData))
            )
            .andExpect(status().isOk());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
        OrderData testOrderData = orderDataList.get(orderDataList.size() - 1);
        assertThat(testOrderData.getPpkNomor()).isEqualTo(UPDATED_PPK_NOMOR);
        assertThat(testOrderData.getPpkTanggal()).isEqualTo(UPDATED_PPK_TANGGAL);
        assertThat(testOrderData.getNasabahNama()).isEqualTo(UPDATED_NASABAH_NAMA);
        assertThat(testOrderData.getNasabahJenis()).isEqualTo(DEFAULT_NASABAH_JENIS);
        assertThat(testOrderData.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderData.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderData.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateOrderDataWithPatch() throws Exception {
        // Initialize the database
        orderDataRepository.saveAndFlush(orderData);

        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();

        // Update the orderData using partial update
        OrderData partialUpdatedOrderData = new OrderData();
        partialUpdatedOrderData.setId(orderData.getId());

        partialUpdatedOrderData
            .ppkNomor(UPDATED_PPK_NOMOR)
            .ppkTanggal(UPDATED_PPK_TANGGAL)
            .nasabahNama(UPDATED_NASABAH_NAMA)
            .nasabahJenis(UPDATED_NASABAH_JENIS)
            .tanggal(UPDATED_TANGGAL)
            .status(UPDATED_STATUS)
            .type(UPDATED_TYPE);

        restOrderDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderData))
            )
            .andExpect(status().isOk());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
        OrderData testOrderData = orderDataList.get(orderDataList.size() - 1);
        assertThat(testOrderData.getPpkNomor()).isEqualTo(UPDATED_PPK_NOMOR);
        assertThat(testOrderData.getPpkTanggal()).isEqualTo(UPDATED_PPK_TANGGAL);
        assertThat(testOrderData.getNasabahNama()).isEqualTo(UPDATED_NASABAH_NAMA);
        assertThat(testOrderData.getNasabahJenis()).isEqualTo(UPDATED_NASABAH_JENIS);
        assertThat(testOrderData.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderData.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderData.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingOrderData() throws Exception {
        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();
        orderData.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, orderData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderData))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrderData() throws Exception {
        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();
        orderData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderData))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrderData() throws Exception {
        int databaseSizeBeforeUpdate = orderDataRepository.findAll().size();
        orderData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderDataMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(orderData))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderData in the database
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrderData() throws Exception {
        // Initialize the database
        orderDataRepository.saveAndFlush(orderData);

        int databaseSizeBeforeDelete = orderDataRepository.findAll().size();

        // Delete the orderData
        restOrderDataMockMvc
            .perform(delete(ENTITY_API_URL_ID, orderData.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderData> orderDataList = orderDataRepository.findAll();
        assertThat(orderDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
