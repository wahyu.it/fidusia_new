package com.project.fidusia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Ppk;
import com.project.fidusia.repository.PpkRepository;
import jakarta.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PpkResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PpkResourceIT {

    private static final String DEFAULT_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_JENIS = "AAAAAAAAAA";
    private static final String UPDATED_JENIS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_TGL_SK = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_SK = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_NILAI_HUTANG_POKOK = 1L;
    private static final Long UPDATED_NILAI_HUTANG_POKOK = 2L;

    private static final Long DEFAULT_NILAI_PENJAMINAN = 1L;
    private static final Long UPDATED_NILAI_PENJAMINAN = 2L;

    private static final Long DEFAULT_PERIODE_TENOR = 1L;
    private static final Long UPDATED_PERIODE_TENOR = 2L;

    private static final LocalDate DEFAULT_TGL_CICIL_AWAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_CICIL_AWAL = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_TGL_CICIL_AKHIR = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_CICIL_AKHIR = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NSB_NAMA_DEBITUR = "AAAAAAAAAA";
    private static final String UPDATED_NSB_NAMA_DEBITUR = "BBBBBBBBBB";

    private static final String DEFAULT_NSB_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NSB_NAMA = "BBBBBBBBBB";

    private static final Integer DEFAULT_NSB_JENIS = 1;
    private static final Integer UPDATED_NSB_JENIS = 2;

    private static final String ENTITY_API_URL = "/api/ppks";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PpkRepository ppkRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPpkMockMvc;

    private Ppk ppk;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ppk createEntity(EntityManager em) {
        Ppk ppk = new Ppk()
            .nomor(DEFAULT_NOMOR)
            .nama(DEFAULT_NAMA)
            .jenis(DEFAULT_JENIS)
            .tanggal(DEFAULT_TANGGAL)
            .tglSk(DEFAULT_TGL_SK)
            .nilaiHutangPokok(DEFAULT_NILAI_HUTANG_POKOK)
            .nilaiPenjaminan(DEFAULT_NILAI_PENJAMINAN)
            .periodeTenor(DEFAULT_PERIODE_TENOR)
            .tglCicilAwal(DEFAULT_TGL_CICIL_AWAL)
            .tglCicilAkhir(DEFAULT_TGL_CICIL_AKHIR)
            .nsbNamaDebitur(DEFAULT_NSB_NAMA_DEBITUR)
            .nsbNama(DEFAULT_NSB_NAMA)
            .nsbJenis(DEFAULT_NSB_JENIS);
        return ppk;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ppk createUpdatedEntity(EntityManager em) {
        Ppk ppk = new Ppk()
            .nomor(UPDATED_NOMOR)
            .nama(UPDATED_NAMA)
            .jenis(UPDATED_JENIS)
            .tanggal(UPDATED_TANGGAL)
            .tglSk(UPDATED_TGL_SK)
            .nilaiHutangPokok(UPDATED_NILAI_HUTANG_POKOK)
            .nilaiPenjaminan(UPDATED_NILAI_PENJAMINAN)
            .periodeTenor(UPDATED_PERIODE_TENOR)
            .tglCicilAwal(UPDATED_TGL_CICIL_AWAL)
            .tglCicilAkhir(UPDATED_TGL_CICIL_AKHIR)
            .nsbNamaDebitur(UPDATED_NSB_NAMA_DEBITUR)
            .nsbNama(UPDATED_NSB_NAMA)
            .nsbJenis(UPDATED_NSB_JENIS);
        return ppk;
    }

    @BeforeEach
    public void initTest() {
        ppk = createEntity(em);
    }

    @Test
    @Transactional
    void createPpk() throws Exception {
        int databaseSizeBeforeCreate = ppkRepository.findAll().size();
        // Create the Ppk
        restPpkMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ppk)))
            .andExpect(status().isCreated());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeCreate + 1);
        Ppk testPpk = ppkList.get(ppkList.size() - 1);
        assertThat(testPpk.getNomor()).isEqualTo(DEFAULT_NOMOR);
        assertThat(testPpk.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testPpk.getJenis()).isEqualTo(DEFAULT_JENIS);
        assertThat(testPpk.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testPpk.getTglSk()).isEqualTo(DEFAULT_TGL_SK);
        assertThat(testPpk.getNilaiHutangPokok()).isEqualTo(DEFAULT_NILAI_HUTANG_POKOK);
        assertThat(testPpk.getNilaiPenjaminan()).isEqualTo(DEFAULT_NILAI_PENJAMINAN);
        assertThat(testPpk.getPeriodeTenor()).isEqualTo(DEFAULT_PERIODE_TENOR);
        assertThat(testPpk.getTglCicilAwal()).isEqualTo(DEFAULT_TGL_CICIL_AWAL);
        assertThat(testPpk.getTglCicilAkhir()).isEqualTo(DEFAULT_TGL_CICIL_AKHIR);
        assertThat(testPpk.getNsbNamaDebitur()).isEqualTo(DEFAULT_NSB_NAMA_DEBITUR);
        assertThat(testPpk.getNsbNama()).isEqualTo(DEFAULT_NSB_NAMA);
        assertThat(testPpk.getNsbJenis()).isEqualTo(DEFAULT_NSB_JENIS);
    }

    @Test
    @Transactional
    void createPpkWithExistingId() throws Exception {
        // Create the Ppk with an existing ID
        ppk.setId(1L);

        int databaseSizeBeforeCreate = ppkRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPpkMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ppk)))
            .andExpect(status().isBadRequest());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPpks() throws Exception {
        // Initialize the database
        ppkRepository.saveAndFlush(ppk);

        // Get all the ppkList
        restPpkMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ppk.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomor").value(hasItem(DEFAULT_NOMOR)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].jenis").value(hasItem(DEFAULT_JENIS)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].tglSk").value(hasItem(DEFAULT_TGL_SK.toString())))
            .andExpect(jsonPath("$.[*].nilaiHutangPokok").value(hasItem(DEFAULT_NILAI_HUTANG_POKOK.intValue())))
            .andExpect(jsonPath("$.[*].nilaiPenjaminan").value(hasItem(DEFAULT_NILAI_PENJAMINAN.intValue())))
            .andExpect(jsonPath("$.[*].periodeTenor").value(hasItem(DEFAULT_PERIODE_TENOR.intValue())))
            .andExpect(jsonPath("$.[*].tglCicilAwal").value(hasItem(DEFAULT_TGL_CICIL_AWAL.toString())))
            .andExpect(jsonPath("$.[*].tglCicilAkhir").value(hasItem(DEFAULT_TGL_CICIL_AKHIR.toString())))
            .andExpect(jsonPath("$.[*].nsbNamaDebitur").value(hasItem(DEFAULT_NSB_NAMA_DEBITUR)))
            .andExpect(jsonPath("$.[*].nsbNama").value(hasItem(DEFAULT_NSB_NAMA)))
            .andExpect(jsonPath("$.[*].nsbJenis").value(hasItem(DEFAULT_NSB_JENIS)));
    }

    @Test
    @Transactional
    void getPpk() throws Exception {
        // Initialize the database
        ppkRepository.saveAndFlush(ppk);

        // Get the ppk
        restPpkMockMvc
            .perform(get(ENTITY_API_URL_ID, ppk.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ppk.getId().intValue()))
            .andExpect(jsonPath("$.nomor").value(DEFAULT_NOMOR))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.jenis").value(DEFAULT_JENIS))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.tglSk").value(DEFAULT_TGL_SK.toString()))
            .andExpect(jsonPath("$.nilaiHutangPokok").value(DEFAULT_NILAI_HUTANG_POKOK.intValue()))
            .andExpect(jsonPath("$.nilaiPenjaminan").value(DEFAULT_NILAI_PENJAMINAN.intValue()))
            .andExpect(jsonPath("$.periodeTenor").value(DEFAULT_PERIODE_TENOR.intValue()))
            .andExpect(jsonPath("$.tglCicilAwal").value(DEFAULT_TGL_CICIL_AWAL.toString()))
            .andExpect(jsonPath("$.tglCicilAkhir").value(DEFAULT_TGL_CICIL_AKHIR.toString()))
            .andExpect(jsonPath("$.nsbNamaDebitur").value(DEFAULT_NSB_NAMA_DEBITUR))
            .andExpect(jsonPath("$.nsbNama").value(DEFAULT_NSB_NAMA))
            .andExpect(jsonPath("$.nsbJenis").value(DEFAULT_NSB_JENIS));
    }

    @Test
    @Transactional
    void getNonExistingPpk() throws Exception {
        // Get the ppk
        restPpkMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPpk() throws Exception {
        // Initialize the database
        ppkRepository.saveAndFlush(ppk);

        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();

        // Update the ppk
        Ppk updatedPpk = ppkRepository.findById(ppk.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedPpk are not directly saved in db
        em.detach(updatedPpk);
        updatedPpk
            .nomor(UPDATED_NOMOR)
            .nama(UPDATED_NAMA)
            .jenis(UPDATED_JENIS)
            .tanggal(UPDATED_TANGGAL)
            .tglSk(UPDATED_TGL_SK)
            .nilaiHutangPokok(UPDATED_NILAI_HUTANG_POKOK)
            .nilaiPenjaminan(UPDATED_NILAI_PENJAMINAN)
            .periodeTenor(UPDATED_PERIODE_TENOR)
            .tglCicilAwal(UPDATED_TGL_CICIL_AWAL)
            .tglCicilAkhir(UPDATED_TGL_CICIL_AKHIR)
            .nsbNamaDebitur(UPDATED_NSB_NAMA_DEBITUR)
            .nsbNama(UPDATED_NSB_NAMA)
            .nsbJenis(UPDATED_NSB_JENIS);

        restPpkMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPpk.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPpk))
            )
            .andExpect(status().isOk());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
        Ppk testPpk = ppkList.get(ppkList.size() - 1);
        assertThat(testPpk.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testPpk.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testPpk.getJenis()).isEqualTo(UPDATED_JENIS);
        assertThat(testPpk.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testPpk.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testPpk.getNilaiHutangPokok()).isEqualTo(UPDATED_NILAI_HUTANG_POKOK);
        assertThat(testPpk.getNilaiPenjaminan()).isEqualTo(UPDATED_NILAI_PENJAMINAN);
        assertThat(testPpk.getPeriodeTenor()).isEqualTo(UPDATED_PERIODE_TENOR);
        assertThat(testPpk.getTglCicilAwal()).isEqualTo(UPDATED_TGL_CICIL_AWAL);
        assertThat(testPpk.getTglCicilAkhir()).isEqualTo(UPDATED_TGL_CICIL_AKHIR);
        assertThat(testPpk.getNsbNamaDebitur()).isEqualTo(UPDATED_NSB_NAMA_DEBITUR);
        assertThat(testPpk.getNsbNama()).isEqualTo(UPDATED_NSB_NAMA);
        assertThat(testPpk.getNsbJenis()).isEqualTo(UPDATED_NSB_JENIS);
    }

    @Test
    @Transactional
    void putNonExistingPpk() throws Exception {
        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();
        ppk.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPpkMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ppk.getId()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ppk))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPpk() throws Exception {
        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();
        ppk.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPpkMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ppk))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPpk() throws Exception {
        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();
        ppk.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPpkMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ppk)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePpkWithPatch() throws Exception {
        // Initialize the database
        ppkRepository.saveAndFlush(ppk);

        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();

        // Update the ppk using partial update
        Ppk partialUpdatedPpk = new Ppk();
        partialUpdatedPpk.setId(ppk.getId());

        partialUpdatedPpk
            .nomor(UPDATED_NOMOR)
            .tglSk(UPDATED_TGL_SK)
            .nilaiHutangPokok(UPDATED_NILAI_HUTANG_POKOK)
            .nilaiPenjaminan(UPDATED_NILAI_PENJAMINAN)
            .periodeTenor(UPDATED_PERIODE_TENOR)
            .tglCicilAwal(UPDATED_TGL_CICIL_AWAL)
            .nsbNamaDebitur(UPDATED_NSB_NAMA_DEBITUR)
            .nsbNama(UPDATED_NSB_NAMA);

        restPpkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPpk.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPpk))
            )
            .andExpect(status().isOk());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
        Ppk testPpk = ppkList.get(ppkList.size() - 1);
        assertThat(testPpk.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testPpk.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testPpk.getJenis()).isEqualTo(DEFAULT_JENIS);
        assertThat(testPpk.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testPpk.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testPpk.getNilaiHutangPokok()).isEqualTo(UPDATED_NILAI_HUTANG_POKOK);
        assertThat(testPpk.getNilaiPenjaminan()).isEqualTo(UPDATED_NILAI_PENJAMINAN);
        assertThat(testPpk.getPeriodeTenor()).isEqualTo(UPDATED_PERIODE_TENOR);
        assertThat(testPpk.getTglCicilAwal()).isEqualTo(UPDATED_TGL_CICIL_AWAL);
        assertThat(testPpk.getTglCicilAkhir()).isEqualTo(DEFAULT_TGL_CICIL_AKHIR);
        assertThat(testPpk.getNsbNamaDebitur()).isEqualTo(UPDATED_NSB_NAMA_DEBITUR);
        assertThat(testPpk.getNsbNama()).isEqualTo(UPDATED_NSB_NAMA);
        assertThat(testPpk.getNsbJenis()).isEqualTo(DEFAULT_NSB_JENIS);
    }

    @Test
    @Transactional
    void fullUpdatePpkWithPatch() throws Exception {
        // Initialize the database
        ppkRepository.saveAndFlush(ppk);

        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();

        // Update the ppk using partial update
        Ppk partialUpdatedPpk = new Ppk();
        partialUpdatedPpk.setId(ppk.getId());

        partialUpdatedPpk
            .nomor(UPDATED_NOMOR)
            .nama(UPDATED_NAMA)
            .jenis(UPDATED_JENIS)
            .tanggal(UPDATED_TANGGAL)
            .tglSk(UPDATED_TGL_SK)
            .nilaiHutangPokok(UPDATED_NILAI_HUTANG_POKOK)
            .nilaiPenjaminan(UPDATED_NILAI_PENJAMINAN)
            .periodeTenor(UPDATED_PERIODE_TENOR)
            .tglCicilAwal(UPDATED_TGL_CICIL_AWAL)
            .tglCicilAkhir(UPDATED_TGL_CICIL_AKHIR)
            .nsbNamaDebitur(UPDATED_NSB_NAMA_DEBITUR)
            .nsbNama(UPDATED_NSB_NAMA)
            .nsbJenis(UPDATED_NSB_JENIS);

        restPpkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPpk.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPpk))
            )
            .andExpect(status().isOk());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
        Ppk testPpk = ppkList.get(ppkList.size() - 1);
        assertThat(testPpk.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testPpk.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testPpk.getJenis()).isEqualTo(UPDATED_JENIS);
        assertThat(testPpk.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testPpk.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testPpk.getNilaiHutangPokok()).isEqualTo(UPDATED_NILAI_HUTANG_POKOK);
        assertThat(testPpk.getNilaiPenjaminan()).isEqualTo(UPDATED_NILAI_PENJAMINAN);
        assertThat(testPpk.getPeriodeTenor()).isEqualTo(UPDATED_PERIODE_TENOR);
        assertThat(testPpk.getTglCicilAwal()).isEqualTo(UPDATED_TGL_CICIL_AWAL);
        assertThat(testPpk.getTglCicilAkhir()).isEqualTo(UPDATED_TGL_CICIL_AKHIR);
        assertThat(testPpk.getNsbNamaDebitur()).isEqualTo(UPDATED_NSB_NAMA_DEBITUR);
        assertThat(testPpk.getNsbNama()).isEqualTo(UPDATED_NSB_NAMA);
        assertThat(testPpk.getNsbJenis()).isEqualTo(UPDATED_NSB_JENIS);
    }

    @Test
    @Transactional
    void patchNonExistingPpk() throws Exception {
        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();
        ppk.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPpkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, ppk.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ppk))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPpk() throws Exception {
        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();
        ppk.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPpkMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ppk))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPpk() throws Exception {
        int databaseSizeBeforeUpdate = ppkRepository.findAll().size();
        ppk.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPpkMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(ppk)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Ppk in the database
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePpk() throws Exception {
        // Initialize the database
        ppkRepository.saveAndFlush(ppk);

        int databaseSizeBeforeDelete = ppkRepository.findAll().size();

        // Delete the ppk
        restPpkMockMvc.perform(delete(ENTITY_API_URL_ID, ppk.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Ppk> ppkList = ppkRepository.findAll();
        assertThat(ppkList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
