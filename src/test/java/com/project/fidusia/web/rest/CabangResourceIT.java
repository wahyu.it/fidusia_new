package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Cabang;
import com.project.fidusia.repository.CabangRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CabangResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CabangResourceIT {

    private static final String DEFAULT_KODE = "AAAAAAAAAA";
    private static final String UPDATED_KODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_ALAMAT = "AAAAAAAAAA";
    private static final String UPDATED_ALAMAT = "BBBBBBBBBB";

    private static final String DEFAULT_KOTA = "AAAAAAAAAA";
    private static final String UPDATED_KOTA = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINSI = "AAAAAAAAAA";
    private static final String UPDATED_PROVINSI = "BBBBBBBBBB";

    private static final String DEFAULT_PENGADILAN_NEGERI = "AAAAAAAAAA";
    private static final String UPDATED_PENGADILAN_NEGERI = "BBBBBBBBBB";

    private static final String DEFAULT_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_NO_KONTAK = "BBBBBBBBBB";

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/cabangs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CabangRepository cabangRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCabangMockMvc;

    private Cabang cabang;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cabang createEntity(EntityManager em) {
        Cabang cabang = new Cabang()
            .kode(DEFAULT_KODE)
            .nama(DEFAULT_NAMA)
            .alamat(DEFAULT_ALAMAT)
            .kota(DEFAULT_KOTA)
            .provinsi(DEFAULT_PROVINSI)
            .pengadilanNegeri(DEFAULT_PENGADILAN_NEGERI)
            .noKontak(DEFAULT_NO_KONTAK)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON)
            .startDate(DEFAULT_START_DATE);
        return cabang;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cabang createUpdatedEntity(EntityManager em) {
        Cabang cabang = new Cabang()
            .kode(UPDATED_KODE)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .pengadilanNegeri(UPDATED_PENGADILAN_NEGERI)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .startDate(UPDATED_START_DATE);
        return cabang;
    }

    @BeforeEach
    public void initTest() {
        cabang = createEntity(em);
    }

    @Test
    @Transactional
    void createCabang() throws Exception {
        int databaseSizeBeforeCreate = cabangRepository.findAll().size();
        // Create the Cabang
        restCabangMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cabang)))
            .andExpect(status().isCreated());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeCreate + 1);
        Cabang testCabang = cabangList.get(cabangList.size() - 1);
        assertThat(testCabang.getKode()).isEqualTo(DEFAULT_KODE);
        assertThat(testCabang.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testCabang.getAlamat()).isEqualTo(DEFAULT_ALAMAT);
        assertThat(testCabang.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testCabang.getProvinsi()).isEqualTo(DEFAULT_PROVINSI);
        assertThat(testCabang.getPengadilanNegeri()).isEqualTo(DEFAULT_PENGADILAN_NEGERI);
        assertThat(testCabang.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
        assertThat(testCabang.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testCabang.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testCabang.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testCabang.getStartDate()).isEqualTo(DEFAULT_START_DATE);
    }

    @Test
    @Transactional
    void createCabangWithExistingId() throws Exception {
        // Create the Cabang with an existing ID
        cabang.setId(1L);

        int databaseSizeBeforeCreate = cabangRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCabangMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cabang)))
            .andExpect(status().isBadRequest());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCabangs() throws Exception {
        // Initialize the database
        cabangRepository.saveAndFlush(cabang);

        // Get all the cabangList
        restCabangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cabang.getId().intValue())))
            .andExpect(jsonPath("$.[*].kode").value(hasItem(DEFAULT_KODE)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].alamat").value(hasItem(DEFAULT_ALAMAT)))
            .andExpect(jsonPath("$.[*].kota").value(hasItem(DEFAULT_KOTA)))
            .andExpect(jsonPath("$.[*].provinsi").value(hasItem(DEFAULT_PROVINSI)))
            .andExpect(jsonPath("$.[*].pengadilanNegeri").value(hasItem(DEFAULT_PENGADILAN_NEGERI)))
            .andExpect(jsonPath("$.[*].noKontak").value(hasItem(DEFAULT_NO_KONTAK)))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())));
    }

    @Test
    @Transactional
    void getCabang() throws Exception {
        // Initialize the database
        cabangRepository.saveAndFlush(cabang);

        // Get the cabang
        restCabangMockMvc
            .perform(get(ENTITY_API_URL_ID, cabang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cabang.getId().intValue()))
            .andExpect(jsonPath("$.kode").value(DEFAULT_KODE))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.alamat").value(DEFAULT_ALAMAT))
            .andExpect(jsonPath("$.kota").value(DEFAULT_KOTA))
            .andExpect(jsonPath("$.provinsi").value(DEFAULT_PROVINSI))
            .andExpect(jsonPath("$.pengadilanNegeri").value(DEFAULT_PENGADILAN_NEGERI))
            .andExpect(jsonPath("$.noKontak").value(DEFAULT_NO_KONTAK))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingCabang() throws Exception {
        // Get the cabang
        restCabangMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCabang() throws Exception {
        // Initialize the database
        cabangRepository.saveAndFlush(cabang);

        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();

        // Update the cabang
        Cabang updatedCabang = cabangRepository.findById(cabang.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedCabang are not directly saved in db
        em.detach(updatedCabang);
        updatedCabang
            .kode(UPDATED_KODE)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .pengadilanNegeri(UPDATED_PENGADILAN_NEGERI)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .startDate(UPDATED_START_DATE);

        restCabangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCabang.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCabang))
            )
            .andExpect(status().isOk());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
        Cabang testCabang = cabangList.get(cabangList.size() - 1);
        assertThat(testCabang.getKode()).isEqualTo(UPDATED_KODE);
        assertThat(testCabang.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testCabang.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testCabang.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testCabang.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testCabang.getPengadilanNegeri()).isEqualTo(UPDATED_PENGADILAN_NEGERI);
        assertThat(testCabang.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testCabang.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testCabang.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testCabang.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testCabang.getStartDate()).isEqualTo(UPDATED_START_DATE);
    }

    @Test
    @Transactional
    void putNonExistingCabang() throws Exception {
        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();
        cabang.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCabangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cabang.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cabang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCabang() throws Exception {
        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();
        cabang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCabangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cabang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCabang() throws Exception {
        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();
        cabang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCabangMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cabang)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCabangWithPatch() throws Exception {
        // Initialize the database
        cabangRepository.saveAndFlush(cabang);

        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();

        // Update the cabang using partial update
        Cabang partialUpdatedCabang = new Cabang();
        partialUpdatedCabang.setId(cabang.getId());

        partialUpdatedCabang.pengadilanNegeri(UPDATED_PENGADILAN_NEGERI).noKontak(UPDATED_NO_KONTAK).recordStatus(UPDATED_RECORD_STATUS);

        restCabangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCabang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCabang))
            )
            .andExpect(status().isOk());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
        Cabang testCabang = cabangList.get(cabangList.size() - 1);
        assertThat(testCabang.getKode()).isEqualTo(DEFAULT_KODE);
        assertThat(testCabang.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testCabang.getAlamat()).isEqualTo(DEFAULT_ALAMAT);
        assertThat(testCabang.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testCabang.getProvinsi()).isEqualTo(DEFAULT_PROVINSI);
        assertThat(testCabang.getPengadilanNegeri()).isEqualTo(UPDATED_PENGADILAN_NEGERI);
        assertThat(testCabang.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testCabang.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testCabang.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testCabang.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testCabang.getStartDate()).isEqualTo(DEFAULT_START_DATE);
    }

    @Test
    @Transactional
    void fullUpdateCabangWithPatch() throws Exception {
        // Initialize the database
        cabangRepository.saveAndFlush(cabang);

        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();

        // Update the cabang using partial update
        Cabang partialUpdatedCabang = new Cabang();
        partialUpdatedCabang.setId(cabang.getId());

        partialUpdatedCabang
            .kode(UPDATED_KODE)
            .nama(UPDATED_NAMA)
            .alamat(UPDATED_ALAMAT)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .pengadilanNegeri(UPDATED_PENGADILAN_NEGERI)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .startDate(UPDATED_START_DATE);

        restCabangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCabang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCabang))
            )
            .andExpect(status().isOk());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
        Cabang testCabang = cabangList.get(cabangList.size() - 1);
        assertThat(testCabang.getKode()).isEqualTo(UPDATED_KODE);
        assertThat(testCabang.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testCabang.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testCabang.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testCabang.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testCabang.getPengadilanNegeri()).isEqualTo(UPDATED_PENGADILAN_NEGERI);
        assertThat(testCabang.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testCabang.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testCabang.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testCabang.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testCabang.getStartDate()).isEqualTo(UPDATED_START_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingCabang() throws Exception {
        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();
        cabang.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCabangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, cabang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cabang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCabang() throws Exception {
        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();
        cabang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCabangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cabang))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCabang() throws Exception {
        int databaseSizeBeforeUpdate = cabangRepository.findAll().size();
        cabang.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCabangMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(cabang)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cabang in the database
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCabang() throws Exception {
        // Initialize the database
        cabangRepository.saveAndFlush(cabang);

        int databaseSizeBeforeDelete = cabangRepository.findAll().size();

        // Delete the cabang
        restCabangMockMvc
            .perform(delete(ENTITY_API_URL_ID, cabang.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cabang> cabangList = cabangRepository.findAll();
        assertThat(cabangList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
