package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.PenerimaKuasa;
import com.project.fidusia.repository.PenerimaKuasaRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PenerimaKuasaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PenerimaKuasaResourceIT {

    private static final String DEFAULT_TITEL = "AAAAAAAAAA";
    private static final String UPDATED_TITEL = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_KOMPARISI = "AAAAAAAAAA";
    private static final String UPDATED_KOMPARISI = "BBBBBBBBBB";

    private static final String DEFAULT_TTS = "AAAAAAAAAA";
    private static final String UPDATED_TTS = "BBBBBBBBBB";

    private static final String DEFAULT_SK = "AAAAAAAAAA";
    private static final String UPDATED_SK = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL_SK = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_SK = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_NO_KONTAK = "BBBBBBBBBB";

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/penerima-kuasas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PenerimaKuasaRepository penerimaKuasaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPenerimaKuasaMockMvc;

    private PenerimaKuasa penerimaKuasa;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PenerimaKuasa createEntity(EntityManager em) {
        PenerimaKuasa penerimaKuasa = new PenerimaKuasa()
            .titel(DEFAULT_TITEL)
            .nama(DEFAULT_NAMA)
            .komparisi(DEFAULT_KOMPARISI)
            .tts(DEFAULT_TTS)
            .sk(DEFAULT_SK)
            .tanggalSk(DEFAULT_TANGGAL_SK)
            .noKontak(DEFAULT_NO_KONTAK)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON);
        return penerimaKuasa;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PenerimaKuasa createUpdatedEntity(EntityManager em) {
        PenerimaKuasa penerimaKuasa = new PenerimaKuasa()
            .titel(UPDATED_TITEL)
            .nama(UPDATED_NAMA)
            .komparisi(UPDATED_KOMPARISI)
            .tts(UPDATED_TTS)
            .sk(UPDATED_SK)
            .tanggalSk(UPDATED_TANGGAL_SK)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);
        return penerimaKuasa;
    }

    @BeforeEach
    public void initTest() {
        penerimaKuasa = createEntity(em);
    }

    @Test
    @Transactional
    void createPenerimaKuasa() throws Exception {
        int databaseSizeBeforeCreate = penerimaKuasaRepository.findAll().size();
        // Create the PenerimaKuasa
        restPenerimaKuasaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(penerimaKuasa)))
            .andExpect(status().isCreated());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeCreate + 1);
        PenerimaKuasa testPenerimaKuasa = penerimaKuasaList.get(penerimaKuasaList.size() - 1);
        assertThat(testPenerimaKuasa.getTitel()).isEqualTo(DEFAULT_TITEL);
        assertThat(testPenerimaKuasa.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testPenerimaKuasa.getKomparisi()).isEqualTo(DEFAULT_KOMPARISI);
        assertThat(testPenerimaKuasa.getTts()).isEqualTo(DEFAULT_TTS);
        assertThat(testPenerimaKuasa.getSk()).isEqualTo(DEFAULT_SK);
        assertThat(testPenerimaKuasa.getTanggalSk()).isEqualTo(DEFAULT_TANGGAL_SK);
        assertThat(testPenerimaKuasa.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
        assertThat(testPenerimaKuasa.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testPenerimaKuasa.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testPenerimaKuasa.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    void createPenerimaKuasaWithExistingId() throws Exception {
        // Create the PenerimaKuasa with an existing ID
        penerimaKuasa.setId(1L);

        int databaseSizeBeforeCreate = penerimaKuasaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPenerimaKuasaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(penerimaKuasa)))
            .andExpect(status().isBadRequest());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPenerimaKuasas() throws Exception {
        // Initialize the database
        penerimaKuasaRepository.saveAndFlush(penerimaKuasa);

        // Get all the penerimaKuasaList
        restPenerimaKuasaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(penerimaKuasa.getId().intValue())))
            .andExpect(jsonPath("$.[*].titel").value(hasItem(DEFAULT_TITEL)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].komparisi").value(hasItem(DEFAULT_KOMPARISI)))
            .andExpect(jsonPath("$.[*].tts").value(hasItem(DEFAULT_TTS)))
            .andExpect(jsonPath("$.[*].sk").value(hasItem(DEFAULT_SK)))
            .andExpect(jsonPath("$.[*].tanggalSk").value(hasItem(DEFAULT_TANGGAL_SK.toString())))
            .andExpect(jsonPath("$.[*].noKontak").value(hasItem(DEFAULT_NO_KONTAK)))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))));
    }

    @Test
    @Transactional
    void getPenerimaKuasa() throws Exception {
        // Initialize the database
        penerimaKuasaRepository.saveAndFlush(penerimaKuasa);

        // Get the penerimaKuasa
        restPenerimaKuasaMockMvc
            .perform(get(ENTITY_API_URL_ID, penerimaKuasa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(penerimaKuasa.getId().intValue()))
            .andExpect(jsonPath("$.titel").value(DEFAULT_TITEL))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.komparisi").value(DEFAULT_KOMPARISI))
            .andExpect(jsonPath("$.tts").value(DEFAULT_TTS))
            .andExpect(jsonPath("$.sk").value(DEFAULT_SK))
            .andExpect(jsonPath("$.tanggalSk").value(DEFAULT_TANGGAL_SK.toString()))
            .andExpect(jsonPath("$.noKontak").value(DEFAULT_NO_KONTAK))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)));
    }

    @Test
    @Transactional
    void getNonExistingPenerimaKuasa() throws Exception {
        // Get the penerimaKuasa
        restPenerimaKuasaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPenerimaKuasa() throws Exception {
        // Initialize the database
        penerimaKuasaRepository.saveAndFlush(penerimaKuasa);

        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();

        // Update the penerimaKuasa
        PenerimaKuasa updatedPenerimaKuasa = penerimaKuasaRepository.findById(penerimaKuasa.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedPenerimaKuasa are not directly saved in db
        em.detach(updatedPenerimaKuasa);
        updatedPenerimaKuasa
            .titel(UPDATED_TITEL)
            .nama(UPDATED_NAMA)
            .komparisi(UPDATED_KOMPARISI)
            .tts(UPDATED_TTS)
            .sk(UPDATED_SK)
            .tanggalSk(UPDATED_TANGGAL_SK)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restPenerimaKuasaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPenerimaKuasa.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPenerimaKuasa))
            )
            .andExpect(status().isOk());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
        PenerimaKuasa testPenerimaKuasa = penerimaKuasaList.get(penerimaKuasaList.size() - 1);
        assertThat(testPenerimaKuasa.getTitel()).isEqualTo(UPDATED_TITEL);
        assertThat(testPenerimaKuasa.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testPenerimaKuasa.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
        assertThat(testPenerimaKuasa.getTts()).isEqualTo(UPDATED_TTS);
        assertThat(testPenerimaKuasa.getSk()).isEqualTo(UPDATED_SK);
        assertThat(testPenerimaKuasa.getTanggalSk()).isEqualTo(UPDATED_TANGGAL_SK);
        assertThat(testPenerimaKuasa.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testPenerimaKuasa.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testPenerimaKuasa.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testPenerimaKuasa.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void putNonExistingPenerimaKuasa() throws Exception {
        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();
        penerimaKuasa.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPenerimaKuasaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, penerimaKuasa.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(penerimaKuasa))
            )
            .andExpect(status().isBadRequest());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPenerimaKuasa() throws Exception {
        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();
        penerimaKuasa.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPenerimaKuasaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(penerimaKuasa))
            )
            .andExpect(status().isBadRequest());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPenerimaKuasa() throws Exception {
        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();
        penerimaKuasa.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPenerimaKuasaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(penerimaKuasa)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePenerimaKuasaWithPatch() throws Exception {
        // Initialize the database
        penerimaKuasaRepository.saveAndFlush(penerimaKuasa);

        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();

        // Update the penerimaKuasa using partial update
        PenerimaKuasa partialUpdatedPenerimaKuasa = new PenerimaKuasa();
        partialUpdatedPenerimaKuasa.setId(penerimaKuasa.getId());

        partialUpdatedPenerimaKuasa
            .nama(UPDATED_NAMA)
            .sk(UPDATED_SK)
            .tanggalSk(UPDATED_TANGGAL_SK)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restPenerimaKuasaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPenerimaKuasa.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPenerimaKuasa))
            )
            .andExpect(status().isOk());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
        PenerimaKuasa testPenerimaKuasa = penerimaKuasaList.get(penerimaKuasaList.size() - 1);
        assertThat(testPenerimaKuasa.getTitel()).isEqualTo(DEFAULT_TITEL);
        assertThat(testPenerimaKuasa.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testPenerimaKuasa.getKomparisi()).isEqualTo(DEFAULT_KOMPARISI);
        assertThat(testPenerimaKuasa.getTts()).isEqualTo(DEFAULT_TTS);
        assertThat(testPenerimaKuasa.getSk()).isEqualTo(UPDATED_SK);
        assertThat(testPenerimaKuasa.getTanggalSk()).isEqualTo(UPDATED_TANGGAL_SK);
        assertThat(testPenerimaKuasa.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testPenerimaKuasa.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testPenerimaKuasa.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testPenerimaKuasa.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void fullUpdatePenerimaKuasaWithPatch() throws Exception {
        // Initialize the database
        penerimaKuasaRepository.saveAndFlush(penerimaKuasa);

        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();

        // Update the penerimaKuasa using partial update
        PenerimaKuasa partialUpdatedPenerimaKuasa = new PenerimaKuasa();
        partialUpdatedPenerimaKuasa.setId(penerimaKuasa.getId());

        partialUpdatedPenerimaKuasa
            .titel(UPDATED_TITEL)
            .nama(UPDATED_NAMA)
            .komparisi(UPDATED_KOMPARISI)
            .tts(UPDATED_TTS)
            .sk(UPDATED_SK)
            .tanggalSk(UPDATED_TANGGAL_SK)
            .noKontak(UPDATED_NO_KONTAK)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restPenerimaKuasaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPenerimaKuasa.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPenerimaKuasa))
            )
            .andExpect(status().isOk());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
        PenerimaKuasa testPenerimaKuasa = penerimaKuasaList.get(penerimaKuasaList.size() - 1);
        assertThat(testPenerimaKuasa.getTitel()).isEqualTo(UPDATED_TITEL);
        assertThat(testPenerimaKuasa.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testPenerimaKuasa.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
        assertThat(testPenerimaKuasa.getTts()).isEqualTo(UPDATED_TTS);
        assertThat(testPenerimaKuasa.getSk()).isEqualTo(UPDATED_SK);
        assertThat(testPenerimaKuasa.getTanggalSk()).isEqualTo(UPDATED_TANGGAL_SK);
        assertThat(testPenerimaKuasa.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testPenerimaKuasa.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testPenerimaKuasa.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testPenerimaKuasa.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void patchNonExistingPenerimaKuasa() throws Exception {
        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();
        penerimaKuasa.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPenerimaKuasaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, penerimaKuasa.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(penerimaKuasa))
            )
            .andExpect(status().isBadRequest());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPenerimaKuasa() throws Exception {
        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();
        penerimaKuasa.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPenerimaKuasaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(penerimaKuasa))
            )
            .andExpect(status().isBadRequest());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPenerimaKuasa() throws Exception {
        int databaseSizeBeforeUpdate = penerimaKuasaRepository.findAll().size();
        penerimaKuasa.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPenerimaKuasaMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(penerimaKuasa))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PenerimaKuasa in the database
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePenerimaKuasa() throws Exception {
        // Initialize the database
        penerimaKuasaRepository.saveAndFlush(penerimaKuasa);

        int databaseSizeBeforeDelete = penerimaKuasaRepository.findAll().size();

        // Delete the penerimaKuasa
        restPenerimaKuasaMockMvc
            .perform(delete(ENTITY_API_URL_ID, penerimaKuasa.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PenerimaKuasa> penerimaKuasaList = penerimaKuasaRepository.findAll();
        assertThat(penerimaKuasaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
