package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Invoice;
import com.project.fidusia.repository.InvoiceRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link InvoiceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class InvoiceResourceIT {

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_TGL_ORDER = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_ORDER = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_JML_ORDER = 1L;
    private static final Long UPDATED_JML_ORDER = 2L;

    private static final Long DEFAULT_JML_ORDER_CANCEL = 1L;
    private static final Long UPDATED_JML_ORDER_CANCEL = 2L;

    private static final String DEFAULT_NO_PROFORMA_PNBP = "AAAAAAAAAA";
    private static final String UPDATED_NO_PROFORMA_PNBP = "BBBBBBBBBB";

    private static final String DEFAULT_NO_PROFORMA_JASA = "AAAAAAAAAA";
    private static final String UPDATED_NO_PROFORMA_JASA = "BBBBBBBBBB";

    private static final String DEFAULT_NO_BILLPYM_PNBP = "AAAAAAAAAA";
    private static final String UPDATED_NO_BILLPYM_PNBP = "BBBBBBBBBB";

    private static final String DEFAULT_NO_BILLPYM_JASA = "AAAAAAAAAA";
    private static final String UPDATED_NO_BILLPYM_JASA = "BBBBBBBBBB";

    private static final Long DEFAULT_AMOUNT_PNBP = 1L;
    private static final Long UPDATED_AMOUNT_PNBP = 2L;

    private static final Long DEFAULT_TAX_PNBP = 1L;
    private static final Long UPDATED_TAX_PNBP = 2L;

    private static final Long DEFAULT_TOTAL_AMOUNT_PNBP = 1L;
    private static final Long UPDATED_TOTAL_AMOUNT_PNBP = 2L;

    private static final Long DEFAULT_AMOUNT_JASA = 1L;
    private static final Long UPDATED_AMOUNT_JASA = 2L;

    private static final Long DEFAULT_VAT_JASA = 1L;
    private static final Long UPDATED_VAT_JASA = 2L;

    private static final Long DEFAULT_TAX_JASA = 1L;
    private static final Long UPDATED_TAX_JASA = 2L;

    private static final Long DEFAULT_TOTAL_AMOUNT_JASA = 1L;
    private static final Long UPDATED_TOTAL_AMOUNT_JASA = 2L;

    private static final String DEFAULT_FILE_PROFORMA_PNBP = "AAAAAAAAAA";
    private static final String UPDATED_FILE_PROFORMA_PNBP = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_PROFORMA_JASA = "AAAAAAAAAA";
    private static final String UPDATED_FILE_PROFORMA_JASA = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_BILLPYM_PNBP = "AAAAAAAAAA";
    private static final String UPDATED_FILE_BILLPYM_PNBP = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_BILLPYM_JASA = "AAAAAAAAAA";
    private static final String UPDATED_FILE_BILLPYM_JASA = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_BULK = "AAAAAAAAAA";
    private static final String UPDATED_FILE_BULK = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_TAX = "AAAAAAAAAA";
    private static final String UPDATED_FILE_TAX = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_TXT = "AAAAAAAAAA";
    private static final String UPDATED_FILE_TXT = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/invoices";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInvoiceMockMvc;

    private Invoice invoice;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Invoice createEntity(EntityManager em) {
        Invoice invoice = new Invoice()
            .type(DEFAULT_TYPE)
            .tanggal(DEFAULT_TANGGAL)
            .tglOrder(DEFAULT_TGL_ORDER)
            .jmlOrder(DEFAULT_JML_ORDER)
            .jmlOrderCancel(DEFAULT_JML_ORDER_CANCEL)
            .noProformaPnbp(DEFAULT_NO_PROFORMA_PNBP)
            .noProformaJasa(DEFAULT_NO_PROFORMA_JASA)
            .noBillpymPnbp(DEFAULT_NO_BILLPYM_PNBP)
            .noBillpymJasa(DEFAULT_NO_BILLPYM_JASA)
            .amountPnbp(DEFAULT_AMOUNT_PNBP)
            .taxPnbp(DEFAULT_TAX_PNBP)
            .totalAmountPnbp(DEFAULT_TOTAL_AMOUNT_PNBP)
            .amountJasa(DEFAULT_AMOUNT_JASA)
            .vatJasa(DEFAULT_VAT_JASA)
            .taxJasa(DEFAULT_TAX_JASA)
            .totalAmountJasa(DEFAULT_TOTAL_AMOUNT_JASA)
            .fileProformaPnbp(DEFAULT_FILE_PROFORMA_PNBP)
            .fileProformaJasa(DEFAULT_FILE_PROFORMA_JASA)
            .fileBillpymPnbp(DEFAULT_FILE_BILLPYM_PNBP)
            .fileBillpymJasa(DEFAULT_FILE_BILLPYM_JASA)
            .fileBulk(DEFAULT_FILE_BULK)
            .fileTax(DEFAULT_FILE_TAX)
            .fileTxt(DEFAULT_FILE_TXT)
            .status(DEFAULT_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON);
        return invoice;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Invoice createUpdatedEntity(EntityManager em) {
        Invoice invoice = new Invoice()
            .type(UPDATED_TYPE)
            .tanggal(UPDATED_TANGGAL)
            .tglOrder(UPDATED_TGL_ORDER)
            .jmlOrder(UPDATED_JML_ORDER)
            .jmlOrderCancel(UPDATED_JML_ORDER_CANCEL)
            .noProformaPnbp(UPDATED_NO_PROFORMA_PNBP)
            .noProformaJasa(UPDATED_NO_PROFORMA_JASA)
            .noBillpymPnbp(UPDATED_NO_BILLPYM_PNBP)
            .noBillpymJasa(UPDATED_NO_BILLPYM_JASA)
            .amountPnbp(UPDATED_AMOUNT_PNBP)
            .taxPnbp(UPDATED_TAX_PNBP)
            .totalAmountPnbp(UPDATED_TOTAL_AMOUNT_PNBP)
            .amountJasa(UPDATED_AMOUNT_JASA)
            .vatJasa(UPDATED_VAT_JASA)
            .taxJasa(UPDATED_TAX_JASA)
            .totalAmountJasa(UPDATED_TOTAL_AMOUNT_JASA)
            .fileProformaPnbp(UPDATED_FILE_PROFORMA_PNBP)
            .fileProformaJasa(UPDATED_FILE_PROFORMA_JASA)
            .fileBillpymPnbp(UPDATED_FILE_BILLPYM_PNBP)
            .fileBillpymJasa(UPDATED_FILE_BILLPYM_JASA)
            .fileBulk(UPDATED_FILE_BULK)
            .fileTax(UPDATED_FILE_TAX)
            .fileTxt(UPDATED_FILE_TXT)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);
        return invoice;
    }

    @BeforeEach
    public void initTest() {
        invoice = createEntity(em);
    }

    @Test
    @Transactional
    void createInvoice() throws Exception {
        int databaseSizeBeforeCreate = invoiceRepository.findAll().size();
        // Create the Invoice
        restInvoiceMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(invoice)))
            .andExpect(status().isCreated());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeCreate + 1);
        Invoice testInvoice = invoiceList.get(invoiceList.size() - 1);
        assertThat(testInvoice.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testInvoice.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testInvoice.getTglOrder()).isEqualTo(DEFAULT_TGL_ORDER);
        assertThat(testInvoice.getJmlOrder()).isEqualTo(DEFAULT_JML_ORDER);
        assertThat(testInvoice.getJmlOrderCancel()).isEqualTo(DEFAULT_JML_ORDER_CANCEL);
        assertThat(testInvoice.getNoProformaPnbp()).isEqualTo(DEFAULT_NO_PROFORMA_PNBP);
        assertThat(testInvoice.getNoProformaJasa()).isEqualTo(DEFAULT_NO_PROFORMA_JASA);
        assertThat(testInvoice.getNoBillpymPnbp()).isEqualTo(DEFAULT_NO_BILLPYM_PNBP);
        assertThat(testInvoice.getNoBillpymJasa()).isEqualTo(DEFAULT_NO_BILLPYM_JASA);
        assertThat(testInvoice.getAmountPnbp()).isEqualTo(DEFAULT_AMOUNT_PNBP);
        assertThat(testInvoice.getTaxPnbp()).isEqualTo(DEFAULT_TAX_PNBP);
        assertThat(testInvoice.getTotalAmountPnbp()).isEqualTo(DEFAULT_TOTAL_AMOUNT_PNBP);
        assertThat(testInvoice.getAmountJasa()).isEqualTo(DEFAULT_AMOUNT_JASA);
        assertThat(testInvoice.getVatJasa()).isEqualTo(DEFAULT_VAT_JASA);
        assertThat(testInvoice.getTaxJasa()).isEqualTo(DEFAULT_TAX_JASA);
        assertThat(testInvoice.getTotalAmountJasa()).isEqualTo(DEFAULT_TOTAL_AMOUNT_JASA);
        assertThat(testInvoice.getFileProformaPnbp()).isEqualTo(DEFAULT_FILE_PROFORMA_PNBP);
        assertThat(testInvoice.getFileProformaJasa()).isEqualTo(DEFAULT_FILE_PROFORMA_JASA);
        assertThat(testInvoice.getFileBillpymPnbp()).isEqualTo(DEFAULT_FILE_BILLPYM_PNBP);
        assertThat(testInvoice.getFileBillpymJasa()).isEqualTo(DEFAULT_FILE_BILLPYM_JASA);
        assertThat(testInvoice.getFileBulk()).isEqualTo(DEFAULT_FILE_BULK);
        assertThat(testInvoice.getFileTax()).isEqualTo(DEFAULT_FILE_TAX);
        assertThat(testInvoice.getFileTxt()).isEqualTo(DEFAULT_FILE_TXT);
        assertThat(testInvoice.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testInvoice.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testInvoice.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    void createInvoiceWithExistingId() throws Exception {
        // Create the Invoice with an existing ID
        invoice.setId(1L);

        int databaseSizeBeforeCreate = invoiceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvoiceMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(invoice)))
            .andExpect(status().isBadRequest());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllInvoices() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        // Get all the invoiceList
        restInvoiceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invoice.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].tglOrder").value(hasItem(DEFAULT_TGL_ORDER.toString())))
            .andExpect(jsonPath("$.[*].jmlOrder").value(hasItem(DEFAULT_JML_ORDER.intValue())))
            .andExpect(jsonPath("$.[*].jmlOrderCancel").value(hasItem(DEFAULT_JML_ORDER_CANCEL.intValue())))
            .andExpect(jsonPath("$.[*].noProformaPnbp").value(hasItem(DEFAULT_NO_PROFORMA_PNBP)))
            .andExpect(jsonPath("$.[*].noProformaJasa").value(hasItem(DEFAULT_NO_PROFORMA_JASA)))
            .andExpect(jsonPath("$.[*].noBillpymPnbp").value(hasItem(DEFAULT_NO_BILLPYM_PNBP)))
            .andExpect(jsonPath("$.[*].noBillpymJasa").value(hasItem(DEFAULT_NO_BILLPYM_JASA)))
            .andExpect(jsonPath("$.[*].amountPnbp").value(hasItem(DEFAULT_AMOUNT_PNBP.intValue())))
            .andExpect(jsonPath("$.[*].taxPnbp").value(hasItem(DEFAULT_TAX_PNBP.intValue())))
            .andExpect(jsonPath("$.[*].totalAmountPnbp").value(hasItem(DEFAULT_TOTAL_AMOUNT_PNBP.intValue())))
            .andExpect(jsonPath("$.[*].amountJasa").value(hasItem(DEFAULT_AMOUNT_JASA.intValue())))
            .andExpect(jsonPath("$.[*].vatJasa").value(hasItem(DEFAULT_VAT_JASA.intValue())))
            .andExpect(jsonPath("$.[*].taxJasa").value(hasItem(DEFAULT_TAX_JASA.intValue())))
            .andExpect(jsonPath("$.[*].totalAmountJasa").value(hasItem(DEFAULT_TOTAL_AMOUNT_JASA.intValue())))
            .andExpect(jsonPath("$.[*].fileProformaPnbp").value(hasItem(DEFAULT_FILE_PROFORMA_PNBP)))
            .andExpect(jsonPath("$.[*].fileProformaJasa").value(hasItem(DEFAULT_FILE_PROFORMA_JASA)))
            .andExpect(jsonPath("$.[*].fileBillpymPnbp").value(hasItem(DEFAULT_FILE_BILLPYM_PNBP)))
            .andExpect(jsonPath("$.[*].fileBillpymJasa").value(hasItem(DEFAULT_FILE_BILLPYM_JASA)))
            .andExpect(jsonPath("$.[*].fileBulk").value(hasItem(DEFAULT_FILE_BULK)))
            .andExpect(jsonPath("$.[*].fileTax").value(hasItem(DEFAULT_FILE_TAX)))
            .andExpect(jsonPath("$.[*].fileTxt").value(hasItem(DEFAULT_FILE_TXT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))));
    }

    @Test
    @Transactional
    void getInvoice() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        // Get the invoice
        restInvoiceMockMvc
            .perform(get(ENTITY_API_URL_ID, invoice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(invoice.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.tglOrder").value(DEFAULT_TGL_ORDER.toString()))
            .andExpect(jsonPath("$.jmlOrder").value(DEFAULT_JML_ORDER.intValue()))
            .andExpect(jsonPath("$.jmlOrderCancel").value(DEFAULT_JML_ORDER_CANCEL.intValue()))
            .andExpect(jsonPath("$.noProformaPnbp").value(DEFAULT_NO_PROFORMA_PNBP))
            .andExpect(jsonPath("$.noProformaJasa").value(DEFAULT_NO_PROFORMA_JASA))
            .andExpect(jsonPath("$.noBillpymPnbp").value(DEFAULT_NO_BILLPYM_PNBP))
            .andExpect(jsonPath("$.noBillpymJasa").value(DEFAULT_NO_BILLPYM_JASA))
            .andExpect(jsonPath("$.amountPnbp").value(DEFAULT_AMOUNT_PNBP.intValue()))
            .andExpect(jsonPath("$.taxPnbp").value(DEFAULT_TAX_PNBP.intValue()))
            .andExpect(jsonPath("$.totalAmountPnbp").value(DEFAULT_TOTAL_AMOUNT_PNBP.intValue()))
            .andExpect(jsonPath("$.amountJasa").value(DEFAULT_AMOUNT_JASA.intValue()))
            .andExpect(jsonPath("$.vatJasa").value(DEFAULT_VAT_JASA.intValue()))
            .andExpect(jsonPath("$.taxJasa").value(DEFAULT_TAX_JASA.intValue()))
            .andExpect(jsonPath("$.totalAmountJasa").value(DEFAULT_TOTAL_AMOUNT_JASA.intValue()))
            .andExpect(jsonPath("$.fileProformaPnbp").value(DEFAULT_FILE_PROFORMA_PNBP))
            .andExpect(jsonPath("$.fileProformaJasa").value(DEFAULT_FILE_PROFORMA_JASA))
            .andExpect(jsonPath("$.fileBillpymPnbp").value(DEFAULT_FILE_BILLPYM_PNBP))
            .andExpect(jsonPath("$.fileBillpymJasa").value(DEFAULT_FILE_BILLPYM_JASA))
            .andExpect(jsonPath("$.fileBulk").value(DEFAULT_FILE_BULK))
            .andExpect(jsonPath("$.fileTax").value(DEFAULT_FILE_TAX))
            .andExpect(jsonPath("$.fileTxt").value(DEFAULT_FILE_TXT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)));
    }

    @Test
    @Transactional
    void getNonExistingInvoice() throws Exception {
        // Get the invoice
        restInvoiceMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingInvoice() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();

        // Update the invoice
        Invoice updatedInvoice = invoiceRepository.findById(invoice.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedInvoice are not directly saved in db
        em.detach(updatedInvoice);
        updatedInvoice
            .type(UPDATED_TYPE)
            .tanggal(UPDATED_TANGGAL)
            .tglOrder(UPDATED_TGL_ORDER)
            .jmlOrder(UPDATED_JML_ORDER)
            .jmlOrderCancel(UPDATED_JML_ORDER_CANCEL)
            .noProformaPnbp(UPDATED_NO_PROFORMA_PNBP)
            .noProformaJasa(UPDATED_NO_PROFORMA_JASA)
            .noBillpymPnbp(UPDATED_NO_BILLPYM_PNBP)
            .noBillpymJasa(UPDATED_NO_BILLPYM_JASA)
            .amountPnbp(UPDATED_AMOUNT_PNBP)
            .taxPnbp(UPDATED_TAX_PNBP)
            .totalAmountPnbp(UPDATED_TOTAL_AMOUNT_PNBP)
            .amountJasa(UPDATED_AMOUNT_JASA)
            .vatJasa(UPDATED_VAT_JASA)
            .taxJasa(UPDATED_TAX_JASA)
            .totalAmountJasa(UPDATED_TOTAL_AMOUNT_JASA)
            .fileProformaPnbp(UPDATED_FILE_PROFORMA_PNBP)
            .fileProformaJasa(UPDATED_FILE_PROFORMA_JASA)
            .fileBillpymPnbp(UPDATED_FILE_BILLPYM_PNBP)
            .fileBillpymJasa(UPDATED_FILE_BILLPYM_JASA)
            .fileBulk(UPDATED_FILE_BULK)
            .fileTax(UPDATED_FILE_TAX)
            .fileTxt(UPDATED_FILE_TXT)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restInvoiceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedInvoice.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedInvoice))
            )
            .andExpect(status().isOk());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
        Invoice testInvoice = invoiceList.get(invoiceList.size() - 1);
        assertThat(testInvoice.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testInvoice.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testInvoice.getTglOrder()).isEqualTo(UPDATED_TGL_ORDER);
        assertThat(testInvoice.getJmlOrder()).isEqualTo(UPDATED_JML_ORDER);
        assertThat(testInvoice.getJmlOrderCancel()).isEqualTo(UPDATED_JML_ORDER_CANCEL);
        assertThat(testInvoice.getNoProformaPnbp()).isEqualTo(UPDATED_NO_PROFORMA_PNBP);
        assertThat(testInvoice.getNoProformaJasa()).isEqualTo(UPDATED_NO_PROFORMA_JASA);
        assertThat(testInvoice.getNoBillpymPnbp()).isEqualTo(UPDATED_NO_BILLPYM_PNBP);
        assertThat(testInvoice.getNoBillpymJasa()).isEqualTo(UPDATED_NO_BILLPYM_JASA);
        assertThat(testInvoice.getAmountPnbp()).isEqualTo(UPDATED_AMOUNT_PNBP);
        assertThat(testInvoice.getTaxPnbp()).isEqualTo(UPDATED_TAX_PNBP);
        assertThat(testInvoice.getTotalAmountPnbp()).isEqualTo(UPDATED_TOTAL_AMOUNT_PNBP);
        assertThat(testInvoice.getAmountJasa()).isEqualTo(UPDATED_AMOUNT_JASA);
        assertThat(testInvoice.getVatJasa()).isEqualTo(UPDATED_VAT_JASA);
        assertThat(testInvoice.getTaxJasa()).isEqualTo(UPDATED_TAX_JASA);
        assertThat(testInvoice.getTotalAmountJasa()).isEqualTo(UPDATED_TOTAL_AMOUNT_JASA);
        assertThat(testInvoice.getFileProformaPnbp()).isEqualTo(UPDATED_FILE_PROFORMA_PNBP);
        assertThat(testInvoice.getFileProformaJasa()).isEqualTo(UPDATED_FILE_PROFORMA_JASA);
        assertThat(testInvoice.getFileBillpymPnbp()).isEqualTo(UPDATED_FILE_BILLPYM_PNBP);
        assertThat(testInvoice.getFileBillpymJasa()).isEqualTo(UPDATED_FILE_BILLPYM_JASA);
        assertThat(testInvoice.getFileBulk()).isEqualTo(UPDATED_FILE_BULK);
        assertThat(testInvoice.getFileTax()).isEqualTo(UPDATED_FILE_TAX);
        assertThat(testInvoice.getFileTxt()).isEqualTo(UPDATED_FILE_TXT);
        assertThat(testInvoice.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testInvoice.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testInvoice.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void putNonExistingInvoice() throws Exception {
        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();
        invoice.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInvoiceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, invoice.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(invoice))
            )
            .andExpect(status().isBadRequest());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchInvoice() throws Exception {
        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();
        invoice.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInvoiceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(invoice))
            )
            .andExpect(status().isBadRequest());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamInvoice() throws Exception {
        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();
        invoice.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInvoiceMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(invoice)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateInvoiceWithPatch() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();

        // Update the invoice using partial update
        Invoice partialUpdatedInvoice = new Invoice();
        partialUpdatedInvoice.setId(invoice.getId());

        partialUpdatedInvoice
            .type(UPDATED_TYPE)
            .jmlOrderCancel(UPDATED_JML_ORDER_CANCEL)
            .noBillpymPnbp(UPDATED_NO_BILLPYM_PNBP)
            .noBillpymJasa(UPDATED_NO_BILLPYM_JASA)
            .amountPnbp(UPDATED_AMOUNT_PNBP)
            .totalAmountPnbp(UPDATED_TOTAL_AMOUNT_PNBP)
            .taxJasa(UPDATED_TAX_JASA)
            .totalAmountJasa(UPDATED_TOTAL_AMOUNT_JASA)
            .fileProformaPnbp(UPDATED_FILE_PROFORMA_PNBP)
            .fileBillpymJasa(UPDATED_FILE_BILLPYM_JASA)
            .fileBulk(UPDATED_FILE_BULK)
            .updateOn(UPDATED_UPDATE_ON);

        restInvoiceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInvoice.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInvoice))
            )
            .andExpect(status().isOk());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
        Invoice testInvoice = invoiceList.get(invoiceList.size() - 1);
        assertThat(testInvoice.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testInvoice.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testInvoice.getTglOrder()).isEqualTo(DEFAULT_TGL_ORDER);
        assertThat(testInvoice.getJmlOrder()).isEqualTo(DEFAULT_JML_ORDER);
        assertThat(testInvoice.getJmlOrderCancel()).isEqualTo(UPDATED_JML_ORDER_CANCEL);
        assertThat(testInvoice.getNoProformaPnbp()).isEqualTo(DEFAULT_NO_PROFORMA_PNBP);
        assertThat(testInvoice.getNoProformaJasa()).isEqualTo(DEFAULT_NO_PROFORMA_JASA);
        assertThat(testInvoice.getNoBillpymPnbp()).isEqualTo(UPDATED_NO_BILLPYM_PNBP);
        assertThat(testInvoice.getNoBillpymJasa()).isEqualTo(UPDATED_NO_BILLPYM_JASA);
        assertThat(testInvoice.getAmountPnbp()).isEqualTo(UPDATED_AMOUNT_PNBP);
        assertThat(testInvoice.getTaxPnbp()).isEqualTo(DEFAULT_TAX_PNBP);
        assertThat(testInvoice.getTotalAmountPnbp()).isEqualTo(UPDATED_TOTAL_AMOUNT_PNBP);
        assertThat(testInvoice.getAmountJasa()).isEqualTo(DEFAULT_AMOUNT_JASA);
        assertThat(testInvoice.getVatJasa()).isEqualTo(DEFAULT_VAT_JASA);
        assertThat(testInvoice.getTaxJasa()).isEqualTo(UPDATED_TAX_JASA);
        assertThat(testInvoice.getTotalAmountJasa()).isEqualTo(UPDATED_TOTAL_AMOUNT_JASA);
        assertThat(testInvoice.getFileProformaPnbp()).isEqualTo(UPDATED_FILE_PROFORMA_PNBP);
        assertThat(testInvoice.getFileProformaJasa()).isEqualTo(DEFAULT_FILE_PROFORMA_JASA);
        assertThat(testInvoice.getFileBillpymPnbp()).isEqualTo(DEFAULT_FILE_BILLPYM_PNBP);
        assertThat(testInvoice.getFileBillpymJasa()).isEqualTo(UPDATED_FILE_BILLPYM_JASA);
        assertThat(testInvoice.getFileBulk()).isEqualTo(UPDATED_FILE_BULK);
        assertThat(testInvoice.getFileTax()).isEqualTo(DEFAULT_FILE_TAX);
        assertThat(testInvoice.getFileTxt()).isEqualTo(DEFAULT_FILE_TXT);
        assertThat(testInvoice.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testInvoice.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testInvoice.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void fullUpdateInvoiceWithPatch() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();

        // Update the invoice using partial update
        Invoice partialUpdatedInvoice = new Invoice();
        partialUpdatedInvoice.setId(invoice.getId());

        partialUpdatedInvoice
            .type(UPDATED_TYPE)
            .tanggal(UPDATED_TANGGAL)
            .tglOrder(UPDATED_TGL_ORDER)
            .jmlOrder(UPDATED_JML_ORDER)
            .jmlOrderCancel(UPDATED_JML_ORDER_CANCEL)
            .noProformaPnbp(UPDATED_NO_PROFORMA_PNBP)
            .noProformaJasa(UPDATED_NO_PROFORMA_JASA)
            .noBillpymPnbp(UPDATED_NO_BILLPYM_PNBP)
            .noBillpymJasa(UPDATED_NO_BILLPYM_JASA)
            .amountPnbp(UPDATED_AMOUNT_PNBP)
            .taxPnbp(UPDATED_TAX_PNBP)
            .totalAmountPnbp(UPDATED_TOTAL_AMOUNT_PNBP)
            .amountJasa(UPDATED_AMOUNT_JASA)
            .vatJasa(UPDATED_VAT_JASA)
            .taxJasa(UPDATED_TAX_JASA)
            .totalAmountJasa(UPDATED_TOTAL_AMOUNT_JASA)
            .fileProformaPnbp(UPDATED_FILE_PROFORMA_PNBP)
            .fileProformaJasa(UPDATED_FILE_PROFORMA_JASA)
            .fileBillpymPnbp(UPDATED_FILE_BILLPYM_PNBP)
            .fileBillpymJasa(UPDATED_FILE_BILLPYM_JASA)
            .fileBulk(UPDATED_FILE_BULK)
            .fileTax(UPDATED_FILE_TAX)
            .fileTxt(UPDATED_FILE_TXT)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restInvoiceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInvoice.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInvoice))
            )
            .andExpect(status().isOk());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
        Invoice testInvoice = invoiceList.get(invoiceList.size() - 1);
        assertThat(testInvoice.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testInvoice.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testInvoice.getTglOrder()).isEqualTo(UPDATED_TGL_ORDER);
        assertThat(testInvoice.getJmlOrder()).isEqualTo(UPDATED_JML_ORDER);
        assertThat(testInvoice.getJmlOrderCancel()).isEqualTo(UPDATED_JML_ORDER_CANCEL);
        assertThat(testInvoice.getNoProformaPnbp()).isEqualTo(UPDATED_NO_PROFORMA_PNBP);
        assertThat(testInvoice.getNoProformaJasa()).isEqualTo(UPDATED_NO_PROFORMA_JASA);
        assertThat(testInvoice.getNoBillpymPnbp()).isEqualTo(UPDATED_NO_BILLPYM_PNBP);
        assertThat(testInvoice.getNoBillpymJasa()).isEqualTo(UPDATED_NO_BILLPYM_JASA);
        assertThat(testInvoice.getAmountPnbp()).isEqualTo(UPDATED_AMOUNT_PNBP);
        assertThat(testInvoice.getTaxPnbp()).isEqualTo(UPDATED_TAX_PNBP);
        assertThat(testInvoice.getTotalAmountPnbp()).isEqualTo(UPDATED_TOTAL_AMOUNT_PNBP);
        assertThat(testInvoice.getAmountJasa()).isEqualTo(UPDATED_AMOUNT_JASA);
        assertThat(testInvoice.getVatJasa()).isEqualTo(UPDATED_VAT_JASA);
        assertThat(testInvoice.getTaxJasa()).isEqualTo(UPDATED_TAX_JASA);
        assertThat(testInvoice.getTotalAmountJasa()).isEqualTo(UPDATED_TOTAL_AMOUNT_JASA);
        assertThat(testInvoice.getFileProformaPnbp()).isEqualTo(UPDATED_FILE_PROFORMA_PNBP);
        assertThat(testInvoice.getFileProformaJasa()).isEqualTo(UPDATED_FILE_PROFORMA_JASA);
        assertThat(testInvoice.getFileBillpymPnbp()).isEqualTo(UPDATED_FILE_BILLPYM_PNBP);
        assertThat(testInvoice.getFileBillpymJasa()).isEqualTo(UPDATED_FILE_BILLPYM_JASA);
        assertThat(testInvoice.getFileBulk()).isEqualTo(UPDATED_FILE_BULK);
        assertThat(testInvoice.getFileTax()).isEqualTo(UPDATED_FILE_TAX);
        assertThat(testInvoice.getFileTxt()).isEqualTo(UPDATED_FILE_TXT);
        assertThat(testInvoice.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testInvoice.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testInvoice.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void patchNonExistingInvoice() throws Exception {
        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();
        invoice.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInvoiceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, invoice.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(invoice))
            )
            .andExpect(status().isBadRequest());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchInvoice() throws Exception {
        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();
        invoice.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInvoiceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(invoice))
            )
            .andExpect(status().isBadRequest());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamInvoice() throws Exception {
        int databaseSizeBeforeUpdate = invoiceRepository.findAll().size();
        invoice.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInvoiceMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(invoice)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteInvoice() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        int databaseSizeBeforeDelete = invoiceRepository.findAll().size();

        // Delete the invoice
        restInvoiceMockMvc
            .perform(delete(ENTITY_API_URL_ID, invoice.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
