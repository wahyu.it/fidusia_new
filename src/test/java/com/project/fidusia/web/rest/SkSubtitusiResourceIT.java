package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.SkSubtitusi;
import com.project.fidusia.repository.SkSubtitusiRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SkSubtitusiResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SkSubtitusiResourceIT {

    private static final String DEFAULT_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_RECORD_STATUS = 1;
    private static final Integer UPDATED_RECORD_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_FILE_NAME_SK = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME_SK = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_NAME_LAMP = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME_LAMP = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS_SIGN = 1;
    private static final Integer UPDATED_STATUS_SIGN = 2;

    private static final String ENTITY_API_URL = "/api/sk-subtitusis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SkSubtitusiRepository skSubtitusiRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSkSubtitusiMockMvc;

    private SkSubtitusi skSubtitusi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SkSubtitusi createEntity(EntityManager em) {
        SkSubtitusi skSubtitusi = new SkSubtitusi()
            .nomor(DEFAULT_NOMOR)
            .tanggal(DEFAULT_TANGGAL)
            .recordStatus(DEFAULT_RECORD_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON)
            .fileNameSk(DEFAULT_FILE_NAME_SK)
            .fileNameLamp(DEFAULT_FILE_NAME_LAMP)
            .statusSign(DEFAULT_STATUS_SIGN);
        return skSubtitusi;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SkSubtitusi createUpdatedEntity(EntityManager em) {
        SkSubtitusi skSubtitusi = new SkSubtitusi()
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .fileNameSk(UPDATED_FILE_NAME_SK)
            .fileNameLamp(UPDATED_FILE_NAME_LAMP)
            .statusSign(UPDATED_STATUS_SIGN);
        return skSubtitusi;
    }

    @BeforeEach
    public void initTest() {
        skSubtitusi = createEntity(em);
    }

    @Test
    @Transactional
    void createSkSubtitusi() throws Exception {
        int databaseSizeBeforeCreate = skSubtitusiRepository.findAll().size();
        // Create the SkSubtitusi
        restSkSubtitusiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(skSubtitusi)))
            .andExpect(status().isCreated());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeCreate + 1);
        SkSubtitusi testSkSubtitusi = skSubtitusiList.get(skSubtitusiList.size() - 1);
        assertThat(testSkSubtitusi.getNomor()).isEqualTo(DEFAULT_NOMOR);
        assertThat(testSkSubtitusi.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testSkSubtitusi.getRecordStatus()).isEqualTo(DEFAULT_RECORD_STATUS);
        assertThat(testSkSubtitusi.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testSkSubtitusi.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testSkSubtitusi.getFileNameSk()).isEqualTo(DEFAULT_FILE_NAME_SK);
        assertThat(testSkSubtitusi.getFileNameLamp()).isEqualTo(DEFAULT_FILE_NAME_LAMP);
        assertThat(testSkSubtitusi.getStatusSign()).isEqualTo(DEFAULT_STATUS_SIGN);
    }

    @Test
    @Transactional
    void createSkSubtitusiWithExistingId() throws Exception {
        // Create the SkSubtitusi with an existing ID
        skSubtitusi.setId(1L);

        int databaseSizeBeforeCreate = skSubtitusiRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSkSubtitusiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(skSubtitusi)))
            .andExpect(status().isBadRequest());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSkSubtitusis() throws Exception {
        // Initialize the database
        skSubtitusiRepository.saveAndFlush(skSubtitusi);

        // Get all the skSubtitusiList
        restSkSubtitusiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(skSubtitusi.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomor").value(hasItem(DEFAULT_NOMOR)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].recordStatus").value(hasItem(DEFAULT_RECORD_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))))
            .andExpect(jsonPath("$.[*].fileNameSk").value(hasItem(DEFAULT_FILE_NAME_SK)))
            .andExpect(jsonPath("$.[*].fileNameLamp").value(hasItem(DEFAULT_FILE_NAME_LAMP)))
            .andExpect(jsonPath("$.[*].statusSign").value(hasItem(DEFAULT_STATUS_SIGN)));
    }

    @Test
    @Transactional
    void getSkSubtitusi() throws Exception {
        // Initialize the database
        skSubtitusiRepository.saveAndFlush(skSubtitusi);

        // Get the skSubtitusi
        restSkSubtitusiMockMvc
            .perform(get(ENTITY_API_URL_ID, skSubtitusi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(skSubtitusi.getId().intValue()))
            .andExpect(jsonPath("$.nomor").value(DEFAULT_NOMOR))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.recordStatus").value(DEFAULT_RECORD_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)))
            .andExpect(jsonPath("$.fileNameSk").value(DEFAULT_FILE_NAME_SK))
            .andExpect(jsonPath("$.fileNameLamp").value(DEFAULT_FILE_NAME_LAMP))
            .andExpect(jsonPath("$.statusSign").value(DEFAULT_STATUS_SIGN));
    }

    @Test
    @Transactional
    void getNonExistingSkSubtitusi() throws Exception {
        // Get the skSubtitusi
        restSkSubtitusiMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSkSubtitusi() throws Exception {
        // Initialize the database
        skSubtitusiRepository.saveAndFlush(skSubtitusi);

        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();

        // Update the skSubtitusi
        SkSubtitusi updatedSkSubtitusi = skSubtitusiRepository.findById(skSubtitusi.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedSkSubtitusi are not directly saved in db
        em.detach(updatedSkSubtitusi);
        updatedSkSubtitusi
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .fileNameSk(UPDATED_FILE_NAME_SK)
            .fileNameLamp(UPDATED_FILE_NAME_LAMP)
            .statusSign(UPDATED_STATUS_SIGN);

        restSkSubtitusiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSkSubtitusi.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSkSubtitusi))
            )
            .andExpect(status().isOk());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
        SkSubtitusi testSkSubtitusi = skSubtitusiList.get(skSubtitusiList.size() - 1);
        assertThat(testSkSubtitusi.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testSkSubtitusi.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testSkSubtitusi.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testSkSubtitusi.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testSkSubtitusi.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testSkSubtitusi.getFileNameSk()).isEqualTo(UPDATED_FILE_NAME_SK);
        assertThat(testSkSubtitusi.getFileNameLamp()).isEqualTo(UPDATED_FILE_NAME_LAMP);
        assertThat(testSkSubtitusi.getStatusSign()).isEqualTo(UPDATED_STATUS_SIGN);
    }

    @Test
    @Transactional
    void putNonExistingSkSubtitusi() throws Exception {
        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();
        skSubtitusi.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSkSubtitusiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, skSubtitusi.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(skSubtitusi))
            )
            .andExpect(status().isBadRequest());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSkSubtitusi() throws Exception {
        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();
        skSubtitusi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSkSubtitusiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(skSubtitusi))
            )
            .andExpect(status().isBadRequest());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSkSubtitusi() throws Exception {
        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();
        skSubtitusi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSkSubtitusiMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(skSubtitusi)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSkSubtitusiWithPatch() throws Exception {
        // Initialize the database
        skSubtitusiRepository.saveAndFlush(skSubtitusi);

        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();

        // Update the skSubtitusi using partial update
        SkSubtitusi partialUpdatedSkSubtitusi = new SkSubtitusi();
        partialUpdatedSkSubtitusi.setId(skSubtitusi.getId());

        partialUpdatedSkSubtitusi
            .nomor(UPDATED_NOMOR)
            .recordStatus(UPDATED_RECORD_STATUS)
            .fileNameSk(UPDATED_FILE_NAME_SK)
            .fileNameLamp(UPDATED_FILE_NAME_LAMP)
            .statusSign(UPDATED_STATUS_SIGN);

        restSkSubtitusiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSkSubtitusi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSkSubtitusi))
            )
            .andExpect(status().isOk());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
        SkSubtitusi testSkSubtitusi = skSubtitusiList.get(skSubtitusiList.size() - 1);
        assertThat(testSkSubtitusi.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testSkSubtitusi.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testSkSubtitusi.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testSkSubtitusi.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testSkSubtitusi.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testSkSubtitusi.getFileNameSk()).isEqualTo(UPDATED_FILE_NAME_SK);
        assertThat(testSkSubtitusi.getFileNameLamp()).isEqualTo(UPDATED_FILE_NAME_LAMP);
        assertThat(testSkSubtitusi.getStatusSign()).isEqualTo(UPDATED_STATUS_SIGN);
    }

    @Test
    @Transactional
    void fullUpdateSkSubtitusiWithPatch() throws Exception {
        // Initialize the database
        skSubtitusiRepository.saveAndFlush(skSubtitusi);

        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();

        // Update the skSubtitusi using partial update
        SkSubtitusi partialUpdatedSkSubtitusi = new SkSubtitusi();
        partialUpdatedSkSubtitusi.setId(skSubtitusi.getId());

        partialUpdatedSkSubtitusi
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .recordStatus(UPDATED_RECORD_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .fileNameSk(UPDATED_FILE_NAME_SK)
            .fileNameLamp(UPDATED_FILE_NAME_LAMP)
            .statusSign(UPDATED_STATUS_SIGN);

        restSkSubtitusiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSkSubtitusi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSkSubtitusi))
            )
            .andExpect(status().isOk());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
        SkSubtitusi testSkSubtitusi = skSubtitusiList.get(skSubtitusiList.size() - 1);
        assertThat(testSkSubtitusi.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testSkSubtitusi.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testSkSubtitusi.getRecordStatus()).isEqualTo(UPDATED_RECORD_STATUS);
        assertThat(testSkSubtitusi.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testSkSubtitusi.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testSkSubtitusi.getFileNameSk()).isEqualTo(UPDATED_FILE_NAME_SK);
        assertThat(testSkSubtitusi.getFileNameLamp()).isEqualTo(UPDATED_FILE_NAME_LAMP);
        assertThat(testSkSubtitusi.getStatusSign()).isEqualTo(UPDATED_STATUS_SIGN);
    }

    @Test
    @Transactional
    void patchNonExistingSkSubtitusi() throws Exception {
        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();
        skSubtitusi.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSkSubtitusiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, skSubtitusi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(skSubtitusi))
            )
            .andExpect(status().isBadRequest());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSkSubtitusi() throws Exception {
        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();
        skSubtitusi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSkSubtitusiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(skSubtitusi))
            )
            .andExpect(status().isBadRequest());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSkSubtitusi() throws Exception {
        int databaseSizeBeforeUpdate = skSubtitusiRepository.findAll().size();
        skSubtitusi.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSkSubtitusiMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(skSubtitusi))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the SkSubtitusi in the database
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSkSubtitusi() throws Exception {
        // Initialize the database
        skSubtitusiRepository.saveAndFlush(skSubtitusi);

        int databaseSizeBeforeDelete = skSubtitusiRepository.findAll().size();

        // Delete the skSubtitusi
        restSkSubtitusiMockMvc
            .perform(delete(ENTITY_API_URL_ID, skSubtitusi.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SkSubtitusi> skSubtitusiList = skSubtitusiRepository.findAll();
        assertThat(skSubtitusiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
