package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.OrderKerja;
import com.project.fidusia.repository.OrderKerjaRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OrderKerjaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrderKerjaResourceIT {

    private static final String DEFAULT_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_KATEGORI = 1;
    private static final Integer UPDATED_KATEGORI = 2;

    private static final LocalDate DEFAULT_TANGGAL_TERIMA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_TERIMA = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_JUMLAH_AKTA = 1;
    private static final Integer UPDATED_JUMLAH_AKTA = 2;

    private static final Integer DEFAULT_JUMLAH_CANCEL = 1;
    private static final Integer UPDATED_JUMLAH_CANCEL = 2;

    private static final Integer DEFAULT_JUMLAH_CETAK_AKTA = 1;
    private static final Integer UPDATED_JUMLAH_CETAK_AKTA = 2;

    private static final String DEFAULT_UPLOAD_FILE = "AAAAAAAAAA";
    private static final String UPDATED_UPLOAD_FILE = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final Boolean DEFAULT_STATUS_SIGNED = false;
    private static final Boolean UPDATED_STATUS_SIGNED = true;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_APPROVE_BY = "AAAAAAAAAA";
    private static final String UPDATED_APPROVE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_APPROVE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_APPROVE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/order-kerjas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrderKerjaRepository orderKerjaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderKerjaMockMvc;

    private OrderKerja orderKerja;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderKerja createEntity(EntityManager em) {
        OrderKerja orderKerja = new OrderKerja()
            .nomor(DEFAULT_NOMOR)
            .tanggal(DEFAULT_TANGGAL)
            .kategori(DEFAULT_KATEGORI)
            .tanggalTerima(DEFAULT_TANGGAL_TERIMA)
            .jumlahAkta(DEFAULT_JUMLAH_AKTA)
            .jumlahCancel(DEFAULT_JUMLAH_CANCEL)
            .jumlahCetakAkta(DEFAULT_JUMLAH_CETAK_AKTA)
            .uploadFile(DEFAULT_UPLOAD_FILE)
            .status(DEFAULT_STATUS)
            .statusSigned(DEFAULT_STATUS_SIGNED)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON)
            .approveBy(DEFAULT_APPROVE_BY)
            .approveOn(DEFAULT_APPROVE_ON);
        return orderKerja;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderKerja createUpdatedEntity(EntityManager em) {
        OrderKerja orderKerja = new OrderKerja()
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .kategori(UPDATED_KATEGORI)
            .tanggalTerima(UPDATED_TANGGAL_TERIMA)
            .jumlahAkta(UPDATED_JUMLAH_AKTA)
            .jumlahCancel(UPDATED_JUMLAH_CANCEL)
            .jumlahCetakAkta(UPDATED_JUMLAH_CETAK_AKTA)
            .uploadFile(UPDATED_UPLOAD_FILE)
            .status(UPDATED_STATUS)
            .statusSigned(UPDATED_STATUS_SIGNED)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .approveBy(UPDATED_APPROVE_BY)
            .approveOn(UPDATED_APPROVE_ON);
        return orderKerja;
    }

    @BeforeEach
    public void initTest() {
        orderKerja = createEntity(em);
    }

    @Test
    @Transactional
    void createOrderKerja() throws Exception {
        int databaseSizeBeforeCreate = orderKerjaRepository.findAll().size();
        // Create the OrderKerja
        restOrderKerjaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderKerja)))
            .andExpect(status().isCreated());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeCreate + 1);
        OrderKerja testOrderKerja = orderKerjaList.get(orderKerjaList.size() - 1);
        assertThat(testOrderKerja.getNomor()).isEqualTo(DEFAULT_NOMOR);
        assertThat(testOrderKerja.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testOrderKerja.getKategori()).isEqualTo(DEFAULT_KATEGORI);
        assertThat(testOrderKerja.getTanggalTerima()).isEqualTo(DEFAULT_TANGGAL_TERIMA);
        assertThat(testOrderKerja.getJumlahAkta()).isEqualTo(DEFAULT_JUMLAH_AKTA);
        assertThat(testOrderKerja.getJumlahCancel()).isEqualTo(DEFAULT_JUMLAH_CANCEL);
        assertThat(testOrderKerja.getJumlahCetakAkta()).isEqualTo(DEFAULT_JUMLAH_CETAK_AKTA);
        assertThat(testOrderKerja.getUploadFile()).isEqualTo(DEFAULT_UPLOAD_FILE);
        assertThat(testOrderKerja.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrderKerja.getStatusSigned()).isEqualTo(DEFAULT_STATUS_SIGNED);
        assertThat(testOrderKerja.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testOrderKerja.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testOrderKerja.getApproveBy()).isEqualTo(DEFAULT_APPROVE_BY);
        assertThat(testOrderKerja.getApproveOn()).isEqualTo(DEFAULT_APPROVE_ON);
    }

    @Test
    @Transactional
    void createOrderKerjaWithExistingId() throws Exception {
        // Create the OrderKerja with an existing ID
        orderKerja.setId(1L);

        int databaseSizeBeforeCreate = orderKerjaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderKerjaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderKerja)))
            .andExpect(status().isBadRequest());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOrderKerjas() throws Exception {
        // Initialize the database
        orderKerjaRepository.saveAndFlush(orderKerja);

        // Get all the orderKerjaList
        restOrderKerjaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderKerja.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomor").value(hasItem(DEFAULT_NOMOR)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].kategori").value(hasItem(DEFAULT_KATEGORI)))
            .andExpect(jsonPath("$.[*].tanggalTerima").value(hasItem(DEFAULT_TANGGAL_TERIMA.toString())))
            .andExpect(jsonPath("$.[*].jumlahAkta").value(hasItem(DEFAULT_JUMLAH_AKTA)))
            .andExpect(jsonPath("$.[*].jumlahCancel").value(hasItem(DEFAULT_JUMLAH_CANCEL)))
            .andExpect(jsonPath("$.[*].jumlahCetakAkta").value(hasItem(DEFAULT_JUMLAH_CETAK_AKTA)))
            .andExpect(jsonPath("$.[*].uploadFile").value(hasItem(DEFAULT_UPLOAD_FILE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].statusSigned").value(hasItem(DEFAULT_STATUS_SIGNED.booleanValue())))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))))
            .andExpect(jsonPath("$.[*].approveBy").value(hasItem(DEFAULT_APPROVE_BY)))
            .andExpect(jsonPath("$.[*].approveOn").value(hasItem(sameInstant(DEFAULT_APPROVE_ON))));
    }

    @Test
    @Transactional
    void getOrderKerja() throws Exception {
        // Initialize the database
        orderKerjaRepository.saveAndFlush(orderKerja);

        // Get the orderKerja
        restOrderKerjaMockMvc
            .perform(get(ENTITY_API_URL_ID, orderKerja.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderKerja.getId().intValue()))
            .andExpect(jsonPath("$.nomor").value(DEFAULT_NOMOR))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.kategori").value(DEFAULT_KATEGORI))
            .andExpect(jsonPath("$.tanggalTerima").value(DEFAULT_TANGGAL_TERIMA.toString()))
            .andExpect(jsonPath("$.jumlahAkta").value(DEFAULT_JUMLAH_AKTA))
            .andExpect(jsonPath("$.jumlahCancel").value(DEFAULT_JUMLAH_CANCEL))
            .andExpect(jsonPath("$.jumlahCetakAkta").value(DEFAULT_JUMLAH_CETAK_AKTA))
            .andExpect(jsonPath("$.uploadFile").value(DEFAULT_UPLOAD_FILE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.statusSigned").value(DEFAULT_STATUS_SIGNED.booleanValue()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)))
            .andExpect(jsonPath("$.approveBy").value(DEFAULT_APPROVE_BY))
            .andExpect(jsonPath("$.approveOn").value(sameInstant(DEFAULT_APPROVE_ON)));
    }

    @Test
    @Transactional
    void getNonExistingOrderKerja() throws Exception {
        // Get the orderKerja
        restOrderKerjaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingOrderKerja() throws Exception {
        // Initialize the database
        orderKerjaRepository.saveAndFlush(orderKerja);

        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();

        // Update the orderKerja
        OrderKerja updatedOrderKerja = orderKerjaRepository.findById(orderKerja.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedOrderKerja are not directly saved in db
        em.detach(updatedOrderKerja);
        updatedOrderKerja
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .kategori(UPDATED_KATEGORI)
            .tanggalTerima(UPDATED_TANGGAL_TERIMA)
            .jumlahAkta(UPDATED_JUMLAH_AKTA)
            .jumlahCancel(UPDATED_JUMLAH_CANCEL)
            .jumlahCetakAkta(UPDATED_JUMLAH_CETAK_AKTA)
            .uploadFile(UPDATED_UPLOAD_FILE)
            .status(UPDATED_STATUS)
            .statusSigned(UPDATED_STATUS_SIGNED)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .approveBy(UPDATED_APPROVE_BY)
            .approveOn(UPDATED_APPROVE_ON);

        restOrderKerjaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOrderKerja.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOrderKerja))
            )
            .andExpect(status().isOk());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
        OrderKerja testOrderKerja = orderKerjaList.get(orderKerjaList.size() - 1);
        assertThat(testOrderKerja.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testOrderKerja.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderKerja.getKategori()).isEqualTo(UPDATED_KATEGORI);
        assertThat(testOrderKerja.getTanggalTerima()).isEqualTo(UPDATED_TANGGAL_TERIMA);
        assertThat(testOrderKerja.getJumlahAkta()).isEqualTo(UPDATED_JUMLAH_AKTA);
        assertThat(testOrderKerja.getJumlahCancel()).isEqualTo(UPDATED_JUMLAH_CANCEL);
        assertThat(testOrderKerja.getJumlahCetakAkta()).isEqualTo(UPDATED_JUMLAH_CETAK_AKTA);
        assertThat(testOrderKerja.getUploadFile()).isEqualTo(UPDATED_UPLOAD_FILE);
        assertThat(testOrderKerja.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderKerja.getStatusSigned()).isEqualTo(UPDATED_STATUS_SIGNED);
        assertThat(testOrderKerja.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testOrderKerja.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testOrderKerja.getApproveBy()).isEqualTo(UPDATED_APPROVE_BY);
        assertThat(testOrderKerja.getApproveOn()).isEqualTo(UPDATED_APPROVE_ON);
    }

    @Test
    @Transactional
    void putNonExistingOrderKerja() throws Exception {
        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();
        orderKerja.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderKerjaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orderKerja.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderKerja))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrderKerja() throws Exception {
        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();
        orderKerja.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderKerjaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderKerja))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrderKerja() throws Exception {
        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();
        orderKerja.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderKerjaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderKerja)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrderKerjaWithPatch() throws Exception {
        // Initialize the database
        orderKerjaRepository.saveAndFlush(orderKerja);

        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();

        // Update the orderKerja using partial update
        OrderKerja partialUpdatedOrderKerja = new OrderKerja();
        partialUpdatedOrderKerja.setId(orderKerja.getId());

        partialUpdatedOrderKerja
            .tanggal(UPDATED_TANGGAL)
            .kategori(UPDATED_KATEGORI)
            .tanggalTerima(UPDATED_TANGGAL_TERIMA)
            .jumlahCetakAkta(UPDATED_JUMLAH_CETAK_AKTA)
            .uploadFile(UPDATED_UPLOAD_FILE)
            .statusSigned(UPDATED_STATUS_SIGNED)
            .updateOn(UPDATED_UPDATE_ON)
            .approveBy(UPDATED_APPROVE_BY);

        restOrderKerjaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderKerja.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderKerja))
            )
            .andExpect(status().isOk());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
        OrderKerja testOrderKerja = orderKerjaList.get(orderKerjaList.size() - 1);
        assertThat(testOrderKerja.getNomor()).isEqualTo(DEFAULT_NOMOR);
        assertThat(testOrderKerja.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderKerja.getKategori()).isEqualTo(UPDATED_KATEGORI);
        assertThat(testOrderKerja.getTanggalTerima()).isEqualTo(UPDATED_TANGGAL_TERIMA);
        assertThat(testOrderKerja.getJumlahAkta()).isEqualTo(DEFAULT_JUMLAH_AKTA);
        assertThat(testOrderKerja.getJumlahCancel()).isEqualTo(DEFAULT_JUMLAH_CANCEL);
        assertThat(testOrderKerja.getJumlahCetakAkta()).isEqualTo(UPDATED_JUMLAH_CETAK_AKTA);
        assertThat(testOrderKerja.getUploadFile()).isEqualTo(UPDATED_UPLOAD_FILE);
        assertThat(testOrderKerja.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrderKerja.getStatusSigned()).isEqualTo(UPDATED_STATUS_SIGNED);
        assertThat(testOrderKerja.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testOrderKerja.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testOrderKerja.getApproveBy()).isEqualTo(UPDATED_APPROVE_BY);
        assertThat(testOrderKerja.getApproveOn()).isEqualTo(DEFAULT_APPROVE_ON);
    }

    @Test
    @Transactional
    void fullUpdateOrderKerjaWithPatch() throws Exception {
        // Initialize the database
        orderKerjaRepository.saveAndFlush(orderKerja);

        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();

        // Update the orderKerja using partial update
        OrderKerja partialUpdatedOrderKerja = new OrderKerja();
        partialUpdatedOrderKerja.setId(orderKerja.getId());

        partialUpdatedOrderKerja
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .kategori(UPDATED_KATEGORI)
            .tanggalTerima(UPDATED_TANGGAL_TERIMA)
            .jumlahAkta(UPDATED_JUMLAH_AKTA)
            .jumlahCancel(UPDATED_JUMLAH_CANCEL)
            .jumlahCetakAkta(UPDATED_JUMLAH_CETAK_AKTA)
            .uploadFile(UPDATED_UPLOAD_FILE)
            .status(UPDATED_STATUS)
            .statusSigned(UPDATED_STATUS_SIGNED)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .approveBy(UPDATED_APPROVE_BY)
            .approveOn(UPDATED_APPROVE_ON);

        restOrderKerjaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderKerja.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderKerja))
            )
            .andExpect(status().isOk());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
        OrderKerja testOrderKerja = orderKerjaList.get(orderKerjaList.size() - 1);
        assertThat(testOrderKerja.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testOrderKerja.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderKerja.getKategori()).isEqualTo(UPDATED_KATEGORI);
        assertThat(testOrderKerja.getTanggalTerima()).isEqualTo(UPDATED_TANGGAL_TERIMA);
        assertThat(testOrderKerja.getJumlahAkta()).isEqualTo(UPDATED_JUMLAH_AKTA);
        assertThat(testOrderKerja.getJumlahCancel()).isEqualTo(UPDATED_JUMLAH_CANCEL);
        assertThat(testOrderKerja.getJumlahCetakAkta()).isEqualTo(UPDATED_JUMLAH_CETAK_AKTA);
        assertThat(testOrderKerja.getUploadFile()).isEqualTo(UPDATED_UPLOAD_FILE);
        assertThat(testOrderKerja.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderKerja.getStatusSigned()).isEqualTo(UPDATED_STATUS_SIGNED);
        assertThat(testOrderKerja.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testOrderKerja.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testOrderKerja.getApproveBy()).isEqualTo(UPDATED_APPROVE_BY);
        assertThat(testOrderKerja.getApproveOn()).isEqualTo(UPDATED_APPROVE_ON);
    }

    @Test
    @Transactional
    void patchNonExistingOrderKerja() throws Exception {
        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();
        orderKerja.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderKerjaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, orderKerja.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderKerja))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrderKerja() throws Exception {
        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();
        orderKerja.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderKerjaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderKerja))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrderKerja() throws Exception {
        int databaseSizeBeforeUpdate = orderKerjaRepository.findAll().size();
        orderKerja.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderKerjaMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(orderKerja))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderKerja in the database
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrderKerja() throws Exception {
        // Initialize the database
        orderKerjaRepository.saveAndFlush(orderKerja);

        int databaseSizeBeforeDelete = orderKerjaRepository.findAll().size();

        // Delete the orderKerja
        restOrderKerjaMockMvc
            .perform(delete(ENTITY_API_URL_ID, orderKerja.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderKerja> orderKerjaList = orderKerjaRepository.findAll();
        assertThat(orderKerjaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
