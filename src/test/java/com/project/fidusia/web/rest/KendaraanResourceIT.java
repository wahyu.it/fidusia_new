package com.project.fidusia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Kendaraan;
import com.project.fidusia.repository.KendaraanRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KendaraanResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class KendaraanResourceIT {

    private static final String DEFAULT_KONDISI = "AAAAAAAAAA";
    private static final String UPDATED_KONDISI = "BBBBBBBBBB";

    private static final Integer DEFAULT_RODA = 1;
    private static final Integer UPDATED_RODA = 2;

    private static final String DEFAULT_MERK = "AAAAAAAAAA";
    private static final String UPDATED_MERK = "BBBBBBBBBB";

    private static final String DEFAULT_TIPE = "AAAAAAAAAA";
    private static final String UPDATED_TIPE = "BBBBBBBBBB";

    private static final String DEFAULT_WARNA = "AAAAAAAAAA";
    private static final String UPDATED_WARNA = "BBBBBBBBBB";

    private static final String DEFAULT_TAHUN_PEMBUATAN = "AAAAAAAAAA";
    private static final String UPDATED_TAHUN_PEMBUATAN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_RANGKA = "AAAAAAAAAA";
    private static final String UPDATED_NO_RANGKA = "BBBBBBBBBB";

    private static final String DEFAULT_NO_MESIN = "AAAAAAAAAA";
    private static final String UPDATED_NO_MESIN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_BPKB = "AAAAAAAAAA";
    private static final String UPDATED_NO_BPKB = "BBBBBBBBBB";

    private static final Long DEFAULT_NILAI_OBJEK_JAMINAN = 1L;
    private static final Long UPDATED_NILAI_OBJEK_JAMINAN = 2L;

    private static final String ENTITY_API_URL = "/api/kendaraans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KendaraanRepository kendaraanRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKendaraanMockMvc;

    private Kendaraan kendaraan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kendaraan createEntity(EntityManager em) {
        Kendaraan kendaraan = new Kendaraan()
            .kondisi(DEFAULT_KONDISI)
            .roda(DEFAULT_RODA)
            .merk(DEFAULT_MERK)
            .tipe(DEFAULT_TIPE)
            .warna(DEFAULT_WARNA)
            .tahunPembuatan(DEFAULT_TAHUN_PEMBUATAN)
            .noRangka(DEFAULT_NO_RANGKA)
            .noMesin(DEFAULT_NO_MESIN)
            .noBpkb(DEFAULT_NO_BPKB)
            .nilaiObjekJaminan(DEFAULT_NILAI_OBJEK_JAMINAN);
        return kendaraan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kendaraan createUpdatedEntity(EntityManager em) {
        Kendaraan kendaraan = new Kendaraan()
            .kondisi(UPDATED_KONDISI)
            .roda(UPDATED_RODA)
            .merk(UPDATED_MERK)
            .tipe(UPDATED_TIPE)
            .warna(UPDATED_WARNA)
            .tahunPembuatan(UPDATED_TAHUN_PEMBUATAN)
            .noRangka(UPDATED_NO_RANGKA)
            .noMesin(UPDATED_NO_MESIN)
            .noBpkb(UPDATED_NO_BPKB)
            .nilaiObjekJaminan(UPDATED_NILAI_OBJEK_JAMINAN);
        return kendaraan;
    }

    @BeforeEach
    public void initTest() {
        kendaraan = createEntity(em);
    }

    @Test
    @Transactional
    void createKendaraan() throws Exception {
        int databaseSizeBeforeCreate = kendaraanRepository.findAll().size();
        // Create the Kendaraan
        restKendaraanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kendaraan)))
            .andExpect(status().isCreated());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeCreate + 1);
        Kendaraan testKendaraan = kendaraanList.get(kendaraanList.size() - 1);
        assertThat(testKendaraan.getKondisi()).isEqualTo(DEFAULT_KONDISI);
        assertThat(testKendaraan.getRoda()).isEqualTo(DEFAULT_RODA);
        assertThat(testKendaraan.getMerk()).isEqualTo(DEFAULT_MERK);
        assertThat(testKendaraan.getTipe()).isEqualTo(DEFAULT_TIPE);
        assertThat(testKendaraan.getWarna()).isEqualTo(DEFAULT_WARNA);
        assertThat(testKendaraan.getTahunPembuatan()).isEqualTo(DEFAULT_TAHUN_PEMBUATAN);
        assertThat(testKendaraan.getNoRangka()).isEqualTo(DEFAULT_NO_RANGKA);
        assertThat(testKendaraan.getNoMesin()).isEqualTo(DEFAULT_NO_MESIN);
        assertThat(testKendaraan.getNoBpkb()).isEqualTo(DEFAULT_NO_BPKB);
        assertThat(testKendaraan.getNilaiObjekJaminan()).isEqualTo(DEFAULT_NILAI_OBJEK_JAMINAN);
    }

    @Test
    @Transactional
    void createKendaraanWithExistingId() throws Exception {
        // Create the Kendaraan with an existing ID
        kendaraan.setId(1L);

        int databaseSizeBeforeCreate = kendaraanRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKendaraanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kendaraan)))
            .andExpect(status().isBadRequest());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllKendaraans() throws Exception {
        // Initialize the database
        kendaraanRepository.saveAndFlush(kendaraan);

        // Get all the kendaraanList
        restKendaraanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kendaraan.getId().intValue())))
            .andExpect(jsonPath("$.[*].kondisi").value(hasItem(DEFAULT_KONDISI)))
            .andExpect(jsonPath("$.[*].roda").value(hasItem(DEFAULT_RODA)))
            .andExpect(jsonPath("$.[*].merk").value(hasItem(DEFAULT_MERK)))
            .andExpect(jsonPath("$.[*].tipe").value(hasItem(DEFAULT_TIPE)))
            .andExpect(jsonPath("$.[*].warna").value(hasItem(DEFAULT_WARNA)))
            .andExpect(jsonPath("$.[*].tahunPembuatan").value(hasItem(DEFAULT_TAHUN_PEMBUATAN)))
            .andExpect(jsonPath("$.[*].noRangka").value(hasItem(DEFAULT_NO_RANGKA)))
            .andExpect(jsonPath("$.[*].noMesin").value(hasItem(DEFAULT_NO_MESIN)))
            .andExpect(jsonPath("$.[*].noBpkb").value(hasItem(DEFAULT_NO_BPKB)))
            .andExpect(jsonPath("$.[*].nilaiObjekJaminan").value(hasItem(DEFAULT_NILAI_OBJEK_JAMINAN.intValue())));
    }

    @Test
    @Transactional
    void getKendaraan() throws Exception {
        // Initialize the database
        kendaraanRepository.saveAndFlush(kendaraan);

        // Get the kendaraan
        restKendaraanMockMvc
            .perform(get(ENTITY_API_URL_ID, kendaraan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kendaraan.getId().intValue()))
            .andExpect(jsonPath("$.kondisi").value(DEFAULT_KONDISI))
            .andExpect(jsonPath("$.roda").value(DEFAULT_RODA))
            .andExpect(jsonPath("$.merk").value(DEFAULT_MERK))
            .andExpect(jsonPath("$.tipe").value(DEFAULT_TIPE))
            .andExpect(jsonPath("$.warna").value(DEFAULT_WARNA))
            .andExpect(jsonPath("$.tahunPembuatan").value(DEFAULT_TAHUN_PEMBUATAN))
            .andExpect(jsonPath("$.noRangka").value(DEFAULT_NO_RANGKA))
            .andExpect(jsonPath("$.noMesin").value(DEFAULT_NO_MESIN))
            .andExpect(jsonPath("$.noBpkb").value(DEFAULT_NO_BPKB))
            .andExpect(jsonPath("$.nilaiObjekJaminan").value(DEFAULT_NILAI_OBJEK_JAMINAN.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingKendaraan() throws Exception {
        // Get the kendaraan
        restKendaraanMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingKendaraan() throws Exception {
        // Initialize the database
        kendaraanRepository.saveAndFlush(kendaraan);

        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();

        // Update the kendaraan
        Kendaraan updatedKendaraan = kendaraanRepository.findById(kendaraan.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedKendaraan are not directly saved in db
        em.detach(updatedKendaraan);
        updatedKendaraan
            .kondisi(UPDATED_KONDISI)
            .roda(UPDATED_RODA)
            .merk(UPDATED_MERK)
            .tipe(UPDATED_TIPE)
            .warna(UPDATED_WARNA)
            .tahunPembuatan(UPDATED_TAHUN_PEMBUATAN)
            .noRangka(UPDATED_NO_RANGKA)
            .noMesin(UPDATED_NO_MESIN)
            .noBpkb(UPDATED_NO_BPKB)
            .nilaiObjekJaminan(UPDATED_NILAI_OBJEK_JAMINAN);

        restKendaraanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedKendaraan.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedKendaraan))
            )
            .andExpect(status().isOk());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
        Kendaraan testKendaraan = kendaraanList.get(kendaraanList.size() - 1);
        assertThat(testKendaraan.getKondisi()).isEqualTo(UPDATED_KONDISI);
        assertThat(testKendaraan.getRoda()).isEqualTo(UPDATED_RODA);
        assertThat(testKendaraan.getMerk()).isEqualTo(UPDATED_MERK);
        assertThat(testKendaraan.getTipe()).isEqualTo(UPDATED_TIPE);
        assertThat(testKendaraan.getWarna()).isEqualTo(UPDATED_WARNA);
        assertThat(testKendaraan.getTahunPembuatan()).isEqualTo(UPDATED_TAHUN_PEMBUATAN);
        assertThat(testKendaraan.getNoRangka()).isEqualTo(UPDATED_NO_RANGKA);
        assertThat(testKendaraan.getNoMesin()).isEqualTo(UPDATED_NO_MESIN);
        assertThat(testKendaraan.getNoBpkb()).isEqualTo(UPDATED_NO_BPKB);
        assertThat(testKendaraan.getNilaiObjekJaminan()).isEqualTo(UPDATED_NILAI_OBJEK_JAMINAN);
    }

    @Test
    @Transactional
    void putNonExistingKendaraan() throws Exception {
        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();
        kendaraan.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKendaraanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, kendaraan.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kendaraan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKendaraan() throws Exception {
        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();
        kendaraan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKendaraanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kendaraan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKendaraan() throws Exception {
        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();
        kendaraan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKendaraanMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kendaraan)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKendaraanWithPatch() throws Exception {
        // Initialize the database
        kendaraanRepository.saveAndFlush(kendaraan);

        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();

        // Update the kendaraan using partial update
        Kendaraan partialUpdatedKendaraan = new Kendaraan();
        partialUpdatedKendaraan.setId(kendaraan.getId());

        partialUpdatedKendaraan
            .tipe(UPDATED_TIPE)
            .tahunPembuatan(UPDATED_TAHUN_PEMBUATAN)
            .noMesin(UPDATED_NO_MESIN)
            .noBpkb(UPDATED_NO_BPKB);

        restKendaraanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKendaraan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKendaraan))
            )
            .andExpect(status().isOk());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
        Kendaraan testKendaraan = kendaraanList.get(kendaraanList.size() - 1);
        assertThat(testKendaraan.getKondisi()).isEqualTo(DEFAULT_KONDISI);
        assertThat(testKendaraan.getRoda()).isEqualTo(DEFAULT_RODA);
        assertThat(testKendaraan.getMerk()).isEqualTo(DEFAULT_MERK);
        assertThat(testKendaraan.getTipe()).isEqualTo(UPDATED_TIPE);
        assertThat(testKendaraan.getWarna()).isEqualTo(DEFAULT_WARNA);
        assertThat(testKendaraan.getTahunPembuatan()).isEqualTo(UPDATED_TAHUN_PEMBUATAN);
        assertThat(testKendaraan.getNoRangka()).isEqualTo(DEFAULT_NO_RANGKA);
        assertThat(testKendaraan.getNoMesin()).isEqualTo(UPDATED_NO_MESIN);
        assertThat(testKendaraan.getNoBpkb()).isEqualTo(UPDATED_NO_BPKB);
        assertThat(testKendaraan.getNilaiObjekJaminan()).isEqualTo(DEFAULT_NILAI_OBJEK_JAMINAN);
    }

    @Test
    @Transactional
    void fullUpdateKendaraanWithPatch() throws Exception {
        // Initialize the database
        kendaraanRepository.saveAndFlush(kendaraan);

        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();

        // Update the kendaraan using partial update
        Kendaraan partialUpdatedKendaraan = new Kendaraan();
        partialUpdatedKendaraan.setId(kendaraan.getId());

        partialUpdatedKendaraan
            .kondisi(UPDATED_KONDISI)
            .roda(UPDATED_RODA)
            .merk(UPDATED_MERK)
            .tipe(UPDATED_TIPE)
            .warna(UPDATED_WARNA)
            .tahunPembuatan(UPDATED_TAHUN_PEMBUATAN)
            .noRangka(UPDATED_NO_RANGKA)
            .noMesin(UPDATED_NO_MESIN)
            .noBpkb(UPDATED_NO_BPKB)
            .nilaiObjekJaminan(UPDATED_NILAI_OBJEK_JAMINAN);

        restKendaraanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKendaraan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKendaraan))
            )
            .andExpect(status().isOk());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
        Kendaraan testKendaraan = kendaraanList.get(kendaraanList.size() - 1);
        assertThat(testKendaraan.getKondisi()).isEqualTo(UPDATED_KONDISI);
        assertThat(testKendaraan.getRoda()).isEqualTo(UPDATED_RODA);
        assertThat(testKendaraan.getMerk()).isEqualTo(UPDATED_MERK);
        assertThat(testKendaraan.getTipe()).isEqualTo(UPDATED_TIPE);
        assertThat(testKendaraan.getWarna()).isEqualTo(UPDATED_WARNA);
        assertThat(testKendaraan.getTahunPembuatan()).isEqualTo(UPDATED_TAHUN_PEMBUATAN);
        assertThat(testKendaraan.getNoRangka()).isEqualTo(UPDATED_NO_RANGKA);
        assertThat(testKendaraan.getNoMesin()).isEqualTo(UPDATED_NO_MESIN);
        assertThat(testKendaraan.getNoBpkb()).isEqualTo(UPDATED_NO_BPKB);
        assertThat(testKendaraan.getNilaiObjekJaminan()).isEqualTo(UPDATED_NILAI_OBJEK_JAMINAN);
    }

    @Test
    @Transactional
    void patchNonExistingKendaraan() throws Exception {
        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();
        kendaraan.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKendaraanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, kendaraan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kendaraan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKendaraan() throws Exception {
        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();
        kendaraan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKendaraanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kendaraan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKendaraan() throws Exception {
        int databaseSizeBeforeUpdate = kendaraanRepository.findAll().size();
        kendaraan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKendaraanMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(kendaraan))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Kendaraan in the database
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKendaraan() throws Exception {
        // Initialize the database
        kendaraanRepository.saveAndFlush(kendaraan);

        int databaseSizeBeforeDelete = kendaraanRepository.findAll().size();

        // Delete the kendaraan
        restKendaraanMockMvc
            .perform(delete(ENTITY_API_URL_ID, kendaraan.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kendaraan> kendaraanList = kendaraanRepository.findAll();
        assertThat(kendaraanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
