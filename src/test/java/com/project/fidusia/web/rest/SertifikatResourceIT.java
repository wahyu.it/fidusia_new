package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Sertifikat;
import com.project.fidusia.repository.SertifikatRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SertifikatResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SertifikatResourceIT {

    private static final String DEFAULT_KODE_VOUCHER = "AAAAAAAAAA";
    private static final String UPDATED_KODE_VOUCHER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TGL_VOUCHER = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TGL_VOUCHER = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_NO_SERTIFIKAT = "AAAAAAAAAA";
    private static final String UPDATED_NO_SERTIFIKAT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TGL_SERTIFIKAT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TGL_SERTIFIKAT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_BIAYA_PNBP = 1L;
    private static final Long UPDATED_BIAYA_PNBP = 2L;

    private static final String DEFAULT_NO_REFERENSI_BNI = "AAAAAAAAAA";
    private static final String UPDATED_NO_REFERENSI_BNI = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_REGISTRATION_ID = "AAAAAAAAAA";
    private static final String UPDATED_REGISTRATION_ID = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TGL_ORDER = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_ORDER = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_TGL_CANCEL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_CANCEL = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_BIAYA_JASA = 1L;
    private static final Long UPDATED_BIAYA_JASA = 2L;

    private static final Boolean DEFAULT_INVOICE_SUBMITTED = false;
    private static final Boolean UPDATED_INVOICE_SUBMITTED = true;

    private static final Boolean DEFAULT_REPORT_SUBMITTED = false;
    private static final Boolean UPDATED_REPORT_SUBMITTED = true;

    private static final String ENTITY_API_URL = "/api/sertifikats";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SertifikatRepository sertifikatRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSertifikatMockMvc;

    private Sertifikat sertifikat;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sertifikat createEntity(EntityManager em) {
        Sertifikat sertifikat = new Sertifikat()
            .kodeVoucher(DEFAULT_KODE_VOUCHER)
            .tglVoucher(DEFAULT_TGL_VOUCHER)
            .noSertifikat(DEFAULT_NO_SERTIFIKAT)
            .tglSertifikat(DEFAULT_TGL_SERTIFIKAT)
            .biayaPnbp(DEFAULT_BIAYA_PNBP)
            .noReferensiBni(DEFAULT_NO_REFERENSI_BNI)
            .status(DEFAULT_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON)
            .registrationId(DEFAULT_REGISTRATION_ID)
            .tglOrder(DEFAULT_TGL_ORDER)
            .tglCancel(DEFAULT_TGL_CANCEL)
            .biayaJasa(DEFAULT_BIAYA_JASA)
            .invoiceSubmitted(DEFAULT_INVOICE_SUBMITTED)
            .reportSubmitted(DEFAULT_REPORT_SUBMITTED);
        return sertifikat;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sertifikat createUpdatedEntity(EntityManager em) {
        Sertifikat sertifikat = new Sertifikat()
            .kodeVoucher(UPDATED_KODE_VOUCHER)
            .tglVoucher(UPDATED_TGL_VOUCHER)
            .noSertifikat(UPDATED_NO_SERTIFIKAT)
            .tglSertifikat(UPDATED_TGL_SERTIFIKAT)
            .biayaPnbp(UPDATED_BIAYA_PNBP)
            .noReferensiBni(UPDATED_NO_REFERENSI_BNI)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .registrationId(UPDATED_REGISTRATION_ID)
            .tglOrder(UPDATED_TGL_ORDER)
            .tglCancel(UPDATED_TGL_CANCEL)
            .biayaJasa(UPDATED_BIAYA_JASA)
            .invoiceSubmitted(UPDATED_INVOICE_SUBMITTED)
            .reportSubmitted(UPDATED_REPORT_SUBMITTED);
        return sertifikat;
    }

    @BeforeEach
    public void initTest() {
        sertifikat = createEntity(em);
    }

    @Test
    @Transactional
    void createSertifikat() throws Exception {
        int databaseSizeBeforeCreate = sertifikatRepository.findAll().size();
        // Create the Sertifikat
        restSertifikatMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sertifikat)))
            .andExpect(status().isCreated());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeCreate + 1);
        Sertifikat testSertifikat = sertifikatList.get(sertifikatList.size() - 1);
        assertThat(testSertifikat.getKodeVoucher()).isEqualTo(DEFAULT_KODE_VOUCHER);
        assertThat(testSertifikat.getTglVoucher()).isEqualTo(DEFAULT_TGL_VOUCHER);
        assertThat(testSertifikat.getNoSertifikat()).isEqualTo(DEFAULT_NO_SERTIFIKAT);
        assertThat(testSertifikat.getTglSertifikat()).isEqualTo(DEFAULT_TGL_SERTIFIKAT);
        assertThat(testSertifikat.getBiayaPnbp()).isEqualTo(DEFAULT_BIAYA_PNBP);
        assertThat(testSertifikat.getNoReferensiBni()).isEqualTo(DEFAULT_NO_REFERENSI_BNI);
        assertThat(testSertifikat.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testSertifikat.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testSertifikat.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testSertifikat.getRegistrationId()).isEqualTo(DEFAULT_REGISTRATION_ID);
        assertThat(testSertifikat.getTglOrder()).isEqualTo(DEFAULT_TGL_ORDER);
        assertThat(testSertifikat.getTglCancel()).isEqualTo(DEFAULT_TGL_CANCEL);
        assertThat(testSertifikat.getBiayaJasa()).isEqualTo(DEFAULT_BIAYA_JASA);
        assertThat(testSertifikat.getInvoiceSubmitted()).isEqualTo(DEFAULT_INVOICE_SUBMITTED);
        assertThat(testSertifikat.getReportSubmitted()).isEqualTo(DEFAULT_REPORT_SUBMITTED);
    }

    @Test
    @Transactional
    void createSertifikatWithExistingId() throws Exception {
        // Create the Sertifikat with an existing ID
        sertifikat.setId(1L);

        int databaseSizeBeforeCreate = sertifikatRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSertifikatMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sertifikat)))
            .andExpect(status().isBadRequest());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSertifikats() throws Exception {
        // Initialize the database
        sertifikatRepository.saveAndFlush(sertifikat);

        // Get all the sertifikatList
        restSertifikatMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sertifikat.getId().intValue())))
            .andExpect(jsonPath("$.[*].kodeVoucher").value(hasItem(DEFAULT_KODE_VOUCHER)))
            .andExpect(jsonPath("$.[*].tglVoucher").value(hasItem(sameInstant(DEFAULT_TGL_VOUCHER))))
            .andExpect(jsonPath("$.[*].noSertifikat").value(hasItem(DEFAULT_NO_SERTIFIKAT)))
            .andExpect(jsonPath("$.[*].tglSertifikat").value(hasItem(sameInstant(DEFAULT_TGL_SERTIFIKAT))))
            .andExpect(jsonPath("$.[*].biayaPnbp").value(hasItem(DEFAULT_BIAYA_PNBP.intValue())))
            .andExpect(jsonPath("$.[*].noReferensiBni").value(hasItem(DEFAULT_NO_REFERENSI_BNI)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))))
            .andExpect(jsonPath("$.[*].registrationId").value(hasItem(DEFAULT_REGISTRATION_ID)))
            .andExpect(jsonPath("$.[*].tglOrder").value(hasItem(DEFAULT_TGL_ORDER.toString())))
            .andExpect(jsonPath("$.[*].tglCancel").value(hasItem(DEFAULT_TGL_CANCEL.toString())))
            .andExpect(jsonPath("$.[*].biayaJasa").value(hasItem(DEFAULT_BIAYA_JASA.intValue())))
            .andExpect(jsonPath("$.[*].invoiceSubmitted").value(hasItem(DEFAULT_INVOICE_SUBMITTED.booleanValue())))
            .andExpect(jsonPath("$.[*].reportSubmitted").value(hasItem(DEFAULT_REPORT_SUBMITTED.booleanValue())));
    }

    @Test
    @Transactional
    void getSertifikat() throws Exception {
        // Initialize the database
        sertifikatRepository.saveAndFlush(sertifikat);

        // Get the sertifikat
        restSertifikatMockMvc
            .perform(get(ENTITY_API_URL_ID, sertifikat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sertifikat.getId().intValue()))
            .andExpect(jsonPath("$.kodeVoucher").value(DEFAULT_KODE_VOUCHER))
            .andExpect(jsonPath("$.tglVoucher").value(sameInstant(DEFAULT_TGL_VOUCHER)))
            .andExpect(jsonPath("$.noSertifikat").value(DEFAULT_NO_SERTIFIKAT))
            .andExpect(jsonPath("$.tglSertifikat").value(sameInstant(DEFAULT_TGL_SERTIFIKAT)))
            .andExpect(jsonPath("$.biayaPnbp").value(DEFAULT_BIAYA_PNBP.intValue()))
            .andExpect(jsonPath("$.noReferensiBni").value(DEFAULT_NO_REFERENSI_BNI))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)))
            .andExpect(jsonPath("$.registrationId").value(DEFAULT_REGISTRATION_ID))
            .andExpect(jsonPath("$.tglOrder").value(DEFAULT_TGL_ORDER.toString()))
            .andExpect(jsonPath("$.tglCancel").value(DEFAULT_TGL_CANCEL.toString()))
            .andExpect(jsonPath("$.biayaJasa").value(DEFAULT_BIAYA_JASA.intValue()))
            .andExpect(jsonPath("$.invoiceSubmitted").value(DEFAULT_INVOICE_SUBMITTED.booleanValue()))
            .andExpect(jsonPath("$.reportSubmitted").value(DEFAULT_REPORT_SUBMITTED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingSertifikat() throws Exception {
        // Get the sertifikat
        restSertifikatMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSertifikat() throws Exception {
        // Initialize the database
        sertifikatRepository.saveAndFlush(sertifikat);

        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();

        // Update the sertifikat
        Sertifikat updatedSertifikat = sertifikatRepository.findById(sertifikat.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedSertifikat are not directly saved in db
        em.detach(updatedSertifikat);
        updatedSertifikat
            .kodeVoucher(UPDATED_KODE_VOUCHER)
            .tglVoucher(UPDATED_TGL_VOUCHER)
            .noSertifikat(UPDATED_NO_SERTIFIKAT)
            .tglSertifikat(UPDATED_TGL_SERTIFIKAT)
            .biayaPnbp(UPDATED_BIAYA_PNBP)
            .noReferensiBni(UPDATED_NO_REFERENSI_BNI)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .registrationId(UPDATED_REGISTRATION_ID)
            .tglOrder(UPDATED_TGL_ORDER)
            .tglCancel(UPDATED_TGL_CANCEL)
            .biayaJasa(UPDATED_BIAYA_JASA)
            .invoiceSubmitted(UPDATED_INVOICE_SUBMITTED)
            .reportSubmitted(UPDATED_REPORT_SUBMITTED);

        restSertifikatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSertifikat.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSertifikat))
            )
            .andExpect(status().isOk());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
        Sertifikat testSertifikat = sertifikatList.get(sertifikatList.size() - 1);
        assertThat(testSertifikat.getKodeVoucher()).isEqualTo(UPDATED_KODE_VOUCHER);
        assertThat(testSertifikat.getTglVoucher()).isEqualTo(UPDATED_TGL_VOUCHER);
        assertThat(testSertifikat.getNoSertifikat()).isEqualTo(UPDATED_NO_SERTIFIKAT);
        assertThat(testSertifikat.getTglSertifikat()).isEqualTo(UPDATED_TGL_SERTIFIKAT);
        assertThat(testSertifikat.getBiayaPnbp()).isEqualTo(UPDATED_BIAYA_PNBP);
        assertThat(testSertifikat.getNoReferensiBni()).isEqualTo(UPDATED_NO_REFERENSI_BNI);
        assertThat(testSertifikat.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testSertifikat.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testSertifikat.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testSertifikat.getRegistrationId()).isEqualTo(UPDATED_REGISTRATION_ID);
        assertThat(testSertifikat.getTglOrder()).isEqualTo(UPDATED_TGL_ORDER);
        assertThat(testSertifikat.getTglCancel()).isEqualTo(UPDATED_TGL_CANCEL);
        assertThat(testSertifikat.getBiayaJasa()).isEqualTo(UPDATED_BIAYA_JASA);
        assertThat(testSertifikat.getInvoiceSubmitted()).isEqualTo(UPDATED_INVOICE_SUBMITTED);
        assertThat(testSertifikat.getReportSubmitted()).isEqualTo(UPDATED_REPORT_SUBMITTED);
    }

    @Test
    @Transactional
    void putNonExistingSertifikat() throws Exception {
        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();
        sertifikat.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSertifikatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sertifikat.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sertifikat))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSertifikat() throws Exception {
        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();
        sertifikat.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSertifikatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sertifikat))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSertifikat() throws Exception {
        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();
        sertifikat.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSertifikatMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sertifikat)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSertifikatWithPatch() throws Exception {
        // Initialize the database
        sertifikatRepository.saveAndFlush(sertifikat);

        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();

        // Update the sertifikat using partial update
        Sertifikat partialUpdatedSertifikat = new Sertifikat();
        partialUpdatedSertifikat.setId(sertifikat.getId());

        partialUpdatedSertifikat
            .noSertifikat(UPDATED_NO_SERTIFIKAT)
            .tglSertifikat(UPDATED_TGL_SERTIFIKAT)
            .noReferensiBni(UPDATED_NO_REFERENSI_BNI)
            .invoiceSubmitted(UPDATED_INVOICE_SUBMITTED)
            .reportSubmitted(UPDATED_REPORT_SUBMITTED);

        restSertifikatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSertifikat.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSertifikat))
            )
            .andExpect(status().isOk());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
        Sertifikat testSertifikat = sertifikatList.get(sertifikatList.size() - 1);
        assertThat(testSertifikat.getKodeVoucher()).isEqualTo(DEFAULT_KODE_VOUCHER);
        assertThat(testSertifikat.getTglVoucher()).isEqualTo(DEFAULT_TGL_VOUCHER);
        assertThat(testSertifikat.getNoSertifikat()).isEqualTo(UPDATED_NO_SERTIFIKAT);
        assertThat(testSertifikat.getTglSertifikat()).isEqualTo(UPDATED_TGL_SERTIFIKAT);
        assertThat(testSertifikat.getBiayaPnbp()).isEqualTo(DEFAULT_BIAYA_PNBP);
        assertThat(testSertifikat.getNoReferensiBni()).isEqualTo(UPDATED_NO_REFERENSI_BNI);
        assertThat(testSertifikat.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testSertifikat.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testSertifikat.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testSertifikat.getRegistrationId()).isEqualTo(DEFAULT_REGISTRATION_ID);
        assertThat(testSertifikat.getTglOrder()).isEqualTo(DEFAULT_TGL_ORDER);
        assertThat(testSertifikat.getTglCancel()).isEqualTo(DEFAULT_TGL_CANCEL);
        assertThat(testSertifikat.getBiayaJasa()).isEqualTo(DEFAULT_BIAYA_JASA);
        assertThat(testSertifikat.getInvoiceSubmitted()).isEqualTo(UPDATED_INVOICE_SUBMITTED);
        assertThat(testSertifikat.getReportSubmitted()).isEqualTo(UPDATED_REPORT_SUBMITTED);
    }

    @Test
    @Transactional
    void fullUpdateSertifikatWithPatch() throws Exception {
        // Initialize the database
        sertifikatRepository.saveAndFlush(sertifikat);

        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();

        // Update the sertifikat using partial update
        Sertifikat partialUpdatedSertifikat = new Sertifikat();
        partialUpdatedSertifikat.setId(sertifikat.getId());

        partialUpdatedSertifikat
            .kodeVoucher(UPDATED_KODE_VOUCHER)
            .tglVoucher(UPDATED_TGL_VOUCHER)
            .noSertifikat(UPDATED_NO_SERTIFIKAT)
            .tglSertifikat(UPDATED_TGL_SERTIFIKAT)
            .biayaPnbp(UPDATED_BIAYA_PNBP)
            .noReferensiBni(UPDATED_NO_REFERENSI_BNI)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .registrationId(UPDATED_REGISTRATION_ID)
            .tglOrder(UPDATED_TGL_ORDER)
            .tglCancel(UPDATED_TGL_CANCEL)
            .biayaJasa(UPDATED_BIAYA_JASA)
            .invoiceSubmitted(UPDATED_INVOICE_SUBMITTED)
            .reportSubmitted(UPDATED_REPORT_SUBMITTED);

        restSertifikatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSertifikat.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSertifikat))
            )
            .andExpect(status().isOk());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
        Sertifikat testSertifikat = sertifikatList.get(sertifikatList.size() - 1);
        assertThat(testSertifikat.getKodeVoucher()).isEqualTo(UPDATED_KODE_VOUCHER);
        assertThat(testSertifikat.getTglVoucher()).isEqualTo(UPDATED_TGL_VOUCHER);
        assertThat(testSertifikat.getNoSertifikat()).isEqualTo(UPDATED_NO_SERTIFIKAT);
        assertThat(testSertifikat.getTglSertifikat()).isEqualTo(UPDATED_TGL_SERTIFIKAT);
        assertThat(testSertifikat.getBiayaPnbp()).isEqualTo(UPDATED_BIAYA_PNBP);
        assertThat(testSertifikat.getNoReferensiBni()).isEqualTo(UPDATED_NO_REFERENSI_BNI);
        assertThat(testSertifikat.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testSertifikat.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testSertifikat.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testSertifikat.getRegistrationId()).isEqualTo(UPDATED_REGISTRATION_ID);
        assertThat(testSertifikat.getTglOrder()).isEqualTo(UPDATED_TGL_ORDER);
        assertThat(testSertifikat.getTglCancel()).isEqualTo(UPDATED_TGL_CANCEL);
        assertThat(testSertifikat.getBiayaJasa()).isEqualTo(UPDATED_BIAYA_JASA);
        assertThat(testSertifikat.getInvoiceSubmitted()).isEqualTo(UPDATED_INVOICE_SUBMITTED);
        assertThat(testSertifikat.getReportSubmitted()).isEqualTo(UPDATED_REPORT_SUBMITTED);
    }

    @Test
    @Transactional
    void patchNonExistingSertifikat() throws Exception {
        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();
        sertifikat.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSertifikatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, sertifikat.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sertifikat))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSertifikat() throws Exception {
        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();
        sertifikat.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSertifikatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sertifikat))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSertifikat() throws Exception {
        int databaseSizeBeforeUpdate = sertifikatRepository.findAll().size();
        sertifikat.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSertifikatMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(sertifikat))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sertifikat in the database
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSertifikat() throws Exception {
        // Initialize the database
        sertifikatRepository.saveAndFlush(sertifikat);

        int databaseSizeBeforeDelete = sertifikatRepository.findAll().size();

        // Delete the sertifikat
        restSertifikatMockMvc
            .perform(delete(ENTITY_API_URL_ID, sertifikat.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sertifikat> sertifikatList = sertifikatRepository.findAll();
        assertThat(sertifikatList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
