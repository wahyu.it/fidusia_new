package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Berkas;
import com.project.fidusia.repository.BerkasRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BerkasResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BerkasResourceIT {

    private static final String DEFAULT_IDENTITAS = "AAAAAAAAAA";
    private static final String UPDATED_IDENTITAS = "BBBBBBBBBB";

    private static final String DEFAULT_KK = "AAAAAAAAAA";
    private static final String UPDATED_KK = "BBBBBBBBBB";

    private static final String DEFAULT_PPK = "AAAAAAAAAA";
    private static final String UPDATED_PPK = "BBBBBBBBBB";

    private static final String DEFAULT_SK_NASABAH = "AAAAAAAAAA";
    private static final String UPDATED_SK_NASABAH = "BBBBBBBBBB";

    private static final String DEFAULT_AKTA_PENDIRIAN = "AAAAAAAAAA";
    private static final String UPDATED_AKTA_PENDIRIAN = "BBBBBBBBBB";

    private static final String DEFAULT_AKTA_PERUBAHAN = "AAAAAAAAAA";
    private static final String UPDATED_AKTA_PERUBAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_SK_PENDIRIAN = "AAAAAAAAAA";
    private static final String UPDATED_SK_PENDIRIAN = "BBBBBBBBBB";

    private static final String DEFAULT_SK_PERUBAHAN = "AAAAAAAAAA";
    private static final String UPDATED_SK_PERUBAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_NPWP = "AAAAAAAAAA";
    private static final String UPDATED_NPWP = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_UPLOAD_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPLOAD_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPLOAD_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPLOAD_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_PPK_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_PPK_NOMOR = "BBBBBBBBBB";

    private static final Integer DEFAULT_NSB_JENIS = 1;
    private static final Integer UPDATED_NSB_JENIS = 2;

    private static final Integer DEFAULT_BERKAS_STATUS = 1;
    private static final Integer UPDATED_BERKAS_STATUS = 2;

    private static final String DEFAULT_NAMA_NASABAH = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_NASABAH = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL_PPK = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_PPK = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_METODE_KIRIM = 1;
    private static final Integer UPDATED_METODE_KIRIM = 2;

    private static final Integer DEFAULT_PARTITION_ID = 1;
    private static final Integer UPDATED_PARTITION_ID = 2;

    private static final String DEFAULT_VERIFY_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFY_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_VERIFY_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VERIFY_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_ORDER_TYPE = 1;
    private static final Integer UPDATED_ORDER_TYPE = 2;

    private static final String ENTITY_API_URL = "/api/berkas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BerkasRepository berkasRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBerkasMockMvc;

    private Berkas berkas;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Berkas createEntity(EntityManager em) {
        Berkas berkas = new Berkas()
            .identitas(DEFAULT_IDENTITAS)
            .kk(DEFAULT_KK)
            .ppk(DEFAULT_PPK)
            .skNasabah(DEFAULT_SK_NASABAH)
            .aktaPendirian(DEFAULT_AKTA_PENDIRIAN)
            .aktaPerubahan(DEFAULT_AKTA_PERUBAHAN)
            .skPendirian(DEFAULT_SK_PENDIRIAN)
            .skPerubahan(DEFAULT_SK_PERUBAHAN)
            .npwp(DEFAULT_NPWP)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON)
            .uploadBy(DEFAULT_UPLOAD_BY)
            .uploadOn(DEFAULT_UPLOAD_ON)
            .ppkNomor(DEFAULT_PPK_NOMOR)
            .nsbJenis(DEFAULT_NSB_JENIS)
            .berkasStatus(DEFAULT_BERKAS_STATUS)
            .namaNasabah(DEFAULT_NAMA_NASABAH)
            .tanggalPpk(DEFAULT_TANGGAL_PPK)
            .metodeKirim(DEFAULT_METODE_KIRIM)
            .partitionId(DEFAULT_PARTITION_ID)
            .verifyBy(DEFAULT_VERIFY_BY)
            .verifyOn(DEFAULT_VERIFY_ON)
            .orderType(DEFAULT_ORDER_TYPE);
        return berkas;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Berkas createUpdatedEntity(EntityManager em) {
        Berkas berkas = new Berkas()
            .identitas(UPDATED_IDENTITAS)
            .kk(UPDATED_KK)
            .ppk(UPDATED_PPK)
            .skNasabah(UPDATED_SK_NASABAH)
            .aktaPendirian(UPDATED_AKTA_PENDIRIAN)
            .aktaPerubahan(UPDATED_AKTA_PERUBAHAN)
            .skPendirian(UPDATED_SK_PENDIRIAN)
            .skPerubahan(UPDATED_SK_PERUBAHAN)
            .npwp(UPDATED_NPWP)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .uploadBy(UPDATED_UPLOAD_BY)
            .uploadOn(UPDATED_UPLOAD_ON)
            .ppkNomor(UPDATED_PPK_NOMOR)
            .nsbJenis(UPDATED_NSB_JENIS)
            .berkasStatus(UPDATED_BERKAS_STATUS)
            .namaNasabah(UPDATED_NAMA_NASABAH)
            .tanggalPpk(UPDATED_TANGGAL_PPK)
            .metodeKirim(UPDATED_METODE_KIRIM)
            .partitionId(UPDATED_PARTITION_ID)
            .verifyBy(UPDATED_VERIFY_BY)
            .verifyOn(UPDATED_VERIFY_ON)
            .orderType(UPDATED_ORDER_TYPE);
        return berkas;
    }

    @BeforeEach
    public void initTest() {
        berkas = createEntity(em);
    }

    @Test
    @Transactional
    void createBerkas() throws Exception {
        int databaseSizeBeforeCreate = berkasRepository.findAll().size();
        // Create the Berkas
        restBerkasMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(berkas)))
            .andExpect(status().isCreated());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeCreate + 1);
        Berkas testBerkas = berkasList.get(berkasList.size() - 1);
        assertThat(testBerkas.getIdentitas()).isEqualTo(DEFAULT_IDENTITAS);
        assertThat(testBerkas.getKk()).isEqualTo(DEFAULT_KK);
        assertThat(testBerkas.getPpk()).isEqualTo(DEFAULT_PPK);
        assertThat(testBerkas.getSkNasabah()).isEqualTo(DEFAULT_SK_NASABAH);
        assertThat(testBerkas.getAktaPendirian()).isEqualTo(DEFAULT_AKTA_PENDIRIAN);
        assertThat(testBerkas.getAktaPerubahan()).isEqualTo(DEFAULT_AKTA_PERUBAHAN);
        assertThat(testBerkas.getSkPendirian()).isEqualTo(DEFAULT_SK_PENDIRIAN);
        assertThat(testBerkas.getSkPerubahan()).isEqualTo(DEFAULT_SK_PERUBAHAN);
        assertThat(testBerkas.getNpwp()).isEqualTo(DEFAULT_NPWP);
        assertThat(testBerkas.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testBerkas.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
        assertThat(testBerkas.getUploadBy()).isEqualTo(DEFAULT_UPLOAD_BY);
        assertThat(testBerkas.getUploadOn()).isEqualTo(DEFAULT_UPLOAD_ON);
        assertThat(testBerkas.getPpkNomor()).isEqualTo(DEFAULT_PPK_NOMOR);
        assertThat(testBerkas.getNsbJenis()).isEqualTo(DEFAULT_NSB_JENIS);
        assertThat(testBerkas.getBerkasStatus()).isEqualTo(DEFAULT_BERKAS_STATUS);
        assertThat(testBerkas.getNamaNasabah()).isEqualTo(DEFAULT_NAMA_NASABAH);
        assertThat(testBerkas.getTanggalPpk()).isEqualTo(DEFAULT_TANGGAL_PPK);
        assertThat(testBerkas.getMetodeKirim()).isEqualTo(DEFAULT_METODE_KIRIM);
        assertThat(testBerkas.getPartitionId()).isEqualTo(DEFAULT_PARTITION_ID);
        assertThat(testBerkas.getVerifyBy()).isEqualTo(DEFAULT_VERIFY_BY);
        assertThat(testBerkas.getVerifyOn()).isEqualTo(DEFAULT_VERIFY_ON);
        assertThat(testBerkas.getOrderType()).isEqualTo(DEFAULT_ORDER_TYPE);
    }

    @Test
    @Transactional
    void createBerkasWithExistingId() throws Exception {
        // Create the Berkas with an existing ID
        berkas.setId(1L);

        int databaseSizeBeforeCreate = berkasRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBerkasMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(berkas)))
            .andExpect(status().isBadRequest());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllBerkas() throws Exception {
        // Initialize the database
        berkasRepository.saveAndFlush(berkas);

        // Get all the berkasList
        restBerkasMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(berkas.getId().intValue())))
            .andExpect(jsonPath("$.[*].identitas").value(hasItem(DEFAULT_IDENTITAS)))
            .andExpect(jsonPath("$.[*].kk").value(hasItem(DEFAULT_KK)))
            .andExpect(jsonPath("$.[*].ppk").value(hasItem(DEFAULT_PPK)))
            .andExpect(jsonPath("$.[*].skNasabah").value(hasItem(DEFAULT_SK_NASABAH)))
            .andExpect(jsonPath("$.[*].aktaPendirian").value(hasItem(DEFAULT_AKTA_PENDIRIAN)))
            .andExpect(jsonPath("$.[*].aktaPerubahan").value(hasItem(DEFAULT_AKTA_PERUBAHAN)))
            .andExpect(jsonPath("$.[*].skPendirian").value(hasItem(DEFAULT_SK_PENDIRIAN)))
            .andExpect(jsonPath("$.[*].skPerubahan").value(hasItem(DEFAULT_SK_PERUBAHAN)))
            .andExpect(jsonPath("$.[*].npwp").value(hasItem(DEFAULT_NPWP)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))))
            .andExpect(jsonPath("$.[*].uploadBy").value(hasItem(DEFAULT_UPLOAD_BY)))
            .andExpect(jsonPath("$.[*].uploadOn").value(hasItem(sameInstant(DEFAULT_UPLOAD_ON))))
            .andExpect(jsonPath("$.[*].ppkNomor").value(hasItem(DEFAULT_PPK_NOMOR)))
            .andExpect(jsonPath("$.[*].nsbJenis").value(hasItem(DEFAULT_NSB_JENIS)))
            .andExpect(jsonPath("$.[*].berkasStatus").value(hasItem(DEFAULT_BERKAS_STATUS)))
            .andExpect(jsonPath("$.[*].namaNasabah").value(hasItem(DEFAULT_NAMA_NASABAH)))
            .andExpect(jsonPath("$.[*].tanggalPpk").value(hasItem(DEFAULT_TANGGAL_PPK.toString())))
            .andExpect(jsonPath("$.[*].metodeKirim").value(hasItem(DEFAULT_METODE_KIRIM)))
            .andExpect(jsonPath("$.[*].partitionId").value(hasItem(DEFAULT_PARTITION_ID)))
            .andExpect(jsonPath("$.[*].verifyBy").value(hasItem(DEFAULT_VERIFY_BY)))
            .andExpect(jsonPath("$.[*].verifyOn").value(hasItem(sameInstant(DEFAULT_VERIFY_ON))))
            .andExpect(jsonPath("$.[*].orderType").value(hasItem(DEFAULT_ORDER_TYPE)));
    }

    @Test
    @Transactional
    void getBerkas() throws Exception {
        // Initialize the database
        berkasRepository.saveAndFlush(berkas);

        // Get the berkas
        restBerkasMockMvc
            .perform(get(ENTITY_API_URL_ID, berkas.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(berkas.getId().intValue()))
            .andExpect(jsonPath("$.identitas").value(DEFAULT_IDENTITAS))
            .andExpect(jsonPath("$.kk").value(DEFAULT_KK))
            .andExpect(jsonPath("$.ppk").value(DEFAULT_PPK))
            .andExpect(jsonPath("$.skNasabah").value(DEFAULT_SK_NASABAH))
            .andExpect(jsonPath("$.aktaPendirian").value(DEFAULT_AKTA_PENDIRIAN))
            .andExpect(jsonPath("$.aktaPerubahan").value(DEFAULT_AKTA_PERUBAHAN))
            .andExpect(jsonPath("$.skPendirian").value(DEFAULT_SK_PENDIRIAN))
            .andExpect(jsonPath("$.skPerubahan").value(DEFAULT_SK_PERUBAHAN))
            .andExpect(jsonPath("$.npwp").value(DEFAULT_NPWP))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)))
            .andExpect(jsonPath("$.uploadBy").value(DEFAULT_UPLOAD_BY))
            .andExpect(jsonPath("$.uploadOn").value(sameInstant(DEFAULT_UPLOAD_ON)))
            .andExpect(jsonPath("$.ppkNomor").value(DEFAULT_PPK_NOMOR))
            .andExpect(jsonPath("$.nsbJenis").value(DEFAULT_NSB_JENIS))
            .andExpect(jsonPath("$.berkasStatus").value(DEFAULT_BERKAS_STATUS))
            .andExpect(jsonPath("$.namaNasabah").value(DEFAULT_NAMA_NASABAH))
            .andExpect(jsonPath("$.tanggalPpk").value(DEFAULT_TANGGAL_PPK.toString()))
            .andExpect(jsonPath("$.metodeKirim").value(DEFAULT_METODE_KIRIM))
            .andExpect(jsonPath("$.partitionId").value(DEFAULT_PARTITION_ID))
            .andExpect(jsonPath("$.verifyBy").value(DEFAULT_VERIFY_BY))
            .andExpect(jsonPath("$.verifyOn").value(sameInstant(DEFAULT_VERIFY_ON)))
            .andExpect(jsonPath("$.orderType").value(DEFAULT_ORDER_TYPE));
    }

    @Test
    @Transactional
    void getNonExistingBerkas() throws Exception {
        // Get the berkas
        restBerkasMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingBerkas() throws Exception {
        // Initialize the database
        berkasRepository.saveAndFlush(berkas);

        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();

        // Update the berkas
        Berkas updatedBerkas = berkasRepository.findById(berkas.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedBerkas are not directly saved in db
        em.detach(updatedBerkas);
        updatedBerkas
            .identitas(UPDATED_IDENTITAS)
            .kk(UPDATED_KK)
            .ppk(UPDATED_PPK)
            .skNasabah(UPDATED_SK_NASABAH)
            .aktaPendirian(UPDATED_AKTA_PENDIRIAN)
            .aktaPerubahan(UPDATED_AKTA_PERUBAHAN)
            .skPendirian(UPDATED_SK_PENDIRIAN)
            .skPerubahan(UPDATED_SK_PERUBAHAN)
            .npwp(UPDATED_NPWP)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .uploadBy(UPDATED_UPLOAD_BY)
            .uploadOn(UPDATED_UPLOAD_ON)
            .ppkNomor(UPDATED_PPK_NOMOR)
            .nsbJenis(UPDATED_NSB_JENIS)
            .berkasStatus(UPDATED_BERKAS_STATUS)
            .namaNasabah(UPDATED_NAMA_NASABAH)
            .tanggalPpk(UPDATED_TANGGAL_PPK)
            .metodeKirim(UPDATED_METODE_KIRIM)
            .partitionId(UPDATED_PARTITION_ID)
            .verifyBy(UPDATED_VERIFY_BY)
            .verifyOn(UPDATED_VERIFY_ON)
            .orderType(UPDATED_ORDER_TYPE);

        restBerkasMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedBerkas.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedBerkas))
            )
            .andExpect(status().isOk());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
        Berkas testBerkas = berkasList.get(berkasList.size() - 1);
        assertThat(testBerkas.getIdentitas()).isEqualTo(UPDATED_IDENTITAS);
        assertThat(testBerkas.getKk()).isEqualTo(UPDATED_KK);
        assertThat(testBerkas.getPpk()).isEqualTo(UPDATED_PPK);
        assertThat(testBerkas.getSkNasabah()).isEqualTo(UPDATED_SK_NASABAH);
        assertThat(testBerkas.getAktaPendirian()).isEqualTo(UPDATED_AKTA_PENDIRIAN);
        assertThat(testBerkas.getAktaPerubahan()).isEqualTo(UPDATED_AKTA_PERUBAHAN);
        assertThat(testBerkas.getSkPendirian()).isEqualTo(UPDATED_SK_PENDIRIAN);
        assertThat(testBerkas.getSkPerubahan()).isEqualTo(UPDATED_SK_PERUBAHAN);
        assertThat(testBerkas.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testBerkas.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testBerkas.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testBerkas.getUploadBy()).isEqualTo(UPDATED_UPLOAD_BY);
        assertThat(testBerkas.getUploadOn()).isEqualTo(UPDATED_UPLOAD_ON);
        assertThat(testBerkas.getPpkNomor()).isEqualTo(UPDATED_PPK_NOMOR);
        assertThat(testBerkas.getNsbJenis()).isEqualTo(UPDATED_NSB_JENIS);
        assertThat(testBerkas.getBerkasStatus()).isEqualTo(UPDATED_BERKAS_STATUS);
        assertThat(testBerkas.getNamaNasabah()).isEqualTo(UPDATED_NAMA_NASABAH);
        assertThat(testBerkas.getTanggalPpk()).isEqualTo(UPDATED_TANGGAL_PPK);
        assertThat(testBerkas.getMetodeKirim()).isEqualTo(UPDATED_METODE_KIRIM);
        assertThat(testBerkas.getPartitionId()).isEqualTo(UPDATED_PARTITION_ID);
        assertThat(testBerkas.getVerifyBy()).isEqualTo(UPDATED_VERIFY_BY);
        assertThat(testBerkas.getVerifyOn()).isEqualTo(UPDATED_VERIFY_ON);
        assertThat(testBerkas.getOrderType()).isEqualTo(UPDATED_ORDER_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingBerkas() throws Exception {
        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();
        berkas.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBerkasMockMvc
            .perform(
                put(ENTITY_API_URL_ID, berkas.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(berkas))
            )
            .andExpect(status().isBadRequest());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBerkas() throws Exception {
        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();
        berkas.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBerkasMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(berkas))
            )
            .andExpect(status().isBadRequest());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBerkas() throws Exception {
        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();
        berkas.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBerkasMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(berkas)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBerkasWithPatch() throws Exception {
        // Initialize the database
        berkasRepository.saveAndFlush(berkas);

        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();

        // Update the berkas using partial update
        Berkas partialUpdatedBerkas = new Berkas();
        partialUpdatedBerkas.setId(berkas.getId());

        partialUpdatedBerkas
            .identitas(UPDATED_IDENTITAS)
            .ppk(UPDATED_PPK)
            .aktaPendirian(UPDATED_AKTA_PENDIRIAN)
            .skPendirian(UPDATED_SK_PENDIRIAN)
            .skPerubahan(UPDATED_SK_PERUBAHAN)
            .npwp(UPDATED_NPWP)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .uploadBy(UPDATED_UPLOAD_BY)
            .ppkNomor(UPDATED_PPK_NOMOR)
            .nsbJenis(UPDATED_NSB_JENIS)
            .namaNasabah(UPDATED_NAMA_NASABAH)
            .tanggalPpk(UPDATED_TANGGAL_PPK)
            .metodeKirim(UPDATED_METODE_KIRIM)
            .partitionId(UPDATED_PARTITION_ID)
            .verifyBy(UPDATED_VERIFY_BY)
            .verifyOn(UPDATED_VERIFY_ON);

        restBerkasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBerkas.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBerkas))
            )
            .andExpect(status().isOk());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
        Berkas testBerkas = berkasList.get(berkasList.size() - 1);
        assertThat(testBerkas.getIdentitas()).isEqualTo(UPDATED_IDENTITAS);
        assertThat(testBerkas.getKk()).isEqualTo(DEFAULT_KK);
        assertThat(testBerkas.getPpk()).isEqualTo(UPDATED_PPK);
        assertThat(testBerkas.getSkNasabah()).isEqualTo(DEFAULT_SK_NASABAH);
        assertThat(testBerkas.getAktaPendirian()).isEqualTo(UPDATED_AKTA_PENDIRIAN);
        assertThat(testBerkas.getAktaPerubahan()).isEqualTo(DEFAULT_AKTA_PERUBAHAN);
        assertThat(testBerkas.getSkPendirian()).isEqualTo(UPDATED_SK_PENDIRIAN);
        assertThat(testBerkas.getSkPerubahan()).isEqualTo(UPDATED_SK_PERUBAHAN);
        assertThat(testBerkas.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testBerkas.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testBerkas.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testBerkas.getUploadBy()).isEqualTo(UPDATED_UPLOAD_BY);
        assertThat(testBerkas.getUploadOn()).isEqualTo(DEFAULT_UPLOAD_ON);
        assertThat(testBerkas.getPpkNomor()).isEqualTo(UPDATED_PPK_NOMOR);
        assertThat(testBerkas.getNsbJenis()).isEqualTo(UPDATED_NSB_JENIS);
        assertThat(testBerkas.getBerkasStatus()).isEqualTo(DEFAULT_BERKAS_STATUS);
        assertThat(testBerkas.getNamaNasabah()).isEqualTo(UPDATED_NAMA_NASABAH);
        assertThat(testBerkas.getTanggalPpk()).isEqualTo(UPDATED_TANGGAL_PPK);
        assertThat(testBerkas.getMetodeKirim()).isEqualTo(UPDATED_METODE_KIRIM);
        assertThat(testBerkas.getPartitionId()).isEqualTo(UPDATED_PARTITION_ID);
        assertThat(testBerkas.getVerifyBy()).isEqualTo(UPDATED_VERIFY_BY);
        assertThat(testBerkas.getVerifyOn()).isEqualTo(UPDATED_VERIFY_ON);
        assertThat(testBerkas.getOrderType()).isEqualTo(DEFAULT_ORDER_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateBerkasWithPatch() throws Exception {
        // Initialize the database
        berkasRepository.saveAndFlush(berkas);

        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();

        // Update the berkas using partial update
        Berkas partialUpdatedBerkas = new Berkas();
        partialUpdatedBerkas.setId(berkas.getId());

        partialUpdatedBerkas
            .identitas(UPDATED_IDENTITAS)
            .kk(UPDATED_KK)
            .ppk(UPDATED_PPK)
            .skNasabah(UPDATED_SK_NASABAH)
            .aktaPendirian(UPDATED_AKTA_PENDIRIAN)
            .aktaPerubahan(UPDATED_AKTA_PERUBAHAN)
            .skPendirian(UPDATED_SK_PENDIRIAN)
            .skPerubahan(UPDATED_SK_PERUBAHAN)
            .npwp(UPDATED_NPWP)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON)
            .uploadBy(UPDATED_UPLOAD_BY)
            .uploadOn(UPDATED_UPLOAD_ON)
            .ppkNomor(UPDATED_PPK_NOMOR)
            .nsbJenis(UPDATED_NSB_JENIS)
            .berkasStatus(UPDATED_BERKAS_STATUS)
            .namaNasabah(UPDATED_NAMA_NASABAH)
            .tanggalPpk(UPDATED_TANGGAL_PPK)
            .metodeKirim(UPDATED_METODE_KIRIM)
            .partitionId(UPDATED_PARTITION_ID)
            .verifyBy(UPDATED_VERIFY_BY)
            .verifyOn(UPDATED_VERIFY_ON)
            .orderType(UPDATED_ORDER_TYPE);

        restBerkasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBerkas.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBerkas))
            )
            .andExpect(status().isOk());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
        Berkas testBerkas = berkasList.get(berkasList.size() - 1);
        assertThat(testBerkas.getIdentitas()).isEqualTo(UPDATED_IDENTITAS);
        assertThat(testBerkas.getKk()).isEqualTo(UPDATED_KK);
        assertThat(testBerkas.getPpk()).isEqualTo(UPDATED_PPK);
        assertThat(testBerkas.getSkNasabah()).isEqualTo(UPDATED_SK_NASABAH);
        assertThat(testBerkas.getAktaPendirian()).isEqualTo(UPDATED_AKTA_PENDIRIAN);
        assertThat(testBerkas.getAktaPerubahan()).isEqualTo(UPDATED_AKTA_PERUBAHAN);
        assertThat(testBerkas.getSkPendirian()).isEqualTo(UPDATED_SK_PENDIRIAN);
        assertThat(testBerkas.getSkPerubahan()).isEqualTo(UPDATED_SK_PERUBAHAN);
        assertThat(testBerkas.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testBerkas.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testBerkas.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
        assertThat(testBerkas.getUploadBy()).isEqualTo(UPDATED_UPLOAD_BY);
        assertThat(testBerkas.getUploadOn()).isEqualTo(UPDATED_UPLOAD_ON);
        assertThat(testBerkas.getPpkNomor()).isEqualTo(UPDATED_PPK_NOMOR);
        assertThat(testBerkas.getNsbJenis()).isEqualTo(UPDATED_NSB_JENIS);
        assertThat(testBerkas.getBerkasStatus()).isEqualTo(UPDATED_BERKAS_STATUS);
        assertThat(testBerkas.getNamaNasabah()).isEqualTo(UPDATED_NAMA_NASABAH);
        assertThat(testBerkas.getTanggalPpk()).isEqualTo(UPDATED_TANGGAL_PPK);
        assertThat(testBerkas.getMetodeKirim()).isEqualTo(UPDATED_METODE_KIRIM);
        assertThat(testBerkas.getPartitionId()).isEqualTo(UPDATED_PARTITION_ID);
        assertThat(testBerkas.getVerifyBy()).isEqualTo(UPDATED_VERIFY_BY);
        assertThat(testBerkas.getVerifyOn()).isEqualTo(UPDATED_VERIFY_ON);
        assertThat(testBerkas.getOrderType()).isEqualTo(UPDATED_ORDER_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingBerkas() throws Exception {
        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();
        berkas.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBerkasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, berkas.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(berkas))
            )
            .andExpect(status().isBadRequest());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBerkas() throws Exception {
        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();
        berkas.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBerkasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(berkas))
            )
            .andExpect(status().isBadRequest());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBerkas() throws Exception {
        int databaseSizeBeforeUpdate = berkasRepository.findAll().size();
        berkas.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBerkasMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(berkas)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Berkas in the database
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBerkas() throws Exception {
        // Initialize the database
        berkasRepository.saveAndFlush(berkas);

        int databaseSizeBeforeDelete = berkasRepository.findAll().size();

        // Delete the berkas
        restBerkasMockMvc
            .perform(delete(ENTITY_API_URL_ID, berkas.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Berkas> berkasList = berkasRepository.findAll();
        assertThat(berkasList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
