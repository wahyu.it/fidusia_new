package com.project.fidusia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Badan;
import com.project.fidusia.repository.BadanRepository;
import jakarta.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BadanResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BadanResourceIT {

    private static final String DEFAULT_GOLONGAN_USAHA = "AAAAAAAAAA";
    private static final String UPDATED_GOLONGAN_USAHA = "BBBBBBBBBB";

    private static final String DEFAULT_JENIS = "AAAAAAAAAA";
    private static final String UPDATED_JENIS = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_KEDUDUKAN = "AAAAAAAAAA";
    private static final String UPDATED_KEDUDUKAN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_AKTA = "AAAAAAAAAA";
    private static final String UPDATED_NO_AKTA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TGL_AKTA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_AKTA = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NAMA_NOTARIS = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_NOTARIS = "BBBBBBBBBB";

    private static final String DEFAULT_WIL_KERJA_NOTARIS = "AAAAAAAAAA";
    private static final String UPDATED_WIL_KERJA_NOTARIS = "BBBBBBBBBB";

    private static final String DEFAULT_SK_NOTARIS = "AAAAAAAAAA";
    private static final String UPDATED_SK_NOTARIS = "BBBBBBBBBB";

    private static final String DEFAULT_ALAMAT = "AAAAAAAAAA";
    private static final String UPDATED_ALAMAT = "BBBBBBBBBB";

    private static final String DEFAULT_RT = "AAAAAAAAAA";
    private static final String UPDATED_RT = "BBBBBBBBBB";

    private static final String DEFAULT_RW = "AAAAAAAAAA";
    private static final String UPDATED_RW = "BBBBBBBBBB";

    private static final String DEFAULT_KELURAHAN = "AAAAAAAAAA";
    private static final String UPDATED_KELURAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_KECAMATAN = "AAAAAAAAAA";
    private static final String UPDATED_KECAMATAN = "BBBBBBBBBB";

    private static final String DEFAULT_KOTA = "AAAAAAAAAA";
    private static final String UPDATED_KOTA = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINSI = "AAAAAAAAAA";
    private static final String UPDATED_PROVINSI = "BBBBBBBBBB";

    private static final String DEFAULT_KODE_POS = "AAAAAAAAAA";
    private static final String UPDATED_KODE_POS = "BBBBBBBBBB";

    private static final String DEFAULT_NO_KONTAK = "AAAAAAAAAA";
    private static final String UPDATED_NO_KONTAK = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_SK = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_SK = "BBBBBBBBBB";

    private static final String DEFAULT_NOMOR_SK = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR_SK = "BBBBBBBBBB";

    private static final String DEFAULT_TGL_SK = "AAAAAAAAAA";
    private static final String UPDATED_TGL_SK = "BBBBBBBBBB";

    private static final String DEFAULT_NPWP = "AAAAAAAAAA";
    private static final String UPDATED_NPWP = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_PJ_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_JENIS_KELAMIN = "AAAAAAAAAA";
    private static final String UPDATED_PJ_JENIS_KELAMIN = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_STATUS_KAWIN = "AAAAAAAAAA";
    private static final String UPDATED_PJ_STATUS_KAWIN = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_KELAHIRAN = "AAAAAAAAAA";
    private static final String UPDATED_PJ_KELAHIRAN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_PJ_TGL_LAHIR = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PJ_TGL_LAHIR = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_PJ_PEKERJAAN = "AAAAAAAAAA";
    private static final String UPDATED_PJ_PEKERJAAN = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_WARGA_NEGARA = "AAAAAAAAAA";
    private static final String UPDATED_PJ_WARGA_NEGARA = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_JENIS_ID = "AAAAAAAAAA";
    private static final String UPDATED_PJ_JENIS_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_NO_ID = "AAAAAAAAAA";
    private static final String UPDATED_PJ_NO_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_ALAMAT = "AAAAAAAAAA";
    private static final String UPDATED_PJ_ALAMAT = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_RT = "AAAAAAAAAA";
    private static final String UPDATED_PJ_RT = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_RW = "AAAAAAAAAA";
    private static final String UPDATED_PJ_RW = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_KELURAHAN = "AAAAAAAAAA";
    private static final String UPDATED_PJ_KELURAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_KECAMATAN = "AAAAAAAAAA";
    private static final String UPDATED_PJ_KECAMATAN = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_KOTA = "AAAAAAAAAA";
    private static final String UPDATED_PJ_KOTA = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_PROVINSI = "AAAAAAAAAA";
    private static final String UPDATED_PJ_PROVINSI = "BBBBBBBBBB";

    private static final String DEFAULT_PJ_KODE_POS = "AAAAAAAAAA";
    private static final String UPDATED_PJ_KODE_POS = "BBBBBBBBBB";

    private static final String DEFAULT_TEMPLATE = "AAAAAAAAAA";
    private static final String UPDATED_TEMPLATE = "BBBBBBBBBB";

    private static final String DEFAULT_KOMPARISI = "AAAAAAAAAA";
    private static final String UPDATED_KOMPARISI = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/badans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BadanRepository badanRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBadanMockMvc;

    private Badan badan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Badan createEntity(EntityManager em) {
        Badan badan = new Badan()
            .golonganUsaha(DEFAULT_GOLONGAN_USAHA)
            .jenis(DEFAULT_JENIS)
            .nama(DEFAULT_NAMA)
            .kedudukan(DEFAULT_KEDUDUKAN)
            .noAkta(DEFAULT_NO_AKTA)
            .tglAkta(DEFAULT_TGL_AKTA)
            .namaNotaris(DEFAULT_NAMA_NOTARIS)
            .wilKerjaNotaris(DEFAULT_WIL_KERJA_NOTARIS)
            .skNotaris(DEFAULT_SK_NOTARIS)
            .alamat(DEFAULT_ALAMAT)
            .rt(DEFAULT_RT)
            .rw(DEFAULT_RW)
            .kelurahan(DEFAULT_KELURAHAN)
            .kecamatan(DEFAULT_KECAMATAN)
            .kota(DEFAULT_KOTA)
            .provinsi(DEFAULT_PROVINSI)
            .kodePos(DEFAULT_KODE_POS)
            .noKontak(DEFAULT_NO_KONTAK)
            .namaSk(DEFAULT_NAMA_SK)
            .nomorSk(DEFAULT_NOMOR_SK)
            .tglSk(DEFAULT_TGL_SK)
            .npwp(DEFAULT_NPWP)
            .pjNama(DEFAULT_PJ_NAMA)
            .pjJenisKelamin(DEFAULT_PJ_JENIS_KELAMIN)
            .pjStatusKawin(DEFAULT_PJ_STATUS_KAWIN)
            .pjKelahiran(DEFAULT_PJ_KELAHIRAN)
            .pjTglLahir(DEFAULT_PJ_TGL_LAHIR)
            .pjPekerjaan(DEFAULT_PJ_PEKERJAAN)
            .pjWargaNegara(DEFAULT_PJ_WARGA_NEGARA)
            .pjJenisId(DEFAULT_PJ_JENIS_ID)
            .pjNoId(DEFAULT_PJ_NO_ID)
            .pjAlamat(DEFAULT_PJ_ALAMAT)
            .pjRt(DEFAULT_PJ_RT)
            .pjRw(DEFAULT_PJ_RW)
            .pjKelurahan(DEFAULT_PJ_KELURAHAN)
            .pjKecamatan(DEFAULT_PJ_KECAMATAN)
            .pjKota(DEFAULT_PJ_KOTA)
            .pjProvinsi(DEFAULT_PJ_PROVINSI)
            .pjKodePos(DEFAULT_PJ_KODE_POS)
            .template(DEFAULT_TEMPLATE)
            .komparisi(DEFAULT_KOMPARISI);
        return badan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Badan createUpdatedEntity(EntityManager em) {
        Badan badan = new Badan()
            .golonganUsaha(UPDATED_GOLONGAN_USAHA)
            .jenis(UPDATED_JENIS)
            .nama(UPDATED_NAMA)
            .kedudukan(UPDATED_KEDUDUKAN)
            .noAkta(UPDATED_NO_AKTA)
            .tglAkta(UPDATED_TGL_AKTA)
            .namaNotaris(UPDATED_NAMA_NOTARIS)
            .wilKerjaNotaris(UPDATED_WIL_KERJA_NOTARIS)
            .skNotaris(UPDATED_SK_NOTARIS)
            .alamat(UPDATED_ALAMAT)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .kelurahan(UPDATED_KELURAHAN)
            .kecamatan(UPDATED_KECAMATAN)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK)
            .namaSk(UPDATED_NAMA_SK)
            .nomorSk(UPDATED_NOMOR_SK)
            .tglSk(UPDATED_TGL_SK)
            .npwp(UPDATED_NPWP)
            .pjNama(UPDATED_PJ_NAMA)
            .pjJenisKelamin(UPDATED_PJ_JENIS_KELAMIN)
            .pjStatusKawin(UPDATED_PJ_STATUS_KAWIN)
            .pjKelahiran(UPDATED_PJ_KELAHIRAN)
            .pjTglLahir(UPDATED_PJ_TGL_LAHIR)
            .pjPekerjaan(UPDATED_PJ_PEKERJAAN)
            .pjWargaNegara(UPDATED_PJ_WARGA_NEGARA)
            .pjJenisId(UPDATED_PJ_JENIS_ID)
            .pjNoId(UPDATED_PJ_NO_ID)
            .pjAlamat(UPDATED_PJ_ALAMAT)
            .pjRt(UPDATED_PJ_RT)
            .pjRw(UPDATED_PJ_RW)
            .pjKelurahan(UPDATED_PJ_KELURAHAN)
            .pjKecamatan(UPDATED_PJ_KECAMATAN)
            .pjKota(UPDATED_PJ_KOTA)
            .pjProvinsi(UPDATED_PJ_PROVINSI)
            .pjKodePos(UPDATED_PJ_KODE_POS)
            .template(UPDATED_TEMPLATE)
            .komparisi(UPDATED_KOMPARISI);
        return badan;
    }

    @BeforeEach
    public void initTest() {
        badan = createEntity(em);
    }

    @Test
    @Transactional
    void createBadan() throws Exception {
        int databaseSizeBeforeCreate = badanRepository.findAll().size();
        // Create the Badan
        restBadanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(badan)))
            .andExpect(status().isCreated());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeCreate + 1);
        Badan testBadan = badanList.get(badanList.size() - 1);
        assertThat(testBadan.getGolonganUsaha()).isEqualTo(DEFAULT_GOLONGAN_USAHA);
        assertThat(testBadan.getJenis()).isEqualTo(DEFAULT_JENIS);
        assertThat(testBadan.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testBadan.getKedudukan()).isEqualTo(DEFAULT_KEDUDUKAN);
        assertThat(testBadan.getNoAkta()).isEqualTo(DEFAULT_NO_AKTA);
        assertThat(testBadan.getTglAkta()).isEqualTo(DEFAULT_TGL_AKTA);
        assertThat(testBadan.getNamaNotaris()).isEqualTo(DEFAULT_NAMA_NOTARIS);
        assertThat(testBadan.getWilKerjaNotaris()).isEqualTo(DEFAULT_WIL_KERJA_NOTARIS);
        assertThat(testBadan.getSkNotaris()).isEqualTo(DEFAULT_SK_NOTARIS);
        assertThat(testBadan.getAlamat()).isEqualTo(DEFAULT_ALAMAT);
        assertThat(testBadan.getRt()).isEqualTo(DEFAULT_RT);
        assertThat(testBadan.getRw()).isEqualTo(DEFAULT_RW);
        assertThat(testBadan.getKelurahan()).isEqualTo(DEFAULT_KELURAHAN);
        assertThat(testBadan.getKecamatan()).isEqualTo(DEFAULT_KECAMATAN);
        assertThat(testBadan.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testBadan.getProvinsi()).isEqualTo(DEFAULT_PROVINSI);
        assertThat(testBadan.getKodePos()).isEqualTo(DEFAULT_KODE_POS);
        assertThat(testBadan.getNoKontak()).isEqualTo(DEFAULT_NO_KONTAK);
        assertThat(testBadan.getNamaSk()).isEqualTo(DEFAULT_NAMA_SK);
        assertThat(testBadan.getNomorSk()).isEqualTo(DEFAULT_NOMOR_SK);
        assertThat(testBadan.getTglSk()).isEqualTo(DEFAULT_TGL_SK);
        assertThat(testBadan.getNpwp()).isEqualTo(DEFAULT_NPWP);
        assertThat(testBadan.getPjNama()).isEqualTo(DEFAULT_PJ_NAMA);
        assertThat(testBadan.getPjJenisKelamin()).isEqualTo(DEFAULT_PJ_JENIS_KELAMIN);
        assertThat(testBadan.getPjStatusKawin()).isEqualTo(DEFAULT_PJ_STATUS_KAWIN);
        assertThat(testBadan.getPjKelahiran()).isEqualTo(DEFAULT_PJ_KELAHIRAN);
        assertThat(testBadan.getPjTglLahir()).isEqualTo(DEFAULT_PJ_TGL_LAHIR);
        assertThat(testBadan.getPjPekerjaan()).isEqualTo(DEFAULT_PJ_PEKERJAAN);
        assertThat(testBadan.getPjWargaNegara()).isEqualTo(DEFAULT_PJ_WARGA_NEGARA);
        assertThat(testBadan.getPjJenisId()).isEqualTo(DEFAULT_PJ_JENIS_ID);
        assertThat(testBadan.getPjNoId()).isEqualTo(DEFAULT_PJ_NO_ID);
        assertThat(testBadan.getPjAlamat()).isEqualTo(DEFAULT_PJ_ALAMAT);
        assertThat(testBadan.getPjRt()).isEqualTo(DEFAULT_PJ_RT);
        assertThat(testBadan.getPjRw()).isEqualTo(DEFAULT_PJ_RW);
        assertThat(testBadan.getPjKelurahan()).isEqualTo(DEFAULT_PJ_KELURAHAN);
        assertThat(testBadan.getPjKecamatan()).isEqualTo(DEFAULT_PJ_KECAMATAN);
        assertThat(testBadan.getPjKota()).isEqualTo(DEFAULT_PJ_KOTA);
        assertThat(testBadan.getPjProvinsi()).isEqualTo(DEFAULT_PJ_PROVINSI);
        assertThat(testBadan.getPjKodePos()).isEqualTo(DEFAULT_PJ_KODE_POS);
        assertThat(testBadan.getTemplate()).isEqualTo(DEFAULT_TEMPLATE);
        assertThat(testBadan.getKomparisi()).isEqualTo(DEFAULT_KOMPARISI);
    }

    @Test
    @Transactional
    void createBadanWithExistingId() throws Exception {
        // Create the Badan with an existing ID
        badan.setId(1L);

        int databaseSizeBeforeCreate = badanRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBadanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(badan)))
            .andExpect(status().isBadRequest());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllBadans() throws Exception {
        // Initialize the database
        badanRepository.saveAndFlush(badan);

        // Get all the badanList
        restBadanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(badan.getId().intValue())))
            .andExpect(jsonPath("$.[*].golonganUsaha").value(hasItem(DEFAULT_GOLONGAN_USAHA)))
            .andExpect(jsonPath("$.[*].jenis").value(hasItem(DEFAULT_JENIS)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].kedudukan").value(hasItem(DEFAULT_KEDUDUKAN)))
            .andExpect(jsonPath("$.[*].noAkta").value(hasItem(DEFAULT_NO_AKTA)))
            .andExpect(jsonPath("$.[*].tglAkta").value(hasItem(DEFAULT_TGL_AKTA.toString())))
            .andExpect(jsonPath("$.[*].namaNotaris").value(hasItem(DEFAULT_NAMA_NOTARIS)))
            .andExpect(jsonPath("$.[*].wilKerjaNotaris").value(hasItem(DEFAULT_WIL_KERJA_NOTARIS)))
            .andExpect(jsonPath("$.[*].skNotaris").value(hasItem(DEFAULT_SK_NOTARIS)))
            .andExpect(jsonPath("$.[*].alamat").value(hasItem(DEFAULT_ALAMAT)))
            .andExpect(jsonPath("$.[*].rt").value(hasItem(DEFAULT_RT)))
            .andExpect(jsonPath("$.[*].rw").value(hasItem(DEFAULT_RW)))
            .andExpect(jsonPath("$.[*].kelurahan").value(hasItem(DEFAULT_KELURAHAN)))
            .andExpect(jsonPath("$.[*].kecamatan").value(hasItem(DEFAULT_KECAMATAN)))
            .andExpect(jsonPath("$.[*].kota").value(hasItem(DEFAULT_KOTA)))
            .andExpect(jsonPath("$.[*].provinsi").value(hasItem(DEFAULT_PROVINSI)))
            .andExpect(jsonPath("$.[*].kodePos").value(hasItem(DEFAULT_KODE_POS)))
            .andExpect(jsonPath("$.[*].noKontak").value(hasItem(DEFAULT_NO_KONTAK)))
            .andExpect(jsonPath("$.[*].namaSk").value(hasItem(DEFAULT_NAMA_SK)))
            .andExpect(jsonPath("$.[*].nomorSk").value(hasItem(DEFAULT_NOMOR_SK)))
            .andExpect(jsonPath("$.[*].tglSk").value(hasItem(DEFAULT_TGL_SK)))
            .andExpect(jsonPath("$.[*].npwp").value(hasItem(DEFAULT_NPWP)))
            .andExpect(jsonPath("$.[*].pjNama").value(hasItem(DEFAULT_PJ_NAMA)))
            .andExpect(jsonPath("$.[*].pjJenisKelamin").value(hasItem(DEFAULT_PJ_JENIS_KELAMIN)))
            .andExpect(jsonPath("$.[*].pjStatusKawin").value(hasItem(DEFAULT_PJ_STATUS_KAWIN)))
            .andExpect(jsonPath("$.[*].pjKelahiran").value(hasItem(DEFAULT_PJ_KELAHIRAN)))
            .andExpect(jsonPath("$.[*].pjTglLahir").value(hasItem(DEFAULT_PJ_TGL_LAHIR.toString())))
            .andExpect(jsonPath("$.[*].pjPekerjaan").value(hasItem(DEFAULT_PJ_PEKERJAAN)))
            .andExpect(jsonPath("$.[*].pjWargaNegara").value(hasItem(DEFAULT_PJ_WARGA_NEGARA)))
            .andExpect(jsonPath("$.[*].pjJenisId").value(hasItem(DEFAULT_PJ_JENIS_ID)))
            .andExpect(jsonPath("$.[*].pjNoId").value(hasItem(DEFAULT_PJ_NO_ID)))
            .andExpect(jsonPath("$.[*].pjAlamat").value(hasItem(DEFAULT_PJ_ALAMAT)))
            .andExpect(jsonPath("$.[*].pjRt").value(hasItem(DEFAULT_PJ_RT)))
            .andExpect(jsonPath("$.[*].pjRw").value(hasItem(DEFAULT_PJ_RW)))
            .andExpect(jsonPath("$.[*].pjKelurahan").value(hasItem(DEFAULT_PJ_KELURAHAN)))
            .andExpect(jsonPath("$.[*].pjKecamatan").value(hasItem(DEFAULT_PJ_KECAMATAN)))
            .andExpect(jsonPath("$.[*].pjKota").value(hasItem(DEFAULT_PJ_KOTA)))
            .andExpect(jsonPath("$.[*].pjProvinsi").value(hasItem(DEFAULT_PJ_PROVINSI)))
            .andExpect(jsonPath("$.[*].pjKodePos").value(hasItem(DEFAULT_PJ_KODE_POS)))
            .andExpect(jsonPath("$.[*].template").value(hasItem(DEFAULT_TEMPLATE)))
            .andExpect(jsonPath("$.[*].komparisi").value(hasItem(DEFAULT_KOMPARISI)));
    }

    @Test
    @Transactional
    void getBadan() throws Exception {
        // Initialize the database
        badanRepository.saveAndFlush(badan);

        // Get the badan
        restBadanMockMvc
            .perform(get(ENTITY_API_URL_ID, badan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(badan.getId().intValue()))
            .andExpect(jsonPath("$.golonganUsaha").value(DEFAULT_GOLONGAN_USAHA))
            .andExpect(jsonPath("$.jenis").value(DEFAULT_JENIS))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.kedudukan").value(DEFAULT_KEDUDUKAN))
            .andExpect(jsonPath("$.noAkta").value(DEFAULT_NO_AKTA))
            .andExpect(jsonPath("$.tglAkta").value(DEFAULT_TGL_AKTA.toString()))
            .andExpect(jsonPath("$.namaNotaris").value(DEFAULT_NAMA_NOTARIS))
            .andExpect(jsonPath("$.wilKerjaNotaris").value(DEFAULT_WIL_KERJA_NOTARIS))
            .andExpect(jsonPath("$.skNotaris").value(DEFAULT_SK_NOTARIS))
            .andExpect(jsonPath("$.alamat").value(DEFAULT_ALAMAT))
            .andExpect(jsonPath("$.rt").value(DEFAULT_RT))
            .andExpect(jsonPath("$.rw").value(DEFAULT_RW))
            .andExpect(jsonPath("$.kelurahan").value(DEFAULT_KELURAHAN))
            .andExpect(jsonPath("$.kecamatan").value(DEFAULT_KECAMATAN))
            .andExpect(jsonPath("$.kota").value(DEFAULT_KOTA))
            .andExpect(jsonPath("$.provinsi").value(DEFAULT_PROVINSI))
            .andExpect(jsonPath("$.kodePos").value(DEFAULT_KODE_POS))
            .andExpect(jsonPath("$.noKontak").value(DEFAULT_NO_KONTAK))
            .andExpect(jsonPath("$.namaSk").value(DEFAULT_NAMA_SK))
            .andExpect(jsonPath("$.nomorSk").value(DEFAULT_NOMOR_SK))
            .andExpect(jsonPath("$.tglSk").value(DEFAULT_TGL_SK))
            .andExpect(jsonPath("$.npwp").value(DEFAULT_NPWP))
            .andExpect(jsonPath("$.pjNama").value(DEFAULT_PJ_NAMA))
            .andExpect(jsonPath("$.pjJenisKelamin").value(DEFAULT_PJ_JENIS_KELAMIN))
            .andExpect(jsonPath("$.pjStatusKawin").value(DEFAULT_PJ_STATUS_KAWIN))
            .andExpect(jsonPath("$.pjKelahiran").value(DEFAULT_PJ_KELAHIRAN))
            .andExpect(jsonPath("$.pjTglLahir").value(DEFAULT_PJ_TGL_LAHIR.toString()))
            .andExpect(jsonPath("$.pjPekerjaan").value(DEFAULT_PJ_PEKERJAAN))
            .andExpect(jsonPath("$.pjWargaNegara").value(DEFAULT_PJ_WARGA_NEGARA))
            .andExpect(jsonPath("$.pjJenisId").value(DEFAULT_PJ_JENIS_ID))
            .andExpect(jsonPath("$.pjNoId").value(DEFAULT_PJ_NO_ID))
            .andExpect(jsonPath("$.pjAlamat").value(DEFAULT_PJ_ALAMAT))
            .andExpect(jsonPath("$.pjRt").value(DEFAULT_PJ_RT))
            .andExpect(jsonPath("$.pjRw").value(DEFAULT_PJ_RW))
            .andExpect(jsonPath("$.pjKelurahan").value(DEFAULT_PJ_KELURAHAN))
            .andExpect(jsonPath("$.pjKecamatan").value(DEFAULT_PJ_KECAMATAN))
            .andExpect(jsonPath("$.pjKota").value(DEFAULT_PJ_KOTA))
            .andExpect(jsonPath("$.pjProvinsi").value(DEFAULT_PJ_PROVINSI))
            .andExpect(jsonPath("$.pjKodePos").value(DEFAULT_PJ_KODE_POS))
            .andExpect(jsonPath("$.template").value(DEFAULT_TEMPLATE))
            .andExpect(jsonPath("$.komparisi").value(DEFAULT_KOMPARISI));
    }

    @Test
    @Transactional
    void getNonExistingBadan() throws Exception {
        // Get the badan
        restBadanMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingBadan() throws Exception {
        // Initialize the database
        badanRepository.saveAndFlush(badan);

        int databaseSizeBeforeUpdate = badanRepository.findAll().size();

        // Update the badan
        Badan updatedBadan = badanRepository.findById(badan.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedBadan are not directly saved in db
        em.detach(updatedBadan);
        updatedBadan
            .golonganUsaha(UPDATED_GOLONGAN_USAHA)
            .jenis(UPDATED_JENIS)
            .nama(UPDATED_NAMA)
            .kedudukan(UPDATED_KEDUDUKAN)
            .noAkta(UPDATED_NO_AKTA)
            .tglAkta(UPDATED_TGL_AKTA)
            .namaNotaris(UPDATED_NAMA_NOTARIS)
            .wilKerjaNotaris(UPDATED_WIL_KERJA_NOTARIS)
            .skNotaris(UPDATED_SK_NOTARIS)
            .alamat(UPDATED_ALAMAT)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .kelurahan(UPDATED_KELURAHAN)
            .kecamatan(UPDATED_KECAMATAN)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK)
            .namaSk(UPDATED_NAMA_SK)
            .nomorSk(UPDATED_NOMOR_SK)
            .tglSk(UPDATED_TGL_SK)
            .npwp(UPDATED_NPWP)
            .pjNama(UPDATED_PJ_NAMA)
            .pjJenisKelamin(UPDATED_PJ_JENIS_KELAMIN)
            .pjStatusKawin(UPDATED_PJ_STATUS_KAWIN)
            .pjKelahiran(UPDATED_PJ_KELAHIRAN)
            .pjTglLahir(UPDATED_PJ_TGL_LAHIR)
            .pjPekerjaan(UPDATED_PJ_PEKERJAAN)
            .pjWargaNegara(UPDATED_PJ_WARGA_NEGARA)
            .pjJenisId(UPDATED_PJ_JENIS_ID)
            .pjNoId(UPDATED_PJ_NO_ID)
            .pjAlamat(UPDATED_PJ_ALAMAT)
            .pjRt(UPDATED_PJ_RT)
            .pjRw(UPDATED_PJ_RW)
            .pjKelurahan(UPDATED_PJ_KELURAHAN)
            .pjKecamatan(UPDATED_PJ_KECAMATAN)
            .pjKota(UPDATED_PJ_KOTA)
            .pjProvinsi(UPDATED_PJ_PROVINSI)
            .pjKodePos(UPDATED_PJ_KODE_POS)
            .template(UPDATED_TEMPLATE)
            .komparisi(UPDATED_KOMPARISI);

        restBadanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedBadan.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedBadan))
            )
            .andExpect(status().isOk());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
        Badan testBadan = badanList.get(badanList.size() - 1);
        assertThat(testBadan.getGolonganUsaha()).isEqualTo(UPDATED_GOLONGAN_USAHA);
        assertThat(testBadan.getJenis()).isEqualTo(UPDATED_JENIS);
        assertThat(testBadan.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testBadan.getKedudukan()).isEqualTo(UPDATED_KEDUDUKAN);
        assertThat(testBadan.getNoAkta()).isEqualTo(UPDATED_NO_AKTA);
        assertThat(testBadan.getTglAkta()).isEqualTo(UPDATED_TGL_AKTA);
        assertThat(testBadan.getNamaNotaris()).isEqualTo(UPDATED_NAMA_NOTARIS);
        assertThat(testBadan.getWilKerjaNotaris()).isEqualTo(UPDATED_WIL_KERJA_NOTARIS);
        assertThat(testBadan.getSkNotaris()).isEqualTo(UPDATED_SK_NOTARIS);
        assertThat(testBadan.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testBadan.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testBadan.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testBadan.getKelurahan()).isEqualTo(UPDATED_KELURAHAN);
        assertThat(testBadan.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testBadan.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testBadan.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testBadan.getKodePos()).isEqualTo(UPDATED_KODE_POS);
        assertThat(testBadan.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testBadan.getNamaSk()).isEqualTo(UPDATED_NAMA_SK);
        assertThat(testBadan.getNomorSk()).isEqualTo(UPDATED_NOMOR_SK);
        assertThat(testBadan.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testBadan.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testBadan.getPjNama()).isEqualTo(UPDATED_PJ_NAMA);
        assertThat(testBadan.getPjJenisKelamin()).isEqualTo(UPDATED_PJ_JENIS_KELAMIN);
        assertThat(testBadan.getPjStatusKawin()).isEqualTo(UPDATED_PJ_STATUS_KAWIN);
        assertThat(testBadan.getPjKelahiran()).isEqualTo(UPDATED_PJ_KELAHIRAN);
        assertThat(testBadan.getPjTglLahir()).isEqualTo(UPDATED_PJ_TGL_LAHIR);
        assertThat(testBadan.getPjPekerjaan()).isEqualTo(UPDATED_PJ_PEKERJAAN);
        assertThat(testBadan.getPjWargaNegara()).isEqualTo(UPDATED_PJ_WARGA_NEGARA);
        assertThat(testBadan.getPjJenisId()).isEqualTo(UPDATED_PJ_JENIS_ID);
        assertThat(testBadan.getPjNoId()).isEqualTo(UPDATED_PJ_NO_ID);
        assertThat(testBadan.getPjAlamat()).isEqualTo(UPDATED_PJ_ALAMAT);
        assertThat(testBadan.getPjRt()).isEqualTo(UPDATED_PJ_RT);
        assertThat(testBadan.getPjRw()).isEqualTo(UPDATED_PJ_RW);
        assertThat(testBadan.getPjKelurahan()).isEqualTo(UPDATED_PJ_KELURAHAN);
        assertThat(testBadan.getPjKecamatan()).isEqualTo(UPDATED_PJ_KECAMATAN);
        assertThat(testBadan.getPjKota()).isEqualTo(UPDATED_PJ_KOTA);
        assertThat(testBadan.getPjProvinsi()).isEqualTo(UPDATED_PJ_PROVINSI);
        assertThat(testBadan.getPjKodePos()).isEqualTo(UPDATED_PJ_KODE_POS);
        assertThat(testBadan.getTemplate()).isEqualTo(UPDATED_TEMPLATE);
        assertThat(testBadan.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
    }

    @Test
    @Transactional
    void putNonExistingBadan() throws Exception {
        int databaseSizeBeforeUpdate = badanRepository.findAll().size();
        badan.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBadanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, badan.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(badan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBadan() throws Exception {
        int databaseSizeBeforeUpdate = badanRepository.findAll().size();
        badan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBadanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(badan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBadan() throws Exception {
        int databaseSizeBeforeUpdate = badanRepository.findAll().size();
        badan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBadanMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(badan)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBadanWithPatch() throws Exception {
        // Initialize the database
        badanRepository.saveAndFlush(badan);

        int databaseSizeBeforeUpdate = badanRepository.findAll().size();

        // Update the badan using partial update
        Badan partialUpdatedBadan = new Badan();
        partialUpdatedBadan.setId(badan.getId());

        partialUpdatedBadan
            .wilKerjaNotaris(UPDATED_WIL_KERJA_NOTARIS)
            .skNotaris(UPDATED_SK_NOTARIS)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .kelurahan(UPDATED_KELURAHAN)
            .kecamatan(UPDATED_KECAMATAN)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK)
            .nomorSk(UPDATED_NOMOR_SK)
            .tglSk(UPDATED_TGL_SK)
            .npwp(UPDATED_NPWP)
            .pjNama(UPDATED_PJ_NAMA)
            .pjJenisKelamin(UPDATED_PJ_JENIS_KELAMIN)
            .pjStatusKawin(UPDATED_PJ_STATUS_KAWIN)
            .pjKelahiran(UPDATED_PJ_KELAHIRAN)
            .pjTglLahir(UPDATED_PJ_TGL_LAHIR)
            .pjPekerjaan(UPDATED_PJ_PEKERJAAN)
            .pjWargaNegara(UPDATED_PJ_WARGA_NEGARA)
            .pjNoId(UPDATED_PJ_NO_ID)
            .pjKelurahan(UPDATED_PJ_KELURAHAN)
            .pjProvinsi(UPDATED_PJ_PROVINSI)
            .pjKodePos(UPDATED_PJ_KODE_POS)
            .komparisi(UPDATED_KOMPARISI);

        restBadanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBadan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBadan))
            )
            .andExpect(status().isOk());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
        Badan testBadan = badanList.get(badanList.size() - 1);
        assertThat(testBadan.getGolonganUsaha()).isEqualTo(DEFAULT_GOLONGAN_USAHA);
        assertThat(testBadan.getJenis()).isEqualTo(DEFAULT_JENIS);
        assertThat(testBadan.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testBadan.getKedudukan()).isEqualTo(DEFAULT_KEDUDUKAN);
        assertThat(testBadan.getNoAkta()).isEqualTo(DEFAULT_NO_AKTA);
        assertThat(testBadan.getTglAkta()).isEqualTo(DEFAULT_TGL_AKTA);
        assertThat(testBadan.getNamaNotaris()).isEqualTo(DEFAULT_NAMA_NOTARIS);
        assertThat(testBadan.getWilKerjaNotaris()).isEqualTo(UPDATED_WIL_KERJA_NOTARIS);
        assertThat(testBadan.getSkNotaris()).isEqualTo(UPDATED_SK_NOTARIS);
        assertThat(testBadan.getAlamat()).isEqualTo(DEFAULT_ALAMAT);
        assertThat(testBadan.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testBadan.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testBadan.getKelurahan()).isEqualTo(UPDATED_KELURAHAN);
        assertThat(testBadan.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testBadan.getKota()).isEqualTo(DEFAULT_KOTA);
        assertThat(testBadan.getProvinsi()).isEqualTo(DEFAULT_PROVINSI);
        assertThat(testBadan.getKodePos()).isEqualTo(UPDATED_KODE_POS);
        assertThat(testBadan.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testBadan.getNamaSk()).isEqualTo(DEFAULT_NAMA_SK);
        assertThat(testBadan.getNomorSk()).isEqualTo(UPDATED_NOMOR_SK);
        assertThat(testBadan.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testBadan.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testBadan.getPjNama()).isEqualTo(UPDATED_PJ_NAMA);
        assertThat(testBadan.getPjJenisKelamin()).isEqualTo(UPDATED_PJ_JENIS_KELAMIN);
        assertThat(testBadan.getPjStatusKawin()).isEqualTo(UPDATED_PJ_STATUS_KAWIN);
        assertThat(testBadan.getPjKelahiran()).isEqualTo(UPDATED_PJ_KELAHIRAN);
        assertThat(testBadan.getPjTglLahir()).isEqualTo(UPDATED_PJ_TGL_LAHIR);
        assertThat(testBadan.getPjPekerjaan()).isEqualTo(UPDATED_PJ_PEKERJAAN);
        assertThat(testBadan.getPjWargaNegara()).isEqualTo(UPDATED_PJ_WARGA_NEGARA);
        assertThat(testBadan.getPjJenisId()).isEqualTo(DEFAULT_PJ_JENIS_ID);
        assertThat(testBadan.getPjNoId()).isEqualTo(UPDATED_PJ_NO_ID);
        assertThat(testBadan.getPjAlamat()).isEqualTo(DEFAULT_PJ_ALAMAT);
        assertThat(testBadan.getPjRt()).isEqualTo(DEFAULT_PJ_RT);
        assertThat(testBadan.getPjRw()).isEqualTo(DEFAULT_PJ_RW);
        assertThat(testBadan.getPjKelurahan()).isEqualTo(UPDATED_PJ_KELURAHAN);
        assertThat(testBadan.getPjKecamatan()).isEqualTo(DEFAULT_PJ_KECAMATAN);
        assertThat(testBadan.getPjKota()).isEqualTo(DEFAULT_PJ_KOTA);
        assertThat(testBadan.getPjProvinsi()).isEqualTo(UPDATED_PJ_PROVINSI);
        assertThat(testBadan.getPjKodePos()).isEqualTo(UPDATED_PJ_KODE_POS);
        assertThat(testBadan.getTemplate()).isEqualTo(DEFAULT_TEMPLATE);
        assertThat(testBadan.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
    }

    @Test
    @Transactional
    void fullUpdateBadanWithPatch() throws Exception {
        // Initialize the database
        badanRepository.saveAndFlush(badan);

        int databaseSizeBeforeUpdate = badanRepository.findAll().size();

        // Update the badan using partial update
        Badan partialUpdatedBadan = new Badan();
        partialUpdatedBadan.setId(badan.getId());

        partialUpdatedBadan
            .golonganUsaha(UPDATED_GOLONGAN_USAHA)
            .jenis(UPDATED_JENIS)
            .nama(UPDATED_NAMA)
            .kedudukan(UPDATED_KEDUDUKAN)
            .noAkta(UPDATED_NO_AKTA)
            .tglAkta(UPDATED_TGL_AKTA)
            .namaNotaris(UPDATED_NAMA_NOTARIS)
            .wilKerjaNotaris(UPDATED_WIL_KERJA_NOTARIS)
            .skNotaris(UPDATED_SK_NOTARIS)
            .alamat(UPDATED_ALAMAT)
            .rt(UPDATED_RT)
            .rw(UPDATED_RW)
            .kelurahan(UPDATED_KELURAHAN)
            .kecamatan(UPDATED_KECAMATAN)
            .kota(UPDATED_KOTA)
            .provinsi(UPDATED_PROVINSI)
            .kodePos(UPDATED_KODE_POS)
            .noKontak(UPDATED_NO_KONTAK)
            .namaSk(UPDATED_NAMA_SK)
            .nomorSk(UPDATED_NOMOR_SK)
            .tglSk(UPDATED_TGL_SK)
            .npwp(UPDATED_NPWP)
            .pjNama(UPDATED_PJ_NAMA)
            .pjJenisKelamin(UPDATED_PJ_JENIS_KELAMIN)
            .pjStatusKawin(UPDATED_PJ_STATUS_KAWIN)
            .pjKelahiran(UPDATED_PJ_KELAHIRAN)
            .pjTglLahir(UPDATED_PJ_TGL_LAHIR)
            .pjPekerjaan(UPDATED_PJ_PEKERJAAN)
            .pjWargaNegara(UPDATED_PJ_WARGA_NEGARA)
            .pjJenisId(UPDATED_PJ_JENIS_ID)
            .pjNoId(UPDATED_PJ_NO_ID)
            .pjAlamat(UPDATED_PJ_ALAMAT)
            .pjRt(UPDATED_PJ_RT)
            .pjRw(UPDATED_PJ_RW)
            .pjKelurahan(UPDATED_PJ_KELURAHAN)
            .pjKecamatan(UPDATED_PJ_KECAMATAN)
            .pjKota(UPDATED_PJ_KOTA)
            .pjProvinsi(UPDATED_PJ_PROVINSI)
            .pjKodePos(UPDATED_PJ_KODE_POS)
            .template(UPDATED_TEMPLATE)
            .komparisi(UPDATED_KOMPARISI);

        restBadanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBadan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBadan))
            )
            .andExpect(status().isOk());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
        Badan testBadan = badanList.get(badanList.size() - 1);
        assertThat(testBadan.getGolonganUsaha()).isEqualTo(UPDATED_GOLONGAN_USAHA);
        assertThat(testBadan.getJenis()).isEqualTo(UPDATED_JENIS);
        assertThat(testBadan.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testBadan.getKedudukan()).isEqualTo(UPDATED_KEDUDUKAN);
        assertThat(testBadan.getNoAkta()).isEqualTo(UPDATED_NO_AKTA);
        assertThat(testBadan.getTglAkta()).isEqualTo(UPDATED_TGL_AKTA);
        assertThat(testBadan.getNamaNotaris()).isEqualTo(UPDATED_NAMA_NOTARIS);
        assertThat(testBadan.getWilKerjaNotaris()).isEqualTo(UPDATED_WIL_KERJA_NOTARIS);
        assertThat(testBadan.getSkNotaris()).isEqualTo(UPDATED_SK_NOTARIS);
        assertThat(testBadan.getAlamat()).isEqualTo(UPDATED_ALAMAT);
        assertThat(testBadan.getRt()).isEqualTo(UPDATED_RT);
        assertThat(testBadan.getRw()).isEqualTo(UPDATED_RW);
        assertThat(testBadan.getKelurahan()).isEqualTo(UPDATED_KELURAHAN);
        assertThat(testBadan.getKecamatan()).isEqualTo(UPDATED_KECAMATAN);
        assertThat(testBadan.getKota()).isEqualTo(UPDATED_KOTA);
        assertThat(testBadan.getProvinsi()).isEqualTo(UPDATED_PROVINSI);
        assertThat(testBadan.getKodePos()).isEqualTo(UPDATED_KODE_POS);
        assertThat(testBadan.getNoKontak()).isEqualTo(UPDATED_NO_KONTAK);
        assertThat(testBadan.getNamaSk()).isEqualTo(UPDATED_NAMA_SK);
        assertThat(testBadan.getNomorSk()).isEqualTo(UPDATED_NOMOR_SK);
        assertThat(testBadan.getTglSk()).isEqualTo(UPDATED_TGL_SK);
        assertThat(testBadan.getNpwp()).isEqualTo(UPDATED_NPWP);
        assertThat(testBadan.getPjNama()).isEqualTo(UPDATED_PJ_NAMA);
        assertThat(testBadan.getPjJenisKelamin()).isEqualTo(UPDATED_PJ_JENIS_KELAMIN);
        assertThat(testBadan.getPjStatusKawin()).isEqualTo(UPDATED_PJ_STATUS_KAWIN);
        assertThat(testBadan.getPjKelahiran()).isEqualTo(UPDATED_PJ_KELAHIRAN);
        assertThat(testBadan.getPjTglLahir()).isEqualTo(UPDATED_PJ_TGL_LAHIR);
        assertThat(testBadan.getPjPekerjaan()).isEqualTo(UPDATED_PJ_PEKERJAAN);
        assertThat(testBadan.getPjWargaNegara()).isEqualTo(UPDATED_PJ_WARGA_NEGARA);
        assertThat(testBadan.getPjJenisId()).isEqualTo(UPDATED_PJ_JENIS_ID);
        assertThat(testBadan.getPjNoId()).isEqualTo(UPDATED_PJ_NO_ID);
        assertThat(testBadan.getPjAlamat()).isEqualTo(UPDATED_PJ_ALAMAT);
        assertThat(testBadan.getPjRt()).isEqualTo(UPDATED_PJ_RT);
        assertThat(testBadan.getPjRw()).isEqualTo(UPDATED_PJ_RW);
        assertThat(testBadan.getPjKelurahan()).isEqualTo(UPDATED_PJ_KELURAHAN);
        assertThat(testBadan.getPjKecamatan()).isEqualTo(UPDATED_PJ_KECAMATAN);
        assertThat(testBadan.getPjKota()).isEqualTo(UPDATED_PJ_KOTA);
        assertThat(testBadan.getPjProvinsi()).isEqualTo(UPDATED_PJ_PROVINSI);
        assertThat(testBadan.getPjKodePos()).isEqualTo(UPDATED_PJ_KODE_POS);
        assertThat(testBadan.getTemplate()).isEqualTo(UPDATED_TEMPLATE);
        assertThat(testBadan.getKomparisi()).isEqualTo(UPDATED_KOMPARISI);
    }

    @Test
    @Transactional
    void patchNonExistingBadan() throws Exception {
        int databaseSizeBeforeUpdate = badanRepository.findAll().size();
        badan.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBadanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, badan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(badan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBadan() throws Exception {
        int databaseSizeBeforeUpdate = badanRepository.findAll().size();
        badan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBadanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(badan))
            )
            .andExpect(status().isBadRequest());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBadan() throws Exception {
        int databaseSizeBeforeUpdate = badanRepository.findAll().size();
        badan.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBadanMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(badan)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Badan in the database
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBadan() throws Exception {
        // Initialize the database
        badanRepository.saveAndFlush(badan);

        int databaseSizeBeforeDelete = badanRepository.findAll().size();

        // Delete the badan
        restBadanMockMvc
            .perform(delete(ENTITY_API_URL_ID, badan.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Badan> badanList = badanRepository.findAll();
        assertThat(badanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
