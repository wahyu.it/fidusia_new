package com.project.fidusia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.RawData;
import com.project.fidusia.repository.RawDataRepository;
import jakarta.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RawDataResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RawDataResourceIT {

    private static final String DEFAULT_UNIQUE_KEY = "AAAAAAAAAA";
    private static final String UPDATED_UNIQUE_KEY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_ORDER_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ORDER_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LINE = "AAAAAAAAAA";
    private static final String UPDATED_LINE = "BBBBBBBBBB";

    private static final String DEFAULT_DELIMETER = "AAAAAAAAAA";
    private static final String UPDATED_DELIMETER = "BBBBBBBBBB";

    private static final Long DEFAULT_LINE_NUMBER = 1L;
    private static final Long UPDATED_LINE_NUMBER = 2L;

    private static final Boolean DEFAULT_VALID = false;
    private static final Boolean UPDATED_VALID = true;

    private static final String DEFAULT_INVALID_REASON = "AAAAAAAAAA";
    private static final String UPDATED_INVALID_REASON = "BBBBBBBBBB";

    private static final Long DEFAULT_STATUS = 1L;
    private static final Long UPDATED_STATUS = 2L;

    private static final String ENTITY_API_URL = "/api/raw-data";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RawDataRepository rawDataRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRawDataMockMvc;

    private RawData rawData;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RawData createEntity(EntityManager em) {
        RawData rawData = new RawData()
            .uniqueKey(DEFAULT_UNIQUE_KEY)
            .orderDate(DEFAULT_ORDER_DATE)
            .fileName(DEFAULT_FILE_NAME)
            .line(DEFAULT_LINE)
            .delimeter(DEFAULT_DELIMETER)
            .lineNumber(DEFAULT_LINE_NUMBER)
            .valid(DEFAULT_VALID)
            .invalidReason(DEFAULT_INVALID_REASON)
            .status(DEFAULT_STATUS);
        return rawData;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RawData createUpdatedEntity(EntityManager em) {
        RawData rawData = new RawData()
            .uniqueKey(UPDATED_UNIQUE_KEY)
            .orderDate(UPDATED_ORDER_DATE)
            .fileName(UPDATED_FILE_NAME)
            .line(UPDATED_LINE)
            .delimeter(UPDATED_DELIMETER)
            .lineNumber(UPDATED_LINE_NUMBER)
            .valid(UPDATED_VALID)
            .invalidReason(UPDATED_INVALID_REASON)
            .status(UPDATED_STATUS);
        return rawData;
    }

    @BeforeEach
    public void initTest() {
        rawData = createEntity(em);
    }

    @Test
    @Transactional
    void createRawData() throws Exception {
        int databaseSizeBeforeCreate = rawDataRepository.findAll().size();
        // Create the RawData
        restRawDataMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rawData)))
            .andExpect(status().isCreated());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeCreate + 1);
        RawData testRawData = rawDataList.get(rawDataList.size() - 1);
        assertThat(testRawData.getUniqueKey()).isEqualTo(DEFAULT_UNIQUE_KEY);
        assertThat(testRawData.getOrderDate()).isEqualTo(DEFAULT_ORDER_DATE);
        assertThat(testRawData.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testRawData.getLine()).isEqualTo(DEFAULT_LINE);
        assertThat(testRawData.getDelimeter()).isEqualTo(DEFAULT_DELIMETER);
        assertThat(testRawData.getLineNumber()).isEqualTo(DEFAULT_LINE_NUMBER);
        assertThat(testRawData.getValid()).isEqualTo(DEFAULT_VALID);
        assertThat(testRawData.getInvalidReason()).isEqualTo(DEFAULT_INVALID_REASON);
        assertThat(testRawData.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createRawDataWithExistingId() throws Exception {
        // Create the RawData with an existing ID
        rawData.setId(1L);

        int databaseSizeBeforeCreate = rawDataRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRawDataMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rawData)))
            .andExpect(status().isBadRequest());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllRawData() throws Exception {
        // Initialize the database
        rawDataRepository.saveAndFlush(rawData);

        // Get all the rawDataList
        restRawDataMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rawData.getId().intValue())))
            .andExpect(jsonPath("$.[*].uniqueKey").value(hasItem(DEFAULT_UNIQUE_KEY)))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)))
            .andExpect(jsonPath("$.[*].line").value(hasItem(DEFAULT_LINE)))
            .andExpect(jsonPath("$.[*].delimeter").value(hasItem(DEFAULT_DELIMETER)))
            .andExpect(jsonPath("$.[*].lineNumber").value(hasItem(DEFAULT_LINE_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].valid").value(hasItem(DEFAULT_VALID.booleanValue())))
            .andExpect(jsonPath("$.[*].invalidReason").value(hasItem(DEFAULT_INVALID_REASON)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.intValue())));
    }

    @Test
    @Transactional
    void getRawData() throws Exception {
        // Initialize the database
        rawDataRepository.saveAndFlush(rawData);

        // Get the rawData
        restRawDataMockMvc
            .perform(get(ENTITY_API_URL_ID, rawData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(rawData.getId().intValue()))
            .andExpect(jsonPath("$.uniqueKey").value(DEFAULT_UNIQUE_KEY))
            .andExpect(jsonPath("$.orderDate").value(DEFAULT_ORDER_DATE.toString()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME))
            .andExpect(jsonPath("$.line").value(DEFAULT_LINE))
            .andExpect(jsonPath("$.delimeter").value(DEFAULT_DELIMETER))
            .andExpect(jsonPath("$.lineNumber").value(DEFAULT_LINE_NUMBER.intValue()))
            .andExpect(jsonPath("$.valid").value(DEFAULT_VALID.booleanValue()))
            .andExpect(jsonPath("$.invalidReason").value(DEFAULT_INVALID_REASON))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingRawData() throws Exception {
        // Get the rawData
        restRawDataMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingRawData() throws Exception {
        // Initialize the database
        rawDataRepository.saveAndFlush(rawData);

        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();

        // Update the rawData
        RawData updatedRawData = rawDataRepository.findById(rawData.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedRawData are not directly saved in db
        em.detach(updatedRawData);
        updatedRawData
            .uniqueKey(UPDATED_UNIQUE_KEY)
            .orderDate(UPDATED_ORDER_DATE)
            .fileName(UPDATED_FILE_NAME)
            .line(UPDATED_LINE)
            .delimeter(UPDATED_DELIMETER)
            .lineNumber(UPDATED_LINE_NUMBER)
            .valid(UPDATED_VALID)
            .invalidReason(UPDATED_INVALID_REASON)
            .status(UPDATED_STATUS);

        restRawDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRawData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRawData))
            )
            .andExpect(status().isOk());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
        RawData testRawData = rawDataList.get(rawDataList.size() - 1);
        assertThat(testRawData.getUniqueKey()).isEqualTo(UPDATED_UNIQUE_KEY);
        assertThat(testRawData.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testRawData.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testRawData.getLine()).isEqualTo(UPDATED_LINE);
        assertThat(testRawData.getDelimeter()).isEqualTo(UPDATED_DELIMETER);
        assertThat(testRawData.getLineNumber()).isEqualTo(UPDATED_LINE_NUMBER);
        assertThat(testRawData.getValid()).isEqualTo(UPDATED_VALID);
        assertThat(testRawData.getInvalidReason()).isEqualTo(UPDATED_INVALID_REASON);
        assertThat(testRawData.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingRawData() throws Exception {
        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();
        rawData.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRawDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, rawData.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(rawData))
            )
            .andExpect(status().isBadRequest());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRawData() throws Exception {
        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();
        rawData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRawDataMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(rawData))
            )
            .andExpect(status().isBadRequest());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRawData() throws Exception {
        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();
        rawData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRawDataMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rawData)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRawDataWithPatch() throws Exception {
        // Initialize the database
        rawDataRepository.saveAndFlush(rawData);

        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();

        // Update the rawData using partial update
        RawData partialUpdatedRawData = new RawData();
        partialUpdatedRawData.setId(rawData.getId());

        partialUpdatedRawData
            .orderDate(UPDATED_ORDER_DATE)
            .fileName(UPDATED_FILE_NAME)
            .line(UPDATED_LINE)
            .delimeter(UPDATED_DELIMETER)
            .lineNumber(UPDATED_LINE_NUMBER)
            .valid(UPDATED_VALID)
            .invalidReason(UPDATED_INVALID_REASON)
            .status(UPDATED_STATUS);

        restRawDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRawData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRawData))
            )
            .andExpect(status().isOk());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
        RawData testRawData = rawDataList.get(rawDataList.size() - 1);
        assertThat(testRawData.getUniqueKey()).isEqualTo(DEFAULT_UNIQUE_KEY);
        assertThat(testRawData.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testRawData.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testRawData.getLine()).isEqualTo(UPDATED_LINE);
        assertThat(testRawData.getDelimeter()).isEqualTo(UPDATED_DELIMETER);
        assertThat(testRawData.getLineNumber()).isEqualTo(UPDATED_LINE_NUMBER);
        assertThat(testRawData.getValid()).isEqualTo(UPDATED_VALID);
        assertThat(testRawData.getInvalidReason()).isEqualTo(UPDATED_INVALID_REASON);
        assertThat(testRawData.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateRawDataWithPatch() throws Exception {
        // Initialize the database
        rawDataRepository.saveAndFlush(rawData);

        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();

        // Update the rawData using partial update
        RawData partialUpdatedRawData = new RawData();
        partialUpdatedRawData.setId(rawData.getId());

        partialUpdatedRawData
            .uniqueKey(UPDATED_UNIQUE_KEY)
            .orderDate(UPDATED_ORDER_DATE)
            .fileName(UPDATED_FILE_NAME)
            .line(UPDATED_LINE)
            .delimeter(UPDATED_DELIMETER)
            .lineNumber(UPDATED_LINE_NUMBER)
            .valid(UPDATED_VALID)
            .invalidReason(UPDATED_INVALID_REASON)
            .status(UPDATED_STATUS);

        restRawDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRawData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRawData))
            )
            .andExpect(status().isOk());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
        RawData testRawData = rawDataList.get(rawDataList.size() - 1);
        assertThat(testRawData.getUniqueKey()).isEqualTo(UPDATED_UNIQUE_KEY);
        assertThat(testRawData.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testRawData.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testRawData.getLine()).isEqualTo(UPDATED_LINE);
        assertThat(testRawData.getDelimeter()).isEqualTo(UPDATED_DELIMETER);
        assertThat(testRawData.getLineNumber()).isEqualTo(UPDATED_LINE_NUMBER);
        assertThat(testRawData.getValid()).isEqualTo(UPDATED_VALID);
        assertThat(testRawData.getInvalidReason()).isEqualTo(UPDATED_INVALID_REASON);
        assertThat(testRawData.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingRawData() throws Exception {
        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();
        rawData.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRawDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, rawData.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(rawData))
            )
            .andExpect(status().isBadRequest());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRawData() throws Exception {
        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();
        rawData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRawDataMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(rawData))
            )
            .andExpect(status().isBadRequest());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRawData() throws Exception {
        int databaseSizeBeforeUpdate = rawDataRepository.findAll().size();
        rawData.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRawDataMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(rawData)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the RawData in the database
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRawData() throws Exception {
        // Initialize the database
        rawDataRepository.saveAndFlush(rawData);

        int databaseSizeBeforeDelete = rawDataRepository.findAll().size();

        // Delete the rawData
        restRawDataMockMvc
            .perform(delete(ENTITY_API_URL_ID, rawData.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RawData> rawDataList = rawDataRepository.findAll();
        assertThat(rawDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
