package com.project.fidusia.web.rest;

import static com.project.fidusia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.Akta;
import com.project.fidusia.repository.AktaRepository;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AktaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AktaResourceIT {

    private static final LocalDate DEFAULT_TGL_ORDER = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TGL_ORDER = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_ORDER_TYPE = 1;
    private static final Integer UPDATED_ORDER_TYPE = 2;

    private static final String DEFAULT_KODE = "AAAAAAAAAA";
    private static final String UPDATED_KODE = "BBBBBBBBBB";

    private static final String DEFAULT_NOMOR = "AAAAAAAAAA";
    private static final String UPDATED_NOMOR = "BBBBBBBBBB";

    private static final String DEFAULT_TANGGAL = "AAAAAAAAAA";
    private static final String UPDATED_TANGGAL = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_PUKUL_AWAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PUKUL_AWAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_PUKUL_AKHIR = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PUKUL_AKHIR = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATE_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATE_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/aktas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AktaRepository aktaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAktaMockMvc;

    private Akta akta;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Akta createEntity(EntityManager em) {
        Akta akta = new Akta()
            .tglOrder(DEFAULT_TGL_ORDER)
            .orderType(DEFAULT_ORDER_TYPE)
            .kode(DEFAULT_KODE)
            .nomor(DEFAULT_NOMOR)
            .tanggal(DEFAULT_TANGGAL)
            .pukulAwal(DEFAULT_PUKUL_AWAL)
            .pukulAkhir(DEFAULT_PUKUL_AKHIR)
            .status(DEFAULT_STATUS)
            .updateBy(DEFAULT_UPDATE_BY)
            .updateOn(DEFAULT_UPDATE_ON);
        return akta;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Akta createUpdatedEntity(EntityManager em) {
        Akta akta = new Akta()
            .tglOrder(UPDATED_TGL_ORDER)
            .orderType(UPDATED_ORDER_TYPE)
            .kode(UPDATED_KODE)
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .pukulAwal(UPDATED_PUKUL_AWAL)
            .pukulAkhir(UPDATED_PUKUL_AKHIR)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);
        return akta;
    }

    @BeforeEach
    public void initTest() {
        akta = createEntity(em);
    }

    @Test
    @Transactional
    void createAkta() throws Exception {
        int databaseSizeBeforeCreate = aktaRepository.findAll().size();
        // Create the Akta
        restAktaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(akta)))
            .andExpect(status().isCreated());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeCreate + 1);
        Akta testAkta = aktaList.get(aktaList.size() - 1);
        assertThat(testAkta.getTglOrder()).isEqualTo(DEFAULT_TGL_ORDER);
        assertThat(testAkta.getOrderType()).isEqualTo(DEFAULT_ORDER_TYPE);
        assertThat(testAkta.getKode()).isEqualTo(DEFAULT_KODE);
        assertThat(testAkta.getNomor()).isEqualTo(DEFAULT_NOMOR);
        assertThat(testAkta.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testAkta.getPukulAwal()).isEqualTo(DEFAULT_PUKUL_AWAL);
        assertThat(testAkta.getPukulAkhir()).isEqualTo(DEFAULT_PUKUL_AKHIR);
        assertThat(testAkta.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAkta.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testAkta.getUpdateOn()).isEqualTo(DEFAULT_UPDATE_ON);
    }

    @Test
    @Transactional
    void createAktaWithExistingId() throws Exception {
        // Create the Akta with an existing ID
        akta.setId(1L);

        int databaseSizeBeforeCreate = aktaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAktaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(akta)))
            .andExpect(status().isBadRequest());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAktas() throws Exception {
        // Initialize the database
        aktaRepository.saveAndFlush(akta);

        // Get all the aktaList
        restAktaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(akta.getId().intValue())))
            .andExpect(jsonPath("$.[*].tglOrder").value(hasItem(DEFAULT_TGL_ORDER.toString())))
            .andExpect(jsonPath("$.[*].orderType").value(hasItem(DEFAULT_ORDER_TYPE)))
            .andExpect(jsonPath("$.[*].kode").value(hasItem(DEFAULT_KODE)))
            .andExpect(jsonPath("$.[*].nomor").value(hasItem(DEFAULT_NOMOR)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL)))
            .andExpect(jsonPath("$.[*].pukulAwal").value(hasItem(sameInstant(DEFAULT_PUKUL_AWAL))))
            .andExpect(jsonPath("$.[*].pukulAkhir").value(hasItem(sameInstant(DEFAULT_PUKUL_AKHIR))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY)))
            .andExpect(jsonPath("$.[*].updateOn").value(hasItem(sameInstant(DEFAULT_UPDATE_ON))));
    }

    @Test
    @Transactional
    void getAkta() throws Exception {
        // Initialize the database
        aktaRepository.saveAndFlush(akta);

        // Get the akta
        restAktaMockMvc
            .perform(get(ENTITY_API_URL_ID, akta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(akta.getId().intValue()))
            .andExpect(jsonPath("$.tglOrder").value(DEFAULT_TGL_ORDER.toString()))
            .andExpect(jsonPath("$.orderType").value(DEFAULT_ORDER_TYPE))
            .andExpect(jsonPath("$.kode").value(DEFAULT_KODE))
            .andExpect(jsonPath("$.nomor").value(DEFAULT_NOMOR))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL))
            .andExpect(jsonPath("$.pukulAwal").value(sameInstant(DEFAULT_PUKUL_AWAL)))
            .andExpect(jsonPath("$.pukulAkhir").value(sameInstant(DEFAULT_PUKUL_AKHIR)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY))
            .andExpect(jsonPath("$.updateOn").value(sameInstant(DEFAULT_UPDATE_ON)));
    }

    @Test
    @Transactional
    void getNonExistingAkta() throws Exception {
        // Get the akta
        restAktaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingAkta() throws Exception {
        // Initialize the database
        aktaRepository.saveAndFlush(akta);

        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();

        // Update the akta
        Akta updatedAkta = aktaRepository.findById(akta.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedAkta are not directly saved in db
        em.detach(updatedAkta);
        updatedAkta
            .tglOrder(UPDATED_TGL_ORDER)
            .orderType(UPDATED_ORDER_TYPE)
            .kode(UPDATED_KODE)
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .pukulAwal(UPDATED_PUKUL_AWAL)
            .pukulAkhir(UPDATED_PUKUL_AKHIR)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restAktaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAkta.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAkta))
            )
            .andExpect(status().isOk());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
        Akta testAkta = aktaList.get(aktaList.size() - 1);
        assertThat(testAkta.getTglOrder()).isEqualTo(UPDATED_TGL_ORDER);
        assertThat(testAkta.getOrderType()).isEqualTo(UPDATED_ORDER_TYPE);
        assertThat(testAkta.getKode()).isEqualTo(UPDATED_KODE);
        assertThat(testAkta.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testAkta.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testAkta.getPukulAwal()).isEqualTo(UPDATED_PUKUL_AWAL);
        assertThat(testAkta.getPukulAkhir()).isEqualTo(UPDATED_PUKUL_AKHIR);
        assertThat(testAkta.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAkta.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testAkta.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void putNonExistingAkta() throws Exception {
        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();
        akta.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAktaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, akta.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(akta))
            )
            .andExpect(status().isBadRequest());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAkta() throws Exception {
        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();
        akta.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAktaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(akta))
            )
            .andExpect(status().isBadRequest());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAkta() throws Exception {
        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();
        akta.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAktaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(akta)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAktaWithPatch() throws Exception {
        // Initialize the database
        aktaRepository.saveAndFlush(akta);

        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();

        // Update the akta using partial update
        Akta partialUpdatedAkta = new Akta();
        partialUpdatedAkta.setId(akta.getId());

        partialUpdatedAkta
            .orderType(UPDATED_ORDER_TYPE)
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .pukulAwal(UPDATED_PUKUL_AWAL)
            .pukulAkhir(UPDATED_PUKUL_AKHIR)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restAktaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAkta.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAkta))
            )
            .andExpect(status().isOk());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
        Akta testAkta = aktaList.get(aktaList.size() - 1);
        assertThat(testAkta.getTglOrder()).isEqualTo(DEFAULT_TGL_ORDER);
        assertThat(testAkta.getOrderType()).isEqualTo(UPDATED_ORDER_TYPE);
        assertThat(testAkta.getKode()).isEqualTo(DEFAULT_KODE);
        assertThat(testAkta.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testAkta.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testAkta.getPukulAwal()).isEqualTo(UPDATED_PUKUL_AWAL);
        assertThat(testAkta.getPukulAkhir()).isEqualTo(UPDATED_PUKUL_AKHIR);
        assertThat(testAkta.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAkta.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testAkta.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void fullUpdateAktaWithPatch() throws Exception {
        // Initialize the database
        aktaRepository.saveAndFlush(akta);

        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();

        // Update the akta using partial update
        Akta partialUpdatedAkta = new Akta();
        partialUpdatedAkta.setId(akta.getId());

        partialUpdatedAkta
            .tglOrder(UPDATED_TGL_ORDER)
            .orderType(UPDATED_ORDER_TYPE)
            .kode(UPDATED_KODE)
            .nomor(UPDATED_NOMOR)
            .tanggal(UPDATED_TANGGAL)
            .pukulAwal(UPDATED_PUKUL_AWAL)
            .pukulAkhir(UPDATED_PUKUL_AKHIR)
            .status(UPDATED_STATUS)
            .updateBy(UPDATED_UPDATE_BY)
            .updateOn(UPDATED_UPDATE_ON);

        restAktaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAkta.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAkta))
            )
            .andExpect(status().isOk());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
        Akta testAkta = aktaList.get(aktaList.size() - 1);
        assertThat(testAkta.getTglOrder()).isEqualTo(UPDATED_TGL_ORDER);
        assertThat(testAkta.getOrderType()).isEqualTo(UPDATED_ORDER_TYPE);
        assertThat(testAkta.getKode()).isEqualTo(UPDATED_KODE);
        assertThat(testAkta.getNomor()).isEqualTo(UPDATED_NOMOR);
        assertThat(testAkta.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testAkta.getPukulAwal()).isEqualTo(UPDATED_PUKUL_AWAL);
        assertThat(testAkta.getPukulAkhir()).isEqualTo(UPDATED_PUKUL_AKHIR);
        assertThat(testAkta.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAkta.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testAkta.getUpdateOn()).isEqualTo(UPDATED_UPDATE_ON);
    }

    @Test
    @Transactional
    void patchNonExistingAkta() throws Exception {
        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();
        akta.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAktaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, akta.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(akta))
            )
            .andExpect(status().isBadRequest());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAkta() throws Exception {
        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();
        akta.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAktaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(akta))
            )
            .andExpect(status().isBadRequest());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAkta() throws Exception {
        int databaseSizeBeforeUpdate = aktaRepository.findAll().size();
        akta.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAktaMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(akta)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Akta in the database
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAkta() throws Exception {
        // Initialize the database
        aktaRepository.saveAndFlush(akta);

        int databaseSizeBeforeDelete = aktaRepository.findAll().size();

        // Delete the akta
        restAktaMockMvc
            .perform(delete(ENTITY_API_URL_ID, akta.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Akta> aktaList = aktaRepository.findAll();
        assertThat(aktaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
