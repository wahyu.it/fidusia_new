package com.project.fidusia.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.project.fidusia.IntegrationTest;
import com.project.fidusia.domain.OrderNotaris;
import com.project.fidusia.repository.OrderNotarisRepository;
import jakarta.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OrderNotarisResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrderNotarisResourceIT {

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String ENTITY_API_URL = "/api/order-notarises";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrderNotarisRepository orderNotarisRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderNotarisMockMvc;

    private OrderNotaris orderNotaris;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderNotaris createEntity(EntityManager em) {
        OrderNotaris orderNotaris = new OrderNotaris().tanggal(DEFAULT_TANGGAL).status(DEFAULT_STATUS);
        return orderNotaris;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderNotaris createUpdatedEntity(EntityManager em) {
        OrderNotaris orderNotaris = new OrderNotaris().tanggal(UPDATED_TANGGAL).status(UPDATED_STATUS);
        return orderNotaris;
    }

    @BeforeEach
    public void initTest() {
        orderNotaris = createEntity(em);
    }

    @Test
    @Transactional
    void createOrderNotaris() throws Exception {
        int databaseSizeBeforeCreate = orderNotarisRepository.findAll().size();
        // Create the OrderNotaris
        restOrderNotarisMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderNotaris)))
            .andExpect(status().isCreated());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeCreate + 1);
        OrderNotaris testOrderNotaris = orderNotarisList.get(orderNotarisList.size() - 1);
        assertThat(testOrderNotaris.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testOrderNotaris.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createOrderNotarisWithExistingId() throws Exception {
        // Create the OrderNotaris with an existing ID
        orderNotaris.setId(1L);

        int databaseSizeBeforeCreate = orderNotarisRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderNotarisMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderNotaris)))
            .andExpect(status().isBadRequest());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOrderNotarises() throws Exception {
        // Initialize the database
        orderNotarisRepository.saveAndFlush(orderNotaris);

        // Get all the orderNotarisList
        restOrderNotarisMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderNotaris.getId().intValue())))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    void getOrderNotaris() throws Exception {
        // Initialize the database
        orderNotarisRepository.saveAndFlush(orderNotaris);

        // Get the orderNotaris
        restOrderNotarisMockMvc
            .perform(get(ENTITY_API_URL_ID, orderNotaris.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderNotaris.getId().intValue()))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    void getNonExistingOrderNotaris() throws Exception {
        // Get the orderNotaris
        restOrderNotarisMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingOrderNotaris() throws Exception {
        // Initialize the database
        orderNotarisRepository.saveAndFlush(orderNotaris);

        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();

        // Update the orderNotaris
        OrderNotaris updatedOrderNotaris = orderNotarisRepository.findById(orderNotaris.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedOrderNotaris are not directly saved in db
        em.detach(updatedOrderNotaris);
        updatedOrderNotaris.tanggal(UPDATED_TANGGAL).status(UPDATED_STATUS);

        restOrderNotarisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOrderNotaris.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedOrderNotaris))
            )
            .andExpect(status().isOk());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
        OrderNotaris testOrderNotaris = orderNotarisList.get(orderNotarisList.size() - 1);
        assertThat(testOrderNotaris.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderNotaris.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingOrderNotaris() throws Exception {
        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();
        orderNotaris.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderNotarisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orderNotaris.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderNotaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrderNotaris() throws Exception {
        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();
        orderNotaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNotarisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderNotaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrderNotaris() throws Exception {
        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();
        orderNotaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNotarisMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderNotaris)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrderNotarisWithPatch() throws Exception {
        // Initialize the database
        orderNotarisRepository.saveAndFlush(orderNotaris);

        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();

        // Update the orderNotaris using partial update
        OrderNotaris partialUpdatedOrderNotaris = new OrderNotaris();
        partialUpdatedOrderNotaris.setId(orderNotaris.getId());

        partialUpdatedOrderNotaris.status(UPDATED_STATUS);

        restOrderNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderNotaris.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderNotaris))
            )
            .andExpect(status().isOk());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
        OrderNotaris testOrderNotaris = orderNotarisList.get(orderNotarisList.size() - 1);
        assertThat(testOrderNotaris.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testOrderNotaris.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateOrderNotarisWithPatch() throws Exception {
        // Initialize the database
        orderNotarisRepository.saveAndFlush(orderNotaris);

        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();

        // Update the orderNotaris using partial update
        OrderNotaris partialUpdatedOrderNotaris = new OrderNotaris();
        partialUpdatedOrderNotaris.setId(orderNotaris.getId());

        partialUpdatedOrderNotaris.tanggal(UPDATED_TANGGAL).status(UPDATED_STATUS);

        restOrderNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderNotaris.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderNotaris))
            )
            .andExpect(status().isOk());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
        OrderNotaris testOrderNotaris = orderNotarisList.get(orderNotarisList.size() - 1);
        assertThat(testOrderNotaris.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testOrderNotaris.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingOrderNotaris() throws Exception {
        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();
        orderNotaris.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, orderNotaris.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderNotaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrderNotaris() throws Exception {
        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();
        orderNotaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderNotaris))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrderNotaris() throws Exception {
        int databaseSizeBeforeUpdate = orderNotarisRepository.findAll().size();
        orderNotaris.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNotarisMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(orderNotaris))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderNotaris in the database
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrderNotaris() throws Exception {
        // Initialize the database
        orderNotarisRepository.saveAndFlush(orderNotaris);

        int databaseSizeBeforeDelete = orderNotarisRepository.findAll().size();

        // Delete the orderNotaris
        restOrderNotarisMockMvc
            .perform(delete(ENTITY_API_URL_ID, orderNotaris.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderNotaris> orderNotarisList = orderNotarisRepository.findAll();
        assertThat(orderNotarisList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
