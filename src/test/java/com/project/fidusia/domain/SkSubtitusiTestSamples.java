package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class SkSubtitusiTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static SkSubtitusi getSkSubtitusiSample1() {
        return new SkSubtitusi()
            .id(1L)
            .nomor("nomor1")
            .recordStatus(1)
            .updateBy("updateBy1")
            .fileNameSk("fileNameSk1")
            .fileNameLamp("fileNameLamp1")
            .statusSign(1);
    }

    public static SkSubtitusi getSkSubtitusiSample2() {
        return new SkSubtitusi()
            .id(2L)
            .nomor("nomor2")
            .recordStatus(2)
            .updateBy("updateBy2")
            .fileNameSk("fileNameSk2")
            .fileNameLamp("fileNameLamp2")
            .statusSign(2);
    }

    public static SkSubtitusi getSkSubtitusiRandomSampleGenerator() {
        return new SkSubtitusi()
            .id(longCount.incrementAndGet())
            .nomor(UUID.randomUUID().toString())
            .recordStatus(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString())
            .fileNameSk(UUID.randomUUID().toString())
            .fileNameLamp(UUID.randomUUID().toString())
            .statusSign(intCount.incrementAndGet());
    }
}
