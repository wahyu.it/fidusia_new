package com.project.fidusia.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class OrderNotarisTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static OrderNotaris getOrderNotarisSample1() {
        return new OrderNotaris().id(1L).status(1);
    }

    public static OrderNotaris getOrderNotarisSample2() {
        return new OrderNotaris().id(2L).status(2);
    }

    public static OrderNotaris getOrderNotarisRandomSampleGenerator() {
        return new OrderNotaris().id(longCount.incrementAndGet()).status(intCount.incrementAndGet());
    }
}
