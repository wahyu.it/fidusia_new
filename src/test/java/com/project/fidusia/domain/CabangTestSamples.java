package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class CabangTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Cabang getCabangSample1() {
        return new Cabang()
            .id(1L)
            .kode("kode1")
            .nama("nama1")
            .alamat("alamat1")
            .kota("kota1")
            .provinsi("provinsi1")
            .pengadilanNegeri("pengadilanNegeri1")
            .noKontak("noKontak1")
            .recordStatus(1)
            .updateBy("updateBy1");
    }

    public static Cabang getCabangSample2() {
        return new Cabang()
            .id(2L)
            .kode("kode2")
            .nama("nama2")
            .alamat("alamat2")
            .kota("kota2")
            .provinsi("provinsi2")
            .pengadilanNegeri("pengadilanNegeri2")
            .noKontak("noKontak2")
            .recordStatus(2)
            .updateBy("updateBy2");
    }

    public static Cabang getCabangRandomSampleGenerator() {
        return new Cabang()
            .id(longCount.incrementAndGet())
            .kode(UUID.randomUUID().toString())
            .nama(UUID.randomUUID().toString())
            .alamat(UUID.randomUUID().toString())
            .kota(UUID.randomUUID().toString())
            .provinsi(UUID.randomUUID().toString())
            .pengadilanNegeri(UUID.randomUUID().toString())
            .noKontak(UUID.randomUUID().toString())
            .recordStatus(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString());
    }
}
