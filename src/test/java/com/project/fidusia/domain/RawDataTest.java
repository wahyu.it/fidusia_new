package com.project.fidusia.domain;

import static com.project.fidusia.domain.RawDataTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RawDataTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RawData.class);
        RawData rawData1 = getRawDataSample1();
        RawData rawData2 = new RawData();
        assertThat(rawData1).isNotEqualTo(rawData2);

        rawData2.setId(rawData1.getId());
        assertThat(rawData1).isEqualTo(rawData2);

        rawData2 = getRawDataSample2();
        assertThat(rawData1).isNotEqualTo(rawData2);
    }
}
