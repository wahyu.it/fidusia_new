package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class PenerimaKuasaTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static PenerimaKuasa getPenerimaKuasaSample1() {
        return new PenerimaKuasa()
            .id(1L)
            .titel("titel1")
            .nama("nama1")
            .komparisi("komparisi1")
            .tts("tts1")
            .sk("sk1")
            .noKontak("noKontak1")
            .recordStatus(1)
            .updateBy("updateBy1");
    }

    public static PenerimaKuasa getPenerimaKuasaSample2() {
        return new PenerimaKuasa()
            .id(2L)
            .titel("titel2")
            .nama("nama2")
            .komparisi("komparisi2")
            .tts("tts2")
            .sk("sk2")
            .noKontak("noKontak2")
            .recordStatus(2)
            .updateBy("updateBy2");
    }

    public static PenerimaKuasa getPenerimaKuasaRandomSampleGenerator() {
        return new PenerimaKuasa()
            .id(longCount.incrementAndGet())
            .titel(UUID.randomUUID().toString())
            .nama(UUID.randomUUID().toString())
            .komparisi(UUID.randomUUID().toString())
            .tts(UUID.randomUUID().toString())
            .sk(UUID.randomUUID().toString())
            .noKontak(UUID.randomUUID().toString())
            .recordStatus(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString());
    }
}
