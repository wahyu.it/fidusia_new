package com.project.fidusia.domain;

import static com.project.fidusia.domain.AktaTestSamples.*;
import static com.project.fidusia.domain.CabangTestSamples.*;
import static com.project.fidusia.domain.InvoiceTestSamples.*;
import static com.project.fidusia.domain.SertifikatTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SertifikatTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sertifikat.class);
        Sertifikat sertifikat1 = getSertifikatSample1();
        Sertifikat sertifikat2 = new Sertifikat();
        assertThat(sertifikat1).isNotEqualTo(sertifikat2);

        sertifikat2.setId(sertifikat1.getId());
        assertThat(sertifikat1).isEqualTo(sertifikat2);

        sertifikat2 = getSertifikatSample2();
        assertThat(sertifikat1).isNotEqualTo(sertifikat2);
    }

    @Test
    void aktaTest() throws Exception {
        Sertifikat sertifikat = getSertifikatRandomSampleGenerator();
        Akta aktaBack = getAktaRandomSampleGenerator();

        sertifikat.setAkta(aktaBack);
        assertThat(sertifikat.getAkta()).isEqualTo(aktaBack);

        sertifikat.akta(null);
        assertThat(sertifikat.getAkta()).isNull();
    }

    @Test
    void cabangTest() throws Exception {
        Sertifikat sertifikat = getSertifikatRandomSampleGenerator();
        Cabang cabangBack = getCabangRandomSampleGenerator();

        sertifikat.setCabang(cabangBack);
        assertThat(sertifikat.getCabang()).isEqualTo(cabangBack);

        sertifikat.cabang(null);
        assertThat(sertifikat.getCabang()).isNull();
    }

    @Test
    void invoiceTest() throws Exception {
        Sertifikat sertifikat = getSertifikatRandomSampleGenerator();
        Invoice invoiceBack = getInvoiceRandomSampleGenerator();

        sertifikat.setInvoice(invoiceBack);
        assertThat(sertifikat.getInvoice()).isEqualTo(invoiceBack);

        sertifikat.invoice(null);
        assertThat(sertifikat.getInvoice()).isNull();
    }
}
