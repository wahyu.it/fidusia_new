package com.project.fidusia.domain;

import static com.project.fidusia.domain.SkSubtitusiTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SkSubtitusiTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SkSubtitusi.class);
        SkSubtitusi skSubtitusi1 = getSkSubtitusiSample1();
        SkSubtitusi skSubtitusi2 = new SkSubtitusi();
        assertThat(skSubtitusi1).isNotEqualTo(skSubtitusi2);

        skSubtitusi2.setId(skSubtitusi1.getId());
        assertThat(skSubtitusi1).isEqualTo(skSubtitusi2);

        skSubtitusi2 = getSkSubtitusiSample2();
        assertThat(skSubtitusi1).isNotEqualTo(skSubtitusi2);
    }
}
