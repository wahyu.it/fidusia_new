package com.project.fidusia.domain;

import static com.project.fidusia.domain.KendaraanTestSamples.*;
import static com.project.fidusia.domain.PpkTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KendaraanTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kendaraan.class);
        Kendaraan kendaraan1 = getKendaraanSample1();
        Kendaraan kendaraan2 = new Kendaraan();
        assertThat(kendaraan1).isNotEqualTo(kendaraan2);

        kendaraan2.setId(kendaraan1.getId());
        assertThat(kendaraan1).isEqualTo(kendaraan2);

        kendaraan2 = getKendaraanSample2();
        assertThat(kendaraan1).isNotEqualTo(kendaraan2);
    }

    @Test
    void ppkTest() throws Exception {
        Kendaraan kendaraan = getKendaraanRandomSampleGenerator();
        Ppk ppkBack = getPpkRandomSampleGenerator();

        kendaraan.setPpk(ppkBack);
        assertThat(kendaraan.getPpk()).isEqualTo(ppkBack);

        kendaraan.ppk(null);
        assertThat(kendaraan.getPpk()).isNull();
    }
}
