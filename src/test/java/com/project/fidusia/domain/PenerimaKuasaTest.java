package com.project.fidusia.domain;

import static com.project.fidusia.domain.NotarisTestSamples.*;
import static com.project.fidusia.domain.PenerimaKuasaTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PenerimaKuasaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PenerimaKuasa.class);
        PenerimaKuasa penerimaKuasa1 = getPenerimaKuasaSample1();
        PenerimaKuasa penerimaKuasa2 = new PenerimaKuasa();
        assertThat(penerimaKuasa1).isNotEqualTo(penerimaKuasa2);

        penerimaKuasa2.setId(penerimaKuasa1.getId());
        assertThat(penerimaKuasa1).isEqualTo(penerimaKuasa2);

        penerimaKuasa2 = getPenerimaKuasaSample2();
        assertThat(penerimaKuasa1).isNotEqualTo(penerimaKuasa2);
    }

    @Test
    void notarisTest() throws Exception {
        PenerimaKuasa penerimaKuasa = getPenerimaKuasaRandomSampleGenerator();
        Notaris notarisBack = getNotarisRandomSampleGenerator();

        penerimaKuasa.setNotaris(notarisBack);
        assertThat(penerimaKuasa.getNotaris()).isEqualTo(notarisBack);
        assertThat(notarisBack.getPenerimaKuasa()).isEqualTo(penerimaKuasa);

        penerimaKuasa.notaris(null);
        assertThat(penerimaKuasa.getNotaris()).isNull();
        assertThat(notarisBack.getPenerimaKuasa()).isNull();
    }
}
