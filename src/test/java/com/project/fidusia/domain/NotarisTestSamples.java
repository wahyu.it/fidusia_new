package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class NotarisTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Notaris getNotarisSample1() {
        return new Notaris()
            .id(1L)
            .nama("nama1")
            .namaShort("namaShort1")
            .titelShortOne("titelShortOne1")
            .titelShortTwo("titelShortTwo1")
            .titelLongOne("titelLongOne1")
            .titelLongTwo("titelLongTwo1")
            .wilayahKerja("wilayahKerja1")
            .kedudukan("kedudukan1")
            .sk("sk1")
            .jenisHariKerja(1)
            .selisihPukul(1)
            .maksAkta(1)
            .maksOrderHarian(1)
            .lembarSk("lembarSk1")
            .lembarSumpah("lembarSumpah1")
            .alamatKantor("alamatKantor1")
            .alamatEmail("alamatEmail1")
            .noKontak("noKontak1")
            .recordStatus(1)
            .updateBy("updateBy1");
    }

    public static Notaris getNotarisSample2() {
        return new Notaris()
            .id(2L)
            .nama("nama2")
            .namaShort("namaShort2")
            .titelShortOne("titelShortOne2")
            .titelShortTwo("titelShortTwo2")
            .titelLongOne("titelLongOne2")
            .titelLongTwo("titelLongTwo2")
            .wilayahKerja("wilayahKerja2")
            .kedudukan("kedudukan2")
            .sk("sk2")
            .jenisHariKerja(2)
            .selisihPukul(2)
            .maksAkta(2)
            .maksOrderHarian(2)
            .lembarSk("lembarSk2")
            .lembarSumpah("lembarSumpah2")
            .alamatKantor("alamatKantor2")
            .alamatEmail("alamatEmail2")
            .noKontak("noKontak2")
            .recordStatus(2)
            .updateBy("updateBy2");
    }

    public static Notaris getNotarisRandomSampleGenerator() {
        return new Notaris()
            .id(longCount.incrementAndGet())
            .nama(UUID.randomUUID().toString())
            .namaShort(UUID.randomUUID().toString())
            .titelShortOne(UUID.randomUUID().toString())
            .titelShortTwo(UUID.randomUUID().toString())
            .titelLongOne(UUID.randomUUID().toString())
            .titelLongTwo(UUID.randomUUID().toString())
            .wilayahKerja(UUID.randomUUID().toString())
            .kedudukan(UUID.randomUUID().toString())
            .sk(UUID.randomUUID().toString())
            .jenisHariKerja(intCount.incrementAndGet())
            .selisihPukul(intCount.incrementAndGet())
            .maksAkta(intCount.incrementAndGet())
            .maksOrderHarian(intCount.incrementAndGet())
            .lembarSk(UUID.randomUUID().toString())
            .lembarSumpah(UUID.randomUUID().toString())
            .alamatKantor(UUID.randomUUID().toString())
            .alamatEmail(UUID.randomUUID().toString())
            .noKontak(UUID.randomUUID().toString())
            .recordStatus(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString());
    }
}
