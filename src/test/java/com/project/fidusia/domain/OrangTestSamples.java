package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class OrangTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Orang getOrangSample1() {
        return new Orang()
            .id(1L)
            .penggunaan("penggunaan1")
            .nama("nama1")
            .jenisKelamin("jenisKelamin1")
            .statusKawin("statusKawin1")
            .kelahiran("kelahiran1")
            .pekerjaan("pekerjaan1")
            .wargaNegara("wargaNegara1")
            .jenisId("jenisId1")
            .noId("noId1")
            .alamat("alamat1")
            .rt("rt1")
            .rw("rw1")
            .kelurahan("kelurahan1")
            .kecamatan("kecamatan1")
            .kota("kota1")
            .provinsi("provinsi1")
            .kodePos("kodePos1")
            .noKontak("noKontak1");
    }

    public static Orang getOrangSample2() {
        return new Orang()
            .id(2L)
            .penggunaan("penggunaan2")
            .nama("nama2")
            .jenisKelamin("jenisKelamin2")
            .statusKawin("statusKawin2")
            .kelahiran("kelahiran2")
            .pekerjaan("pekerjaan2")
            .wargaNegara("wargaNegara2")
            .jenisId("jenisId2")
            .noId("noId2")
            .alamat("alamat2")
            .rt("rt2")
            .rw("rw2")
            .kelurahan("kelurahan2")
            .kecamatan("kecamatan2")
            .kota("kota2")
            .provinsi("provinsi2")
            .kodePos("kodePos2")
            .noKontak("noKontak2");
    }

    public static Orang getOrangRandomSampleGenerator() {
        return new Orang()
            .id(longCount.incrementAndGet())
            .penggunaan(UUID.randomUUID().toString())
            .nama(UUID.randomUUID().toString())
            .jenisKelamin(UUID.randomUUID().toString())
            .statusKawin(UUID.randomUUID().toString())
            .kelahiran(UUID.randomUUID().toString())
            .pekerjaan(UUID.randomUUID().toString())
            .wargaNegara(UUID.randomUUID().toString())
            .jenisId(UUID.randomUUID().toString())
            .noId(UUID.randomUUID().toString())
            .alamat(UUID.randomUUID().toString())
            .rt(UUID.randomUUID().toString())
            .rw(UUID.randomUUID().toString())
            .kelurahan(UUID.randomUUID().toString())
            .kecamatan(UUID.randomUUID().toString())
            .kota(UUID.randomUUID().toString())
            .provinsi(UUID.randomUUID().toString())
            .kodePos(UUID.randomUUID().toString())
            .noKontak(UUID.randomUUID().toString());
    }
}
