package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class SertifikatTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Sertifikat getSertifikatSample1() {
        return new Sertifikat()
            .id(1L)
            .kodeVoucher("kodeVoucher1")
            .noSertifikat("noSertifikat1")
            .biayaPnbp(1L)
            .noReferensiBni("noReferensiBni1")
            .status(1)
            .updateBy("updateBy1")
            .registrationId("registrationId1")
            .biayaJasa(1L);
    }

    public static Sertifikat getSertifikatSample2() {
        return new Sertifikat()
            .id(2L)
            .kodeVoucher("kodeVoucher2")
            .noSertifikat("noSertifikat2")
            .biayaPnbp(2L)
            .noReferensiBni("noReferensiBni2")
            .status(2)
            .updateBy("updateBy2")
            .registrationId("registrationId2")
            .biayaJasa(2L);
    }

    public static Sertifikat getSertifikatRandomSampleGenerator() {
        return new Sertifikat()
            .id(longCount.incrementAndGet())
            .kodeVoucher(UUID.randomUUID().toString())
            .noSertifikat(UUID.randomUUID().toString())
            .biayaPnbp(longCount.incrementAndGet())
            .noReferensiBni(UUID.randomUUID().toString())
            .status(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString())
            .registrationId(UUID.randomUUID().toString())
            .biayaJasa(longCount.incrementAndGet());
    }
}
