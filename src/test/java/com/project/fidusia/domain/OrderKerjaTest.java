package com.project.fidusia.domain;

import static com.project.fidusia.domain.NotarisTestSamples.*;
import static com.project.fidusia.domain.OrderKerjaTestSamples.*;
import static com.project.fidusia.domain.PenerimaKuasaTestSamples.*;
import static com.project.fidusia.domain.SkSubtitusiTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderKerjaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderKerja.class);
        OrderKerja orderKerja1 = getOrderKerjaSample1();
        OrderKerja orderKerja2 = new OrderKerja();
        assertThat(orderKerja1).isNotEqualTo(orderKerja2);

        orderKerja2.setId(orderKerja1.getId());
        assertThat(orderKerja1).isEqualTo(orderKerja2);

        orderKerja2 = getOrderKerjaSample2();
        assertThat(orderKerja1).isNotEqualTo(orderKerja2);
    }

    @Test
    void skSubtitusiTest() throws Exception {
        OrderKerja orderKerja = getOrderKerjaRandomSampleGenerator();
        SkSubtitusi skSubtitusiBack = getSkSubtitusiRandomSampleGenerator();

        orderKerja.setSkSubtitusi(skSubtitusiBack);
        assertThat(orderKerja.getSkSubtitusi()).isEqualTo(skSubtitusiBack);

        orderKerja.skSubtitusi(null);
        assertThat(orderKerja.getSkSubtitusi()).isNull();
    }

    @Test
    void notarisTest() throws Exception {
        OrderKerja orderKerja = getOrderKerjaRandomSampleGenerator();
        Notaris notarisBack = getNotarisRandomSampleGenerator();

        orderKerja.setNotaris(notarisBack);
        assertThat(orderKerja.getNotaris()).isEqualTo(notarisBack);

        orderKerja.notaris(null);
        assertThat(orderKerja.getNotaris()).isNull();
    }

    @Test
    void penerimaKuasaTest() throws Exception {
        OrderKerja orderKerja = getOrderKerjaRandomSampleGenerator();
        PenerimaKuasa penerimaKuasaBack = getPenerimaKuasaRandomSampleGenerator();

        orderKerja.setPenerimaKuasa(penerimaKuasaBack);
        assertThat(orderKerja.getPenerimaKuasa()).isEqualTo(penerimaKuasaBack);

        orderKerja.penerimaKuasa(null);
        assertThat(orderKerja.getPenerimaKuasa()).isNull();
    }
}
