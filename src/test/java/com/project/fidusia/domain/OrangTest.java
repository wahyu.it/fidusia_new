package com.project.fidusia.domain;

import static com.project.fidusia.domain.OrangTestSamples.*;
import static com.project.fidusia.domain.PpkTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrangTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Orang.class);
        Orang orang1 = getOrangSample1();
        Orang orang2 = new Orang();
        assertThat(orang1).isNotEqualTo(orang2);

        orang2.setId(orang1.getId());
        assertThat(orang1).isEqualTo(orang2);

        orang2 = getOrangSample2();
        assertThat(orang1).isNotEqualTo(orang2);
    }

    @Test
    void ppkTest() throws Exception {
        Orang orang = getOrangRandomSampleGenerator();
        Ppk ppkBack = getPpkRandomSampleGenerator();

        orang.setPpk(ppkBack);
        assertThat(orang.getPpk()).isEqualTo(ppkBack);
        assertThat(ppkBack.getOrang()).isEqualTo(orang);

        orang.ppk(null);
        assertThat(orang.getPpk()).isNull();
        assertThat(ppkBack.getOrang()).isNull();
    }
}
