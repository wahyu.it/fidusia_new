package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class KendaraanTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Kendaraan getKendaraanSample1() {
        return new Kendaraan()
            .id(1L)
            .kondisi("kondisi1")
            .roda(1)
            .merk("merk1")
            .tipe("tipe1")
            .warna("warna1")
            .tahunPembuatan("tahunPembuatan1")
            .noRangka("noRangka1")
            .noMesin("noMesin1")
            .noBpkb("noBpkb1")
            .nilaiObjekJaminan(1L);
    }

    public static Kendaraan getKendaraanSample2() {
        return new Kendaraan()
            .id(2L)
            .kondisi("kondisi2")
            .roda(2)
            .merk("merk2")
            .tipe("tipe2")
            .warna("warna2")
            .tahunPembuatan("tahunPembuatan2")
            .noRangka("noRangka2")
            .noMesin("noMesin2")
            .noBpkb("noBpkb2")
            .nilaiObjekJaminan(2L);
    }

    public static Kendaraan getKendaraanRandomSampleGenerator() {
        return new Kendaraan()
            .id(longCount.incrementAndGet())
            .kondisi(UUID.randomUUID().toString())
            .roda(intCount.incrementAndGet())
            .merk(UUID.randomUUID().toString())
            .tipe(UUID.randomUUID().toString())
            .warna(UUID.randomUUID().toString())
            .tahunPembuatan(UUID.randomUUID().toString())
            .noRangka(UUID.randomUUID().toString())
            .noMesin(UUID.randomUUID().toString())
            .noBpkb(UUID.randomUUID().toString())
            .nilaiObjekJaminan(longCount.incrementAndGet());
    }
}
