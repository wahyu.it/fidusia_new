package com.project.fidusia.domain;

import static com.project.fidusia.domain.BerkasTestSamples.*;
import static com.project.fidusia.domain.CabangTestSamples.*;
import static com.project.fidusia.domain.RemarkTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class BerkasTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Berkas.class);
        Berkas berkas1 = getBerkasSample1();
        Berkas berkas2 = new Berkas();
        assertThat(berkas1).isNotEqualTo(berkas2);

        berkas2.setId(berkas1.getId());
        assertThat(berkas1).isEqualTo(berkas2);

        berkas2 = getBerkasSample2();
        assertThat(berkas1).isNotEqualTo(berkas2);
    }

    @Test
    void remarkTest() throws Exception {
        Berkas berkas = getBerkasRandomSampleGenerator();
        Remark remarkBack = getRemarkRandomSampleGenerator();

        berkas.setRemark(remarkBack);
        assertThat(berkas.getRemark()).isEqualTo(remarkBack);

        berkas.remark(null);
        assertThat(berkas.getRemark()).isNull();
    }

    @Test
    void cabangTest() throws Exception {
        Berkas berkas = getBerkasRandomSampleGenerator();
        Cabang cabangBack = getCabangRandomSampleGenerator();

        berkas.setCabang(cabangBack);
        assertThat(berkas.getCabang()).isEqualTo(cabangBack);

        berkas.cabang(null);
        assertThat(berkas.getCabang()).isNull();
    }
}
