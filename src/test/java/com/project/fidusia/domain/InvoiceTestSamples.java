package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class InvoiceTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Invoice getInvoiceSample1() {
        return new Invoice()
            .id(1L)
            .type(1)
            .jmlOrder(1L)
            .jmlOrderCancel(1L)
            .noProformaPnbp("noProformaPnbp1")
            .noProformaJasa("noProformaJasa1")
            .noBillpymPnbp("noBillpymPnbp1")
            .noBillpymJasa("noBillpymJasa1")
            .amountPnbp(1L)
            .taxPnbp(1L)
            .totalAmountPnbp(1L)
            .amountJasa(1L)
            .vatJasa(1L)
            .taxJasa(1L)
            .totalAmountJasa(1L)
            .fileProformaPnbp("fileProformaPnbp1")
            .fileProformaJasa("fileProformaJasa1")
            .fileBillpymPnbp("fileBillpymPnbp1")
            .fileBillpymJasa("fileBillpymJasa1")
            .fileBulk("fileBulk1")
            .fileTax("fileTax1")
            .fileTxt("fileTxt1")
            .status(1)
            .updateBy("updateBy1");
    }

    public static Invoice getInvoiceSample2() {
        return new Invoice()
            .id(2L)
            .type(2)
            .jmlOrder(2L)
            .jmlOrderCancel(2L)
            .noProformaPnbp("noProformaPnbp2")
            .noProformaJasa("noProformaJasa2")
            .noBillpymPnbp("noBillpymPnbp2")
            .noBillpymJasa("noBillpymJasa2")
            .amountPnbp(2L)
            .taxPnbp(2L)
            .totalAmountPnbp(2L)
            .amountJasa(2L)
            .vatJasa(2L)
            .taxJasa(2L)
            .totalAmountJasa(2L)
            .fileProformaPnbp("fileProformaPnbp2")
            .fileProformaJasa("fileProformaJasa2")
            .fileBillpymPnbp("fileBillpymPnbp2")
            .fileBillpymJasa("fileBillpymJasa2")
            .fileBulk("fileBulk2")
            .fileTax("fileTax2")
            .fileTxt("fileTxt2")
            .status(2)
            .updateBy("updateBy2");
    }

    public static Invoice getInvoiceRandomSampleGenerator() {
        return new Invoice()
            .id(longCount.incrementAndGet())
            .type(intCount.incrementAndGet())
            .jmlOrder(longCount.incrementAndGet())
            .jmlOrderCancel(longCount.incrementAndGet())
            .noProformaPnbp(UUID.randomUUID().toString())
            .noProformaJasa(UUID.randomUUID().toString())
            .noBillpymPnbp(UUID.randomUUID().toString())
            .noBillpymJasa(UUID.randomUUID().toString())
            .amountPnbp(longCount.incrementAndGet())
            .taxPnbp(longCount.incrementAndGet())
            .totalAmountPnbp(longCount.incrementAndGet())
            .amountJasa(longCount.incrementAndGet())
            .vatJasa(longCount.incrementAndGet())
            .taxJasa(longCount.incrementAndGet())
            .totalAmountJasa(longCount.incrementAndGet())
            .fileProformaPnbp(UUID.randomUUID().toString())
            .fileProformaJasa(UUID.randomUUID().toString())
            .fileBillpymPnbp(UUID.randomUUID().toString())
            .fileBillpymJasa(UUID.randomUUID().toString())
            .fileBulk(UUID.randomUUID().toString())
            .fileTax(UUID.randomUUID().toString())
            .fileTxt(UUID.randomUUID().toString())
            .status(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString());
    }
}
