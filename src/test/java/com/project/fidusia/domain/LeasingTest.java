package com.project.fidusia.domain;

import static com.project.fidusia.domain.LeasingTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LeasingTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Leasing.class);
        Leasing leasing1 = getLeasingSample1();
        Leasing leasing2 = new Leasing();
        assertThat(leasing1).isNotEqualTo(leasing2);

        leasing2.setId(leasing1.getId());
        assertThat(leasing1).isEqualTo(leasing2);

        leasing2 = getLeasingSample2();
        assertThat(leasing1).isNotEqualTo(leasing2);
    }
}
