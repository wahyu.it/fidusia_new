package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class RawDataTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static RawData getRawDataSample1() {
        return new RawData()
            .id(1L)
            .uniqueKey("uniqueKey1")
            .fileName("fileName1")
            .line("line1")
            .delimeter("delimeter1")
            .lineNumber(1L)
            .invalidReason("invalidReason1")
            .status(1L);
    }

    public static RawData getRawDataSample2() {
        return new RawData()
            .id(2L)
            .uniqueKey("uniqueKey2")
            .fileName("fileName2")
            .line("line2")
            .delimeter("delimeter2")
            .lineNumber(2L)
            .invalidReason("invalidReason2")
            .status(2L);
    }

    public static RawData getRawDataRandomSampleGenerator() {
        return new RawData()
            .id(longCount.incrementAndGet())
            .uniqueKey(UUID.randomUUID().toString())
            .fileName(UUID.randomUUID().toString())
            .line(UUID.randomUUID().toString())
            .delimeter(UUID.randomUUID().toString())
            .lineNumber(longCount.incrementAndGet())
            .invalidReason(UUID.randomUUID().toString())
            .status(longCount.incrementAndGet());
    }
}
