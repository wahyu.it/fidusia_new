package com.project.fidusia.domain;

import static com.project.fidusia.domain.AktaTestSamples.*;
import static com.project.fidusia.domain.BadanTestSamples.*;
import static com.project.fidusia.domain.OrangTestSamples.*;
import static com.project.fidusia.domain.PpkTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PpkTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Ppk.class);
        Ppk ppk1 = getPpkSample1();
        Ppk ppk2 = new Ppk();
        assertThat(ppk1).isNotEqualTo(ppk2);

        ppk2.setId(ppk1.getId());
        assertThat(ppk1).isEqualTo(ppk2);

        ppk2 = getPpkSample2();
        assertThat(ppk1).isNotEqualTo(ppk2);
    }

    @Test
    void orangTest() throws Exception {
        Ppk ppk = getPpkRandomSampleGenerator();
        Orang orangBack = getOrangRandomSampleGenerator();

        ppk.setOrang(orangBack);
        assertThat(ppk.getOrang()).isEqualTo(orangBack);

        ppk.orang(null);
        assertThat(ppk.getOrang()).isNull();
    }

    @Test
    void badanTest() throws Exception {
        Ppk ppk = getPpkRandomSampleGenerator();
        Badan badanBack = getBadanRandomSampleGenerator();

        ppk.setBadan(badanBack);
        assertThat(ppk.getBadan()).isEqualTo(badanBack);

        ppk.badan(null);
        assertThat(ppk.getBadan()).isNull();
    }

    @Test
    void aktaTest() throws Exception {
        Ppk ppk = getPpkRandomSampleGenerator();
        Akta aktaBack = getAktaRandomSampleGenerator();

        ppk.setAkta(aktaBack);
        assertThat(ppk.getAkta()).isEqualTo(aktaBack);
        assertThat(aktaBack.getPpk()).isEqualTo(ppk);

        ppk.akta(null);
        assertThat(ppk.getAkta()).isNull();
        assertThat(aktaBack.getPpk()).isNull();
    }
}
