package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class PpkTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Ppk getPpkSample1() {
        return new Ppk()
            .id(1L)
            .nomor("nomor1")
            .nama("nama1")
            .jenis("jenis1")
            .nilaiHutangPokok(1L)
            .nilaiPenjaminan(1L)
            .periodeTenor(1L)
            .nsbNamaDebitur("nsbNamaDebitur1")
            .nsbNama("nsbNama1")
            .nsbJenis(1);
    }

    public static Ppk getPpkSample2() {
        return new Ppk()
            .id(2L)
            .nomor("nomor2")
            .nama("nama2")
            .jenis("jenis2")
            .nilaiHutangPokok(2L)
            .nilaiPenjaminan(2L)
            .periodeTenor(2L)
            .nsbNamaDebitur("nsbNamaDebitur2")
            .nsbNama("nsbNama2")
            .nsbJenis(2);
    }

    public static Ppk getPpkRandomSampleGenerator() {
        return new Ppk()
            .id(longCount.incrementAndGet())
            .nomor(UUID.randomUUID().toString())
            .nama(UUID.randomUUID().toString())
            .jenis(UUID.randomUUID().toString())
            .nilaiHutangPokok(longCount.incrementAndGet())
            .nilaiPenjaminan(longCount.incrementAndGet())
            .periodeTenor(longCount.incrementAndGet())
            .nsbNamaDebitur(UUID.randomUUID().toString())
            .nsbNama(UUID.randomUUID().toString())
            .nsbJenis(intCount.incrementAndGet());
    }
}
