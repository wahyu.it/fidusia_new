package com.project.fidusia.domain;

import static com.project.fidusia.domain.BadanTestSamples.*;
import static com.project.fidusia.domain.PpkTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class BadanTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Badan.class);
        Badan badan1 = getBadanSample1();
        Badan badan2 = new Badan();
        assertThat(badan1).isNotEqualTo(badan2);

        badan2.setId(badan1.getId());
        assertThat(badan1).isEqualTo(badan2);

        badan2 = getBadanSample2();
        assertThat(badan1).isNotEqualTo(badan2);
    }

    @Test
    void ppkTest() throws Exception {
        Badan badan = getBadanRandomSampleGenerator();
        Ppk ppkBack = getPpkRandomSampleGenerator();

        badan.setPpk(ppkBack);
        assertThat(badan.getPpk()).isEqualTo(ppkBack);
        assertThat(ppkBack.getBadan()).isEqualTo(badan);

        badan.ppk(null);
        assertThat(badan.getPpk()).isNull();
        assertThat(ppkBack.getBadan()).isNull();
    }
}
