package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class RemarkTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Remark getRemarkSample1() {
        return new Remark().id(1L).category("category1").name("name1").description("description1").berkasRevisi("berkasRevisi1");
    }

    public static Remark getRemarkSample2() {
        return new Remark().id(2L).category("category2").name("name2").description("description2").berkasRevisi("berkasRevisi2");
    }

    public static Remark getRemarkRandomSampleGenerator() {
        return new Remark()
            .id(longCount.incrementAndGet())
            .category(UUID.randomUUID().toString())
            .name(UUID.randomUUID().toString())
            .description(UUID.randomUUID().toString())
            .berkasRevisi(UUID.randomUUID().toString());
    }
}
