package com.project.fidusia.domain;

import static com.project.fidusia.domain.AktaTestSamples.*;
import static com.project.fidusia.domain.CabangTestSamples.*;
import static com.project.fidusia.domain.NotarisTestSamples.*;
import static com.project.fidusia.domain.PenerimaKuasaTestSamples.*;
import static com.project.fidusia.domain.PpkTestSamples.*;
import static com.project.fidusia.domain.SaksiTestSamples.*;
import static com.project.fidusia.domain.SkSubtitusiTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AktaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Akta.class);
        Akta akta1 = getAktaSample1();
        Akta akta2 = new Akta();
        assertThat(akta1).isNotEqualTo(akta2);

        akta2.setId(akta1.getId());
        assertThat(akta1).isEqualTo(akta2);

        akta2 = getAktaSample2();
        assertThat(akta1).isNotEqualTo(akta2);
    }

    @Test
    void ppkTest() throws Exception {
        Akta akta = getAktaRandomSampleGenerator();
        Ppk ppkBack = getPpkRandomSampleGenerator();

        akta.setPpk(ppkBack);
        assertThat(akta.getPpk()).isEqualTo(ppkBack);

        akta.ppk(null);
        assertThat(akta.getPpk()).isNull();
    }

    @Test
    void notarisTest() throws Exception {
        Akta akta = getAktaRandomSampleGenerator();
        Notaris notarisBack = getNotarisRandomSampleGenerator();

        akta.setNotaris(notarisBack);
        assertThat(akta.getNotaris()).isEqualTo(notarisBack);

        akta.notaris(null);
        assertThat(akta.getNotaris()).isNull();
    }

    @Test
    void penerimaKuasaTest() throws Exception {
        Akta akta = getAktaRandomSampleGenerator();
        PenerimaKuasa penerimaKuasaBack = getPenerimaKuasaRandomSampleGenerator();

        akta.setPenerimaKuasa(penerimaKuasaBack);
        assertThat(akta.getPenerimaKuasa()).isEqualTo(penerimaKuasaBack);

        akta.penerimaKuasa(null);
        assertThat(akta.getPenerimaKuasa()).isNull();
    }

    @Test
    void saksiOneTest() throws Exception {
        Akta akta = getAktaRandomSampleGenerator();
        Saksi saksiBack = getSaksiRandomSampleGenerator();

        akta.setSaksiOne(saksiBack);
        assertThat(akta.getSaksiOne()).isEqualTo(saksiBack);

        akta.saksiOne(null);
        assertThat(akta.getSaksiOne()).isNull();
    }

    @Test
    void saksiTwoTest() throws Exception {
        Akta akta = getAktaRandomSampleGenerator();
        Saksi saksiBack = getSaksiRandomSampleGenerator();

        akta.setSaksiTwo(saksiBack);
        assertThat(akta.getSaksiTwo()).isEqualTo(saksiBack);

        akta.saksiTwo(null);
        assertThat(akta.getSaksiTwo()).isNull();
    }

    @Test
    void cabangTest() throws Exception {
        Akta akta = getAktaRandomSampleGenerator();
        Cabang cabangBack = getCabangRandomSampleGenerator();

        akta.setCabang(cabangBack);
        assertThat(akta.getCabang()).isEqualTo(cabangBack);

        akta.cabang(null);
        assertThat(akta.getCabang()).isNull();
    }

    @Test
    void skSubtitusiTest() throws Exception {
        Akta akta = getAktaRandomSampleGenerator();
        SkSubtitusi skSubtitusiBack = getSkSubtitusiRandomSampleGenerator();

        akta.setSkSubtitusi(skSubtitusiBack);
        assertThat(akta.getSkSubtitusi()).isEqualTo(skSubtitusiBack);

        akta.skSubtitusi(null);
        assertThat(akta.getSkSubtitusi()).isNull();
    }
}
