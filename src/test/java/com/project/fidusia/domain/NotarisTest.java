package com.project.fidusia.domain;

import static com.project.fidusia.domain.NotarisTestSamples.*;
import static com.project.fidusia.domain.PenerimaKuasaTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class NotarisTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Notaris.class);
        Notaris notaris1 = getNotarisSample1();
        Notaris notaris2 = new Notaris();
        assertThat(notaris1).isNotEqualTo(notaris2);

        notaris2.setId(notaris1.getId());
        assertThat(notaris1).isEqualTo(notaris2);

        notaris2 = getNotarisSample2();
        assertThat(notaris1).isNotEqualTo(notaris2);
    }

    @Test
    void penerimaKuasaTest() throws Exception {
        Notaris notaris = getNotarisRandomSampleGenerator();
        PenerimaKuasa penerimaKuasaBack = getPenerimaKuasaRandomSampleGenerator();

        notaris.setPenerimaKuasa(penerimaKuasaBack);
        assertThat(notaris.getPenerimaKuasa()).isEqualTo(penerimaKuasaBack);

        notaris.penerimaKuasa(null);
        assertThat(notaris.getPenerimaKuasa()).isNull();
    }
}
