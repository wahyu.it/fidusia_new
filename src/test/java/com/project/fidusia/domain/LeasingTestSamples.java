package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class LeasingTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Leasing getLeasingSample1() {
        return new Leasing()
            .id(1L)
            .kode("kode1")
            .nama("nama1")
            .alamat("alamat1")
            .kota("kota1")
            .provinsi("provinsi1")
            .kecamatan("kecamatan1")
            .kelurahan("kelurahan1")
            .rt("rt1")
            .rw("rw1")
            .npwp("npwp1")
            .kodePose("kodePose1")
            .noKontak("noKontak1")
            .techNoKontak("techNoKontak1")
            .techEmail("techEmail1")
            .acctNoKontak("acctNoKontak1")
            .acctEmail("acctEmail1")
            .recordStatus(1)
            .updateBy("updateBy1")
            .email("email1")
            .sk("sk1");
    }

    public static Leasing getLeasingSample2() {
        return new Leasing()
            .id(2L)
            .kode("kode2")
            .nama("nama2")
            .alamat("alamat2")
            .kota("kota2")
            .provinsi("provinsi2")
            .kecamatan("kecamatan2")
            .kelurahan("kelurahan2")
            .rt("rt2")
            .rw("rw2")
            .npwp("npwp2")
            .kodePose("kodePose2")
            .noKontak("noKontak2")
            .techNoKontak("techNoKontak2")
            .techEmail("techEmail2")
            .acctNoKontak("acctNoKontak2")
            .acctEmail("acctEmail2")
            .recordStatus(2)
            .updateBy("updateBy2")
            .email("email2")
            .sk("sk2");
    }

    public static Leasing getLeasingRandomSampleGenerator() {
        return new Leasing()
            .id(longCount.incrementAndGet())
            .kode(UUID.randomUUID().toString())
            .nama(UUID.randomUUID().toString())
            .alamat(UUID.randomUUID().toString())
            .kota(UUID.randomUUID().toString())
            .provinsi(UUID.randomUUID().toString())
            .kecamatan(UUID.randomUUID().toString())
            .kelurahan(UUID.randomUUID().toString())
            .rt(UUID.randomUUID().toString())
            .rw(UUID.randomUUID().toString())
            .npwp(UUID.randomUUID().toString())
            .kodePose(UUID.randomUUID().toString())
            .noKontak(UUID.randomUUID().toString())
            .techNoKontak(UUID.randomUUID().toString())
            .techEmail(UUID.randomUUID().toString())
            .acctNoKontak(UUID.randomUUID().toString())
            .acctEmail(UUID.randomUUID().toString())
            .recordStatus(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString())
            .email(UUID.randomUUID().toString())
            .sk(UUID.randomUUID().toString());
    }
}
