package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class OrderKerjaTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static OrderKerja getOrderKerjaSample1() {
        return new OrderKerja()
            .id(1L)
            .nomor("nomor1")
            .kategori(1)
            .jumlahAkta(1)
            .jumlahCancel(1)
            .jumlahCetakAkta(1)
            .uploadFile("uploadFile1")
            .status(1)
            .updateBy("updateBy1")
            .approveBy("approveBy1");
    }

    public static OrderKerja getOrderKerjaSample2() {
        return new OrderKerja()
            .id(2L)
            .nomor("nomor2")
            .kategori(2)
            .jumlahAkta(2)
            .jumlahCancel(2)
            .jumlahCetakAkta(2)
            .uploadFile("uploadFile2")
            .status(2)
            .updateBy("updateBy2")
            .approveBy("approveBy2");
    }

    public static OrderKerja getOrderKerjaRandomSampleGenerator() {
        return new OrderKerja()
            .id(longCount.incrementAndGet())
            .nomor(UUID.randomUUID().toString())
            .kategori(intCount.incrementAndGet())
            .jumlahAkta(intCount.incrementAndGet())
            .jumlahCancel(intCount.incrementAndGet())
            .jumlahCetakAkta(intCount.incrementAndGet())
            .uploadFile(UUID.randomUUID().toString())
            .status(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString())
            .approveBy(UUID.randomUUID().toString());
    }
}
