package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class AktaTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Akta getAktaSample1() {
        return new Akta().id(1L).orderType(1).kode("kode1").nomor("nomor1").tanggal("tanggal1").status(1).updateBy("updateBy1");
    }

    public static Akta getAktaSample2() {
        return new Akta().id(2L).orderType(2).kode("kode2").nomor("nomor2").tanggal("tanggal2").status(2).updateBy("updateBy2");
    }

    public static Akta getAktaRandomSampleGenerator() {
        return new Akta()
            .id(longCount.incrementAndGet())
            .orderType(intCount.incrementAndGet())
            .kode(UUID.randomUUID().toString())
            .nomor(UUID.randomUUID().toString())
            .tanggal(UUID.randomUUID().toString())
            .status(intCount.incrementAndGet())
            .updateBy(UUID.randomUUID().toString());
    }
}
