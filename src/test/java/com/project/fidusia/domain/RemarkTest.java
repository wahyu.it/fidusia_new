package com.project.fidusia.domain;

import static com.project.fidusia.domain.RemarkTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RemarkTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Remark.class);
        Remark remark1 = getRemarkSample1();
        Remark remark2 = new Remark();
        assertThat(remark1).isNotEqualTo(remark2);

        remark2.setId(remark1.getId());
        assertThat(remark1).isEqualTo(remark2);

        remark2 = getRemarkSample2();
        assertThat(remark1).isNotEqualTo(remark2);
    }
}
