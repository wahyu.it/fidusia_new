package com.project.fidusia.domain;

import static com.project.fidusia.domain.CabangTestSamples.*;
import static com.project.fidusia.domain.LeasingTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CabangTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cabang.class);
        Cabang cabang1 = getCabangSample1();
        Cabang cabang2 = new Cabang();
        assertThat(cabang1).isNotEqualTo(cabang2);

        cabang2.setId(cabang1.getId());
        assertThat(cabang1).isEqualTo(cabang2);

        cabang2 = getCabangSample2();
        assertThat(cabang1).isNotEqualTo(cabang2);
    }

    @Test
    void leasingTest() throws Exception {
        Cabang cabang = getCabangRandomSampleGenerator();
        Leasing leasingBack = getLeasingRandomSampleGenerator();

        cabang.setLeasing(leasingBack);
        assertThat(cabang.getLeasing()).isEqualTo(leasingBack);

        cabang.leasing(null);
        assertThat(cabang.getLeasing()).isNull();
    }
}
