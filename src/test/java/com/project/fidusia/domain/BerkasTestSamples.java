package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class BerkasTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Berkas getBerkasSample1() {
        return new Berkas()
            .id(1L)
            .identitas("identitas1")
            .kk("kk1")
            .ppk("ppk1")
            .skNasabah("skNasabah1")
            .aktaPendirian("aktaPendirian1")
            .aktaPerubahan("aktaPerubahan1")
            .skPendirian("skPendirian1")
            .skPerubahan("skPerubahan1")
            .npwp("npwp1")
            .updateBy("updateBy1")
            .uploadBy("uploadBy1")
            .ppkNomor("ppkNomor1")
            .nsbJenis(1)
            .berkasStatus(1)
            .namaNasabah("namaNasabah1")
            .metodeKirim(1)
            .partitionId(1)
            .verifyBy("verifyBy1")
            .orderType(1);
    }

    public static Berkas getBerkasSample2() {
        return new Berkas()
            .id(2L)
            .identitas("identitas2")
            .kk("kk2")
            .ppk("ppk2")
            .skNasabah("skNasabah2")
            .aktaPendirian("aktaPendirian2")
            .aktaPerubahan("aktaPerubahan2")
            .skPendirian("skPendirian2")
            .skPerubahan("skPerubahan2")
            .npwp("npwp2")
            .updateBy("updateBy2")
            .uploadBy("uploadBy2")
            .ppkNomor("ppkNomor2")
            .nsbJenis(2)
            .berkasStatus(2)
            .namaNasabah("namaNasabah2")
            .metodeKirim(2)
            .partitionId(2)
            .verifyBy("verifyBy2")
            .orderType(2);
    }

    public static Berkas getBerkasRandomSampleGenerator() {
        return new Berkas()
            .id(longCount.incrementAndGet())
            .identitas(UUID.randomUUID().toString())
            .kk(UUID.randomUUID().toString())
            .ppk(UUID.randomUUID().toString())
            .skNasabah(UUID.randomUUID().toString())
            .aktaPendirian(UUID.randomUUID().toString())
            .aktaPerubahan(UUID.randomUUID().toString())
            .skPendirian(UUID.randomUUID().toString())
            .skPerubahan(UUID.randomUUID().toString())
            .npwp(UUID.randomUUID().toString())
            .updateBy(UUID.randomUUID().toString())
            .uploadBy(UUID.randomUUID().toString())
            .ppkNomor(UUID.randomUUID().toString())
            .nsbJenis(intCount.incrementAndGet())
            .berkasStatus(intCount.incrementAndGet())
            .namaNasabah(UUID.randomUUID().toString())
            .metodeKirim(intCount.incrementAndGet())
            .partitionId(intCount.incrementAndGet())
            .verifyBy(UUID.randomUUID().toString())
            .orderType(intCount.incrementAndGet());
    }
}
