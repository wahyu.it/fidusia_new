package com.project.fidusia.domain;

import static com.project.fidusia.domain.AktaTestSamples.*;
import static com.project.fidusia.domain.NotarisTestSamples.*;
import static com.project.fidusia.domain.OrderDataTestSamples.*;
import static com.project.fidusia.domain.OrderKerjaTestSamples.*;
import static com.project.fidusia.domain.OrderNotarisTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderNotarisTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderNotaris.class);
        OrderNotaris orderNotaris1 = getOrderNotarisSample1();
        OrderNotaris orderNotaris2 = new OrderNotaris();
        assertThat(orderNotaris1).isNotEqualTo(orderNotaris2);

        orderNotaris2.setId(orderNotaris1.getId());
        assertThat(orderNotaris1).isEqualTo(orderNotaris2);

        orderNotaris2 = getOrderNotarisSample2();
        assertThat(orderNotaris1).isNotEqualTo(orderNotaris2);
    }

    @Test
    void orderDataTest() throws Exception {
        OrderNotaris orderNotaris = getOrderNotarisRandomSampleGenerator();
        OrderData orderDataBack = getOrderDataRandomSampleGenerator();

        orderNotaris.setOrderData(orderDataBack);
        assertThat(orderNotaris.getOrderData()).isEqualTo(orderDataBack);

        orderNotaris.orderData(null);
        assertThat(orderNotaris.getOrderData()).isNull();
    }

    @Test
    void notarisTest() throws Exception {
        OrderNotaris orderNotaris = getOrderNotarisRandomSampleGenerator();
        Notaris notarisBack = getNotarisRandomSampleGenerator();

        orderNotaris.setNotaris(notarisBack);
        assertThat(orderNotaris.getNotaris()).isEqualTo(notarisBack);

        orderNotaris.notaris(null);
        assertThat(orderNotaris.getNotaris()).isNull();
    }

    @Test
    void aktaTest() throws Exception {
        OrderNotaris orderNotaris = getOrderNotarisRandomSampleGenerator();
        Akta aktaBack = getAktaRandomSampleGenerator();

        orderNotaris.setAkta(aktaBack);
        assertThat(orderNotaris.getAkta()).isEqualTo(aktaBack);

        orderNotaris.akta(null);
        assertThat(orderNotaris.getAkta()).isNull();
    }

    @Test
    void orderKerjaTest() throws Exception {
        OrderNotaris orderNotaris = getOrderNotarisRandomSampleGenerator();
        OrderKerja orderKerjaBack = getOrderKerjaRandomSampleGenerator();

        orderNotaris.setOrderKerja(orderKerjaBack);
        assertThat(orderNotaris.getOrderKerja()).isEqualTo(orderKerjaBack);

        orderNotaris.orderKerja(null);
        assertThat(orderNotaris.getOrderKerja()).isNull();
    }
}
