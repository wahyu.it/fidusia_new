package com.project.fidusia.domain;

import static com.project.fidusia.domain.NotarisTestSamples.*;
import static com.project.fidusia.domain.SaksiTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SaksiTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Saksi.class);
        Saksi saksi1 = getSaksiSample1();
        Saksi saksi2 = new Saksi();
        assertThat(saksi1).isNotEqualTo(saksi2);

        saksi2.setId(saksi1.getId());
        assertThat(saksi1).isEqualTo(saksi2);

        saksi2 = getSaksiSample2();
        assertThat(saksi1).isNotEqualTo(saksi2);
    }

    @Test
    void notarisTest() throws Exception {
        Saksi saksi = getSaksiRandomSampleGenerator();
        Notaris notarisBack = getNotarisRandomSampleGenerator();

        saksi.setNotaris(notarisBack);
        assertThat(saksi.getNotaris()).isEqualTo(notarisBack);

        saksi.notaris(null);
        assertThat(saksi.getNotaris()).isNull();
    }
}
