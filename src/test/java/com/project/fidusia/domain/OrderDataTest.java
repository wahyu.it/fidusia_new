package com.project.fidusia.domain;

import static com.project.fidusia.domain.BerkasTestSamples.*;
import static com.project.fidusia.domain.CabangTestSamples.*;
import static com.project.fidusia.domain.OrderDataTestSamples.*;
import static com.project.fidusia.domain.RawDataTestSamples.*;
import static com.project.fidusia.domain.SkSubtitusiTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.project.fidusia.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderDataTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderData.class);
        OrderData orderData1 = getOrderDataSample1();
        OrderData orderData2 = new OrderData();
        assertThat(orderData1).isNotEqualTo(orderData2);

        orderData2.setId(orderData1.getId());
        assertThat(orderData1).isEqualTo(orderData2);

        orderData2 = getOrderDataSample2();
        assertThat(orderData1).isNotEqualTo(orderData2);
    }

    @Test
    void rawDataTest() throws Exception {
        OrderData orderData = getOrderDataRandomSampleGenerator();
        RawData rawDataBack = getRawDataRandomSampleGenerator();

        orderData.setRawData(rawDataBack);
        assertThat(orderData.getRawData()).isEqualTo(rawDataBack);

        orderData.rawData(null);
        assertThat(orderData.getRawData()).isNull();
    }

    @Test
    void skSubtitusiTest() throws Exception {
        OrderData orderData = getOrderDataRandomSampleGenerator();
        SkSubtitusi skSubtitusiBack = getSkSubtitusiRandomSampleGenerator();

        orderData.setSkSubtitusi(skSubtitusiBack);
        assertThat(orderData.getSkSubtitusi()).isEqualTo(skSubtitusiBack);

        orderData.skSubtitusi(null);
        assertThat(orderData.getSkSubtitusi()).isNull();
    }

    @Test
    void berkasTest() throws Exception {
        OrderData orderData = getOrderDataRandomSampleGenerator();
        Berkas berkasBack = getBerkasRandomSampleGenerator();

        orderData.setBerkas(berkasBack);
        assertThat(orderData.getBerkas()).isEqualTo(berkasBack);

        orderData.berkas(null);
        assertThat(orderData.getBerkas()).isNull();
    }

    @Test
    void cabangTest() throws Exception {
        OrderData orderData = getOrderDataRandomSampleGenerator();
        Cabang cabangBack = getCabangRandomSampleGenerator();

        orderData.setCabang(cabangBack);
        assertThat(orderData.getCabang()).isEqualTo(cabangBack);

        orderData.cabang(null);
        assertThat(orderData.getCabang()).isNull();
    }
}
