package com.project.fidusia.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class OrderDataTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static OrderData getOrderDataSample1() {
        return new OrderData().id(1L).ppkNomor("ppkNomor1").nasabahNama("nasabahNama1").nasabahJenis(1).status(1).type(1);
    }

    public static OrderData getOrderDataSample2() {
        return new OrderData().id(2L).ppkNomor("ppkNomor2").nasabahNama("nasabahNama2").nasabahJenis(2).status(2).type(2);
    }

    public static OrderData getOrderDataRandomSampleGenerator() {
        return new OrderData()
            .id(longCount.incrementAndGet())
            .ppkNomor(UUID.randomUUID().toString())
            .nasabahNama(UUID.randomUUID().toString())
            .nasabahJenis(intCount.incrementAndGet())
            .status(intCount.incrementAndGet())
            .type(intCount.incrementAndGet());
    }
}
