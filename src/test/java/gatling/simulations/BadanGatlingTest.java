package gatling.simulations;

import static io.gatling.javaapi.core.CoreDsl.StringBody;
import static io.gatling.javaapi.core.CoreDsl.exec;
import static io.gatling.javaapi.core.CoreDsl.rampUsers;
import static io.gatling.javaapi.core.CoreDsl.scenario;
import static io.gatling.javaapi.http.HttpDsl.header;
import static io.gatling.javaapi.http.HttpDsl.headerRegex;
import static io.gatling.javaapi.http.HttpDsl.http;
import static io.gatling.javaapi.http.HttpDsl.status;

import ch.qos.logback.classic.LoggerContext;
import io.gatling.javaapi.core.ChainBuilder;
import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpProtocolBuilder;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import org.slf4j.LoggerFactory;

/**
 * Performance test for the Badan entity.
 */
public class BadanGatlingTest extends Simulation {

    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

    {
        // Log all HTTP requests
        //context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"));
        // Log failed HTTP requests
        //context.getLogger("io.gatling.http").setLevel(Level.valueOf("DEBUG"));
    }

    String baseURL = Optional.ofNullable(System.getProperty("baseURL")).orElse("http://localhost:8080");

    HttpProtocolBuilder httpConf = http
        .baseUrl(baseURL)
        .inferHtmlResources()
        .acceptHeader("*/*")
        .acceptEncodingHeader("gzip, deflate")
        .acceptLanguageHeader("fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3")
        .connectionHeader("keep-alive")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:33.0) Gecko/20100101 Firefox/33.0")
        .silentResources(); // Silence all resources like css or css so they don't clutter the results

    Map<String, String> headers_http = Map.of("Accept", "application/json");

    Map<String, String> headers_http_authentication = Map.of("Content-Type", "application/json", "Accept", "application/json");

    Map<String, String> headers_http_authenticated = Map.of("Accept", "application/json", "Authorization", "${access_token}");

    ChainBuilder scn = exec(http("First unauthenticated request").get("/api/account").headers(headers_http).check(status().is(401)))
        .exitHereIfFailed()
        .pause(10)
        .exec(
            http("Authentication")
                .post("/api/authenticate")
                .headers(headers_http_authentication)
                .body(StringBody("{\"username\":\"admin\", \"password\":\"admin\"}"))
                .asJson()
                .check(header("Authorization").saveAs("access_token"))
        )
        .exitHereIfFailed()
        .pause(2)
        .exec(http("Authenticated request").get("/api/account").headers(headers_http_authenticated).check(status().is(200)))
        .pause(10)
        .repeat(2)
        .on(
            exec(http("Get all badans").get("/api/badans").headers(headers_http_authenticated).check(status().is(200)))
                .pause(Duration.ofSeconds(10), Duration.ofSeconds(20))
                .exec(
                    http("Create new badan")
                        .post("/api/badans")
                        .headers(headers_http_authenticated)
                        .body(
                            StringBody(
                                "{" +
                                "\"golonganUsaha\": \"SAMPLE_TEXT\"" +
                                ", \"jenis\": \"SAMPLE_TEXT\"" +
                                ", \"nama\": \"SAMPLE_TEXT\"" +
                                ", \"kedudukan\": \"SAMPLE_TEXT\"" +
                                ", \"noAkta\": \"SAMPLE_TEXT\"" +
                                ", \"tglAkta\": \"2020-01-01T00:00:00.000Z\"" +
                                ", \"namaNotaris\": \"SAMPLE_TEXT\"" +
                                ", \"wilKerjaNotaris\": \"SAMPLE_TEXT\"" +
                                ", \"skNotaris\": \"SAMPLE_TEXT\"" +
                                ", \"alamat\": \"SAMPLE_TEXT\"" +
                                ", \"rt\": \"SAMPLE_TEXT\"" +
                                ", \"rw\": \"SAMPLE_TEXT\"" +
                                ", \"kelurahan\": \"SAMPLE_TEXT\"" +
                                ", \"kecamatan\": \"SAMPLE_TEXT\"" +
                                ", \"kota\": \"SAMPLE_TEXT\"" +
                                ", \"provinsi\": \"SAMPLE_TEXT\"" +
                                ", \"kodePos\": \"SAMPLE_TEXT\"" +
                                ", \"noKontak\": \"SAMPLE_TEXT\"" +
                                ", \"namaSk\": \"SAMPLE_TEXT\"" +
                                ", \"nomorSk\": \"SAMPLE_TEXT\"" +
                                ", \"tglSk\": \"SAMPLE_TEXT\"" +
                                ", \"npwp\": \"SAMPLE_TEXT\"" +
                                ", \"pjNama\": \"SAMPLE_TEXT\"" +
                                ", \"pjJenisKelamin\": \"SAMPLE_TEXT\"" +
                                ", \"pjStatusKawin\": \"SAMPLE_TEXT\"" +
                                ", \"pjKelahiran\": \"SAMPLE_TEXT\"" +
                                ", \"pjTglLahir\": \"2020-01-01T00:00:00.000Z\"" +
                                ", \"pjPekerjaan\": \"SAMPLE_TEXT\"" +
                                ", \"pjWargaNegara\": \"SAMPLE_TEXT\"" +
                                ", \"pjJenisId\": \"SAMPLE_TEXT\"" +
                                ", \"pjNoId\": \"SAMPLE_TEXT\"" +
                                ", \"pjAlamat\": \"SAMPLE_TEXT\"" +
                                ", \"pjRt\": \"SAMPLE_TEXT\"" +
                                ", \"pjRw\": \"SAMPLE_TEXT\"" +
                                ", \"pjKelurahan\": \"SAMPLE_TEXT\"" +
                                ", \"pjKecamatan\": \"SAMPLE_TEXT\"" +
                                ", \"pjKota\": \"SAMPLE_TEXT\"" +
                                ", \"pjProvinsi\": \"SAMPLE_TEXT\"" +
                                ", \"pjKodePos\": \"SAMPLE_TEXT\"" +
                                ", \"template\": \"SAMPLE_TEXT\"" +
                                ", \"komparisi\": \"SAMPLE_TEXT\"" +
                                "}"
                            )
                        )
                        .asJson()
                        .check(status().is(201))
                        .check(headerRegex("Location", "(.*)").saveAs("new_badan_url"))
                )
                .exitHereIfFailed()
                .pause(10)
                .repeat(5)
                .on(exec(http("Get created badan").get("${new_badan_url}").headers(headers_http_authenticated)).pause(10))
                .exec(http("Delete created badan").delete("${new_badan_url}").headers(headers_http_authenticated))
                .pause(10)
        );

    ScenarioBuilder users = scenario("Test the Badan entity").exec(scn);

    {
        setUp(users.injectOpen(rampUsers(Integer.getInteger("users", 100)).during(Duration.ofMinutes(Integer.getInteger("ramp", 1)))))
            .protocols(httpConf);
    }
}
